(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Exports complements about logic and numbers for numerical analysis.

 * Description

 ** Complements about logic

 Provides:
 - classical logic (ie excluded middle in [Prop]);
 - functional and propositional extensionality;
 - proof irrelevance;
 - excluded middle in [Set];
 - unique and non-unique choice;
 - a constructive form of indefinite description.

 Also provides:
 - [functional_extensionality] as [fun_ext];
 - [equal_f] as [fun_ext_rev];
 - [propositional_extensionality] as [prop_ext];
 - [proof_irrelevance] as [proof_irrel];
 - [excluded_middle_informative] as [classic_dec];
 - [constructive_indefinite_description] as [ex_EX].

 See also [Logic.logic_compl].

 ** Complements about numbers

 Additional results about natural numbers, for both the Coq standard library
 and the Mathematical Components library.

 See also [Numbers.nat_compl].

 * Usage

 This module is meant to be imported after all imports from the Coq standard
 library, from the Mathematical Components library, and from other external Coq
 libraries such as Coquelicot or Flocq.

 This module may also be used through the import of [Subsets.Subsets_wDep],
 [Algebra.Algebra_wDep], [Lebesgue.Lebesgue_p_wDep], or
 [Lebesgue.Bochner.Bochner_wDep], where it is exported.
*)

From Logic Require Export logic_compl.
From Numbers Require Export nat_compl.
