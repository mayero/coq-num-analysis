(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Export commonly used capabilities of the Coq standard library for the
 Coq Numerical Analysis library

 * Usage

 If needed, this module (or its companion [Requisite.stdlib_wR]) is meant to be
 imported first, or right after other imports from the Coq standard library.
*)

From Coq Require Export Arith Lia.
