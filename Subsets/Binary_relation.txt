OPERATORS

conv / compl / conv_compl
strict / equiv
comp / incomp

incomp = equiv compl = equiv conv_compl


ELEMENTARY PROPERTIES

T / nT
R / nR
S
aS / C
nS / sC
3

inhab -> R -> ~ nR
inhab -> nR -> ~ R
inhab -> R -> ~ nS
<> empty -> S -> ~ nS
<> empty -> nS -> ~ S
xdiag -> S -> ~ aS
xdiag -> aS -> ~ S
T -> nR <-> nS
nS <-> nR /\ aS <-> sC compl (=dec)
aS <-> C compl
C <-> aS compl
T <-> nT compl
nT <-> T compl
=dec -> aS -> nT -> T
=dec -> C -> T -> nT
=dec -> sC <-> R /\ C <-> nS compl
=dec -> 3 <-> nS /\ C
=dec -> 3 compl <-> aS /\ sC

nR -> T incompar -> Equiv incompar


COMPOUND PROPERTIES

PER := T + S
Equiv := T + R + S = PER + R = PrO + S

PrO := T + R
TPrO := T + R + sC = T + R + C (=dec) = PrO + C (=dec)

PO := T + R + aS = PrO + aS
TO := T + R + aS + sC = TPrO + aS
      = T + R + 3 compl (=dec) = PrO + 3 compl (=dec)
      = T + R + aS + C (=dec) = PrO + aS + C (=dec) = PO + C (=dec)

SPO := T + nR + nS = T + nR = T + nS
STO := T + nR + nS + C = SPO + C = T + nR + C = T + nS + C
       = T + 3 (=dec) = SWO + C (=dec)
SWO := nT + nR + nS = nT + nS

Compat:
br_and: T, R|nR, S|aS|nS => PER|Equiv, PrO, PO, SPO
br_or: nT, R|nR, S, C|sC
conv: T|nT, R|nR, S|aS|nS, C|sC|3 => PER|Equiv, PrO|TPrO, PO|TO, SPO|STO|SWO
compl: T|nT, R|nR, S|aS|nS, C|sC|3 => PER|Equiv, PrO|TPrO, PO|TO, SPO|STO|SWO
conv_compl: T|nT, R|nR, S|aS|nS, C|sC|3 => PER|Equiv, PrO|TPrO, PO|TO, SPO|STO|SWO
strict: T, nR, aS|nS, C => SPO|STO
equiv: T, R, S => PER|Equiv
compar: R|nR, S
incompar: T, R|nR, S => PER|Equiv
br_and_neq: T|nT, nR, S|aS|nS, C, 3 => SPO|STO|SWO
br_or_eq: T|nT, R, S|aS, C|sC => PER|Equiv, PrO|TPrO, PO|TO

=dec -> TO -> STO strict
PrO -> Equiv equiv
SPO -> PO br_or_eq
=dec -> STO -> TO br_or_eq


LEX ORDERS

*lex eq = eq
inhab -> *lex neq = neq

Compat: conv, br_and, equiv
Compat (inhab): br_and_neq
Compat (~diag): br_or, br_or_eq
Compat (inhab, =dec, R\/nR): compl, conv_compl, strict

Compat: R, S|aS
Compat (inhab): nR, nS
Compat (=dec): C|sC
Compat (inhab, =dec): 3
Compat (=dec, aS): T
Compat (inhab, =dec, R\/nR, C): nT

Compat (=dec): PO|TO
Compat (inhab, =dec): SPO|STO
