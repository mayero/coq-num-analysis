(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Exports support for subsets and functions.

 * Description

 It is partly based on capabilities from SSReflect and the Mathematical
 Components library.

 ** Support for operations on subsets

 See [Subsets.Subset], [Subsets.Subset_dec], [Subsets.Subset_charac], and
 [Subsets.Subset_any].

 ** Support for functions

 See [Subsets.Function], [Subsets.Function_sub], and [Subsets.Sub_type].

 ** Support for homogeneous binary relations

 See [Subsets.Binary_relation].

 ** Support for finite families

 It is based on ordinals and big operators from the Mathematical Components
 library.

 See [Subsets.ord_compl], [Subsets.Finite_family], and [Subsets.Finite_table].

 * Usage

 This module is meant to be imported after all imports from the Coq standard
 library, from the Mathematical Components library, and from other external Coq
 libraries such as Coquelicot or Flocq.

 This module may also be used through the import of [Subsets.Subsets_wDep],
 [Algebra.Algebra_wDep], [Lebesgue.Lebesgue_p_wDep], or
 [Lebesgue.Bochner.Bochner_wDep], where it is exported.
*)

From Subsets Require Export Subset Subset_dec Subset_charac Subset_any.
From Subsets Require Export Function Function_sub Sub_type.
From Subsets Require Export Binary_relation.
From Subsets Require Export ord_compl Finite_family Finite_table.
