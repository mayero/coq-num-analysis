(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**
  * Description

 This file is about the definitions of C and A.
 CSdk d k is the vector of all vectors of size (d+1) and of sum k.
 Adk d k is the vector of all vectors of size d and of sum <= k.

 This file contains their definitions (with the correct size) and properties.
 In particular, we order the vectiors so that they respect a specific order MOn
 defined below (akin to grevlex order).

 * Bibliography

 #<DIV><A NAME="RR9557v1"></A></DIV>#
 [[RR9557v1]]
 François Clément and Vincent Martin,
 Finite element method. Detailed proofs to be formalized in Coq,
 RR-9557, first version, Inria Paris, 2024,
 #<A HREF="https://inria.hal.science/hal-04713897v1">#
 https://inria.hal.science/hal-04713897v1#</A>#.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Algebra Require Import Algebra_wDep.

Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.


Section MI_Def.

Definition Slice_op {d:nat} (i:nat)
      := fun alpha : 'nat^d =>
          castF (add1n d) (concatF (singleF i) alpha).
    (* of type 'nat^(d.+ 1) *)

Lemma Slice_op_sum: forall d k i (alpha:'nat^d),
   (i <= k)%coq_nat -> sum alpha = (k-i)%coq_nat -> sum (Slice_op i alpha) = k.
Proof.
intros dd k i alpha H0 H1; unfold Slice_op.
rewrite sum_castF sum_concatF.
rewrite sum_singleF H1.
auto with arith.
Qed.

(* exists unique, useful? *)
Lemma Slice_op_correct : forall {d:nat} (k:nat) (alpha: 'nat^d.+1),
    sum alpha = k ->
     { ibeta : nat * 'nat^d | let i := fst ibeta in let beta := snd ibeta in
          (i <= k)%coq_nat /\ sum beta = (k-i)%coq_nat /\ Slice_op i beta =  alpha }.
Proof.
intros d k alpha H.
exists (alpha ord0, liftF_S alpha); split; try split; simpl.
(* *)
apply sum_ub_alt_nat; try easy.
now apply Nat.eq_le_incl.
(* *)
apply nat_add_sub_r.
rewrite <- H.
rewrite -skipF_first.
apply (sum_skipF alpha ord0).
(* *)
unfold Slice_op; extF i; unfold castF; simpl.
case (Nat.eq_0_gt_0_cases i); intros Hi.
rewrite concatF_correct_l.
simpl; rewrite Hi; auto with arith.
intros V; simpl; rewrite singleF_0; f_equal.
apply ord_inj; now simpl.
rewrite concatF_correct_r.
simpl; auto with arith.
intros V; unfold liftF_S; simpl; f_equal.
apply ord_inj; simpl; unfold bump; simpl; auto with arith.
Qed.

Definition Slice_fun {d n:nat} (u:nat) (a:'I_n -> 'nat^d) : 'I_n -> 'nat^d.+1
   := mapF (Slice_op u) a.

Lemma Slice_fun_skipF0: forall {d n} u (a:'I_n -> 'nat^d.+1) i,
   skipF (Slice_fun u a i) ord0 = a i.
Proof.
intros d n u a i.
unfold Slice_fun; rewrite mapF_correct; unfold Slice_op.
extF j.
unfold skipF, castF, concatF.
case (lt_dec _ _).
intros V; contradict V.
simpl; unfold bump; simpl; auto with arith.
intros V; f_equal.
apply ord_inj; simpl; unfold bump; simpl; auto with arith.
Qed.

Lemma Slice_fun_monot : forall {d n} u (alpha:'I_n -> 'nat^d)
     (i j:'I_n), (i < j)%coq_nat ->
     grsymlex_lt (alpha i) (alpha j) ->
     grsymlex_lt (Slice_fun u alpha i) (Slice_fun u alpha j).
Proof.
intros d; induction d.
intros n u alpha i j H1 H.
contradict H; apply grsymlex_lt_nil.
(* *)
intros n u alpha i j H1 H'.
assert (H'2 : grsymlex_lt (alpha i) (alpha j)) by assumption.
apply grsymlex_lt_S in H'; case H'.
intros H2; rewrite grsymlex_lt_eq; left.
rewrite (Slice_op_sum _ (sum (alpha i)+u) u); auto with arith.
rewrite (Slice_op_sum _ (sum (alpha j)+u) u); auto with arith.
now rewrite Nat.add_sub.
now rewrite Nat.add_sub.
intros (H2,H3).
apply grsymlex_lt_S; right; split.
rewrite (Slice_op_sum _ (sum (alpha i)+u) u); auto with arith.
rewrite (Slice_op_sum _ (sum (alpha j)+u) u); auto with arith.
now rewrite Nat.add_sub.
now rewrite Nat.add_sub.
rewrite 2!Slice_fun_skipF0; easy.
Qed.

Lemma Slice_fun_sortedF : forall {d n} u (alpha:'I_n -> 'nat^d),
   sortedF grsymlex_lt alpha -> sortedF grsymlex_lt (Slice_fun u alpha).
Proof.
move=>> H =>> H1; apply Slice_fun_monot; [| apply H]; easy.
Qed.

Lemma CSdk_aux : forall (d k:nat),
  (sum (succF (fun i : 'I_k.+1 => pbinom d i))).-1 = pbinom d.+1 k.
Proof. intros; rewrite pbinomS_rising_sum_l; easy. Qed.

(* \cal C^d_k has no meaning for dimension 0,
   CSdk d k is therefore \cal C^{d+1}_k. *)
Fixpoint CSdk (d k : nat) : 'nat^{(pbinom d k).+1,d.+1} :=
  match d with
  | O => fun _ _ => k
  | S d1 => castF (pbinomS_rising_sum_l d1 k)
                  (concatnF (fun i => Slice_fun (k - i) (CSdk d1 i)))
  end.

Lemma CS0k_eq : forall k, CSdk 0 k = fun _ _ => k.
Proof. easy. Qed.

Lemma CSdk_eq :
  forall d k, CSdk d.+1 k =
    castF (pbinomS_rising_sum_l d k)
          (concatnF (fun i => Slice_fun (k - i)%coq_nat (CSdk d i))).
Proof. intros; extF; simpl; unfold castF; f_equal. Qed.

Lemma CSdk_ext :
  forall d k1 k2 idk1 idk2,
    k1 = k2 -> nat_of_ord idk1 = nat_of_ord idk2 ->
    CSdk d k1 idk1 = CSdk d k2 idk2.
Proof. intros; subst; f_equal; apply ord_inj; easy. Qed.

Lemma CSdk_sum : forall d k idk, sum (CSdk d k idk) = k.
Proof.
intros d; induction d.
intros; rewrite sum_1 CS0k_eq; easy.
(* *)
intros k iSdk.
rewrite CSdk_eq.
unfold castF.
rewrite (splitn_ord (cast_ord (eq_sym (pbinomS_rising_sum_l d k)) iSdk)).
rewrite concatn_ord_correct.
unfold Slice_fun; rewrite mapF_correct.
apply Slice_op_sum.
auto with arith zarith.
rewrite IHd.
assert (splitn_ord1
  (cast_ord (eq_sym (pbinomS_rising_sum_l d k)) iSdk)< k.+1)%coq_nat.
apply /ltP; easy.
apply eq_sym, Nat.add_sub_eq_l.
auto with arith zarith.
Qed.

Lemma CSd0_eq : forall d, CSdk d 0 = fun=> constF d.+1 0.
Proof.
intros d; case d.
rewrite CS0k_eq; easy.
clear d; intros d.
extF iSd0 j.
rewrite constF_correct.
assert (V: sum (CSdk d.+1 0 iSd0) = 0).
apply CSdk_sum; auto with arith.
case_eq (CSdk d.+1 0 iSd0 j); try easy.
intros m Hm.
absurd (1 <= sum (CSdk d.+1 0 iSd0))%coq_nat.
rewrite V; auto with arith.
apply Nat.le_trans with (2:=sum_ub_nat _ j).
rewrite Hm; auto with arith.
Qed.

Lemma CSd1_eq_aux : forall d, d.+1 = (pbinom d 1).+1.
Proof. intros; now rewrite pbinom_1_r. Qed.

Lemma CSd1_eq : forall d, CSdk d 1 = castF (CSd1_eq_aux d) (itemF d.+1 1%nat).
Proof.
intros d; induction d.
rewrite CS0k_eq.
extF; extF.
rewrite !ord_one; unfold castF.
rewrite itemF_correct_l; try easy.
apply ord_inj; easy.
(* *)
rewrite CSdk_eq.
rewrite concatnF_two.
simpl (nat_of_ord ord0); simpl (nat_of_ord ord_max).
rewrite Nat.sub_0_r Nat.sub_diag.
rewrite CSd0_eq IHd.
rewrite (itemF_ind_l d.+1).
rewrite castF_comp.
apply: concatF_castF_eq.
apply pbinomS_0_r.
apply pbinomS_1_r.
intros K1; extF i; unfold castF, Slice_fun; simpl.
rewrite mapF_correct; unfold Slice_op.
unfold singleF; rewrite constF_correct.
unfold itemF.
extF j; unfold castF.
case (ord_eq_dec j ord0); intros Hj.
rewrite replaceF_correct_l; try easy.
rewrite concatF_correct_l; try easy.
rewrite Hj; simpl; auto with arith.
rewrite replaceF_correct_r; try easy.
rewrite concatF_correct_r; try easy.
simpl; assert (nat_of_ord j <> 0); auto with arith.
intros T; apply Hj; apply ord_inj; easy.
apply Nat.le_ngt, Nat.neq_0_le_1; easy.
intros K1; rewrite mapF_correct.
extF i.
unfold castF, Slice_fun; simpl.
rewrite mapF_correct; unfold Slice_op.
extF j; unfold castF.
f_equal.
f_equal.
apply ord_inj; easy.
apply ord_inj; easy.
Qed.

Lemma CSdk_0 : forall d k, CSdk d k ord0 = itemF d.+1 k ord0.
Proof.
intros d; induction d.
intros k; rewrite CS0k_eq.
extF i.
unfold itemF, replaceF; simpl.
rewrite ord_one; case (ord_eq_dec _ _); easy.
intros k; rewrite CSdk_eq.
unfold castF; extF i.
rewrite concatnF_splitn_ord.
unfold Slice_fun; rewrite mapF_correct; unfold Slice_op, castF.
rewrite splitn_ord2_0; try easy; auto with arith.
rewrite IHd.
rewrite splitn_ord1_0; try easy; auto with arith.
case (ord_eq_dec i ord0); intros Hi.
rewrite Hi concatF_correct_l singleF_0.
rewrite itemF_diag; simpl; auto with arith.
rewrite concatF_correct_r.
simpl; intros V; apply Hi; apply ord_inj; simpl; auto with zarith.
intros K.
rewrite itemF_zero_compat; try easy.
rewrite fct_zero_eq.
rewrite itemF_correct_r; try easy.
Qed.

Lemma CSdk_max : forall d k, CSdk d k ord_max = itemF d.+1 k ord_max.
Proof.
intros d; induction d.
intros k; rewrite CS0k_eq.
extF i.
unfold castF; simpl; rewrite 2!ord_one; simpl.
rewrite itemF_diag; try easy.
(* *)
intros k; rewrite CSdk_eq.
generalize (CSdk_aux d k); intros H1.
unfold castF.
rewrite concatnF_splitn_ord.
rewrite splitn_ord2_max; try easy.
unfold Slice_fun, Slice_op; rewrite mapF_correct.
rewrite IHd.
replace (k - _)%coq_nat with 0%nat.
unfold castF; extF i.
rewrite splitn_ord1_max; simpl; try easy.
case (ord_eq_dec i ord_max); intros Hi.
rewrite Hi concatF_correct_r.
simpl; auto with zarith.
intros K; replace (concat_r_ord K) with (ord_max:'I_d.+1).
rewrite 2!itemF_diag; simpl; auto with arith.
apply ord_inj; simpl; auto with arith.
rewrite itemF_correct_r; try easy.
case (ord_eq_dec i ord0); intros Hi2.
rewrite Hi2; rewrite concatF_correct_l; try easy.
rewrite concatF_correct_r; try easy.
simpl; intros V; apply Hi2; apply ord_inj; simpl; auto with zarith.
intros K; rewrite itemF_correct_r; try easy.
apply ord_neq; simpl; rewrite -minusE; intros V; apply Hi.
apply ord_inj; simpl.
assert (nat_of_ord i <> 0).
intros V'; apply Hi2; apply ord_inj; easy.
destruct i; simpl in *; auto with zarith.
rewrite splitn_ord1_max; simpl; try easy; auto with arith.
Qed.

Lemma CSdk_sortedF_aux :
  forall {n} (A B : 'nat^n.+1) i,
    sum A = sum B -> (B i < A i)%coq_nat ->
    (sum (skipF A i) < sum (skipF B i))%coq_nat.
Proof.
intros n A B i H1 H2.
apply Nat.add_lt_mono_l with (A i).
apply Nat.le_lt_trans with (sum A).
rewrite (sum_skipF A i); unfold plus; simpl; auto with arith.
rewrite H1; rewrite (sum_skipF B i).
unfold plus; simpl.
apply Nat.add_lt_mono_r; auto with arith.
Qed.

Lemma CSdk_sortedF : forall d k, sortedF grsymlex_lt (CSdk d k).
Proof.
intros d; induction d.
intros k; apply (sortedF_castF_rev (pbinomS_0_l _)).
intros i j H.
contradict H; rewrite 2!ord_one; auto with arith.
(* *)
intros k; rewrite CSdk_eq.
apply sortedF_castF.
apply concatnF_order.
apply grsymlex_lt_strict_total_order.
intros i; apply Slice_fun_sortedF.
apply IHd.
intros i Him.
apply grsymlex_lt_S; right.
assert (i < k)%coq_nat.
assert (nat_of_ord i <> k).
intros V; apply Him.
apply ord_inj; rewrite V; simpl; auto with arith.
assert (i < k.+1)%coq_nat; try auto with zarith.
apply /ltP; easy.
split.
(* . *)
rewrite (Slice_op_sum _ k); try auto with arith zarith.
rewrite (Slice_op_sum _ k); try auto with arith zarith.
rewrite CSdk_0 sum_itemF.
simpl; rewrite bump_r; auto with zarith.
rewrite CSdk_max sum_itemF; simpl; auto with arith zarith.
(* . *)
rewrite grsymlex_lt_eq.
left.
apply CSdk_sortedF_aux.
rewrite (Slice_op_sum _ k); try auto with zarith.
rewrite (Slice_op_sum _ k); try auto with zarith.
rewrite CSdk_0 sum_itemF.
simpl; rewrite bump_r; auto with zarith arith.
rewrite CSdk_max sum_itemF; simpl; auto with arith zarith.
unfold Slice_fun, Slice_op; rewrite 2!mapF_correct; simpl.
unfold castF; rewrite 2!concatF_correct_l; simpl.
rewrite 2!singleF_0.
rewrite bump_r; auto with zarith arith.
Qed.

Lemma CSdk_surj :
  forall d k (b : 'nat^d.+1), sum b = k -> { idk | b = CSdk d k idk }.
Proof.
intros d; induction d.
intros k b.
rewrite sum_1; intros H.
exists ord0.
rewrite CS0k_eq.
extF i.
rewrite ord_one; easy.
(* *)
intros k b Hb.
destruct (Slice_op_correct k b Hb) as ((u,beta),(Hu1,(Hu2,Hu3))).
destruct (IHd (k-u)%coq_nat beta Hu2) as (i1,Hi1).
rewrite CSdk_eq.
assert (Vu : (k - u < k.+1)).
apply /ltP; rewrite -minusE; auto with arith zarith.
assert (K : ((sum (succF (fun i : 'I_k.+1 => pbinom d i))) =
           (pbinom (d.+2.-1) k).+1)).
rewrite -CSdk_aux.
assert (T: (0 < sum (succF (fun i : 'I_k.+1 => pbinom d i)))%coq_nat).
apply Nat.lt_le_trans with ((pbinom d 0).+1); auto with arith.
apply (sum_ub_nat (succF (fun i : 'I_k.+1 => pbinom d i)) ord0).
rewrite Nat.succ_pred_pos; easy.
exists (cast_ord K (concatn_ord _ (Ordinal Vu) i1)).
extF z; unfold castF.
rewrite (concatn_ord_correct' _ _ (Ordinal Vu) i1).
2: now simpl.
rewrite -Hu3 Hi1.
unfold Slice_fun; rewrite mapF_correct; unfold Slice_op.
unfold castF; f_equal; f_equal; simpl.
apply eq_sym, Nat.add_sub_eq_l; auto with arith zarith.
apply Nat.sub_add; easy.
Qed.

Lemma CSdk_inj :  forall d k, injective (CSdk d k).
Proof.
intros; apply sortedF_inj with grsymlex_lt;
    [apply grsymlex_lt_strict_total_order | apply CSdk_sortedF].
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1486, p. 67.#<BR>#
 See also Lem 1496, p. 70. *)
Definition Adk (d k : nat) : 'nat^{(pbinom d k).+1,d} :=
  match d with
  | 0 => 0
  | S d1 => castF (pbinomS_rising_sum_l d1 k) (concatnF (CSdk d1))
  end.

Lemma Adk_ext :
  forall d k1 k2 idk1 idk2,
    k1 = k2 -> nat_of_ord idk1 = nat_of_ord idk2 ->
    Adk d k1 idk1 = Adk d k2 idk2.
Proof. intros; subst; f_equal; apply ord_inj; easy. Qed.

Lemma A0k_eq : forall k, Adk O k = 0.
Proof. intros k; easy. Qed.

Lemma Ad0_eq : forall d, Adk d 0 = 0.
Proof.
intros [| d]; [easy |]; simpl; apply castF_zero_compat.
rewrite concatnF_one; apply castF_zero_compat, CSd0_eq.
Qed.

Lemma Ad1_eq_aux : forall d, 1%nat + d = (pbinom d 1).+1.
Proof. intros; rewrite pbinomS_1_r; easy. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1497, Eq (9.14), p. 70. *)
Lemma Ad1_eq :
  forall d, Adk d 1 =
    castF (Ad1_eq_aux d)
          (concatF (singleF (constF d 0)) (itemF d 1%nat)).
Proof.
intros [| d1]; [rewrite A0k_eq; apply eq_sym, hatm0T_eq; easy |].
simpl; rewrite concatnF_two CSd0_eq CSd1_eq castF_comp.
apply: concatF_castF_eq; [apply pbinomS_0_r | apply pbinomS_1_r | easy |].
intro; rewrite castF_comp castF_refl; easy.
Qed.

Lemma Ad1_S :
  forall d id1 (H : id1 <> ord0),
    Adk d 1 id1 = itemF d 1%nat (cast_ord (pbinom_1_r d) (lower_S H)).
Proof.
intros d id1 H; rewrite Ad1_eq; unfold castF.
rewrite concatF_correct_r.
simpl; intro; apply H, ord_inj; simpl; apply lt_1; easy.
intros K; f_equal; apply ord_inj; easy.
Qed.

Lemma AdSk_eq_aux :
  forall d k,
    (pbinom d.+1 k).+1 + (pbinom d k.+1).+1 = (pbinom d.+1 k.+1).+1.
Proof. intros; rewrite plus_comm; apply pbinomS_pascal. Qed.

Lemma AdSk_eq :
  forall d k, Adk d.+1 k.+1 =
    castF (AdSk_eq_aux d k) (concatF (Adk d.+1 k) (CSdk d k.+1)).
Proof.
intros d k; simpl; rewrite concatnF_ind_r castF_comp. apply: concatF_castF_eq;
    [apply pbinomS_rising_sum_l | | apply castF_refl].
intro; extF; unfold castF; f_equal; apply ord_inj; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1497, Eq (9.12), p. 70. *)
Lemma A1k_eq : forall k, Adk 1 k = constF 1.
Proof.
intros k; induction k.
extT; simpl; unfold castF; rewrite concatnF_one !ord_one; easy.
extF i1k j; rewrite AdSk_eq; unfold castF; rewrite IHk CS0k_eq.
destruct (ord_eq_dec i1k ord_max) as [-> | H1].
rewrite concatF_correct_r /= pbinom_1_l// pbinomS_1_l; apply Nat.lt_irrefl.
rewrite concatF_correct_l// /=.
assert (H : (i1k < (pbinom 1 k.+1).+1)%coq_nat) by now apply /ltP.
apply lt_S in H; destruct H as [H | H]; [| contradict H1; apply ord_inj; easy].
destruct i1k; move: H; simpl; rewrite !pbinom_1_l; easy.
Qed.

Lemma Adk_0 : forall d k, Adk d k ord0 = 0.
Proof.
intros [| d] k; [easy |].
simpl; unfold castF; rewrite concatnF_splitn_ord.
rewrite splitn_ord2_0// splitn_ord1_0// CSd0_eq; easy.
Qed.

Lemma Adk_sum : forall d k idk, (sum (Adk d k idk) <= k)%coq_nat.
Proof.
intros [| d] k idk; [rewrite A0k_eq sum_zero; apply Nat.le_0_l |].
simpl; unfold castF.
rewrite (splitn_ord (cast_ord _ _)) concatn_ord_correct CSdk_sum.
apply Nat.lt_succ_r; apply /ltP; easy.
Qed.

Lemma Adk_le : forall d k idk j, (Adk d k idk j <= k)%coq_nat.
Proof.
intros d k idk j; apply Nat.le_trans with (2:= Adk_sum d k idk), sum_ub_nat.
Qed.

Lemma Adk_last_layer :
  forall d k idk, (0 < d)%coq_nat -> (0 < k)%coq_nat ->
    sum (Adk d k idk) = k <-> ((pbinom d k.-1).+1 <= idk)%coq_nat.
Proof.
intros [| d] [| k] idk Hd Hk; [easy.. |]; clear.
unfold Adk; unfold castF.
pose (jdk := cast_ord (eq_sym (pbinomS_rising_sum_l d k.+1)) idk).
assert (Hjdk : nat_of_ord jdk = idk) by easy.
rewrite (sum_ext _ (concatnF (CSdk d) jdk))// concatnF_splitn_ord CSdk_sum /=.
split; intros H1.
(* *)
simpl; destruct (splitn_ord1_correct jdk) as [H2 _].
rewrite -Hjdk; apply Nat.le_trans with (2:=H2), Nat.eq_le_incl,
    nat_plus_reg_r with ((pbinom d (splitn_ord1 jdk)).+1).
rewrite -(sum_part_ind_r (fun i0 => (pbinom d i0).+1) (splitn_ord1 jdk)).
replace (lift_S (splitn_ord1 jdk)) with (ord_max : 'I_k.+3)
    by now apply ord_inj; rewrite lift_S_correct H1.
rewrite H1 sum_part_ord_max (pbinomS_rising_sum_l d k.+1) plus_comm.
apply pbinomS_pascal.
(* *)
apply trans_eq with (nat_of_ord (ord_max : 'I_k.+2)); [| easy]; f_equal.
apply splitn_ord1_inj with jdk; [apply splitn_ord1_correct |]; split.
(* . *)
rewrite Hjdk; apply Nat.le_trans with (2:=H1), Nat.eq_le_incl,
    nat_plus_reg_r with ((pbinom d (ord_max : 'I_k.+2)).+1).
rewrite -(sum_part_ind_r (fun i0 => (pbinom d i0).+1) ord_max).
rewrite lift_S_max sum_part_ord_max (pbinomS_rising_sum_l d k.+1) plus_comm.
apply eq_sym, pbinomS_pascal.
(* . *)
rewrite Hjdk lift_S_max sum_part_ord_max.
apply Nat.lt_le_trans with ((pbinom d.+1 k.+1).+1); [apply /ltP; easy |].
apply Nat.eq_le_incl; rewrite -(pbinomS_rising_sum_l d k.+1); easy.
Qed.

Lemma Adk_previous_layer :
  forall d k idPk (idk : 'I_(pbinom d k).+1),
    nat_of_ord idPk = idk -> Adk d k.-1 idPk = Adk d k idk.
Proof.
intros [| d]; [intros; rewrite 2!A0k_eq; easy |].
intros [| k] idPk idk H; [f_equal; apply ord_inj; easy |].
rewrite AdSk_eq; unfold castF; rewrite concatF_correct_l.
simpl; rewrite -H; apply /ltP; easy.
intros; f_equal; apply ord_inj; easy.
Qed.

Lemma Adk_sortedF : forall d k, sortedF grsymlex_lt (Adk d k).
Proof.
intros [| d] k; simpl.
(* *)
apply (sortedF_castF_rev (pbinomS_0_l _)).
intros i j H; contradict H; rewrite !ord_one; easy.
(* *)
apply sortedF_castF, concatnF_order.
apply grsymlex_lt_strict_total_order.
intro; apply CSdk_sortedF.
intros; apply grsymlex_lt_l; rewrite !CSdk_sum lift_S_correct narrow_S_correct.
apply nat_ltS.
Qed.

Lemma Adk_inj : forall d k, injective (Adk d k).
Proof.
intros; apply sortedF_inj with grsymlex_lt;
    [apply grsymlex_lt_strict_total_order | apply Adk_sortedF].
Qed.

Lemma Adk_surj :
  forall d k (b : 'nat^d), (sum b <= k)%coq_nat -> { idk | b = Adk d k idk }.
Proof.
move=> [| d] k b /nat_le_ltS/ltP Hb; [exists ord0; extF i; destruct i; easy |].
destruct (CSdk_surj d (sum b) b) as [idsb Hidsb]; [easy |].
exists (cast_ord (pbinomS_rising_sum_l d k)
    (concatn_ord (fun i => (pbinom (d.+1.-1) i).+1) (Ordinal Hb) idsb)).
simpl; unfold castF; rewrite cast_ord_comp cast_ord_id concatn_ord_correct//.
Qed.

Definition Adk_inv (d k : nat) (b : 'nat^d) : 'I_(pbinom d k).+1 :=
  match le_dec (sum b) k with
  | left H => proj1_sig (Adk_surj d k b H)
  | right _ => ord0
  end.

Lemma Adk_inv_correct_r :
  forall d k (b : 'nat^d), (sum b <= k)%coq_nat -> Adk d k (Adk_inv d k b) = b.
Proof.
intros; unfold Adk_inv; case (le_dec _ _); [| easy].
intro; destruct Adk_surj; simpl; easy.
Qed.

Lemma Adk_inv_correct_l : forall d k idk, Adk_inv d k (Adk d k idk) = idk.
Proof.
intros; unfold Adk_inv; case (le_dec _ _); intros H.
destruct Adk_surj; apply Adk_inj; simpl; easy.
contradict H; apply Adk_sum.
Qed.

Lemma Adk_kron :
  forall d k i,
    mapF INR (Adk d k (Adk_inv d k (itemF d k i))) = fun j => INR k * kronR i j.
Proof.
intros; rewrite -itemF_kron_eq Adk_inv_correct_r;
    [apply mapF_itemF_0 | rewrite sum_itemF]; easy.
Qed.

End MI_Def.
