(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Algebra Require Import Algebra_wDep.

Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.

(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!
 TODO FC (24/02/2025): check what is still missing in Binary_relation /
 Finite_family (lex) / Monomial_order (grlex).
 This includes stuff about weighted orders. *)


Section About_is_order.

Context {G : AbelianMonoid}.
Variable order : G -> G -> Prop.

Definition order_irrefl := forall x, ~ order x x.
Definition order_trans := forall x y z, order x y -> order y z -> order x z.
Definition order_asym := forall x y, order x y -> order y x -> False.
Definition order_monomial := forall x y z,
    order x y -> order (plus x z) (plus y z).
Definition order_total := forall x y, order x y \/ x = y \/ order y x.

Lemma order_irrefl_asym :
  order_trans -> order_irrefl -> order_asym.
Proof.
intros H1 H2 x y H3 H4.
apply (H2 x).
apply (H1 x y x); easy.
Qed.

Lemma asym_irrefl : order_trans -> order_asym -> order_irrefl.
Proof. intros H1 H2 x H3; apply (H2 x x); easy. Qed.

Lemma irrefl_asym_equiv : order_trans -> order_irrefl <-> order_asym.
Proof.
intro; split; [apply order_irrefl_asym | apply asym_irrefl]; easy.
Qed.

(** This order is strict, monomial, irreflexive, transitive and total *)
Definition is_smitt_order : Prop :=
  order_irrefl /\ order_trans /\ order_monomial /\ order_total.

(** Be aware that this is a strong requirement; this applies
   to (nat,lt) and many groups but not to (R^*, mul) or
 ( [0,1,..,n], + with saturation at n) for instance *)

Lemma is_smitt_irrefl :
   is_smitt_order -> order_irrefl.
Proof.
intros H; apply H.
Qed.

Lemma is_smitt_trans :
   is_smitt_order -> order_trans.
Proof.
intros H; apply H.
Qed.

Lemma is_smitt_monomial :
   is_smitt_order -> order_monomial.
Proof.
intros H; apply H.
Qed.

Lemma is_smitt_total :
   is_smitt_order -> order_total.
Proof.
intros H; apply H.
Qed.

Lemma is_smitt_asym :
   is_smitt_order -> order_asym.
Proof.
intros H; apply order_irrefl_asym; apply H.
Qed.

Lemma is_smitt_monomial_inv_r : is_smitt_order -> forall x y z,
  order (plus x z) (plus y z) -> order x y.
Proof.
intros H x y z H1.
case (is_smitt_total H x y); intros V; try easy.
case V; intros W.
rewrite W in H1; exfalso; apply (is_smitt_irrefl H _ H1).
exfalso; apply (is_smitt_asym H (x+z) (y+z)); try easy.
apply (is_smitt_monomial H); easy.
Qed.

Lemma is_smitt_monomial_inv_l : is_smitt_order -> forall x y z,
  order (plus z x) (plus z y) -> order x y.
Proof.
intros H x y z H1.
apply is_smitt_monomial_inv_r with z; try easy.
rewrite 2!(plus_comm _ z); easy.
Qed.

Lemma is_smitt_total_alt : is_smitt_order ->
   forall  (x y : G), ~ order x y -> ~ order y x -> x = y.
Proof.
intros H x y H1 H2.
case (is_smitt_total H x y); intros H4; try easy; case H4; easy.
Qed.

(** equivalent to Nat.add_cancel_l for nat ; requires G to have such an order *)
Lemma is_smitt_plus_cancel_r : is_smitt_order ->
   forall  (x y z: G), plus x z = plus y z ->  x = y.
Proof.
intros H x y z H1.
apply is_smitt_total_alt; try easy; intros T;
   apply (is_smitt_irrefl H (x+z)).
rewrite {2}H1.
apply is_smitt_monomial; easy.
rewrite {1}H1.
apply is_smitt_monomial; easy.
Qed.

Lemma is_smitt_plus_cancel_l : is_smitt_order ->
   forall  (x y z: G), plus z x = plus z y ->  x = y.
Proof.
intros H x y z H1.
apply is_smitt_plus_cancel_r with z; try easy.
rewrite plus_comm H1 plus_comm; easy.
Qed.

End About_is_order.


Section is_order_props.

Lemma is_smitt_lt : is_smitt_order lt.
Proof.
split; try split; try split.
intros x; auto with arith.
intros x y z; auto with zarith.
intros x y z; unfold plus; simpl; auto with zarith.
intros x y; apply Nat.lt_total.
Qed.

Definition order_sym {T : Type} (order: T -> T -> Prop) := fun x y => order y x.

Lemma order_sym_sym : forall {T : Type} (order: T -> T -> Prop),
   order_sym (order_sym order) = order.
Proof.
intros T order.
fun_ext x y; easy.
Qed.

Lemma is_smitt_sym : forall {G : AbelianMonoid} (Glt Ggt: G -> G -> Prop),
     (forall x y, Ggt x y = Glt y x) ->
      is_smitt_order Glt -> is_smitt_order Ggt.
Proof.
intros G Glt Ggt H (Y1,(Y2,(Y3,Y4))); split; try split; try split.
intros x; rewrite H; apply Y1.
intros x y z; repeat rewrite H; intros H1 H2; apply (Y2 z y x); easy.
intros x y z; rewrite 2!H; apply Y3.
intros x y; case (Y4 x y); repeat rewrite H; intros H1;[right; now right|idtac].
case H1; intros H2; [right; now left|now left].
Qed.

End is_order_props.


Section Mon_def1.

(** a first bunch of monomial strict orders *)

(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!
vieux commentaire
The two following ones mean that MOn is a monomial order
See https://en.wikipedia.org/wiki/Monomial_order
and David Cox; John Little; Donal O'Shea (2007).
Ideals, Varieties, and Algorithms: An Introduction to Computational Algebraic Geometry and Commutative Algebra.

This is also a weight order with the sequence of weights:
(1,1,1,...,1), (0,1,1,...,1), ... (0,...,0,1)
*)

Context {G: AbelianMonoid}.
Variable order:G -> G -> Prop.

Fixpoint lex_alt {n} (x y:'G^n) : Prop :=
  match n as p return (n = p -> _) with
    | 0 => fun H => reflexive order
    | S m => fun H => (order (castF H x ord0) (castF H y ord0)) \/
          ((castF H x) ord0 = (castF H y) ord0 /\ lex_alt (skipF (castF H x) ord0) (skipF (castF H y) ord0))
  end erefl.

Lemma truc : forall {n} (g:G), order_irrefl order
   -> order_irrefl (@lex_alt n).
Proof.
induction n.
simpl.
intros g H1 x H2.
specialize (H2 g).
now apply (H1 g).
intros g H1 x Hx.
case Hx; repeat rewrite castF_refl.
apply H1.
intros (_,H2); generalize H2.
apply IHn; easy.
Qed.

Lemma truc2 : forall {n}, reflexive order
   -> reflexive (@lex_alt n).
Proof.
induction n.
simpl; intros H1 x; easy.
intros H1 x.
right; repeat rewrite castF_refl.
split; try easy.
apply IHn; easy.
Qed.

Fixpoint lex {n} (x y:'G^n) : Prop :=
  match n as p return (n = p -> _) with
    | 0 => fun H => False
    | S m => fun H => (order (castF H x ord0) (castF H y ord0)) \/
          ((castF H x) ord0 = (castF H y) ord0 /\ lex (skipF (castF H x) ord0) (skipF (castF H y) ord0))
  end erefl.

Fixpoint colex {n} (x y:'G^n) : Prop :=
  match n as p return (n = p -> _) with
    | 0 => fun H => False
    | S m => fun H => (order (castF H x ord_max) (castF H y ord_max)) \/
          ((castF H x) ord_max = (castF H y) ord_max /\ colex (skipF (castF H x) ord_max) (skipF (castF H y) ord_max))
  end erefl.

Lemma lex_nil : forall (x y :'G^0), ~ lex x y.
Proof.
intros x y; easy.
Qed.

Lemma lex_S : forall {n} (x y :'G^n.+1),
    lex x y = (order (x ord0) (y ord0) \/
          (x ord0 = y ord0 /\ lex (skipF x ord0) (skipF y ord0))).
Proof.
intros n x y; simpl; repeat rewrite castF_refl; easy.
Qed.

Lemma colex_nil : forall (x y :'G^0), ~ colex x y.
Proof.
intros x y; easy.
Qed.

Lemma colex_S : forall {n} (x y :'G^n.+1),
    colex x y = (order (x ord_max) (y ord_max) \/
          (x ord_max = y ord_max /\ colex (skipF x ord_max) (skipF y ord_max))).
Proof.
intros n x y; simpl; repeat rewrite castF_refl; easy.
Qed.

Definition symlex {n} : ('G^n -> 'G^n -> Prop) := order_sym (@lex n).
Definition revlex {n} : ('G^n -> 'G^n -> Prop) := order_sym (@colex n).

Definition graded {n} (ordern:'G^n -> 'G^n -> Prop) (x y:'G^n) : Prop :=
  (order (sum x) (sum y)) \/ (sum x = sum y /\ ordern x y).

Definition grlex    {n} := graded (@lex n).
Definition grcolex  {n} := graded (@colex n).
Definition grevlex  {n} := graded (@revlex n).
Definition grsymlex {n} := graded (@symlex n).

Lemma sum_order_graded : forall {n} ordern (x y:'G^n),
   order (sum x) (sum y) -> graded ordern x y.
Proof.
intros ordern x y H; now left.
Qed.

Lemma graded_idem : forall {n:nat} (ordern:'G^n -> 'G^n -> Prop),
   graded ordern  = graded (graded ordern).
Proof.
intros n ordern.
fun_ext x y; apply prop_ext; split.
intros H; destruct H; try now left.
right; split; try easy.
right; easy.
intros H; destruct H; [now left| easy].
Qed.

Lemma is_smitt_lex : forall {n},
  is_smitt_order order -> is_smitt_order (@lex n).
Proof.
intros n H; repeat split; induction n; try easy.
intros x; rewrite lex_S; intros T; case T; try apply H.
intros (_,H1); generalize H1; apply IHn.
intros x y z; rewrite 3!lex_S; intros V1 V2; case V1; case V2.
intros H1 H2; left; apply (is_smitt_trans _ H _ (y ord0)); easy.
intros (H1,H2); rewrite H1; intros H3; now left.
intros H1 (H2,H3); rewrite -H2 in H1; now left.
intros (H1,H2) (H3,H4); right; split; [now rewrite -H1|idtac].
apply IHn with (skipF y ord0); easy.
intros x y z; rewrite 2!lex_S; intros H1; case H1.
intros H2; left; apply is_smitt_monomial; easy.
intros (H2,H3); right; split.
unfold plus; simpl; unfold fct_plus; simpl; now rewrite H2.
apply IHn; easy.
intros x y; rewrite (hat0F_unit x _ y)//; right; left; easy.
intros x y; rewrite 2!lex_S.
case (is_smitt_total _ H (x ord0) (y ord0)); intros H1.
left; left; easy.
case H1; clear H1; intros H1.
case (IHn (skipF x ord0) (skipF y ord0)); intros H2.
left; right; easy.
case H2; clear H2; intros H2.
right; left.
apply (eqxF_reg _ ord0); try easy.
apply eqxF_equiv; easy.
right; right; right; now split.
right; right; now left.
Qed.

Lemma is_smitt_colex : forall {n},
  is_smitt_order order -> is_smitt_order (@colex n).
Proof.
intros n H; repeat split; induction n; try easy.
intros x; rewrite colex_S; intros T; case T; try apply H.
intros (_,H1); generalize H1; apply IHn.
intros x y z; rewrite 3!colex_S; intros V1 V2; case V1; case V2.
intros H1 H2; left; apply (is_smitt_trans _ H _ (y ord_max)); easy.
intros (H1,H2); rewrite H1; intros H3; now left.
intros H1 (H2,H3); rewrite -H2 in H1; now left.
intros (H1,H2) (H3,H4); right; split; [now rewrite -H1|idtac].
apply IHn with (skipF y ord_max); easy.
intros x y z; rewrite 2!colex_S; intros H1; case H1.
intros H2; left; apply is_smitt_monomial; easy.
intros (H2,H3); right; split.
unfold plus; simpl; unfold fct_plus; simpl; now rewrite H2.
apply IHn; easy.
intros x y; rewrite (hat0F_unit x _ y)//; right; left; easy.
intros x y; rewrite 2!colex_S.
case (is_smitt_total _ H (x ord_max) (y ord_max)); intros H1.
left; left; easy.
case H1; clear H1; intros H1.
case (IHn (skipF x ord_max) (skipF y ord_max)); intros H2.
left; right; easy.
case H2; clear H2; intros H2.
right; left.
apply (eqxF_reg _ ord_max); try easy.
apply eqxF_equiv; easy.
right; right; right; now split.
right; right; now left.
Qed.

Lemma is_smitt_symlex : forall {n},
  is_smitt_order order -> is_smitt_order (@symlex n).
Proof.
intros n H; apply is_smitt_sym with (@lex n); try easy.
apply is_smitt_lex; easy.
Qed.

Lemma is_smitt_revlex : forall {n},
  is_smitt_order order -> is_smitt_order (@revlex n).
Proof.
intros n H; apply is_smitt_sym with (@colex n); try easy.
apply is_smitt_colex; easy.
Qed.

Lemma is_smitt_graded : forall {n} (ordern: 'G^n -> 'G^n -> Prop),
  is_smitt_order order -> is_smitt_order ordern
    -> is_smitt_order (graded ordern).
Proof.
intros n ordern (Y1,(Y2,(Y3,Y4))) (Z1,(Z2,(Z3,Z4))); unfold graded; repeat split.
(* *)
intros x T; case T.
apply Y1.
intros (_,H); generalize H; apply Z1.
(* *)
intros x y z H1 H2.
case H1; [intros U1|intros (U1,U2)].
case H2; [intros U3|intros (U3,U4)].
left; apply (Y2 _ (sum y)); easy.
left; rewrite -U3; easy.
case H2; [intros U3|intros (U3,U4)].
left; rewrite U1; easy.
right; split.
rewrite U1; easy.
apply (Z2 _ y); easy.
(* *)
intros x y z; intros H1.
case H1; [intros U1|intros (U1,U2)].
left; rewrite 2!sum_plus.
apply Y3; easy.
right; split.
rewrite 2!sum_plus; rewrite U1; easy.
apply Z3; easy.
(* *)
intros x y.
case (Y4 (sum x) (sum y)); intros U1.
left; now left.
case U1; clear U1; intros U1.
case (Z4 x y); intros U2.
left; right; now split.
case U2; clear U2; intros U2.
right; now left.
right; right; right; now split.
right; right; now left.
Qed.

Lemma is_smitt_grsymlex :  forall {n},
   is_smitt_order order -> is_smitt_order (@grsymlex n).
Proof.
intros n H.
apply is_smitt_graded; try easy.
apply is_smitt_symlex; easy.
Qed.

Lemma is_smitt_grlex :  forall {n},
   is_smitt_order order -> is_smitt_order (@grlex n).
Proof.
intros n H.
apply is_smitt_graded; try easy.
apply is_smitt_lex; easy.
Qed.

Lemma is_smitt_grcolex :  forall {n},
   is_smitt_order order -> is_smitt_order (@grcolex n).
Proof.
intros n H.
apply is_smitt_graded; try easy.
apply is_smitt_colex; easy.
Qed.

Lemma is_smitt_grevlex :  forall {n},
   is_smitt_order order -> is_smitt_order (@grevlex n).
Proof.
intros n H.
apply is_smitt_graded; try easy.
apply is_smitt_revlex; easy.
Qed.

End Mon_def1.


Section Mon_prop1.

Context {G: AbelianMonoid}.
Variable order: G -> G -> Prop.
Hypothesis Ho : is_smitt_order order.

Lemma graded_nil : forall (ordern:'G^0 -> 'G^0 -> Prop) (x y :'G^0),
  is_smitt_order ordern ->  ~ graded order ordern x y.
Proof.
intros ordern x y H0.
rewrite (hat0F_unit x _ y)//.
apply is_smitt_graded; easy.
Qed.

Lemma grevlex_S : forall {n} (x y :'G^n.+1),
   grevlex order x y <-> (order (sum x) (sum y) \/
           (sum x = sum y /\ grevlex order (skipF x ord_max) (skipF y ord_max))).
Proof.
intros n x y; split; intros H1.
(* *)
case H1;[intros H2|intros (H2,H3)].
now left.
right; split; try easy.
case H3; repeat rewrite castF_refl;[intros H4|intros (H4,H5)].
left.
apply (is_smitt_monomial_inv_l _ Ho _ _ (y ord_max)).
rewrite -(sum_skipF y ord_max) -H2.
rewrite (sum_skipF _ ord_max).
apply is_smitt_monomial; easy.
(* . *)
right; split; try easy.
apply (is_smitt_plus_cancel_l _ Ho) with (x ord_max).
rewrite -(sum_skipF x ord_max) H2 -H4.
rewrite (sum_skipF _ ord_max); easy.
(* *)
case H1;[intros H2|intros (H2,H3)].
now left.
right; split; try easy.
case H3; repeat rewrite castF_refl;[intros H4|intros (H4,H5)].
left; rewrite 2!castF_refl.
apply (is_smitt_monomial_inv_r _ Ho _ _ (sum (skipF y ord_max))).
rewrite -(sum_skipF y ord_max) -H2.
rewrite (sum_skipF _ ord_max).
rewrite 2!(plus_comm (x ord_max)).
apply is_smitt_monomial; easy.
(* . *)
right; split; repeat rewrite castF_refl; try easy.
apply (is_smitt_plus_cancel_r _ Ho _ _ (sum (skipF x ord_max))).
rewrite -(sum_skipF x ord_max) H2 H4.
rewrite (sum_skipF _ ord_max); easy.
Qed.

Lemma grsymlex_S : forall {n} (x y :'G^n.+1),
   grsymlex order x y <-> (order (sum x) (sum y) \/
           (sum x = sum y /\ grsymlex order (skipF x ord0) (skipF y ord0))).
Proof.
intros n x y; split; intros H1.
(* *)
case H1;[intros H2|intros (H2,H3)].
now left.
right; split; try easy.
case H3; repeat rewrite castF_refl;[intros H4|intros (H4,H5)].
left.
apply (is_smitt_monomial_inv_l _ Ho _ _ (y ord0)).
rewrite -(sum_skipF y ord0) -H2.
rewrite (sum_skipF _ ord0).
apply is_smitt_monomial; easy.
(* . *)
right; split; try easy.
apply (is_smitt_plus_cancel_l _ Ho) with (x ord0).
rewrite -(sum_skipF x ord0) H2 -H4.
rewrite (sum_skipF _ ord0); easy.
(* *)
case H1;[intros H2|intros (H2,H3)].
now left.
right; split; try easy.
case H3; repeat rewrite castF_refl;[intros H4|intros (H4,H5)].
left; rewrite 2!castF_refl.
apply (is_smitt_monomial_inv_r _ Ho _ _ (sum (skipF y ord0))).
rewrite -(sum_skipF y ord0) -H2.
rewrite (sum_skipF _ ord0).
rewrite 2!(plus_comm (x ord0)).
apply is_smitt_monomial; easy.
(* . *)
right; split; repeat rewrite castF_refl; try easy.
apply (is_smitt_plus_cancel_r _ Ho _ _ (sum (skipF x ord0))).
rewrite -(sum_skipF x ord0) H2 H4.
rewrite (sum_skipF _ ord0); easy.
Qed.

(* TODO : déplacer ? *)
Lemma graded_lt_nonneg : forall {n} (ordern:'nat^n -> 'nat^n -> Prop) (x:'nat^n),
   x = zero \/ graded lt ordern zero x.
Proof.
intros n ordern x.
case (Nat.eq_0_gt_0_cases (sum x)); intros H0.
left; clear ordern; induction n.
extF i; destruct i; easy.
rewrite sum_ind_l in H0.
destruct (nat_plus_def _ _ H0) as (H1,H2).
specialize (IHn _ H2).
apply extF.
apply PAF_ind_l; try easy.
intros j; generalize (fun_ext_rev IHn j).
unfold liftF_S; easy.
(* *)
right; apply sum_order_graded.
rewrite sum_zero; easy.
Qed.

End Mon_prop1.


Section Mon_sym.

Context {G: AbelianMonoid}.

Lemma lex_sym : forall (order:G -> G -> Prop) {n},
  @lex _ (order_sym order) n = symlex order.
Proof.
intros order n; induction n; try easy.
fun_ext x y; apply prop_ext.
split; intros H; destruct H as [H|H]; try (left;easy); right; split; try easy.
rewrite -IHn; easy.
rewrite IHn; easy.
Qed.

Lemma colex_sym : forall (order:G -> G -> Prop) {n:nat},
  @colex _ (order_sym order) n = revlex order.
Proof.
intros order n; induction n; try easy.
fun_ext x y; apply prop_ext.
split; intros H; destruct H as [H|H]; try (left;easy); right; split; try easy.
rewrite -IHn; easy.
rewrite IHn; easy.
Qed.

Lemma symlex_sym : forall (order:G -> G -> Prop) {n:nat},
   @symlex _ (order_sym order) n = lex order.
Proof.
intros order n.
rewrite -{2}(order_sym_sym order).
now rewrite lex_sym.
Qed.

Lemma revlex_sym : forall (order:G -> G -> Prop) {n:nat},
  @revlex _ (order_sym order) n = colex order.
Proof.
intros order n.
rewrite -{2}(order_sym_sym order).
now rewrite colex_sym.
Qed.

Lemma graded_sym : forall (order:G -> G -> Prop) {n:nat} (ordern:'G^n -> 'G^n -> Prop),
  graded (order_sym order) (order_sym ordern) = order_sym (graded order ordern).
Proof.
intros order n ordern.
fun_ext x y.
unfold graded, order_sym; apply prop_ext; split; intros H.
case H; [intros H1; now left|intros (H1,H2); right; now split].
case H; [intros H1; now left|intros (H1,H2); right; now split].
Qed.

(* Essai
Lemma graded_idem' : forall (o1 o2 : G -> G -> Prop)
         {n : nat} (ordern : 'G^n -> 'G^n -> Prop),
       graded o1 ordern =
       graded o1 (graded o2 ordern).
Proof.
intros o1 o2; induction n.
intros ordern; fun_ext x y; apply prop_ext; split.
unfold graded; simpl; repeat rewrite sum_nil.
apply modus_ponens_or; try easy; tauto.
unfold graded; simpl; repeat rewrite sum_nil.
apply modus_ponens_or; try easy.
admit. (* devrait être vrai avec bonne déf de lex *)
(* *)
intros ordern; fun_ext x y; apply prop_ext; split.
intros H; destruct H; try now left.
right; split; try easy.
right; easy.
intros H; destruct H; [now left| try easy].
right; split; try easy.
destruct (proj2 H).
admit. (* devrait être vrai avec bonne déf de lex *)
easy.

Admitted.

Lemma toto : forall {n} (order:G -> G -> Prop) (alpha:'G^n),
 order_irrefl order (* inutile avec modif de lex ssi *) ->
 grlex order zero alpha <-> (order zero (sum alpha) \/
               (sum alpha = zero /\ grsymlex order alpha zero)).
Proof.
intros n order alpha H.
apply iff_trans with (graded (order_sym order) (grsymlex order) alpha zero).
2: unfold graded, order_sym.
2: rewrite sum_zero; easy.
rewrite -graded_idem'.
unfold symlex; rewrite graded_sym; easy.
(*
intros n; induction n.
intros order alpha H0; simpl.
unfold grlex, grsymlex, graded; simpl.
repeat rewrite sum_nil.
split; tauto.
intros order alpha H0.
unfold grlex, grsymlex, graded.
rewrite sum_zero.
split; apply modus_ponens_or; try easy.
intros (H1,H2); split; try easy.
right; split; try easy.
(* *)
intros H; destruct H as (H1,H2).
case H2; intros H3.
exfalso.
apply (H0 (sum alpha)).
rewrite {2}H1; easy.
split; try easy.*)
Qed.
*)

(*
0 <(grlex) alpha <-> 0 < |alpha|  OU ( |alpha| = 0  ET alpha <(grevlex) 0   ) O_o

0 <(grevlex) alpha <-> 0 < |alpha|  OU ( |alpha| = 0  ET  alpha <(grlex) 0 )
*)

End Mon_sym.


Section lexico_MO.

Definition grsymlex_lt {n} := @grsymlex _ lt n.

Lemma grsymlex_lt_nil : forall (x y : 'nat^0),
      ~ grsymlex_lt x y.
Proof.
intros x y; apply graded_nil; try apply is_smitt_symlex; apply is_smitt_lt.
Qed.

Lemma grsymlex_lt_S : forall {n : nat} (x y : 'nat^n.+1),
       grsymlex_lt x y <->
       (sum x < sum y)%coq_nat \/
       sum x = sum y /\ grsymlex_lt (skipF x ord0) (skipF y ord0).
Proof.
intros n x y.
apply grsymlex_S, is_smitt_lt.
Qed.

Lemma is_smitt_grsymlex_lt : forall {n}, is_smitt_order (@grsymlex_lt n).
Proof.
intros n; apply is_smitt_graded.
apply is_smitt_lt.
apply is_smitt_symlex, is_smitt_lt.
Qed.

Lemma grsymlex_lt_trans : forall {n} (x y z:'nat^n),
   grsymlex_lt x y -> grsymlex_lt y z -> grsymlex_lt x z.
Proof.
intros n; apply is_smitt_grsymlex_lt.
Qed.

Lemma grsymlex_lt_irrefl : forall {n} (x:'nat^n), ~ grsymlex_lt x x.
Proof.
intros n; apply is_smitt_grsymlex_lt.
Qed.

Lemma grsymlex_lt_total_strict : forall {n} (x y:'nat^n),
   x <> y -> grsymlex_lt x y \/ grsymlex_lt y x.
Proof.
intros n x y H.
case (is_smitt_total _ is_smitt_grsymlex_lt x y) as [H1|H1];
  [now left| destruct H1; try easy; now right].
Qed.

Fixpoint grsymlex_alt {n} (x y : 'nat^n) : Prop :=
  match n as p return (n = p -> _) with
    | 0 => fun H => False
    | S m => fun (H:n=m.+1) => (sum x < sum y)%coq_nat \/
          (sum x = sum y /\ grsymlex_alt (skipF (castF H x) ord0) (skipF (castF H y) ord0))
  end erefl.

Lemma grsymlex_alt_correct : forall {n}, @grsymlex_alt n = grsymlex_lt.
Proof.
induction n.
fun_ext x y; apply prop_ext; simpl.
split; try easy.
apply grsymlex_lt_nil.
fun_ext x y.
apply prop_ext; simpl; apply iff_sym.
apply iff_trans with (1:=grsymlex_lt_S x y).
repeat rewrite castF_refl; rewrite IHn; easy.
Qed.

Lemma grsymlex_lt_correct1 : forall x y : 'nat^1, grsymlex_lt x y = (x ord0 < y ord0)%coq_nat.
Proof.
intros x y; unfold grsymlex_lt, grsymlex, graded.
rewrite 2!sum_1; simpl; rewrite castF_id; apply prop_ext.
split; intros H; try now left.
case H; try easy.
intros (H1,H2); case H2; try easy; intros H3; auto with zarith.
Qed.

Lemma grsymlex_lt_itemF_monotS : forall n (i j:'I_n), (nat_of_ord j = i.+1)%coq_nat ->
  grsymlex_lt (itemF n 1%nat i) (itemF n 1%nat j).
Proof.
intros [| n]; [intros [i Hi]; easy |].
induction n.
intros i j H; contradict H.
rewrite 2!ord_one; easy.
(* *)
intros i j H; rewrite grsymlex_lt_S.
right; split.
rewrite 2!sum_itemF; easy.
case (ord_eq_dec i ord0); intros Hi.
(* *)
rewrite Hi; rewrite skipF_itemF_diag.
left.
rewrite sum_zero.
rewrite skipF_itemF_0.
apply ord_neq; rewrite H; easy.
intros Hj; rewrite sum_itemF.
now auto with arith.
(* *)
rewrite skipF_itemF_0.
rewrite skipF_itemF_0.
apply ord_neq; rewrite H; easy.
intros Hj; apply IHn.
rewrite 2!lower_S_correct H.
assert (nat_of_ord i <> 0) by now contradict Hi; apply ord_inj.
rewrite Nat.succ_pred; easy.
Qed.

Lemma  grsymlex_lt_itemF_monot : forall n (i j:'I_n), (i < j)%coq_nat ->
  grsymlex_lt (itemF n 1%nat i) (itemF n 1%nat j).
Proof.
intros n i j H.
pose (t:= (j-i)%coq_nat).
assert (Ht: (nat_of_ord j = i + t)%coq_nat) by auto with zarith.
generalize t i j Ht H; clear i j t Ht H.
induction t.
intros i j H1 H2; contradict H2; auto with zarith.
intros i j Ht H.
case_eq t.
intros Ht0.
apply grsymlex_lt_itemF_monotS.
rewrite Ht Ht0; auto with zarith.
intros tt Htt.
assert (Hk: ((i + t)%coq_nat < n)).
apply ltn_trans with j.
apply /ltP; rewrite Ht; auto with zarith.
destruct j; easy.
apply is_smitt_trans with (itemF n 1%nat (Ordinal Hk)).
apply is_smitt_grsymlex_lt.
apply IHt; try easy.
simpl; auto with zarith.
apply grsymlex_lt_itemF_monotS.
simpl; rewrite Ht; auto with zarith.
Qed.

Goal
  let x := itemF 3 1%nat ord1 in
  let y := itemF 3 1%nat ord_max in
  grsymlex_lt x y /\ ~ grevlex lt x y.
Proof.
intros x y; split.
apply grsymlex_lt_itemF_monot.
simpl; rewrite bump_r; auto with arith.
intros T; case T.
unfold x, y; rewrite 2!sum_itemF; auto with zarith.
intros (_,T1); generalize T1; clear T T1.
unfold revlex, order_sym; rewrite colex_S.
unfold x, y; rewrite itemF_diag itemF_correct_r.
2: apply ord_neq; simpl; rewrite bump_r; auto with arith.
intros T; case T; [intros H|intros (H,_)];
  contradict H; auto with zarith arith.
Qed.

End lexico_MO.


Section Weighted_orders.

(* essai sur weighted orders *)

Context {K:Ring}.
Variable (R: K -> K -> Prop).

Fixpoint weight_order {n m : nat} (weight : 'I_m -> 'K^n) (x y :'K^n) :=
 match m as p return (m = p -> _) with
  | 0 => fun H => False (* cas d'égalité *)
  | S mm => fun H =>
       R (dot_product x (castF H weight ord0)) (dot_product y (castF H weight ord0))
           \/
       ((dot_product x (castF H weight ord0) =  dot_product y (castF H weight ord0)) /\
          weight_order (liftF_S (castF H weight)) x y)
  end erefl.

Lemma weight_order_S : forall {n m} (weight:'I_m.+1 -> 'K^n) (x y:'K^n),
   weight_order weight x y =
   (R (dot_product x (weight ord0)) (dot_product y (weight ord0))
           \/
       ((dot_product x (weight ord0) =  dot_product y (weight ord0)) /\
          weight_order (liftF_S weight) x y)).
Proof.
intros; simpl; rewrite !castF_refl; easy.
Qed.

Lemma weight_order_equiv : forall {m n1 n2} (w1: 'I_m -> 'K^n1) (w2:'I_m -> 'K^n2) x1 y1 x2 y2,
   (forall i, dot_product x1 (w1 i) = dot_product x2 (w2 i)) ->
   (forall i, dot_product y1 (w1 i) = dot_product y2 (w2 i)) ->
      weight_order w1 x1 y1 = weight_order w2 x2 y2.
Proof.
intros m; induction m.
intros; easy.
intros n1 n2 w1 w2 x1 y1 x2 y2 H1 H2.
rewrite 2!weight_order_S.
apply prop_ext; split; apply modus_ponens_or.
rewrite H1 H2; easy.
intros (H3,H4); split.
rewrite -H1 -H2; easy.
rewrite (IHm _ _ (liftF_S w2) (liftF_S w1) x2 y2 x1 y1); try easy.
intros i; now rewrite H1.
intros i; now rewrite H2.
rewrite -H1 -H2; easy.
intros (H3,H4); split.
rewrite H1 H2; easy.
rewrite (IHm _ _ (liftF_S w1) (liftF_S w2) x1 y1 x2 y2); try easy.
intros i; now rewrite H1.
intros i; now rewrite H2.
Qed.

Lemma lex_weight_aux : forall {n} (x:'K^n) i,
  dot_product x (itemF n 1 i) = x i.
Proof.
intros n x i.
unfold dot_product; rewrite lc_itemF_r scal_one_r; easy.
Qed.

Lemma lex_weight_aux2 : forall {n} (x y:'K^n.+1),
 weight_order (liftF_S (itemF n.+1 1)) x y =
 weight_order (itemF n 1) (skipF x ord0) (skipF y ord0).
Proof.
intros n x y; rewrite -skipF_first.
apply weight_order_equiv; intros i.
rewrite 2!lex_weight_aux; easy.
rewrite 2!lex_weight_aux; easy.
Qed.

Lemma lex_weight : forall n,
   weight_order (itemF n (@one K)) = lex R.
Proof.
induction n.
simpl; easy.
fun_ext x y; apply prop_ext.
rewrite weight_order_S.
repeat rewrite lex_weight_aux.
rewrite lex_S.
split; apply modus_ponens_or; try easy; intros (H1,H2); split; try easy.
rewrite -IHn.
rewrite -lex_weight_aux2; easy.
rewrite lex_weight_aux2 IHn; easy.
Qed.

End Weighted_orders.
