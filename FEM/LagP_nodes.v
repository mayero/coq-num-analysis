(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Bibliography

 #<DIV><A NAME="RR9557v1"></A></DIV>#
 [[RR9557v1]]
 François Clément and Vincent Martin,
 Finite element method. Detailed proofs to be formalized in Coq,
 RR-9557, first version, Inria Paris, 2024,
 #<A HREF="https://inria.hal.science/hal-04713897v1">#
 https://inria.hal.science/hal-04713897v1#</A>#.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Algebra Require Import Algebra_wDep.
From FEM Require Import R_compl multi_index poly_Pdk poly_LagP1k.
From FEM Require Import poly_LagPd1_ref geom_transf_affine poly_LagPd1.

Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.


Section Nodes_Facts1.

Context {d k : nat}.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1588, Eq (9.80), p. 97. *)
Definition node_d0 (vtx : 'R^{d.+1,d}) : 'R^d := isobarycenter_ms vtx.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1591, Eq (9.84), p. 98.#<BR>#
 a_alpha = (1 - |alpha|/k) v_0 + \sum (alpha_i / k) v_i.#<BR>#
 This definition uses total function [inv].
 It will be used only for k>0, and the specific definition for k=0 is
 [node_d0] above. *)
(* TODO FC (08/02/2025): il faut utiliser un barycentre !
 Donc définir LagPd1_ref avant... *)
Definition node (vtx : 'R^{d.+1,d}) : 'R^{(pbinom d k).+1,d} :=
  fun idk =>
    scal (1 - sum (scal (/ INR k) (mapF INR (Adk d k idk)))) (vtx ord0) +
      lin_comb (scal (/ INR k) (mapF INR (Adk d k idk))) (liftF_S vtx).

Definition node_ref : 'R^{(pbinom d k).+1,d} :=
  fun idk i => INR (Adk d k idk i) / INR k.

Definition node' (vtx : 'R^{d.+1,d}) : 'R^{(pbinom d k).+1,d} :=
  fun idk => barycenter_ms (LagPd1_ref^~ (node_ref idk)) vtx.

Lemma node'_correct : node' = node.
Proof.
fun_ext vtx idk; unfold node, node', node_ref.
rewrite baryc_ms_correct baryc_ms_eq; [| apply LagPd1_ref_sum].
rewrite lc_ind_l LagPd1_ref_0// LagPd1_ref_S_eq; repeat f_equal;
    extF; rewrite fct_scal_eq mapF_correct scal_eq_K mult_comm_R; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1599, Eq (9.92), p. 100. *)
Lemma node_ref_node : node_ref = node vtx_ref.
Proof.
unfold node_ref, node; extF idk i.
rewrite (lc_eq_r _ _ _ vtx_ref_liftF_S) -lc_kron_r_decomp.
rewrite fct_plus_eq fct_scal_r_eq vtx_ref_0; [| easy].
rewrite scal_zero_r plus_zero_l fct_scal_eq mapF_correct.
rewrite div_eq mult_comm_R; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1601, Eq (9.94), p. 101. *)
Lemma node_ref_eq :
  forall idk, node_ref idk = scal (/ INR k) (mapF INR (Adk d k idk)).
Proof. intros; unfold node_ref; extF; rewrite div_eq mult_comm_R; easy. Qed.

Lemma node_ref_is_0_equiv :
  forall idk j, (0 < k)%coq_nat ->
    node_ref idk j = 0 <-> Adk d k idk j = 0%nat.
Proof.
move=> idk j /INR_n0 /invertible_equiv_R Hk.
unfold node_ref; rewrite div_zero_equiv//; apply INR_0_equiv.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1588, Eq (9.81), p. 97.#<BR>#
 a_alpha = v_0 + \sum (alpha_i / k) (v_i - v_0).#<BR>#
 The choice for 0 is arbitrary, it could be any index k<=d. *)
Lemma node_eq :
  forall vtx, node vtx = fun idk =>
    vtx ord0 + lin_comb (scal (/ INR k) (mapF INR (Adk d k idk)))
                        (liftF_S vtx - constF d (vtx ord0)).
Proof.
intros vtx; unfold node; extF idk.
rewrite scal_minus_l scal_one lc_minus_r lc_constF_r.
rewrite minus_eq -!plus_assoc; apply plus_compat_l, plus_comm.
Qed.

Lemma node_inj : forall {vtx}, aff_indep_ms vtx -> injective (node vtx).
Proof.
case (Nat.eq_dec k 0); intros Hk vtx Hvtx.
(* *)
intros [i Hi] [j Hj] _; subst; apply ord_inj; simpl.
rewrite pbinom_0_r in Hi, Hj; move: Hi => /ltP Hi; move: Hj => /ltP Hj; lia.
(* *)
intros idk jdk; rewrite node_eq.
move=> /plus_reg_l /lc_minus_zero_l_equiv.
rewrite -scal_minus_r lc_scal_l -skipF_first.
move=> /(scal_zero_reg_r_R _ _ (INR_inv_n0 Hk)) /Hvtx
    /minus_zero_equiv /(mapF_inj _ _ _ INR_eq).
apply Adk_inj.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1592, p. 98. *)
Lemma vtx_node : forall vtx, (0 < k)%coq_nat -> invalF vtx (node vtx).
Proof.
intros vtx Hk i; destruct (ord_eq_dec i ord0) as [-> | Hi].
(* i = ord0 *)
exists ord0;
    rewrite node_eq (Adk_0 d k) scal_zero_r lc_zero_l plus_zero_r; easy.
(* i <> ord0 *)
assert (H0 : invertible (INR k)) by now apply INR_invertible.
assert (H :
    scal (/ INR k) (fun j : 'I_d => INR k * kronR (lower_S Hi) j) =
      fun j => kronR (lower_S Hi) j).
  extF j; rewrite fct_scal_r_eq scal_eq_K.
  rewrite mult_assoc mult_inv_l// mult_one_l; easy.
exists (Adk_inv d k (itemF d k (lower_S Hi))).
rewrite node_eq Adk_kron (lc_eq_l _ (kronR (lower_S Hi)))// lc_kron_l_in_r.
rewrite fct_minus_eq constF_correct liftF_lower_S plus_minus_r; easy.
Qed.

Lemma vtx_node_ref : (0 < k)%coq_nat -> invalF vtx_ref node_ref.
Proof. rewrite node_ref_node; apply vtx_node. Qed.

End Nodes_Facts1.


Section Nodes_Facts2.

Context {d : nat}.
Context {vtx : 'R^{d.+1,d}}.
Hypothesis Hvtx : aff_indep_ms vtx.

Lemma T_geom_transports_node :
  forall k (idk : 'I_(pbinom d k).+1),
    T_geom vtx (node_ref idk) = node vtx idk.
Proof.
intros; unfold node; rewrite T_geom_correct lc_ind_l LagPd1_ref_0//
    LagPd1_ref_S_eq node_ref_eq; easy.
Qed.

Lemma T_geom_inv_transports_node :
  forall k (idk : 'I_(pbinom d k).+1),
    T_geom_inv Hvtx (node vtx idk) = node_ref idk.
Proof. intros; rewrite -T_geom_transports_node; apply T_geom_inv_can_l. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1605, Eq (9.95), p. 101. #<BR>#
 a_alpha \in Hface_0 <-> alpha \in CSdk. *)
Lemma node_Hface_0_in_CSdk :
  forall {k} (idk : 'I_((pbinom d k).+1)),
    (0 < d)%coq_nat -> (0 < k)%coq_nat ->
    Hface Hvtx ord0 (node vtx idk) <-> ((pbinom d k.-1).+1 <= idk)%coq_nat.
Proof.
intros k idk H1 H2; move: (@LagPd1_ref_sum d) => H0; unfold Hface, LagPd1.
rewrite -Adk_last_layer// -T_geom_transports_node Ker_equiv T_geom_inv_can_l.
rewrite LagPd1_ref_0// node_ref_eq -scal_sum_distr_l sum_INR.
assert (H : INR k <> 0) by now apply INR_n0.
rewrite one_minus_scal_inv_equiv// INR_eq_equiv; easy.
Qed.

(** a_alpha \notin Hface_0 <-> alpha \in Ad(k-1) *)
Lemma node_Hface_0_in_CSdk_contra :
  forall {k} (idk : 'I_((pbinom d k).+1)),
    (0 < d)%coq_nat -> (0 < k)%coq_nat ->
    ~ Hface Hvtx ord0 (node vtx idk) <-> (idk < ((pbinom d k.-1).+1))%coq_nat.
Proof.
intros; rewrite iff_not_l_equiv Nat.nlt_ge; apply node_Hface_0_in_CSdk; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1605, Eq (9.96), p. 101. #<BR>#
 For i between 0 and d-1, we have
 a_alpha \in Hface_{i+1} iff alpha \in Adki, ie alpha_i = 0. *)
Lemma node_Hface_S_in_Adki :
  forall {k} (idk : 'I_((pbinom d k).+1)) (j : 'I_d),
    (0 < d)%coq_nat -> (0 < k)%coq_nat ->
    Hface Hvtx (lift_S j) (node vtx idk) <-> Adk d k idk j = O.
Proof.
intros k idk j H1 H2; move: (@LagPd1_ref_sum d) => H0; unfold Hface, LagPd1.
rewrite -T_geom_transports_node Ker_equiv T_geom_inv_can_l.
fold (liftF_S LagPd1_ref j); rewrite LagPd1_ref_S_eq_alt.
apply node_ref_is_0_equiv; easy.
Qed.

End Nodes_Facts2.


Section T_node_Hface_Facts.

Context {d k : nat}.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1500, pp. 70-71.
 Function #f<SUP>d</SUP><SUB>{k,0}</SUB>.<BR>#
 See also Rem 1499, p. 70. *)
Definition T_node_Hface_0 : 'nat^{(pbinom d k).+1,d.+1} :=
  fun idk => Slice_op (k - sum (Adk d k idk)) (Adk d k idk).

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1501, pp. 71-72.
 Function #f<SUP>d</SUP><SUB>{k,i}</SUB>.<BR>#
 See also Rem 1499, p. 70. *)
Definition T_node_Hface_S : 'I_d.+1 -> 'nat^{(pbinom d k).+1,d.+1} :=
  fun i idk => insertF (Adk d k idk) O i.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1500, pp. 70-71.#<BR>#
 T_node_Hface_0 has image in $C^d_k$ (sum=k). *)
Lemma T_node_Hface_0_sum : forall idk, sum (T_node_Hface_0 idk) = k.
Proof.
intros; unfold T_node_Hface_0; apply Slice_op_sum;
    [apply Nat.le_sub_l | rewrite nat_sub2_r//; apply Adk_sum].
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1500, pp. 70-71.#<BR>#
 T_node_Hface_S preserves sum. *)
Lemma T_node_Hface_S_sum :
  forall i idk, sum (T_node_Hface_S i idk) = sum (Adk d k idk).
Proof. intros; unfold T_node_Hface_S; rewrite sum_insertF; easy. Qed.

End T_node_Hface_Facts.


Section Nodes_Facts3.

(** We need a dimension which is structurally nonzero.
 We take d = d1.+1 with d1 : nat. *)
Context {d1 : nat}.
Variable vtx : 'R^{d1.+2,d1.+1}.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1607, p. 102, case i=0.#<BR># 
 See also Rem 1606. *)
Lemma T_geom_Hface_0_map_node :
  forall {k} (id1k : 'I_(pbinom d1 k).+1), (0 < k)%coq_nat ->
    T_geom_Hface vtx ord0 (node_ref id1k) =
      node vtx (Adk_inv d1.+1 k (T_node_Hface_0 id1k)).
Proof.
intros k id1k Hk.
unfold node, node_ref.
rewrite Adk_inv_correct_r.
2: rewrite T_node_Hface_0_sum; easy.
rewrite -scal_sum_distr_l.
replace (sum (mapF _ _)) with (INR k).
2 : {rewrite sum_INR; f_equal.
rewrite T_node_Hface_0_sum; easy. }
rewrite lc_scal_l.
unfold scal; simpl.
unfold fct_scal, mult; simpl.
rewrite inv_eq_R.
rewrite Rinv_l.
rewrite minus_eq_zero.
unfold plus; simpl.
unfold fct_plus, scal, zero; simpl.
unfold mult, plus; simpl.
extF i.
unfold T_node_Hface_0, Slice_op.
rewrite Rmult_0_l Rplus_0_l mapF_castF castF_id mapF_concatF.
rewrite (lc_splitF_r (mapF INR (singleF (k - sum (Adk d1 k id1k))%coq_nat))
      (mapF INR (Adk d1 k id1k))).
rewrite lc_1 mapF_singleF singleF_0.
rewrite T_geom_Hface_0_eq_alt.
rewrite Rmult_plus_distr_l fct_plus_eq.
unfold plus, Rdiv; simpl; f_equal.
rewrite -mult_sum_l mult_comm_R fct_scal_r_eq.
rewrite -(div_diag (INR_invertible Hk)) div_eq mult_comm_R -mult_minus_l.
rewrite -scal_assoc.
rewrite scal_eq_K.
rewrite sum_INR minus_INR.
unfold mult; simpl; f_equal.
apply inv_eq_R.
rewrite fct_scal_r_eq; f_equal.
rewrite -liftF_S_0.
unfold firstF; f_equal.
unfold first_ord, lshift.
apply ord_inj; easy.
apply Adk_sum.
(* *)
rewrite -lastF_S1p.
unfold castF_S1p.
rewrite castF_id.
rewrite 2!fct_lc_r_eq.
unfold mapF, mapiF, Rdiv.
rewrite (lc_eq_l (fun i0 : 'I_d1 =>
   (INR (Adk d1 k id1k i0) * / INR k))
  (fun i0 : 'I_d1 => scal (/ INR k)
   (INR (Adk d1 k id1k i0))) _).
rewrite inv_eq_R.
rewrite lc_scal_l.
rewrite scal_eq_K.
unfold mult; simpl; f_equal.
extF j.
rewrite scal_eq_K.
rewrite mult_comm_R.
unfold mult; simpl; easy.
apply not_0_INR, nat_neq_0_equiv; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1607, p. 102, case i>0.#<BR>#
 See also Rem 1606. *)
Lemma T_geom_Hface_S_map_node :
  forall {k} i (id1k : 'I_(pbinom d1 k).+1), (0 < k)%coq_nat ->
    T_geom_Hface vtx (lift_S i) (node_ref id1k) =
      node vtx (Adk_inv d1.+1 k (T_node_Hface_S i id1k)).
Proof.
intros k i id1k Hk.
rewrite node_eq.
rewrite Adk_inv_correct_r.
2: rewrite T_node_Hface_S_sum.
2: apply Adk_sum.
rewrite T_geom_Hface_S_eq; try easy.
f_equal.
rewrite (lc_skipF i).
rewrite mapF_correct fct_scal_eq.
replace (T_node_Hface_S i id1k i) with O.
2: unfold T_node_Hface_S.
2: now rewrite insertF_correct_l.
rewrite scal_zero_r scal_zero_l plus_zero_l.
apply lc_ext.
intros j; unfold node_ref.
unfold skipF, scal; simpl; unfold fct_scal; simpl.
unfold scal; simpl; unfold mult; simpl.
unfold Rdiv; rewrite Rmult_comm; f_equal; f_equal.
unfold T_node_Hface_S.
rewrite -{1}(skipF_insertF (Adk d1 k id1k) O i); easy.
intros j.
unfold liftF_S, skipF, constF; simpl.
unfold minus, plus, opp; simpl.
unfold fct_plus, fct_opp; simpl; f_equal; f_equal.
case (lt_dec j i); intros Hij.
rewrite skip_ord_correct_l.
rewrite skip_ord_correct_l; try easy.
apply ord_inj; easy.
rewrite 2!lift_S_correct; auto with zarith.
rewrite skip_ord_correct_r.
rewrite skip_ord_correct_r; try easy.
rewrite 2!lift_S_correct; auto with zarith.
Qed.

End Nodes_Facts3.


Section Nodes_d1.

Context {d : nat}.

Lemma vtx_node_d1 : forall vtx, vtx = castF (pbinomS_1_r d) (node vtx).
Proof.
intros vtx; extF i; rewrite node_eq; unfold  castF.
rewrite INR_1 inv_one scal_one; destruct (ord_eq_dec i ord0) as [-> | Hi].
(* i = ord0. *)
rewrite cast_ordS_0// Adk_0 lc_zero_l plus_zero_r; easy.
(* i <> ord0. *)
assert (Hi' : ~ (i < 1)%coq_nat) by now apply ord_n0_nlt_equiv.
rewrite Ad1_eq; unfold castF.
rewrite concatF_correct_r mapF_itemF_0// itemF_kron_eq INR_1.
replace (fun j => 1 * _) with (fun j : 'I_d => kronR (i - 1) j)
    by now extF; rewrite mult_one_l.
rewrite (lc_kron_l_in_r (lower_S Hi)) fct_minus_eq liftF_lower_S.
rewrite constF_correct plus_minus_r; easy.
Qed.

Lemma node_vtx_d1 : forall vtx, node vtx = castF (eq_sym (pbinomS_1_r d)) vtx.
Proof. intros; rewrite eq_sym_equiv -castF_sym_equiv -vtx_node_d1//. Qed.

Lemma vtx_node_ref_d1 : vtx_ref = castF (pbinomS_1_r d) node_ref.
Proof. rewrite node_ref_node; apply vtx_node_d1. Qed.

Context {vtx : 'R^{d.+1,d}}.
Hypothesis Hvtx : aff_indep_ms vtx.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1554, p. 85.#<BR>#
 See also Lem 1593, p. 98. *)
Lemma LagPd1_kron_node :
  forall (id1 : 'I_(pbinom d 1).+1), LagPd1 Hvtx ^~ (node vtx id1) = kronR id1.
Proof.
intros; rewrite -(castF_cast_ord (pbinomS_1_r d) (pbinomS_1_r _)) -vtx_node_d1.
apply: LagPd1_kron_vtx.
Qed.

Lemma LagPd1_decomp_node :
  forall {p}, Pdk d 1 p ->
    p = lin_comb (mapF p (node vtx))
      (castF (eq_sym (pbinomS_1_r d)) (LagPd1 Hvtx)).
Proof.
intro; rewrite node_vtx_d1 mapF_castF lc_castF; apply LagPd1_decomp_vtx.
Qed.

End Nodes_d1.


Section Nodes_11.

Lemma LagP11_ref_eq :
  forall {i11 : 'I_(pbinom 1 1).+1}, LagP1k node_ref i11 = LagPd1_ref i11.
Proof.
assert (H0 : invertible ((@node_ref 1 1 ord_max ord0)
    - (@node_ref 1 1 ord0 ord0)))
    by (rewrite !node_ref_node !node_vtx_d1; apply vtx_ref_1_aff_indep_alt).
assert (H1 : invertible ((@node_ref 1 1 ord0 ord0)
    - (@node_ref 1 1 ord_max ord0)))
    by now apply invertible_opp_equiv; rewrite opp_minus.
intros i11; destruct (ord_eq_dec i11 ord0) as [-> | Hi]; fun_ext x.
(* i11 = ord0 *)
rewrite LagPd1_ref_0// LagP11_0// -div_opp// !opp_minus sum_1.
rewrite !node_ref_node !node_vtx_d1 castF_id.
rewrite (vtx_ref_0 ord0)// vtx_ref_S_is_1//.
rewrite minus_zero_r div_one_r; easy.
(* i11 <> ord0 *)
destruct (ord2_dec i11) as [| ->]; [easy |].
rewrite LagPd1_ref_S LagP11_1// -div_opp// !opp_minus.
rewrite !node_ref_node !node_vtx_d1 castF_id.
rewrite (vtx_ref_0 ord0)// vtx_ref_S_is_1//.
rewrite !minus_zero_l div_opp_one_r// opp_opp lower_S_max; f_equal.
apply I_1_unit; easy.
Qed.

End Nodes_11.


Section Sub_vertices_Def.

Context {d : nat}.
Variable k : nat.
Variable vtx : 'R^{d.+1,d}.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1594, Eq (9.88), p. 98.
 sub_vtx 0 = \underline{v}_0 = v_0,
 sub_vtx i = \underline{v}_i = a_{(k-1)e^i}, for i <> 0. *)
Definition sub_vtx : 'R^{d.+1,d} :=
  fun i => match ord_eq_dec i ord0 with
    | left _ => vtx ord0
    | right Hi => node vtx (Adk_inv d k (itemF d k.-1 (lower_S Hi)))
    end.

Lemma sub_vtx_0 : forall {i}, i = ord0 -> sub_vtx i = vtx ord0.
Proof. intros; unfold sub_vtx; case (ord_eq_dec _ _); easy. Qed.

Lemma sub_vtx_S :
  forall {i} (Hi : i <> ord0),
     sub_vtx i = node vtx (Adk_inv d k (itemF d k.-1 (lower_S Hi))).
Proof.
intros; unfold sub_vtx; case (ord_eq_dec _ _);
    [| intros; do 3 f_equal; apply ord_inj]; easy.
Qed.

Lemma sub_vtx_d1 : forall i, k = 1%nat -> sub_vtx i = vtx ord0.
Proof.
intros i Hk; unfold sub_vtx; destruct (ord_eq_dec _ _) as [Hi | Hi]; [easy |].
subst; rewrite node_eq Adk_inv_correct_r; [| rewrite sum_itemF; lia].
rewrite lc_scal_l mapF_itemF_0// lc_itemF_l
    scal_zero_l scal_zero_r plus_zero_r; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1595, p. 99.
 \underline{v}_i = 1/k v_0 + (k-1)/k v_i. *)
Lemma sub_vtx_eq :
  (0 < k)%coq_nat ->
  liftF_S sub_vtx =
    constF d (scal (/ INR k) (vtx ord0)) +
    scal (INR k.-1 / INR k) (liftF_S vtx).
Proof.
intros Hk; extF i; unfold liftF_S.
rewrite (sub_vtx_S (lift_S_not_first i)); unfold node.
rewrite -lc_ones_r Adk_inv_correct_r; [| rewrite sum_itemF; lia].
rewrite !lc_scal_l mapF_itemF_0// !lc_itemF_l.
rewrite ones_correct liftF_lower_S !scal_assoc scal_one_r//.
unfold mult; simpl; rewrite Rmult_comm -(INR_inv_decomp Hk).
unfold Rdiv; rewrite minus_plus_r; easy.
Qed.

End Sub_vertices_Def.


Section Sub_vertices_Facts.

Context {d k : nat}.
Hypothesis Hk : (1 < k)%coq_nat.
Context {vtx : 'R^{d.+1,d}}.
Hypothesis Hvtx : aff_indep_ms vtx.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1597, pp. 99-100. *)
Lemma sub_vtx_aff_indep : aff_indep_ms (sub_vtx k vtx).
Proof.
assert (Hk0 : (0 < k)%coq_nat) by lia.
intros L; rewrite frameF_ms_eq skipF_first.
rewrite sub_vtx_0// sub_vtx_eq// plus_comm -plus_minus_assoc.
rewrite -constF_minus -{2}(scal_one (vtx _)) -scal_minus_l INR_inv_pred//.
rewrite constF_scal div_opp_l scal_opp_l -minus_eq -scal_minus_r lc_scal_r.
assert (Hk1 : invertible (INR k.-1 / INR k)).
  unfold Rdiv; apply invertible_equiv_R, is_integral_R;
      [apply INR_n0 | apply INR_inv_n0]; lia.
move=> /(scal_zero_reg_r _ _ Hk1); rewrite -skipF_first; apply Hvtx.
Qed.

End Sub_vertices_Facts.


Section Sub_nodes_Def.

Context {d : nat}.
Variable k : nat.
Variable vtx : 'R^{d.+1,d}.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1598, p. 100. *)
Definition sub_node : 'R^{(pbinom d k.-1).+1,d} := node (sub_vtx k vtx).

End Sub_nodes_Def.


Section Sub_nodes_Facts1.

Context {d k : nat}.
Hypothesis Hk : (1 < k)%coq_nat.
Variable vtx : 'R^{d.+1,d}.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1598, p. 100. *)
Lemma sub_node_eq :
  sub_node k vtx = widenF (pbinomS_monot_pred d k) (node vtx).
Proof.
assert (Hk0 : (0 < k)%coq_nat) by lia.
assert (Hk1 : invertible (INR k.-1)) by (apply INR_invertible; lia).
extF; unfold sub_node, node, widenF.
rewrite sub_vtx_0// sub_vtx_eq// lc_plus_r lc_constF_r.
rewrite plus_assoc scal_assoc -scal_distr_r -!scal_sum_distr_l.
rewrite lc_scal_r -lc_scal_l scal_assoc -minus2.
rewrite -mult_comm_R mult_assoc -mult_minus_r scal_eq_K.
repeat f_equal.
(* *)
2,4: apply Adk_previous_layer; easy.
2: rewrite div_eq mult_comm_R mult_assoc mult_inv_l// mult_one_l; easy.
rewrite -{1}(mult_one_l (/ INR k.-1)) -mult_minus_r; apply opp_inj.
rewrite -mult_opp_l opp_minus INR_inv_pred// div_opp_l mult_opp_l; f_equal.
rewrite mult_comm_R mult_assoc mult_inv_l// mult_one_l; easy.
Qed.

End Sub_nodes_Facts1.


Section Sub_nodes_Facts2.

Context {d : nat}.
Context {vtx : 'R^{d.+1,d}}.
Hypothesis Hvtx : aff_indep_ms vtx.

(** sub_node \notin Hface_0 *)
Lemma sub_node_out_Hface_0 :
  forall {k} (idk : 'I_((pbinom d k).+1)),
    (0 < d)%coq_nat -> (0 < k)%coq_nat ->
    ~ Hface Hvtx ord0 (sub_node k.+1 vtx idk).
Proof.
intros; rewrite sub_node_eq; [| rewrite -Nat.succ_lt_mono; easy].
apply node_Hface_0_in_CSdk_contra; [easy | apply S_pos |].
simpl; apply /ltP; easy.
Qed.

End Sub_nodes_Facts2.
