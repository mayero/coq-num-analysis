(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Bibliography

 #<DIV><A NAME="RR9557v1"></A></DIV>#
 [[RR9557v1]]
 François Clément and Vincent Martin,
 Finite element method. Detailed proofs to be formalized in Coq,
 RR-9557, first version, Inria Paris, 2024,
 #<A HREF="https://inria.hal.science/hal-04713897v1">#
 https://inria.hal.science/hal-04713897v1#</A>#.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Algebra Require Import Algebra_wDep.
From FEM Require Import multi_index poly_Pdk.

Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.


Section LagP1k_Def.

Context {k : nat}.
Variable node : 'R^{(pbinom 1 k).+1,1}.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1448, p. 58.#<BR>#
 LagP1k i = Prod_{j<>i} (x - node j) / Prod_{j<>i} (node i - node j). *)
Definition LagP1k_aux : '(FRd 1)^(pbinom 1 k).+1 :=
  fun i1k x => prod_R ((skipF node i1k - constF _ x)^~ ord0).

Definition LagP1k : '(FRd 1)^(pbinom 1 k).+1 :=
  fun i1k x => LagP1k_aux i1k x / LagP1k_aux i1k (node i1k).

Lemma LagP10 :
  forall {i10 : 'I_(pbinom 1 k).+1}, k = 0 -> LagP1k i10 = fun=> 1.
Proof.
intros; fun_ext x; unfold LagP1k, LagP1k_aux; subst; apply div_one_compat.
apply invertible_eq_one, prod_R_nil.
rewrite !prod_R_nil_alt; easy.
Qed.

Lemma LagP11_0 :
  forall {i11 : 'I_(pbinom 1 k).+1},
    k = 1%nat -> i11 = ord0 ->
    invertible (node ord0 ord0 - node ord_max ord0) ->
    LagP1k i11 = fun x =>
      (x ord0 - node ord_max ord0) / (node ord0 ord0 - node ord_max ord0).
Proof.
intros; fun_ext x; unfold LagP1k, LagP1k_aux; subst.
rewrite !(prod_R_singleF_alt _ (eq_sym (eq_sym (pbinom_1_l _)))) !castF_1.
rewrite !fct_minus_eq skipF_2l0 !constF_correct.
rewrite -(div_opp (node _ _ - _)); [rewrite !opp_minus; easy |].
apply invertible_opp_equiv; rewrite opp_minus; easy.
Qed.

Lemma LagP11_1 :
  forall {i11 : 'I_(pbinom 1 k).+1},
    k = 1%nat -> i11 = ord_max ->
    invertible (node ord_max ord0 - node ord0 ord0) ->
    LagP1k i11 = fun x =>
      (x ord0 - node ord0 ord0) / (node ord_max ord0 - node ord0 ord0).
Proof.
intros; fun_ext x; unfold LagP1k, LagP1k_aux; subst.
rewrite !(prod_R_singleF_alt _ (eq_sym (eq_sym (pbinom_1_l _)))) !castF_1.
rewrite !fct_minus_eq skipF_2r0 !constF_correct.
rewrite -(div_opp (node _ _ - _)); [rewrite !opp_minus; easy |].
apply invertible_opp_equiv; rewrite opp_minus; easy.
Qed.

End LagP1k_Def.


Section LagP1k_Facts.

Context {k : nat}.
Context {node : 'R^{(pbinom 1 k).+1,1}}.
Hypothesis Hnode : injective node. (* ie pairwise disjoint. *)

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1449, Eq (8.5), p. 58. *)
Lemma LagP1k_kron : forall i1k, LagP1k node ^~ (node i1k) = kronR i1k.
Proof.
intros i1k; extF j1k; unfold LagP1k, LagP1k_aux.
destruct (ord_eq_dec i1k j1k) as [<- | H1k].
(* i = j *)
rewrite kronR_is_1// div_diag//.
apply invertible_equiv_R, prod_R_nonzero; move: Hnode => /injF_ms_equiv.
apply (spec_hyp i1k); rewrite frameF_ms_eq -contra_equiv; apply inFF_1; easy.
(* i <> j *)
rewrite kronR_is_0; [| contradict H1k; apply ord_inj; easy].
apply: div_eq_zero_l; apply prod_R_zero; exists (insert_ord H1k).
rewrite fct_minus_eq skipF_correct constF_correct minus_eq_zero; easy.
Qed.

Lemma LagP1k_aux_is_P1k : inclF (LagP1k_aux node) (Pdk 1 k).
Proof.
intro; apply (Pdk_mult_iter (fun=> 1%nat));
    [| rewrite sum_constF_nat Nat.mul_1_r pbinom_1_l; easy].
intro; apply: (cms_minus Pdk_cms); [apply Pdk_const; easy |].
apply Pdk_ext with (Monom_dk 1 1 ord1); [| apply lin_span_inclF_diag].
intro; rewrite Monom_1k A1k_eq !constF_correct; apply pow_1.
Qed.

Lemma LagP1k_is_P1k : inclF (LagP1k node) (Pdk 1 k).
Proof. intro; apply Pdk_mult_const; [apply LagP1k_aux_is_P1k | easy]. Qed.

Lemma LagP1k_lin_indep : lin_indep (LagP1k node).
Proof.
move=> L /fun_ext_rev HL; rewrite (lc_kron_r_decomp_sym L); extF.
rewrite fct_lc_r_eq -LagP1k_kron -fct_lc_r_eq; apply HL.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1449, p. 58. *)
Lemma LagP1k_basis : basis (Pdk 1 k) (LagP1k node).
Proof.
apply lin_indep_basis;
    [apply Pdk_has_dim | apply LagP1k_is_P1k | apply LagP1k_lin_indep].
Qed.

Lemma LagP1k_lin_gen : lin_gen (Pdk 1 k) (LagP1k node).
Proof. apply basis_lin_gen, LagP1k_basis. Qed.

Lemma P1k_has_dim : has_dim (Pdk 1 k) k.+1.
Proof. apply (has_dim_ext _ (pbinomS_1_l k)), (Dim _ _ _ LagP1k_basis). Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1450, p. 58. *)
Lemma LagP1k_decomp :
  forall {p}, Pdk 1 k p -> p = lin_comb (mapF p node) (LagP1k node).
Proof.
intros p Hp; destruct (basis_ex_decomp LagP1k_basis p Hp) as [L ->].
rewrite {1}(lc_kron_r_decomp_sym L); apply lc_ext_l; intros.
rewrite mapF_correct !fct_lc_r_eq LagP1k_kron; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1449, Eq (8.6), p. 58. *)
Lemma LagP1k_sum : sum (LagP1k node) = 1.
Proof.
apply eq_trans with (Monom_dk 1 k ord0); [| rewrite Monom_dk_0; easy].
rewrite (LagP1k_decomp (lin_span_inclF_diag _ _)).
rewrite -lc_ones_l; apply lc_ext_l; intros i1k; rewrite Monom_dk_0; easy.
Qed.

End LagP1k_Facts.
