(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Bibliography

 #<DIV><A NAME="RR9557v1"></A></DIV>#
 [[RR9557v1]]
 François Clément and Vincent Martin,
 Finite element method. Detailed proofs to be formalized in Coq,
 RR-9557, first version, Inria Paris, 2024,
 #<A HREF="https://inria.hal.science/hal-04713897v1">#
 https://inria.hal.science/hal-04713897v1#</A>#.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Algebra Require Import Algebra_wDep.
From FEM Require Import R_compl geometry multi_index poly_Pdk poly_LagPd1_ref.
From FEM Require Import geom_transf_affine.

Local Open Scope R_scope. (* For order notations. *)
Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.


Section LagPd1_Facts.

Context {d : nat}.
Context {vtx : 'R^{d.+1,d}}.
Hypothesis Hvtx : aff_indep_ms vtx.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1552, p. 84. *)
Definition LagPd1 : '(FRd d)^d.+1 :=
  fun i x => LagPd1_ref i (T_geom_inv Hvtx x).

Lemma LagPd1_eq : LagPd1 = compF_l LagPd1_ref (T_geom_inv Hvtx).
Proof. easy. Qed.

Lemma LagPd1_ref_eq : LagPd1_ref = compF_l LagPd1 (T_geom vtx).
Proof. rewrite LagPd1_eq -compF_l_comp f_inv_id_l; easy. Qed.

Lemma LagPd1_0 :
  forall i, i = ord0 -> LagPd1 i = fun x => 1 - sum (T_geom_inv Hvtx x).
Proof. intros; unfold LagPd1; rewrite LagPd1_ref_0; easy. Qed.

Lemma LagPd1_S :
  forall {i} (Hi : i <> ord0),
    LagPd1 i = fun x => T_geom_inv Hvtx x (lower_S Hi).
Proof. intros; unfold LagPd1; rewrite LagPd1_ref_S; easy. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1554, Eq (9.46), p. 85. *)
Lemma LagPd1_sum : forall x, sum (LagPd1^~ x) = 1.
Proof. intros; apply LagPd1_ref_sum. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1554, Eq (9.45), p. 85. *)
Lemma LagPd1_kron_vtx : forall i, LagPd1^~ (vtx i) = kronR i.
Proof.
intros; unfold LagPd1; rewrite T_geom_inv_transports_vtx.
apply LagPd1_ref_kron_vtx.
Qed.

Lemma LagPd1_am : forall i, aff_map_ms (LagPd1 i).
Proof.
(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!
intros; apply am_ext with (LagPd1_ref i \o T_geom_inv Hvtx); [easy |].
apply: am_comp. (* Ne fonctione pas ! *)*)

(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!
intros; apply am_ext with
    ((LagPd1_ref i : fct_ModuleSpace -> R_ModuleSpace) \o
     (T_geom_inv Hvtx : fct_ModuleSpace -> fct_ModuleSpace)); [easy |].
apply am_comp; [apply T_geom_inv_am | apply LagPd1_ref_am]. (* Fonctionne ! *)*)

intros; eapply am_ext.
2: apply: am_comp.
2: apply: (T_geom_inv_am Hvtx).
2: apply LagPd1_ref_am.
easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1560, p. 87.#<BR>#
 [LagPd1] are the barycentric coordinates wrt [vtx]. *)
Lemma LagPd1_baryc :
  forall x L, x = lin_comb L vtx -> sum L = 1 -> L = LagPd1^~ x.
Proof.
intros x L H1 H2; unfold LagPd1; rewrite H1 T_geom_inv_transports_baryc//.
extF; rewrite -LagPd1_ref_am_alt; easy.
Qed.

Lemma LagPd1_baryc_alt : forall x, x = lin_comb (LagPd1^~ x) vtx.
Proof.
intros x; destruct (aff_indep_aff_gen_R _ has_dim_Rn Hvtx x) as [L HL].
rewrite -(LagPd1_baryc _ L); easy.
Qed.

Lemma LagPd1_ge_0 : forall i x, convex_envelop vtx x -> 0 <= LagPd1 i x.
Proof.
intros i x [L HL1 HL2]; unfold LagPd1; apply LagPd1_ref_ge_0.
rewrite baryc_ms_eq// T_geom_inv_transports_baryc// -baryc_ms_eq; easy.
Qed.

Lemma LagPd1_le_1 : forall i x, convex_envelop vtx x -> LagPd1 i x <= 1.
Proof.
intros i x [L HL1 HL2]; unfold LagPd1; apply LagPd1_ref_le_1.
rewrite baryc_ms_eq// T_geom_inv_transports_baryc// -baryc_ms_eq; easy.
Qed.

Lemma LagPd1_lin_indep : lin_indep LagPd1.
Proof.
unfold LagPd1; intros L HL; apply LagPd1_ref_lin_indep.
fun_ext x_ref; rewrite fct_lc_r_eq -{1}(T_geom_inv_can_l Hvtx x_ref).
apply trans_eq with
    (lin_comb L (fun i x => LagPd1_ref i (T_geom_inv Hvtx x))
      (T_geom vtx x_ref));
    [rewrite fct_lc_r_eq | rewrite HL]; easy.
Qed.

Lemma LagPd1_lin_gen : lin_gen (Pdk d 1) LagPd1.
Proof.
rewrite LagPd1_eq; apply (lin_gen_castF_reg (eq_sym (pbinomS_1_r d))).
apply (lin_gen_ext (compF_l
      (castF (eq_sym (pbinomS_1_r d)) LagPd1_ref) (T_geom_inv Hvtx))); [easy |].
apply Pdk_lin_gen_comp.
apply basis_castF_compat, LagPd1_ref_basis.
apply T_geom_inv_am.
apply f_inv_bij.
Qed.

Lemma LagPd1_is_Pd1 : inclF LagPd1 (Pdk d 1).
Proof. rewrite LagPd1_lin_gen; apply lin_span_inclF_diag. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1554, p. 85. *)
Lemma LagPd1_basis : basis (Pdk d 1) LagPd1.
Proof.
rewrite LagPd1_lin_gen; apply basis_lin_span_equiv, LagPd1_lin_indep.
Qed.

Lemma LagPd1_decomp_vtx :
  forall {p}, Pdk d 1 p -> p = lin_comb (mapF p vtx) LagPd1.
Proof.
intros p Hp; destruct (basis_ex_decomp LagPd1_basis p Hp) as [L ->].
rewrite {1}(lc_kron_r_decomp_sym L); apply lc_ext_l; intros.
rewrite mapF_correct !fct_lc_r_eq LagPd1_kron_vtx; easy.
Qed.

End LagPd1_Facts.


Section Face_hyperplane_Def.

Context {d : nat}.
Context {vtx : 'R^{d.+1,d}}.
Hypothesis Hvtx : aff_indep_ms vtx.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1563, Eq (9.57), pp. 88-89.#<BR>#
 See also Def 1562, p. 88.#<BR>#
 Note that [Hface Hvtx i] is the face hyperplane opposite vertex v_i. *)
Definition Hface : '('R^d -> Prop)^d.+1 := fun i => Ker (LagPd1 Hvtx i).

Lemma Hface_equiv : forall i x, Hface i x <-> LagPd1 Hvtx i x = 0.
Proof. intros; apply Ker_equiv. Qed.

End Face_hyperplane_Def.


Section Face_hyperplane_Facts.

Context {d : nat}.
Context {vtx : 'R^{d.+1,d}}.
Hypothesis Hvtx : aff_indep_ms vtx.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1564, p. 89. *)
Lemma Hface_ref_eq :
  forall (i : 'I_d.+1), Hface_ref i = Hface vtx_ref_aff_indep i.
Proof.
intros; fun_ext; unfold Hface_ref, Hface.
rewrite LagPd1_eq T_geom_inv_ref_id// compF_l_id; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1563, Eq (9.56), p. 88. *)
Lemma Hface_equiv_baryc :
  forall i x, Hface Hvtx i x <->
    (exists L, sum L = 1 /\ x = lin_comb L (skipF vtx i)).
Proof.
intros i x; split.
(* => *)
intros H; exists (skipF (LagPd1 Hvtx^~ x) i); split.
apply (plus_reg_l (LagPd1 Hvtx i x)).
rewrite -sum_skipF H plus_zero_l LagPd1_sum; easy.
rewrite {1}(LagPd1_baryc_alt Hvtx x) (lc_skipF i)
    H scal_zero_l plus_zero_l//.
(* <= *)
intros [L [HL1 HL2]]; pose (L1 := insertF L 0 i).
assert (HL1a : L1 i = 0) by now apply insertF_correct_l.
assert (HL1b : skipF L1 i = L) by now rewrite skipF_insertF.
assert (HL1c : sum L1 = 1) by now rewrite (sum_skip_zero i)// HL1b.
assert (HL1d : L1 = (LagPd1 Hvtx)^~ x).
  apply LagPd1_baryc; [| easy].
  rewrite (lc_skip_zero_l i)// HL1b -HL2; easy.
apply Ker_correct; rewrite -(fun_ext_rev HL1d i); easy.
Qed.

End Face_hyperplane_Facts.


Section T_geom_permutF_Def.

Context {d : nat}.
Variable vtx : 'R^{d.+1,d}.

(** [pi_d] is a permutation of [0..d].
 The intention is to switch vertices. *)
Variable pi_d : 'I_[d.+1].

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1586, p. 96.#<BR>#
 See also Def 1578, p. 92. *)
Definition T_geom_permutF : 'R^d -> 'R^d := T_geom (permutF pi_d vtx).

End T_geom_permutF_Def.


Section T_geom_permutF_Facts.

Context {d : nat}.
Context {vtx : 'R^{d.+1,d}}.
Hypothesis Hvtx: aff_indep_ms vtx.

Context {pi_d : 'I_[d.+1]}.
Hypothesis Hpi_d : injective pi_d.
Let pi_d_bij := injF_bij Hpi_d.
Let pi_d_inv := f_inv pi_d_bij.
Let pi_d_inv_bij := f_inv_bij pi_d_bij.
Let pi_d_inv_inj := f_inv_inj pi_d_bij.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1586, p. 96.#<BR>#
 See also Def 1578, p. 92. *)
Lemma T_geom_permutF_correct :
  forall x_ref,
    T_geom_permutF vtx pi_d x_ref =
      lin_comb (LagPd1_ref^~ x_ref) (permutF pi_d vtx).
Proof. apply T_geom_correct. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1586, p. 96. *)
Lemma T_geom_permutF_bij : bijective (T_geom_permutF vtx pi_d).
Proof.
apply T_geom_bij, aff_indep_permutF_equiv; [ apply injF_bij |]; easy.
Qed.

Definition T_geom_permutF_inv : 'R^d -> 'R^d := f_inv (T_geom_permutF_bij).

Lemma T_geom_permutF_inv_can_l :
  cancel (T_geom_permutF vtx pi_d) T_geom_permutF_inv.
Proof. apply f_inv_can_l. Qed.

Lemma T_geom_permutF_inv_can_r :
  cancel T_geom_permutF_inv (T_geom_permutF vtx pi_d).
Proof. apply f_inv_can_r. Qed.

Lemma T_geom_permutF_inv_id_l :
  T_geom_permutF_inv \o T_geom_permutF vtx pi_d = id.
Proof. apply f_inv_id_l. Qed.

Lemma T_geom_permutF_inv_id_r :
  T_geom_permutF vtx pi_d \o T_geom_permutF_inv = id.
Proof. apply f_inv_id_r. Qed.

Lemma T_geom_permutF_inv_correct :
  T_geom_permutF_inv = T_geom_inv (aff_indep_permutF pi_d_bij Hvtx).
Proof. apply f_inv_uniq_l, f_inv_can_l. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1586, p. 96. *)
Lemma T_geom_permutF_am : aff_map_ms (T_geom_permutF vtx pi_d).
Proof. apply T_geom_am. Qed.

Lemma T_geom_permutF_inv_am : aff_map_ms T_geom_permutF_inv.
Proof. apply: am_bij_compat T_geom_permutF_am. Qed.

Lemma T_geom_permutF_comp :
  forall k p, Pdk d k p -> Pdk d k (p \o T_geom_permutF vtx pi_d).
Proof. intros; apply Pdk_comp_am; [apply T_geom_permutF_am |]; easy. Qed.

Lemma T_geom_permutF_inv_comp :
  forall k p, Pdk d k p -> Pdk d k (p \o T_geom_permutF_inv).
Proof. intros; apply Pdk_comp_am; [apply T_geom_permutF_inv_am | easy]. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1586, Eq (9.75), pp. 96-97. *)
Lemma T_geom_permutF_eq :
  forall x_ref,
    T_geom_permutF vtx pi_d x_ref =
      lin_comb (permutF pi_d_inv (LagPd1_ref^~ x_ref)) vtx.
Proof.
intro; rewrite T_geom_permutF_correct; apply lc_permutF_r, f_inv_can_l.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1586, Eq (9.77), pp. 96-97. *)
Lemma LagPd1_eq_permutF :
  forall j,
    LagPd1 Hvtx j = permutF pi_d_inv LagPd1_ref j \o T_geom_permutF_inv.
Proof.
assert (H0 : forall x,
  sum (fun j => (permutF pi_d_inv LagPd1_ref j \o T_geom_permutF_inv) x) = 1).
  intros; rewrite -fct_sum_eq sum_permutF;
      [rewrite LagPd1_ref_sum_alt; easy | apply pi_d_inv_inj].
intros j; fun_ext x; move: j; apply extF_rev.
apply (baryc_coord_uniq Hvtx); [apply LagPd1_sum | easy |].
rewrite !baryc_ms_eq; [| easy | apply LagPd1_ref_sum].
rewrite -LagPd1_baryc_alt -{1}(T_geom_permutF_inv_can_r x)
    T_geom_permutF_eq; apply lc_eq_l; easy.
Qed.

Lemma LagPd1_ref_eq_permutF :
  forall j, LagPd1_ref j =
    permutF pi_d (LagPd1 Hvtx) j \o T_geom_permutF vtx pi_d.
Proof.
intro; rewrite permutF_correct LagPd1_eq_permutF.
rewrite -comp_assoc T_geom_permutF_inv_id_l comp_id_r.
rewrite -permutF_correct -permutF_f_inv_r; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1586, Eq (9.78), pp. 96-97. *)
Lemma T_geom_permutF_Hface :
  forall j, image (T_geom_permutF vtx pi_d) (Hface_ref j) = Hface Hvtx (pi_d j).
Proof.
intros; unfold Hface_ref; rewrite LagPd1_ref_eq_permutF Ker_comp_l; [easy |].
apply bij_surj, T_geom_permutF_bij.
Qed.

End T_geom_permutF_Facts.


Section T_geom_transpF_Def.

Context {d : nat}.
Variable vtx : 'R^{d.+1,d}.

Definition T_geom_transpF (i0 : 'I_d.+1) : 'R^d -> 'R^d :=
  T_geom_permutF vtx (transp_ord ord_max i0).

Lemma T_geom_transpF_correct :
  forall i0, T_geom_transpF i0 = T_geom (transpF ord_max i0 vtx).
Proof. easy. Qed.

End T_geom_transpF_Def.


Section T_geom_transpF_Facts.

Context {d : nat}.
Context {vtx : 'R^{d.+1,d}}.
Hypothesis Hvtx: aff_indep_ms vtx.

Definition T_geom_transpF_inv (i0 : 'I_d.+1) : 'R^d -> 'R^d :=
  T_geom_permutF_inv Hvtx (transp_ord_inj ord_max i0).

Lemma LagPd1_eq_transpF :
  forall i0 j,
    LagPd1 Hvtx j = transpF ord_max i0 LagPd1_ref j \o T_geom_transpF_inv i0.
Proof.
intros i0 j.
rewrite transpF_sym (LagPd1_eq_permutF Hvtx (transp_ord_inj ord_max i0)).
rewrite (transp_ord_inv ord_max i0); easy.
Qed.

Lemma T_geom_transpF_Hface :
  forall i0, image (T_geom_transpF vtx i0) (Hface_ref ord_max) = Hface Hvtx i0.
Proof.
intros i0.
assert (H0 : transp_ord ord_max i0 ord_max = i0)
    by now apply transp_ord_correct_l0.
rewrite -{2}H0; apply T_geom_permutF_Hface, transp_ord_inj.
Qed.

End T_geom_transpF_Facts.


Section Pdk_Facts.

Context {d k : nat}.
Hypothesis Hd : (0 < d)%coq_nat.
Context {vtx : 'R^{d.+1,d}}.
Hypothesis Hvtx : aff_indep_ms vtx.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1623, direct way,  p. 105#<BR>#
 (with the transposition instead of the circular permutation,
 see Rem 1622, p. 105. *)
Lemma factor_zero_on_Hface :
  forall {p} i, Pdk d k.+1 p ->
    (forall x, Hface Hvtx i x -> p x = 0) ->
    exists q, Pdk d k q /\ p = LagPd1 Hvtx i * q.
Proof.
intros p i Hp1 Hp2; pose (p_ref := p \o T_geom_transpF vtx i).
assert (H0 : p = p_ref \o T_geom_transpF_inv Hvtx i)
    by now unfold p_ref; rewrite -comp_assoc f_inv_id_r comp_id_r.
assert (H1 : Pdk d k.+1 p_ref) by now apply T_geom_permutF_comp.
assert (H2 : forall x_ref, Hface_ref ord_max x_ref -> p_ref x_ref = 0).
    intros x_ref Hx; unfold p_ref; apply Hp2.
    rewrite -(T_geom_transpF_Hface Hvtx i); easy.
apply (factor_zero_on_last_Hface_ref Hd H1) in H2;
    destruct H2 as [q_ref [Hq1 Hq2]].
exists (q_ref \o T_geom_transpF_inv Hvtx i); split.
apply T_geom_permutF_inv_comp; easy.
rewrite (LagPd1_eq_transpF _ i) transpF_correct_l1//
    -f_mult_compat_comp_l -Hq2; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1623, reverse way, p. 105. *)
Lemma factor_zero_on_Hface_rev :
  forall {p} i, Pdk d k.+1 p ->
    (exists q, Pdk d k q /\ p = LagPd1 Hvtx i * q) ->
    forall x, Hface Hvtx i x -> p x = 0.
Proof.
intros p i Hp [q [Hq ->]] x Hx; rewrite fct_mult_eq Hx mult_zero_l; easy.
Qed.

Lemma factor_zero_on_Hface_equiv :
  forall {p} i, Pdk d k.+1 p ->
    (forall x, Hface Hvtx i x -> p x = 0) <->
    (exists q, Pdk d k q /\ p = LagPd1 Hvtx i * q).
Proof.
intros; split; [apply factor_zero_on_Hface |
    apply factor_zero_on_Hface_rev]; easy.
Qed.

End Pdk_Facts.


Section T_geom_Hface_Facts.

(** We need a dimension which is structurally nonzero.
 We take d = d1.+1 with d1 : nat. *)
Context {d1 : nat}.
Variable vtx : 'R^{d1.+2,d1.+1}.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1578, p. 92#<BR>#
 (with l=d-1=d1 and pi_l = theta^d_i (i>0) from Lem 1368, p. 41,#<BR>#
 and with theta^d_i j = j if j < i, and j+1 otherwise). *)
Definition T_geom_Hface : '('R^d1 -> 'R^d1.+1)^d1.+2 :=
  fun i x_ref => lin_comb (LagPd1_ref^~ x_ref) (skipF vtx i).

Lemma T_geom_Hface_0_eq_alt :
  forall x_ref,
    T_geom_Hface ord0 x_ref = scal (1 - sum x_ref) (vtx ord1) +
      lin_comb x_ref (liftF_S (liftF_S vtx)).
Proof.
intros; unfold T_geom_Hface.
rewrite lc_ind_l LagPd1_ref_0// skipF_first liftF_S_0 LagPd1_ref_S_eq; easy.
Qed.

Lemma T_geom_Hface_S_eq_alt :
  forall i x_ref, i <> ord0 ->
    T_geom_Hface i x_ref = scal (1 - sum x_ref) (vtx ord0) +
      lin_comb x_ref (liftF_S (skipF vtx i)).
Proof.
intros; unfold T_geom_Hface.
assert (Hi' : (@ord0 d1 < i)%coq_nat) by now apply ord_n0_gt_equiv.
rewrite lc_ind_l LagPd1_ref_0// skipF_correct_l//
    widenF_S_0 LagPd1_ref_S_eq; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1584, Eq (9.72), pp. 94-95.#<BR>#
 See also Lem 1581, Eq (9.67) first part, p. 93#<BR>#
 (with l=d-1=d1 and pi_l = theta^d_0 from Lem 1368, p. 41). *)
Lemma T_geom_Hface_0_eq :
  forall x_ref,
    T_geom_Hface ord0 x_ref = vtx ord1 +
      lin_comb x_ref (liftF_S (liftF_S vtx) - constF d1 (vtx ord1)).
Proof.
intros; rewrite T_geom_Hface_0_eq_alt.
rewrite scal_minus_l scal_one -plus_assoc -minus_sym.
rewrite lc_minus_r lc_constF_r; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1584, Eq (9.73), pp. 94-95.#<BR>#
 See also Lem 1581, Eq (9.67) first part, p. 93#<BR>#
 (with l=d-1=d1 and pi_l = theta^d_i (i>0) from Lem 1368, p. 41). *)
Lemma T_geom_Hface_S_eq :
  forall {i} x_ref, i <> ord0 ->
    T_geom_Hface i x_ref = vtx ord0 +
      lin_comb x_ref (liftF_S (skipF vtx i) - constF d1 (vtx ord0)).
Proof.
intros; rewrite T_geom_Hface_S_eq_alt//.
rewrite scal_minus_l scal_one -plus_assoc -minus_sym.
rewrite lc_minus_r lc_constF_r; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1584, pp. 94-95. *)
Lemma T_geom_Hface_am : forall i, aff_map_ms (T_geom_Hface i).
Proof.
intros i; destruct (ord_eq_dec i ord0) as [-> | Hi]; eapply am_ext; intro.
rewrite T_geom_Hface_0_eq plus_comm//.
2: rewrite (T_geom_Hface_S_eq _ Hi) plus_comm//.
1,2: apply: am_lm_ms; apply lc_lm_l.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1584, Eq (9.73), pp. 94-95.#<BR>#
 See also Lem 1581, Eq (9.67) with B_{pi_l}, p. 93#<BR>#
 (with l=d-1=d1 and pi_l = theta^d_i (i>0) from Lem 1368, p. 41). *)
Lemma T_geom_Hface_0_fct_lm :
  forall x_ref,
    fct_lm_ms (T_geom_Hface ord0) 0 x_ref =
      lin_comb x_ref (liftF_S (liftF_S vtx) - constF d1 (vtx ord1)).
Proof.
intros x_ref; unfold fct_lm_ms; rewrite fct_lm_ms_eq.
rewrite plus_zero_l !T_geom_Hface_0_eq lc_zero_l plus_zero_r.
apply: minus_plus_l.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1584, Eq (9.73), pp. 94-95.#<BR>#
 See also Lem 1581, Eq (9.67) with B_{pi_l}, p. 93#<BR>#
 (with l=d-1=d1 and pi_l = theta^d_i (i>0) from Lem 1368, p. 41). *)
Lemma  T_geom_Hface_S_fct_lm :
  forall i x_ref, i <> ord0 ->
    fct_lm_ms (T_geom_Hface i) 0 x_ref =
      lin_comb x_ref (liftF_S (skipF vtx i) - constF d1 (vtx ord0)).
Proof.
intros i x_ref Hi; unfold fct_lm_ms; rewrite fct_lm_ms_eq.
rewrite plus_zero_l !T_geom_Hface_S_eq// lc_zero_l plus_zero_r.
apply: minus_plus_l.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1585, p. 95. *)
Lemma T_geom_Hface_comp :
  forall i k p, Pdk d1.+1 k p -> Pdk d1 k (p \o T_geom_Hface i).
Proof. intros; apply Pdk_comp_am; [apply T_geom_Hface_am | easy]. Qed.

End T_geom_Hface_Facts.


Section T_geom_Hface_invS_Facts.

(** We need a dimension which is structurally nonzero.
 We take d = d1.+1 with d1 : nat. *)
Context {d1 : nat}.
Context {vtx : 'R^{d1.+2,d1.+1}}.
Variable Hvtx : aff_indep_ms vtx.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1584, pp. 94-95.#<BR>#
 See also Lem 1581, Eq (9.69), p. 93#<BR>#
 (with pi_l = theta^d_i (i>0) from Lem 1368, p. 41). *)
Lemma T_geom_Hface_in_Hface :
  forall i, funS fullset (Hface Hvtx i) (T_geom_Hface vtx i).
Proof.
intros i H1 [x H2]; rewrite Hface_equiv_baryc.
exists (LagPd1_ref^~ x); split; [apply LagPd1_ref_sum | easy].
Qed.

Lemma T_geom_Hface_injS : forall i, injS fullset (T_geom_Hface vtx i).
Proof.
intros i; rewrite (injS_aff_lin_equiv_alt invertible_2 cas_fullset
    (T_geom_Hface_am vtx i) 0); [| easy].
intros x [_ Hx]; apply (aff_indep_skipF i Hvtx); rewrite frameF_ms_eq.
destruct (ord_eq_dec i ord0) as [-> | Hi].
(* i = ord0 *)
rewrite !skipF_first liftF_S_0 -T_geom_Hface_0_fct_lm; easy.
(* i <> ord0 *)
rewrite skipF_first skipF_correct_l; [| now apply ord_n0_gt_equiv].
rewrite widenF_S_0 -T_geom_Hface_S_fct_lm; easy.
Qed.

Lemma T_geom_Hface_surjS :
  forall i, surjS fullset (Hface Hvtx i) (T_geom_Hface vtx i).
Proof.
intros i y Hy; rewrite (Hface_equiv_baryc Hvtx i y) in Hy.
destruct Hy as [L [H1 H2]]; exists (liftF_S L); split; [easy |].
rewrite H2; unfold T_geom_Hface; apply lc_eq_l; rewrite LagPd1_ref_liftF_S//.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1584, pp. 94-95. *)
Lemma T_geom_Hface_bijS :
  forall i, bijS fullset (Hface Hvtx i) (T_geom_Hface vtx i).
Proof.
intros; apply (injS_surjS_bijS inhabited_fct_ms).
apply T_geom_Hface_in_Hface.
apply T_geom_Hface_injS.
apply T_geom_Hface_surjS.
Qed.

Definition T_geom_Hface_invS (i:'I_d1.+2) : 'R^d1.+1 -> 'R^d1 :=
  f_invS (T_geom_Hface_bijS i).

End T_geom_Hface_invS_Facts.
