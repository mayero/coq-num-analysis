(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Faissole, Martin, Mayero

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Algebra Require Import Algebra_wDep.

Local Open Scope R_scope. (* For order notations. *)
Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.


Section INR_Facts.

Lemma INR_eq_equiv : forall m n, INR m = INR n <-> m = n.
Proof. intros; split; [apply INR_eq | intros; subst; easy]. Qed.

Lemma INR_0_equiv : forall n, INR n = 0 <-> n = 0.
Proof. intros; rewrite -INR_eq_equiv INR_0; easy. Qed.

Lemma INR_n0 : forall {n}, (0 < n)%coq_nat -> INR n <> 0.
Proof. move=>> /Nat.lt_neq H; apply not_0_INR; easy. Qed.

Lemma INR_pred : forall {n}, (0 < n)%coq_nat -> INR n.-1 = INR n - 1.
Proof. intros; rewrite -Nat.sub_1_r minus_INR; auto with arith. Qed.

Lemma INR_invertible : forall {n}, (0 < n)%coq_nat -> invertible (INR n).
Proof. intros; apply invertible_equiv_R, INR_n0; easy. Qed.

Lemma INR_inv_eq :
  forall {n}, (0 < n)%coq_nat -> / INR n = 1 - INR n.-1 / INR n.
Proof.
intros n H; rewrite INR_pred// -(div_diag (INR_invertible H)).
rewrite -div_minus minus2_r (div_diag (INR_invertible H)) div_one_l//.
Qed.

Lemma INR_inv_pred :
  forall {n}, (0 < n)%coq_nat -> / INR n - 1 = - INR n.-1 / INR n.
Proof. intros; rewrite INR_inv_eq// minus2_l div_opp_l; easy. Qed.

Lemma INR_inv_decomp :
  forall {n}, (0 < n)%coq_nat -> / INR n + INR n.-1 / INR n = 1.
Proof. intros; rewrite INR_inv_eq// plus_minus_l; easy. Qed.

Lemma INR_inv_n0 : forall {n}, n <> 0%N -> / INR n <> 0.
Proof. intros; rewrite inv_eq_R; apply Rinv_neq_0_compat, not_0_INR; easy. Qed.

End INR_Facts.
