(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Bibliography

 #<DIV><A NAME="Ern"></A></DIV>#
 [[Ern]]
 Alexandre Ern, Éléments finis, Aide-mémoire, Dunod/L'Usine Nouvelle, 2005,
 #<A HREF="https://www.dunod.com/sciences-techniques/aide-memoire-elements-finis">#
 https://www.dunod.com/sciences-techniques/aide-memoire-elements-finis#</A>#.

 #<DIV><A NAME="RR9557v1"></A></DIV>#
 [[RR9557v1]]
 François Clément and Vincent Martin,
 Finite element method. Detailed proofs to be formalized in Coq,
 RR-9557, first version, Inria Paris, 2024,
 #<A HREF="https://inria.hal.science/hal-04713897v1">#
 https://inria.hal.science/hal-04713897v1#</A>#.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Algebra Require Import Algebra_wDep.
From FEM Require Import geometry multi_index poly_LagPd1_ref.

Local Open Scope R_scope. (* For order notations. *)
Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.


Section T_geom_Facts.

Context {d : nat}.
Variable vtx : 'R^{d.+1,d}.

(**
 #<A HREF="##Ern">#[[Ern]]#</A>#
 Eq (3.23), p. 58.#<BR>#
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1547, p. 82. *)
Definition T_geom : 'R^d -> 'R^d :=
  fun x_ref => barycenter_ms (LagPd1_ref^~ x_ref) vtx.

Lemma T_geom_correct :
  forall x_ref, T_geom x_ref = lin_comb (LagPd1_ref^~ x_ref) vtx.
Proof. intro; rewrite -baryc_ms_eq//; apply LagPd1_ref_sum. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1550, p. 83. *)
Lemma T_geom_am : aff_map_ms T_geom.
Proof.
apply am_lm_0, lm_ext
    with (fun x => lin_comb (LagPd1_ref^~ x - LagPd1_ref^~ 0) vtx).
intros; rewrite lc_minus_l fct_minus_eq !T_geom_correct; easy.
apply fct_lc_l_lm; intro; apply: am_lm_0_rev; apply LagPd1_ref_am.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1550, Eq (9.39), p. 83. *)
Lemma T_geom_transports_vtx : forall i, T_geom (vtx_ref i) = vtx i.
Proof.
intros i; rewrite T_geom_correct (lc_scalF_compat (kronR i) vtx);
    [apply lc_kron_l_in_r | rewrite LagPd1_ref_kron_vtx; easy].
Qed.

Lemma T_geom_transports_baryc :
  forall L, sum L = 1 -> T_geom (lin_comb L vtx_ref) = lin_comb L vtx.
Proof.
intros L HL; rewrite (am_ac_compat T_geom_am HL);
    apply lc_ext_r, T_geom_transports_vtx.
Qed.

End T_geom_Facts.


Section T_geom_inv_Facts.

Context {d : nat}.
Context {vtx : 'R^{d.+1,d}}.
Hypothesis Hvtx : aff_indep_ms vtx.

Lemma T_geom_inj : injective (T_geom vtx).
Proof.
intros x_ref y_ref H; apply LagPd1_ref_inj; apply fun_ext_rev; move: H.
move: (@LagPd1_ref_sum d) => H0; rewrite T_geom_correct -!baryc_ms_eq//.
move=> H1; apply (baryc_coord_uniq Hvtx) in H1; easy.
Qed.

Lemma T_geom_surj : surjective (T_geom vtx).
Proof.
intros x; destruct (aff_indep_aff_gen_R _ has_dim_Rn Hvtx x) as [L HL],
    (LagPd1_ref_surj L) as [x_ref Hx_ref]; [easy |].
exists x_ref; rewrite T_geom_correct Hx_ref; easy.
Qed.

Lemma T_geom_bij : bijective (T_geom vtx).
Proof. apply bij_equiv; split; [apply T_geom_inj | apply T_geom_surj]. Qed.

Definition T_geom_inv : 'R^d -> 'R^d := f_inv T_geom_bij.

Lemma T_geom_inv_can_r : cancel T_geom_inv (T_geom vtx).
Proof. apply f_inv_can_r. Qed.

Lemma T_geom_inv_can_l : cancel (T_geom vtx) T_geom_inv.
Proof. apply f_inv_can_l. Qed.

Lemma T_geom_inv_am : aff_map_ms T_geom_inv.
Proof. apply: am_bij_compat; apply T_geom_am. Qed.

Lemma T_geom_inv_transports_vtx : forall i, T_geom_inv (vtx i) = vtx_ref i.
Proof.
intro; rewrite -(T_geom_transports_vtx vtx); apply T_geom_inv_can_l.
Qed.

Lemma T_geom_inv_transports_baryc :
  forall L, sum L = 1 -> T_geom_inv (lin_comb L vtx) = lin_comb L vtx_ref.
Proof.
intros L HL; rewrite (am_ac_compat T_geom_inv_am HL);
    apply lc_ext_r, T_geom_inv_transports_vtx.
Qed.

Let K_geom_ref : 'R^d -> Prop := convex_envelop vtx_ref.
Let K_geom : 'R^d -> Prop := convex_envelop vtx.

Lemma T_geom_K_geom : incl K_geom_ref (preimage (T_geom vtx) K_geom).
Proof.
move=>> [L HL1 HL2]; unfold K_geom, preimage;
    rewrite baryc_ms_eq// T_geom_transports_baryc// -baryc_ms_eq; easy.
Qed.

Lemma T_geom_inv_K_geom : incl K_geom (preimage T_geom_inv K_geom_ref).
Proof.
move=>> [L HL1 HL2]; unfold K_geom, preimage;
    rewrite baryc_ms_eq// T_geom_inv_transports_baryc// -baryc_ms_eq; easy.
Qed.

Lemma T_geom_surjS : surjS K_geom_ref K_geom (T_geom vtx).
Proof.
intros x Hx; exists (T_geom_inv x); split;
    [apply T_geom_inv_K_geom; easy | apply T_geom_inv_can_r].
Qed.

Lemma T_geom_inv_ge_0 : forall i, incl K_geom (fun x => 0 <= T_geom_inv x i).
Proof.
move=> i x /T_geom_inv_K_geom [L HL1 HL2]; rewrite baryc_ms_eq//;
    rewrite fct_lc_r_eq; apply (lc_ge_0 _ _ HL1); intro; apply vtx_ref_ge_0.
Qed.

Lemma T_geom_image : image (T_geom vtx) K_geom_ref = K_geom.
Proof.
apply subset_ext_equiv; split; intros x Hx.
destruct Hx as [x_ref Hx_ref]; apply T_geom_K_geom; easy.
rewrite image_ex; exists (T_geom_inv x); split;
    [apply T_geom_inv_K_geom; easy | apply T_geom_inv_can_r].
Qed.

Lemma T_geom_inv_image : image T_geom_inv (K_geom) = K_geom_ref.
Proof.
apply subset_ext_equiv; split; intros x_ref Hx_ref.
destruct Hx_ref as [x Hx]; apply T_geom_inv_K_geom; easy.
rewrite image_ex; exists (T_geom vtx x_ref); split;
    [apply T_geom_K_geom; easy | apply T_geom_inv_can_l].
Qed.

Lemma T_geom_bijS_spec :
  bijS_spec (convex_envelop vtx) K_geom_ref T_geom_inv (T_geom vtx).
Proof.
repeat split.
intros _ [x H]; apply T_geom_inv_K_geom; easy.
intros _ [x_ref H]; apply T_geom_K_geom; easy.
intros x _; apply T_geom_inv_can_r.
intros x_ref _; apply T_geom_inv_can_l.
Qed.

Lemma T_geom_inv_bijS_spec :
  bijS_spec K_geom_ref (convex_envelop vtx) (T_geom vtx) T_geom_inv.
Proof.
repeat split.
intros _ [x_ref H]; apply T_geom_K_geom; easy.
intros _ [x H]; apply T_geom_inv_K_geom; easy.
intros x_ref _; apply T_geom_inv_can_l.
intros x _; apply T_geom_inv_can_r.
Qed.

Lemma T_geom_inv_bijS : bijS (convex_envelop vtx) K_geom_ref T_geom_inv.
Proof. apply (BijS _ T_geom_bijS_spec). Qed.

End T_geom_inv_Facts.


Section T_geom_ref_Facts.

Context {d : nat}.
Variable vtx : 'R^{d.+1,d}.
Hypothesis Hvtx : vtx = @vtx_ref d.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1549, p. 82. *)
Lemma T_geom_ref_id : T_geom vtx = (id : 'R^d -> 'R^d).
Proof.
subst; fun_ext x; destruct (aff_indep_aff_gen_R _
    has_dim_Rn vtx_ref_aff_indep x) as [L [HL ->]].
apply T_geom_transports_baryc; easy.
Qed.

Lemma T_geom_inv_ref_id : forall (H : aff_indep_ms vtx), T_geom_inv H = id.
Proof. intro; apply f_inv_is_id, T_geom_ref_id. Qed.

End T_geom_ref_Facts.


Section T_geom_1k_Facts.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1548, p. 82.#<BR>#
 See also Def 1461, p. 60. *)
Lemma T_geom_1k :
  forall (vtx : 'R^{2,1}) x_ref,
    T_geom vtx x_ref = (scal (x_ref ord0) (vtx ord_max - vtx ord0) + vtx ord0).
Proof.
intros vtx x_ref; rewrite T_geom_correct lc_ind_l.
rewrite LagPd1_ref_0// LagPd1_ref_S_eq sum_1 lc_1 liftF_S_0 ord2_1_max.
rewrite scal_minus_l scal_one scal_minus_r.
unfold minus; rewrite -plus_assoc plus_comm; f_equal; apply plus_comm.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1462, Eq (8.14), p. 60. *)
Lemma T_geom_inv_1k :
  forall {vtx : 'R^{2,1}} (Hvtx : aff_indep_ms vtx) x,
    T_geom_inv Hvtx x =
      scal (/ (vtx ord_max ord0 - vtx ord0 ord0)) (x - vtx ord0).
Proof.
intros vtx Hvtx x; apply (T_geom_inj Hvtx); rewrite T_geom_inv_can_r T_geom_1k.
extF; rewrite ord_one.
rewrite fct_plus_eq !fct_scal_r_eq !fct_minus_eq.
rewrite scal_comm_R scal_assoc mult_inv_r.
rewrite scal_one_l// plus_minus_l; easy.
apply invertible_equiv_R.
move: Hvtx => /aff_indep_2_equiv /minus_zero_sym_equiv H; contradict H.
extF; rewrite ord_one fct_minus_eq; easy.
Qed.

End T_geom_1k_Facts.
