(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Bibliography

 #<DIV><A NAME="RR9557v1"></A></DIV>#
 [[RR9557v1]]
 François Clément and Vincent Martin,
 Finite element method. Detailed proofs to be formalized in Coq,
 RR-9557, first version, Inria Paris, 2024,
 #<A HREF="https://inria.hal.science/hal-04713897v1">#
 https://inria.hal.science/hal-04713897v1#</A>#.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Algebra Require Import Algebra_wDep.
From FEM Require Import R_compl geometry multi_index poly_Pdk poly_LagP1k.

Local Open Scope R_scope. (* For order notations. *)
Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.


Section Unit_Vertices.

Context {d : nat}.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1434, p. 55.#<BR>#
 See also Def 1365, p. 40. *)
Definition vtx_ref : 'R^{d.+1,d} :=
  fun i => match ord_eq_dec i ord0 with
    | left _ => 0
    | right _ => kronR (i - 1)
    end.

(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!
 TODO FC (27/02/2025): this could be the definition... *)
Lemma vtx_ref_eq : vtx_ref = castF (Nat.add_1_l _) (concatF 0 II).
Proof.
extF i j; unfold vtx_ref; destruct (ord_eq_dec i ord0) as [-> | Hi]; [easy |].
unfold castF; rewrite mx_one_eq concatF_correct_r//.
contradict Hi; apply ord_inj, lt_1; easy.
Qed.

Lemma vtx_ref_0 : forall i, i = ord0 -> vtx_ref i = 0.
Proof. intros; unfold vtx_ref; case (ord_eq_dec _ _); easy. Qed.

Lemma vtx_ref_S_is_0 :
  forall i (j : 'I_d), i <> ord0 -> (i - 1)%coq_nat <> j -> vtx_ref i j = 0.
Proof.
intros i j Hi Hj; unfold vtx_ref;
    destruct (ord_eq_dec _ _); [| apply kron_is_0]; easy.
Qed.

Lemma vtx_ref_S_is_1 :
  forall i (j : 'I_d), i <> ord0 -> (i - 1)%coq_nat = j -> vtx_ref i j = 1.
Proof.
intros i j Hi Hj; unfold vtx_ref;
    destruct (ord_eq_dec _ _); [| apply kron_is_1]; easy.
Qed.

Lemma vtx_ref_ge_0 : forall i j, 0 <= vtx_ref i j.
Proof.
intros i j; destruct (ord_eq_dec i ord0) as [Hi | Hi].
rewrite vtx_ref_0; easy.
destruct (Nat.eq_dec (i - 1)%coq_nat j) as [Hj | Hj].
rewrite vtx_ref_S_is_1//; apply Rle_0_1.
rewrite vtx_ref_S_is_0; easy.
Qed.

Lemma vtx_ref_kron :
  forall {i} (Hi : i <> ord0), vtx_ref i = kronR (lower_S Hi).
Proof.
intros i Hi; extF j; destruct (Nat.eq_dec (i - 1)%coq_nat j) as [Hj | Hj].
rewrite vtx_ref_S_is_1// kronR_is_1//.
rewrite kronR_is_0// vtx_ref_S_is_0//; easy.
Qed.

Lemma vtx_ref_liftF_S : liftF_S vtx_ref = kronR.
Proof.
fun_ext2; unfold liftF_S;
    rewrite (vtx_ref_kron (lift_S_not_first _)) lower_lift_S; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1436, p. 55. *)
Lemma vtx_ref_aff_indep : aff_indep_ms vtx_ref.
Proof.
intros L HL.
rewrite (lc_kron_r_decomp L) -HL; apply lc_ext_r; intros; extF.
rewrite -(minus_zero_r (kron _ _ _)) frameF_ms_eq !fct_minus_eq skipF_first.
rewrite vtx_ref_liftF_S constF_correct vtx_ref_0; easy.
Qed.

End Unit_Vertices.


Section Unit_Vertices_1.

Lemma vtx_ref_1_aff_indep_alt :
  let H := eq_sym (pbinomS_1_r 1) in
  invertible (castF H vtx_ref ord_max ord0 - castF H vtx_ref ord0 ord0).
Proof.
intros; rewrite castF_id; clear; apply invertible_equiv_R.
rewrite minus_zero_equiv (vtx_ref_0 ord0)// vtx_ref_S_is_1//.
apply one_not_zero_R.
Qed.

End Unit_Vertices_1.


Section LagPd1_ref_Facts.

Context {d : nat}.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1541, p. 81.#<BR>#
 For simplices (P1 in dim d), with x = (x_i)_{i=1..d}:
 - LagPd1_ref 0 x = 1 - \sum_{i=1}^d x_i,
 - LagPd1_ref i x = x_(i-1), for i=1..d. *)
Definition LagPd1_ref : '(FRd d)^d.+1 :=
  fun i x => match ord_eq_dec i ord0 with
    | left _ => 1 - sum x
    | right Hi => x (lower_S Hi)
    end.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1564, p. 89.#<BR>#
 See also Def 1562, p. 88. *)
Definition Hface_ref : '('R^d -> Prop)^d.+1 := fun i => Ker (LagPd1_ref i).

Lemma Hface_ref_equiv : forall i x, Hface_ref i x <-> LagPd1_ref i x = 0.
Proof. intros; apply Ker_equiv. Qed.

Lemma LagPd1_ref_0 :
  forall {i}, i = ord0 -> LagPd1_ref i = fun x => 1 - sum x.
Proof. intros; unfold LagPd1_ref; case (ord_eq_dec _ _); easy. Qed.

Lemma LagPd1_ref_S :
  forall {i} (Hi : i <> ord0), LagPd1_ref i = fun x => x (lower_S Hi).
Proof.
intros; unfold LagPd1_ref; case (ord_eq_dec _ _);
    [| intros; fun_ext; f_equal; apply ord_inj]; easy.
Qed.

Lemma LagPd1_ref_S_eq : forall x, liftF_S (LagPd1_ref^~ x) = x.
Proof.
intros; extF; unfold liftF_S.
rewrite (LagPd1_ref_S (lift_S_not_first _)) lower_lift_S; easy.
Qed.

Lemma LagPd1_ref_S_eq_alt : liftF_S LagPd1_ref = fun i : 'I_d => @^~ i.
Proof. fun_ext i x; move: i; apply extF_rev, LagPd1_ref_S_eq. Qed.

Lemma LagPd1_ref_liftF_S :
  forall (x : 'R^d.+1), sum x = 1 -> LagPd1_ref^~ (liftF_S x) = x.
Proof.
intros x Hx; extF i; destruct (ord_eq_dec i ord0) as [-> | Hi].
rewrite LagPd1_ref_0// -Hx sum_ind_l minus_plus_r; easy.
rewrite LagPd1_ref_S liftF_lower_S; easy.
Qed.

(* Could be: injective (gather LagPd1_ref). *)
Lemma LagPd1_ref_inj :
  forall (x y : 'R^d), (forall i, LagPd1_ref i x = LagPd1_ref i y) -> x = y.
Proof.
intros x y H; extF; rewrite -(LagPd1_ref_S_eq x) -(LagPd1_ref_S_eq y); apply H.
Qed.

(* Could be: surjS fullset (fun L => sum L = 1) (fun x => LagPd1_ref^~ x). *)
Lemma LagPd1_ref_surj :
  forall (L : 'R^d.+1), sum L = 1 -> exists x, LagPd1_ref^~ x = L.
Proof. intros L HL; exists (liftF_S L); apply LagPd1_ref_liftF_S; easy. Qed.

Lemma LagPd1_ref_S_sum : forall x, sum (liftF_S (LagPd1_ref^~ x)) = sum x.
Proof. intros; rewrite LagPd1_ref_S_eq; easy. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1543, Eq (9.33), p. 81. *)
Lemma LagPd1_ref_sum : forall x, sum (LagPd1_ref^~ x) = 1.
Proof.
intros; rewrite sum_ind_l LagPd1_ref_0// LagPd1_ref_S_sum plus_minus_l; easy.
Qed.

Lemma LagPd1_ref_sum_alt : sum LagPd1_ref = 1.
Proof. fun_ext; rewrite fct_sum_eq; apply LagPd1_ref_sum. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1543, Eq (9.32), p. 81. *)
Lemma LagPd1_ref_kron_vtx : forall i, LagPd1_ref^~ (vtx_ref i) = kronR i.
Proof.
intros i; extF j.
destruct (ord_eq_dec j ord0) as [-> | Hj];
    [rewrite LagPd1_ref_0// | rewrite LagPd1_ref_S//];
    (destruct (ord_eq_dec i ord0) as [-> | Hi];
      [rewrite vtx_ref_0// | rewrite vtx_ref_kron//]).
(* i = j = ord0. *)
rewrite sum_zero_compat// minus_zero_r kronR_is_1; easy.
(* i <> ord0, j = ord0. *)
rewrite sum_kron_r// minus_eq_zero kronR_is_0;
    [| contradict Hi; apply ord_inj]; easy.
(* i = ord0, j <> ord0. *)
rewrite kronR_is_0; [| contradict Hj; apply ord_inj]; easy.
(* i, j <> ord0. *)
simpl; apply kron_pred_eq;
    [contradict Hi | contradict Hj]; apply ord_inj; easy.
Qed.

Lemma LagPd1_ref_am : forall i, aff_map_ms (LagPd1_ref i).
Proof.
intros i; apply am_lm_0; destruct (ord_eq_dec i ord0) as [-> | Hi].
(* i = ord0. *)
apply (lm_ext (- fun x => sum x)); [| apply lm_fct_opp, lm_component_sum].
intros; rewrite fct_minus_eq LagPd1_ref_0// sum_zero minus_zero_r minus2_l//.
(* i <> ord0. *)
apply (lm_ext (fun x => x (lower_S Hi))); [| apply lm_component].
intros; rewrite LagPd1_ref_S fct_zero_eq minus_zero_r; easy.
Qed.

Lemma LagPd1_ref_am_alt :
  forall L i, sum L = 1 -> LagPd1_ref i (lin_comb L vtx_ref) = L i.
Proof.
intros L i HL; rewrite am_ac_compat//; [| apply LagPd1_ref_am].
rewrite (lc_ext_r (kronR^~ i)); [apply lc_kron_r_in_l |].
intros; move: i; apply extF_rev, LagPd1_ref_kron_vtx.
Qed.

Lemma LagPd1_ref_ge_0 :
  forall i (x : 'R^d), convex_envelop vtx_ref x -> 0 <= LagPd1_ref i x.
Proof. intros i x [L HL1 HL2]; rewrite baryc_ms_eq// LagPd1_ref_am_alt//. Qed.

Lemma LagPd1_ref_le_1 :
  forall i (x : 'R^d), convex_envelop vtx_ref x -> LagPd1_ref i x <= 1.
Proof.
replace (1 : R) with (1 : R_Ring) by easy.
intros i x [L HL1 HL2]; rewrite baryc_ms_eq// LagPd1_ref_am_alt// -HL2.
apply sum_nonneg_ub_R; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1560 (with reference vertices), p. 87.#<BR>#
 [LagPd1_ref] are the barycentric coordinates wrt [vtx_ref]. *)
Lemma LagPd1_ref_baryc : forall x, barycenter_ms (LagPd1_ref^~ x) vtx_ref = x.
Proof.
intro; extF; rewrite baryc_ms_correct baryc_ms_eq; [| apply LagPd1_ref_sum].
rewrite lc_ind_l vtx_ref_0// scal_zero_r plus_zero_l LagPd1_ref_S_eq.
rewrite vtx_ref_liftF_S fct_lc_r_eq lc_kron_r_in_l; easy.
Qed.

Lemma LagPd1_ref_S_Monom :
  liftF_S LagPd1_ref = liftF_S (castF (pbinomS_1_r d) (Monom_dk d 1)).
Proof.
extF i; unfold liftF_S, castF; move: (lift_S_not_first i) => Hi0.
assert (Hi1 : cast_ord (eq_sym (pbinomS_1_r d)) (lift_S i) <> ord0)
    by now apply cast_ordS_n0.
rewrite LagPd1_ref_S Monom_d1_S.
fun_ext; f_equal; apply ord_inj; easy.
Qed.

Lemma LagPd1_ref_S_Monom_rev :
  liftF_S (Monom_dk d 1) = liftF_S (castF (eq_sym (pbinomS_1_r d)) LagPd1_ref).
Proof.
rewrite liftF_S_castF LagPd1_ref_S_Monom liftF_S_castF castF_comp castF_refl//.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1543, p. 81. *)
Lemma LagPd1_ref_is_Pd1 : inclF LagPd1_ref (Pdk d 1).
Proof.
intros i; destruct (ord_eq_dec i ord0) as [-> | Hi]; apply lin_span_ex.
(* i = ord0. *)
exists (replaceF (- ones) 1 ord0); fun_ext x.
rewrite LagPd1_ref_0// fct_lc_r_eq (lc_skipF ord0).
rewrite replaceF_correct_l// scal_one_l// Monom_dk_0 -LagPd1_ref_S_sum.
rewrite skipF_replaceF skipF_opp lc_opp_l skipF_constF lc_constF_l scal_one.
rewrite skipF_first !fct_liftF_S_eq LagPd1_ref_S_Monom liftF_S_castF.
rewrite -!fct_sum_eq sum_castF; easy.
(* i <> ord0. *)
pose (id1 := cast_ord (eq_sym (pbinom_1_r d)) (lower_S Hi)).
exists (insertF (kronR id1) 0 ord0).
rewrite (lc_skipF ord0) insertF_correct_l// scal_zero_l plus_zero_l.
rewrite skipF_insertF !skipF_first LagPd1_ref_S_Monom_rev lc_kron_l_in_r.
unfold id1; rewrite liftF_S_castF castF_cast_ord liftF_lower_S; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1543, p. 81. *)
Lemma LagPd1_ref_lin_indep : lin_indep LagPd1_ref.
Proof.
intros L HL; extF; rewrite -lc_kron_r_in_r.
rewrite -LagPd1_ref_kron_vtx -fct_lc_r_eq HL; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1543, p. 81. *)
Lemma LagPd1_ref_basis : basis (Pdk d 1) LagPd1_ref.
Proof.
apply lin_indep_basis.
rewrite -(pbinomS_1_r d); apply Pdk_has_dim.
apply LagPd1_ref_is_Pd1.
apply LagPd1_ref_lin_indep.
Qed.

Lemma Pd1_has_dim : has_dim (Pdk d 1) d.+1.
Proof. apply (Dim _ _ _ LagPd1_ref_basis). Qed.

End LagPd1_ref_Facts.


Section Pdk_Facts.

(** Factorization result. *)

Context {d k : nat}.
Hypothesis Hd : (0 < d)%coq_nat.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1621, direct way, pp. 104-105. *)
Lemma factor_zero_on_last_Hface_ref :
  forall {p}, Pdk d k.+1 p ->
    (forall x, Hface_ref ord_max x -> p x = 0) ->
    exists q, Pdk d k q /\ p = LagPd1_ref ord_max * q.
Proof.
assert (H0 : forall n, (ord_max : 'I_n.+1) = lower_S ord_max_not_0)
    by (intro; apply eq_sym, lower_S_max).
intros p Hp1; destruct d as [| d1]; [easy |].
intros Hp2; destruct (PdSk_split k p Hp1) as [p0 [q [Hp0 [Hq Hp3]]]].
exists q; split; [easy |]; fun_ext x.
rewrite Hp3 fct_mult_eq (LagPd1_ref_S ord_max_not_0).
rewrite -(plus_zero_l (x (lower_S _) * _)); repeat f_equal; [| easy].
pose (y := widenF_S x); fold y; rewrite -(Hp2 (insertF y 0 ord_max)).
rewrite Hp3 widenF_S_insertF_max insertF_correct_l// mult_zero_l plus_zero_r//.
rewrite Hface_ref_equiv (LagPd1_ref_S ord_max_not_0) insertF_correct_l; easy.
Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1621, reverse way, pp. 104-105. *)
Lemma factor_zero_on_last_Hface_ref_rev :
  forall {p}, Pdk d k.+1 p ->
    (exists q, Pdk d k q /\ p = LagPd1_ref ord_max * q) ->
    forall x, Hface_ref ord_max x -> p x = 0.
Proof.
intros p Hp [q [Hq1 Hq2]] x Hx; rewrite Hq2 fct_mult_eq Hx mult_zero_l; easy.
Qed.

Lemma factor_zero_on_last_Hface_ref_equiv :
  forall {p}, Pdk d k.+1 p ->
    (forall x, Hface_ref ord_max x -> p x = 0) <->
    (exists q, Pdk d k q /\ p = LagPd1_ref ord_max * q).
Proof.
intros; split; [apply factor_zero_on_last_Hface_ref |
    apply factor_zero_on_last_Hface_ref_rev]; easy.
Qed.

End Pdk_Facts.
