(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(** This file is about the convex_envelop of a set of points in a vector space.
    It includes the definition and a few lemmas about it.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Algebra Require Import Algebra_wDep.

Local Open Scope R_scope. (* For order notations. *)
Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.


Section Convex_envelop.

Context {E : ModuleSpace R_Ring}.

Inductive convex_envelop {n} (V : 'E^n) : E -> Prop :=
  | Cvx : forall L, (forall i, 0 <= L i) -> sum L = 1 ->
      convex_envelop V (barycenter L V).

Lemma convex_envelop_ex :
  forall {n} (V : 'E^n) x,
    convex_envelop V x <->
    exists L, (forall i, 0 <= L i) /\ sum L = 1 /\ x = barycenter L V.
Proof.
intros; split.
intros [L HL1 HL2]; exists L; easy.
intros [L [HL1 [HL2 HL3]]]; subst; easy.
Qed.

Lemma convex_envelop_inclF : forall {n} (V : 'E^n), inclF V (convex_envelop V).
Proof.
intros n V i; rewrite -baryc_kron_r; apply Cvx;
    [intros; apply kronR_bound | rewrite sum_kron_r; easy].
Qed.

Lemma convex_envelop_castF :
  forall {n1 n2} (H : n1 = n2) (V1 : 'E^n1) (V2 : 'E^n2),
    V2 = castF H V1 -> convex_envelop V1 = convex_envelop V2.
Proof. intros; subst; rewrite castF_id; easy. Qed.

End Convex_envelop.


Section Non_deg.

Context {E : ModuleSpace R_Ring}.

Definition non_deg {n} (v : 'E^n.+1) : Prop :=
  forall i, convex_envelop v <> convex_envelop (skipF v i).

Lemma aff_indep_non_deg_aux :
  forall {n} {v : 'E^n.+1} i,
    aff_indep v ->
    convex_envelop v (v i) /\ ~ convex_envelop (skipF v i) (v i).
Proof.
intros n v i Hv; split; [apply convex_envelop_inclF |].
intros Hi; inversion Hi as [L HL1 HL2 HL3].
assert (H0 : sum (insertF L 0 i) = 1) by now rewrite sum_insertF plus_zero_l.
assert (H1 : insertF L 0 i = kronR i).
  apply (baryc_coord_uniq Hv); [easy.. | rewrite sum_kron_r; easy |].
  move: HL3; rewrite baryc_kron_r !baryc_ms_eq//.
  rewrite lc_insertF_l scal_zero_l plus_zero_l; easy.
move: H1 => /(fun_ext_rev^~ i); rewrite kronR_is_1// insertF_correct_l//.
apply not_eq_sym, R1_neq_R0.
Qed.

Lemma aff_indep_non_deg :
  forall {n} (v : 'E^n.+1), aff_indep v -> non_deg v.
Proof.
intros n v Hv i; destruct (aff_indep_non_deg_aux i Hv) as [H1 H2].
contradict H2; rewrite -H2; easy.
Qed.

End Non_deg.
