(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Bibliography

 #<DIV><A NAME="Ern"></A></DIV>#
 [[Ern]]
 Alexandre Ern, Éléments finis, Aide-mémoire, Dunod/L'Usine Nouvelle, 2005,
 #<A HREF="https://www.dunod.com/sciences-techniques/aide-memoire-elements-finis">#
 https://www.dunod.com/sciences-techniques/aide-memoire-elements-finis#</A>#.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Algebra Require Import Algebra_wDep.
From FEM Require Import geometry poly_Pdk poly_LagPd1_ref geom_transf_affine FE.

Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.


Section Transf.

Context {d : nat}.

Definition transf : ('R^d -> 'R^d) -> FRd d -> FRd d := fun phi f => f \o phi.

Context {phi : 'R^d -> 'R^d}.
Hypothesis Hphi : bijective phi.
Let phi_inv := f_inv Hphi.
Let transf_inv := transf phi_inv.

Lemma transf_inv_can_l : cancel (transf phi) transf_inv.
Proof.
intro; unfold transf_inv, transf; rewrite -comp_assoc f_inv_id_r comp_id_r//.
Qed.

Lemma transf_inv_can_r : cancel transf_inv (transf phi).
Proof.
intro; unfold transf_inv, transf; rewrite -comp_assoc f_inv_id_l comp_id_r//.
Qed.

Lemma transf_bij : bijective (transf phi).
Proof. apply (Bijective transf_inv_can_l transf_inv_can_r). Qed.

Lemma transf_inv_eq : transf_inv = f_inv transf_bij.
Proof. apply f_inv_uniq_l, transf_inv_can_l. Qed.

Lemma transf_lm : lin_map (transf phi).
Proof. apply lm_comp_l. Qed.

Hypothesis Hphi1 : aff_map_ms phi.

Lemma transf_am_Pdk : forall k, preimage (transf phi) (Pdk d k) = Pdk d k.
Proof.
intros; unfold preimage; apply subset_ext_equiv; split; intro;
    [apply Pdk_comp_am_rev | apply Pdk_comp_am]; easy.
Qed.

End Transf.


Section FE_transf.

(** Transformation of an FE of any shape through a bijective mapping of the
 geometric space R^d, shape and ndof are kept fixed. *)

Context {d : nat}.
Context {phi : 'R^d -> 'R^d}.
Hypothesis Hphi : bijective phi.

Variable FE_in : FE d.

Definition P_transf : FRd d -> Prop := preimage (transf phi) (P_approx FE_in).

Lemma P_transf_has_dim : has_dim P_transf (ndof FE_in).
Proof.
assert (H1 : bijective (transf phi)) by now apply transf_bij.
assert (H2 : surjective (transf phi)) by now apply bij_surj.
apply: (lm_has_dim_rev _ (transf phi) transf_lm H1).
apply cms_preimage; [apply transf_lm | eapply has_dim_cms, FE_in].
rewrite image_preimage Rg_is_full// inter_full_r; apply FE_in.
Qed.

Definition Sigma_transf : '(FRd d -> R)^(ndof FE_in) :=
  fun i p => Sigma FE_in i (transf phi p).

Lemma Sigma_transf_lm : forall i, lin_map (Sigma_transf i).
Proof. intro; unfold Sigma_transf; apply lm_dual_comp_l, (Sigma_lm FE_in _). Qed.

Lemma unisolvence_inj_transf : KerS0 P_transf (gather Sigma_transf).
Proof.
intros f Hf1 Hf2; apply (bij_inj (transf_bij Hphi)).
apply (unisolvence_inj FE_in); easy.
Qed.

Definition FE_transf : FE d :=
  mk_FE (shape FE_in) (mapF phi (vertices FE_in))
    (ndof FE_in) P_transf_has_dim Sigma_transf_lm unisolvence_inj_transf.

Lemma FE_transf_K_geom :
  K_geom FE_transf = convex_envelop (mapF phi (vertices FE_in)).
Proof. easy. Qed.

Lemma transf_bijS : bijS P_transf (P_approx FE_in) (transf phi).
Proof.
apply (BijS (transf (f_inv Hphi))); repeat split.
apply funS_preimage.
apply funS_preimage_can; [apply transf_inv_can_l | apply transf_inv_can_r].
intros g _; apply transf_inv_can_l.
intros g _; apply transf_inv_can_r.
Qed.

Lemma shape_fun_transf :
  shape_fun FE_transf = mapF (transf (f_inv Hphi)) (shape_fun FE_in).
Proof.
extF i; rewrite transf_inv_eq; apply f_inv_eq_equiv; move: i; apply extF_rev.
apply eq_sym, (is_predual_dof_uniq _
    (shape_fun FE_in) (mapF (transf phi) (shape_fun FE_transf)));
    [apply shape_fun_correct | apply (shape_fun_correct FE_transf)].
Qed.

(**
 #<A HREF="##Ern">#[[Ern]]#</A>#
 Prop 4.5, Eq (4.13), pp. 79-80. *)
(**
 #<A HREF="##ErnGuermond">#[[ErnGuermond]]#</A>#
 Prop 9.3 (for k=0), p. 103.#<BR>#
 Ip (f o T) = (Ip f) o T. *)
Lemma local_interp_transf :
  forall f,
    local_interp FE_in (transf phi f) = transf phi (local_interp FE_transf f).
Proof.
intro; unfold local_interp; rewrite shape_fun_transf (lm_lc transf_lm)
    -mapF_comp transf_inv_eq f_inv_id_r; easy.
Qed.

End FE_transf.

Lemma FE_transf_ext :
  forall {d} (FE_in : FE d)
      {phi1 phi2} (H1 : bijective phi1) (H2 : bijective phi2),
    phi1 = phi2 -> FE_transf H1 FE_in = FE_transf H2  FE_in.
Proof. intros; subst; f_equal; apply proof_irrelevance. Qed.


Section FE_vtx.

(** Specialization of the previous transformation to the simplex case (with
 affinely independent vertices) where the mapping if specified by the output
 vertices (simplex shape, and ndof are kept fixed). *)

Context {d : nat}.

Variable FE_in : FE d.
Hypothesis Hshp_in : shape FE_in = Simplex.
Let vtx_in := castF (nvtx_Simplex Hshp_in) (vertices FE_in).
Hypothesis Hvtx_in : aff_indep_ms vtx_in.

Variable vtx : 'R^{d.+1,d}.
Hypothesis Hvtx : aff_indep_ms vtx.
Let phi_vtx := T_geom vtx \o T_geom_inv Hvtx_in.

Lemma Hphi_vtx : bijective phi_vtx.
Proof.
unfold phi_vtx; apply bij_comp; [now apply T_geom_bij | apply f_inv_bij].
Qed.

Definition FE_vtx := FE_transf Hphi_vtx FE_in.

Lemma FE_vtx_vertices_rev :
  vtx = castF (nvtx_Simplex Hshp_in) (vertices FE_vtx).
Proof.
extF; simpl; rewrite -mapF_castF; fold vtx_in; unfold phi_vtx.
rewrite mapF_correct comp_correct
    T_geom_inv_transports_vtx T_geom_transports_vtx; easy.
Qed.

Lemma FE_vtx_K_geom : K_geom FE_vtx = convex_envelop vtx.
Proof.
apply convex_envelop_castF with (nvtx_Simplex Hshp_in), FE_vtx_vertices_rev.
Qed.

End FE_vtx.


Section FE_from_ref.

(** Specialization of the previous transformation to the case where input
 vertices are the reference vertices (simplex shape, and ndof are kept fixed).
*)

Context {d : nat}.

Variable FE_in : FE d.
Hypothesis Hshp_in : shape FE_in = Simplex.
Let vtx_in := castF (nvtx_Simplex Hshp_in) (vertices FE_in).
Hypothesis vtx_in_is_ref : vtx_in = @vtx_ref d.

Variable vtx : 'R^{d.+1,d}.
Hypothesis Hvtx : aff_indep_ms vtx.

Definition FE_from_ref := FE_transf (T_geom_bij Hvtx) FE_in.

Lemma FE_from_ref_K_geom : K_geom FE_from_ref = convex_envelop vtx.
Proof.
apply convex_envelop_castF with (nvtx_Simplex Hshp_in); simpl.
rewrite -mapF_castF; fold vtx_in; rewrite vtx_in_is_ref.
extF; rewrite mapF_correct T_geom_transports_vtx; easy.
Qed.

Lemma Hvtx_in : aff_indep_ms vtx_in.
Proof. rewrite vtx_in_is_ref; apply vtx_ref_aff_indep. Qed.

Lemma phi_from_ref_is_vtx :
  T_geom vtx = T_geom vtx \o T_geom_inv Hvtx_in.
Proof. rewrite T_geom_inv_ref_id//. Qed.

Lemma FE_from_ref_is_vtx :
  FE_from_ref = FE_vtx FE_in Hshp_in Hvtx_in vtx Hvtx.
Proof. apply FE_transf_ext; apply phi_from_ref_is_vtx. Qed.

End FE_from_ref.

(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!
 Note du 30/11/23 :
à un moment, prouver aff_ind vtx -> existe boule ouverte dans K
 prouver pour elt de ref et en déduire avec Tgeom
 isobarycenter
 intérieur non vide devrait être dans le Record
*)

(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!
 20/12/23 : Extract definitions and lemmas that will be applied to both simplex and Quad.
  Do we need modules ? *)
