(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is lin_indep software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Description

 This file is about defining the vector space of polynomials from R^d to R,
 with degree <= k, called Pdk, by defining its basis Monom_dk.
 Special interest for Pd0 and P1k: another basis is given.

 For simplices (Pk in dim d), with x = (x_i)_{i=1..d}:
 - Pdk = lin_span (x_1^alpha_1 * x_2^alpha_2 * ... * x_d^alpha_d)
         such that 0 <= alpha_1, ..., alpha_d <= k /\ sum \alpha_i <= k,
 - p = lin_comb L x_i^(alpha_i) \in Pdk.

 For simplices (P1 in dim d), with x = (x_i)_{i=1..d}:
 - Pd1 = lin_span (1, x_1, ..., x_d) = lin_span LagPd1_ref,
 - p = lin_comb L x \in Pd1.

 * Bibliography

 #<DIV><A NAME="RR9557v1"></A></DIV>#
 [[RR9557v1]]
 François Clément and Vincent Martin,
 Finite element method. Detailed proofs to be formalized in Coq,
 RR-9557, first version, Inria Paris, 2024,
 #<A HREF="https://inria.hal.science/hal-04713897v1">#
 https://inria.hal.science/hal-04713897v1#</A>#.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy Derive.
Close Scope R_scope.

From Algebra Require Import Algebra_wDep.
From FEM Require Import monomial multi_index.

Local Open Scope R_scope. (* For pow notation. *)
Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.


Section R_compl.

Lemma pow_plus : forall x n1 n2, x ^ (n1 + n2)%coq_nat = x ^ n1 * x ^ n2.
Proof. exact pow_add. Qed.

Lemma pow_S : forall x n, x ^ n.+1 = x ^ n * x.
Proof. intros; rewrite -tech_pow_Rmult Rmult_comm; easy. Qed.

End R_compl.


Section Pdk_Def.

(* TODO SB 20/3/24 ; à effacer après récupération dans article *)
(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!

   on a une déf des multinômes
   on a pas explicitement de déf de degré ;
   on a presque que c'est tous les multinômes de degré <= k
   on n'a pas de différentielle
   on n'a pas que c'est toutes les fns dont la k+1-ème différentielle est nulle
   on a que c'est un EV de dimension finie
   on aura que c'est de dimension dimPdk
   on veut l'ordre gravlax (~~)


   multinomial
   on a EV, on a de dimension finie (mais majorée nettement)
   on a la formal derivative
   ordre sur multi-indices, mais pas exactement celui qu'on veut
   vect_n_k
  https://github.com/math-comp/Coq-Combi/blob/master/theories/Combi/vectNK.v

 Pour la defintion de Monom_dk, essayer de comprendre les multinomes de Florent Hivert.

ON VEUT
  un EV de dimension dim_Pk
  le degré
  ce sont ttes les fonctions d degré <= k
  dérivée formelle
  ce sont ttes les fonctions de différentielle (k+1)eme nulle
  formule de Taylor : x, x_0 \in R^d
    p(x) = p(x_0) + Dp(x_0) (x-x_0) + D^2p(x_0) (x-x_0,x-x_0) + ...
  Dp(x_0) est une matrice / une appli linéaire
      = \sum_{alpha , vecteur de somme 1} d^alpha p(x_0)
      = \delta p / \delta x_1 ++ \delta_p / \delta x_2 ++ ... d
*)

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1503, p. 72. *)
Definition Monom_dk d k : '(FRd d)^(pbinom d k).+1 :=
  fun idk x => powF_P (Adk d k idk) x.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1505, p. 72. *)
Definition Pdk d k : FRd d -> Prop := lin_span (Monom_dk d k).

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1507, p. 72. *)
Lemma Pdk_cms : forall d k, compatible_ms (Pdk d k).
Proof. intros; apply lin_span_cms. Qed.

Definition Pdk_ms := fun d k => sub_ModuleSpace (Pdk_cms d k).

End Pdk_Def.

Arguments Pdk_cms {d k}.


Section Pkd_Facts1.

Lemma Monom_0k_eq : forall k, Monom_dk O k = fun _ _ => 1.
Proof.
intros; unfold Monom_dk; extF; fun_ext; rewrite A0k_eq; apply powF_P_nil.
Qed.

Lemma P0k_full : forall k, Pdk 0 k = fullset.
Proof.
intros k; assert (Hk : invertible (Monom_dk 0 k ord0 0))
    by (rewrite Monom_0k_eq; apply invertible_one).
apply (FT0_cms eq_refl Pdk_cms Hk), lin_span_inclF_diag.
Qed.

Lemma P0k_eq : forall k, Pdk 0 k = fun f => f = (fun=> f 0).
Proof.
intros k; rewrite P0k_full eq_sym_equiv.
apply full_incl; intros f _; apply FR0_eq; easy.
Qed.

Lemma Monom_d0_eq : forall d, Monom_dk d 0 = fun _ _ => 1.
Proof.
intros [| d]; unfold Monom_dk; extF; fun_ext;
    [rewrite A0k_eq | rewrite Ad0_eq]; apply powF_P_zero_ext_r; easy.
Qed.

Lemma Pd0_eq : forall {d} (p:FRd d), Pdk d 0 p <-> p = fun=> p zero.
Proof.
intros d p; unfold Pdk.
rewrite Monom_d0_eq; split; intros H.
inversion H as [L HL].
fun_ext x.
now repeat rewrite fct_lc_r_eq.
apply lin_span_ex.
exists (fun=> p zero).
rewrite H.
fun_ext x.
rewrite fct_lc_r_eq.
rewrite lc_ones_r.
assert (T: (S (pbinom d 0)) = S O).
apply pbinomS_0_r.
rewrite -(sum_castF T).
now rewrite sum_singleF.
Qed.

Lemma Monom_dk_0 : forall d k, Monom_dk d k ord0 = fun=> 1.
Proof.
intros; fun_ext; unfold Monom_dk.
apply powF_P_zero_ext_r; intros; rewrite Adk_0; easy.
Qed.

Lemma Monom_1k :
  forall {k} i1k, Monom_dk 1 k i1k = fun x => x ord0 ^ Adk 1 k i1k ord0.
Proof.
intros; unfold Monom_dk; fun_ext; apply powF_P_one.
extF; rewrite ord_one itemF_correct_l; easy.
Qed.

Lemma Monom_d1_S :
  forall {d} id1 (H : id1 <> ord0),
    Monom_dk d 1 id1 = fun x => x (lower_S (cast_ordS_n0 (pbinomS_1_r d) _ H)).
Proof.
intros d id1 H; unfold Monom_dk; fun_ext x; rewrite Ad1_S.
pose (k := cast_ord (pbinom_1_r d) (lower_S H)).
rewrite (powF_P_one _ _ 1 k)// pow_1; f_equal; apply ord_inj; easy.
Qed.

Lemma Monom_d1_S_alt :
  forall {d} i,
    @^~ i = Monom_dk d 1 (lift_S (cast_ord (eq_sym (pbinom_1_r d)) i)).
Proof.
intros; rewrite (Monom_d1_S _ (lift_S_not_first _)).
fun_ext; f_equal; rewrite lower_lift_S_cast_ord; easy.
Qed.

Lemma Monom_d1_am : forall {d} id1, aff_map_ms (Monom_dk d 1 id1).
Proof.
intros d id1; destruct (ord_eq_dec id1 ord0) as [-> | Hid1].
rewrite Monom_dk_0; apply: fct_cst_am.
rewrite Monom_d1_S; apply am_lm_0_equiv;
    rewrite minus_zero_r; apply lm_component.
Qed.

Lemma Pdk_ext :
  forall {d} k p q, (forall x, p x = q x) -> Pdk d k p -> Pdk d k q.
Proof. intros d k p q H1 H2; replace q with p; [| fun_ext]; easy. Qed.

Lemma Pdk_lc :
  forall {d} {n} k p,
    (forall i, Pdk d k (p i)) -> forall (L : 'R^n), Pdk d k (lin_comb L p).
Proof. intros; apply lin_span_lc_closed; easy. Qed.

Lemma Pdk_powF_P : forall d k idk, Pdk d k (powF_P (Adk d k idk)).
Proof.
intros d k idk; apply lin_span_ex.
exists (itemF _ 1 idk); rewrite lc_itemF_l scal_one; easy.
Qed.

Lemma Pdk_monot_S : forall {d} k p, Pdk d k p -> Pdk d k.+1 p.
Proof.
intros [| d] k p Hp; [rewrite P0k_full; easy |].
inversion Hp as [L HL].
apply lin_span_ex.
exists (castF (AdSk_eq_aux d k) (concatF L zero)).
unfold Monom_dk.
rewrite AdSk_eq.
rewrite (lc_ext_r
((castF (AdSk_eq_aux d k)
    (concatF (fun iSdk : 'I_(pbinom d.+1 k).+1 =>
               powF_P (Adk d.+1 k iSdk))
        (fun idSk => powF_P (CSdk d k.+1 idSk)))))).
rewrite lc_castF.
rewrite (lc_concatF_zero_lr L); easy.
intros iSdSk; unfold castF; unfold concatF.
simpl (nat_of_ord (cast_ord _ iSdSk)).
case (lt_dec _ _); intros Hi; easy.
Qed.

Lemma Pdk_monot :
  forall {d} k1 k2 p, (k1 <= k2)%coq_nat -> Pdk d k1 p -> Pdk d k2 p.
Proof.
intros d k1 k2 p; apply (nat_ind_monot (Pdk d ^~ p)) => k; apply Pdk_monot_S.
Qed.

Lemma PdSk_split_aux1 :
  forall {d n} k (p :'I_n -> FRd d.+1) L,
    (forall i, exists p0 p1,
      Pdk d k.+1 p0 /\ Pdk d.+1 k p1 /\
      p i = fun x => p0 (widenF_S x) + x ord_max * p1 x) ->
    exists p0 p1,
      Pdk d k.+1 p0 /\ Pdk d.+1 k p1 /\
      lin_comb L p = fun x => p0 (widenF_S x) + x ord_max * p1 x.
Proof.
intros d n k p L H.
apply choiceF in H; destruct H as (p0,H).
apply choiceF in H; destruct H as (p1,H).
exists (lin_comb L p0); exists (lin_comb L p1); split.
apply Pdk_lc; apply H.
split.
apply Pdk_lc; apply H.
fun_ext x.
rewrite 3!fct_lc_r_eq.
replace (_ + _) with (lin_comb L (p0^~ (widenF_S x)) +
    scal (x ord_max) (lin_comb L ((p1^~ x)))); try easy.
rewrite -lc_scal_r -lc_plus_r.
apply lc_ext_r.
intros i; rewrite (proj2 (proj2 (H i))); easy.
Qed.

Lemma PdSk_split_aux2 :
  forall {d} k (iSdSk : 'I_(pbinom d.+1 k.+1).+1),
    exists (p0 : FRd d) (p1 : FRd d.+1),
      Pdk d k.+1 p0 /\ Pdk d.+1 k p1 /\
      powF_P (Adk d.+1 k.+1 iSdSk) = fun x => p0 (widenF_S x) + x ord_max * p1 x.
Proof.
intros d k iSdSk.
case (Nat.eq_dec (Adk d.+1 k.+1 iSdSk ord_max) O); intros H.
(* monôme sans ord_max *)
pose (jdSk:= Adk_inv d k.+1 (widenF_S (Adk d.+1 k.+1 iSdSk))).
exists (powF_P (Adk d k.+1 jdSk)).
exists (fun=> 0).
split; try split.
apply Pdk_powF_P.
apply Pdk_monot with O; auto with arith.
apply Pd0_eq; easy.
fun_ext x.
rewrite mult_zero_r plus_zero_r.
unfold jdSk; rewrite Adk_inv_correct_r.
rewrite {1}(concatF_splitF_Sp1 x).
rewrite {1}(concatF_splitF_Sp1 (Adk d.+1 k.+1 iSdSk)).
rewrite powF_P_castF powF_P_concatF H.
rewrite (powF_P_zero_ext_r (singleF _)).
rewrite mult_one_r; easy.
intro; rewrite singleF_0; easy.
apply Nat.add_le_mono_r with (Adk d.+1 k.+1 iSdSk ord_max).
replace (_ + _)%coq_nat with
    (sum (widenF_S (Adk d.+1 k.+1 iSdSk)) + Adk d.+1 k.+1 iSdSk ord_max); try easy.
replace (k.+1 + _)%coq_nat with (k.+1 + Adk d.+1 k.+1 iSdSk ord_max); try easy.
rewrite -sum_ind_r H plus_zero_r; apply Adk_sum.
(* monôme avec ord_max *)
exists (fun=> 0).
pose (jSdk:=Adk_inv d.+1 k
        (replaceF (Adk d.+1 k.+1 iSdSk)
                  (Adk d.+1 k.+1 iSdSk ord_max).-1 ord_max)).
exists (powF_P (Adk d.+1 k jSdk)).
split; try split.
apply Pdk_monot with O; auto with arith.
apply Pd0_eq; easy.
apply Pdk_powF_P.
fun_ext x.
rewrite plus_zero_l.
case (Req_dec (x ord_max) 0); intros Hz.
(* . *)
rewrite Hz mult_zero_l.
apply powF_P_zero_compat.
exists ord_max.
rewrite powF_correct.
rewrite Hz pow_i; auto with zarith.
(* . *)
unfold jSdk; rewrite Adk_inv_correct_r.
apply mult_reg_l with ((x ord_max)^(Adk d.+1 k.+1 iSdSk ord_max).-1).
apply invertible_equiv_R, pow_nonzero; easy.
rewrite mult_assoc.
rewrite -{3}(pow_1 (x ord_max)) -pow_plus.
rewrite Nat.add_1_r Nat.succ_pred_pos; auto with zarith.
rewrite powF_P_replaceF_l; easy.
apply Nat.add_le_mono_l with (Adk d.+1 k.+1 iSdSk ord_max).
replace (_ + _)%coq_nat with (Adk d.+1 k.+1 iSdSk ord_max +
  sum (replaceF (Adk d.+1 k.+1 iSdSk) (Adk d.+1 k.+1 iSdSk ord_max).-1
       ord_max)); try easy.
rewrite sum_replaceF.
replace (_ + _) with
   ((Adk d.+1 k.+1 iSdSk ord_max).-1 + sum (Adk d.+1 k.+1 iSdSk))%coq_nat; try easy.
generalize (Adk_sum d.+1 k.+1 iSdSk); lia.
Qed.

Lemma PdSk_split :
  forall {d} k (p : FRd d.+1),
    Pdk d.+1 k.+1 p ->
    exists p0 p1,
      Pdk d k.+1 p0 /\ Pdk d.+1 k p1 /\
      p = fun x => p0 (widenF_S x) + x ord_max * p1 x.
Proof.
intros d k p H; inversion H as (L,HL).
apply PdSk_split_aux1; intros; unfold Monom_dk; apply PdSk_split_aux2.
Qed.

Lemma Pdk_mult_const :
  forall {d} k (p q : FRd d),
    Pdk d k p -> (q = fun=> q zero) -> Pdk d k (p * q).
Proof.
intros d k p q H1 H2.
replace (p * q) with (scal (q zero) p).
apply lin_span_scal_closed; easy.
fun_ext; rewrite H2 fct_mult_eq mult_comm_R; easy.
Qed.

Lemma Pdk_const : forall {d} k (p : FRd d), p = (fun=> p zero) -> Pdk d k p.
Proof.
intros d k p Hp; apply Pdk_ext with (Monom_dk d k ord0 * p).
intros; rewrite fct_mult_eq Monom_dk_0 mult_one_l; easy.
apply Pdk_mult_const; [apply lin_span_inclF_diag | easy].
Qed.

Lemma Pdk_mul_var :
  forall {d} k (p : FRd d.+1) i,
    Pdk d.+1 k p -> Pdk d.+1 k.+1 (fun x => x i * p x).
Proof.
intros d k p i H; inversion H as [L HL].
apply Pdk_ext with
   (lin_comb L (fun j x => x i * Monom_dk d.+1 k j x)).
intros x.
apply trans_eq with (scal (x i) (lin_comb L (Monom_dk d.+1 k) x)); try easy.
rewrite fct_lc_r_eq.
rewrite lc_scal_r.
f_equal; now rewrite fct_lc_r_eq.
(* *)
apply Pdk_lc.
intros jSdk; unfold Monom_dk.
apply Pdk_ext with
   (powF_P (Adk d.+1 k.+1
             (Adk_inv d.+1 k.+1
                 (replaceF (Adk d.+1 k jSdk)  (Adk d.+1 k jSdk i).+1%coq_nat i)))).
2: apply Pdk_powF_P.
intros x; rewrite Adk_inv_correct_r.
case (Req_dec (x i) 0); intros Hxi.
rewrite Hxi; rewrite mult_zero_l.
apply powF_P_zero_compat.
exists i; unfold powF, map2F; rewrite Hxi.
rewrite pow_ne_zero; try easy.
rewrite replaceF_correct_l; auto with arith.
apply mult_reg_l with ((x i)^(Adk d.+1 k jSdk i)).
apply invertible_equiv_R, pow_nonzero; easy.
rewrite powF_P_replaceF_l.
rewrite pow_S mult_assoc; easy.
(* *)
apply Nat.add_le_mono_l with (Adk d.+1 k jSdk i).
replace (_ + _)%coq_nat with
   (Adk d.+1 k jSdk i +
  sum (replaceF (Adk d.+1 k jSdk) (Adk d.+1 k jSdk i).+1 i)); try easy.
rewrite sum_replaceF.
replace (_ + _) with ((Adk d.+1 k jSdk i).+1 + sum (Adk d.+1 k jSdk))%coq_nat; try easy.
generalize (Adk_sum d.+1 k jSdk); lia.
Qed.

Lemma Pdk_widenF_S :
  forall {d} k (p : FRd d), Pdk d k p -> Pdk d.+1 k (fun x => p (widenF_S x)).
Proof.
intros d k p H; inversion H.
apply Pdk_ext with
   (lin_comb L (fun idk x => Monom_dk d k idk (widenF_S x))).
intros x.
now rewrite 2!fct_lc_r_eq.
apply Pdk_lc.
intros idk; unfold Monom_dk.
apply Pdk_ext with
   (powF_P (Adk d.+1 k
             (Adk_inv d.+1 k
                 (castF (Nat.add_1_r d) (concatF (Adk d k idk) (singleF zero)))))).
2: apply Pdk_powF_P.
intros x; rewrite Adk_inv_correct_r.
assert (T: x = (castF (Nat.add_1_r d)
          (concatF (widenF_S x) (singleF (x ord_max))))).
rewrite concatF_splitF_Sp1'.
extF j; unfold castF, castF_Sp1, castF.
f_equal; apply ord_inj; easy.
rewrite {1}T.
rewrite powF_P_castF.
rewrite powF_P_concatF.
rewrite (powF_P_zero_ext_r (singleF _)).
apply mult_one_r.
intro; apply singleF_0.
(* *)
rewrite sum_castF sum_concatF.
rewrite sum_singleF plus_zero_r.
apply Adk_sum.
Qed.

Lemma Pdk_mult_le :
  forall {d} n k l p q,
    Pdk d k p -> Pdk d l q -> (k + l <= n)%coq_nat -> Pdk d n (p * q).
Proof.
intros d n ; apply nat_ind2_strong with
   (P:= fun d n => forall k l p q,
    Pdk d k p ->  Pdk d l q
    -> (k+l <= n)%coq_nat
     ->   Pdk d n (p*q)); clear d n.
(* d=0 *)
intros d; case d.
intros n Hrec k l p q H1 H2 H3.
rewrite P0k_full; easy.
clear d; intros d n Hrec k.
case k; clear k.
(* k=0 *)
clear; intros l p q H1 H2 H3.
apply Pdk_monot with l; auto with arith.
replace (p*q) with (q*p).
apply Pdk_mult_const; try easy.
apply Pd0_eq; easy.
fun_ext x.
unfold mult; simpl; unfold fct_mult; simpl.
unfold mult; simpl; auto with real.
intros k l.
case l; clear l.
(* l=0 *)
clear; intros p q H1 H2 H3.
apply Pdk_monot with k.+1.
apply Nat.le_trans with (2:=H3).
auto with arith.
apply Pdk_mult_const; try easy.
apply Pd0_eq; easy.
(* d.+1 k.+1 l.+1 *)
intros l p q Hp Hq Hn.
assert (Hn2: (1 < n)%coq_nat).
apply Nat.lt_le_trans with (2:=Hn).
case k; case l; auto with arith.
destruct (PdSk_split _ p Hp) as (p1,(p2,(Yp1,(Yp2,Yp3)))).
destruct (PdSk_split _ q Hq) as (q1,(q2,(Yq1,(Yq2,Yq3)))).
replace (p*q) with
     ((fun x => (p1*q1) (widenF_S x))
     + (fun x => x ord_max * (p1 (widenF_S x) * q2 x + p2 x * q1 (widenF_S x)))
     + (fun x => x ord_max * (x ord_max * (p2*q2) x))).
repeat apply: lin_span_plus_closed.
(* . p1 q1 *)
apply Pdk_widenF_S.
apply: (Hrec d n _ _ _ k.+1 l.+1); auto with arith.
apply pair_neq_spec; left; easy.
(* . *)
replace n with ((n.-1).+1) by auto with zarith.
apply Pdk_mul_var.
apply: lin_span_plus_closed.
(* . p1 q2*)
apply: (Hrec d.+1 n.-1 _ _ _ k.+1 l); auto with arith.
apply pair_neq_spec; right; auto with zarith.
apply Pdk_widenF_S; easy.
apply le_S_n; apply Nat.le_trans with n; auto with zarith.
apply Nat.le_trans with (2:=Hn); auto with arith.
(* . p2 q1*)
apply: (Hrec d.+1 n.-1 _ _ _ k l.+1); auto with arith.
apply pair_neq_spec; right; auto with zarith.
apply Pdk_widenF_S; easy.
apply le_S_n; apply Nat.le_trans with n; auto with zarith.
(* . p2 * q2 *)
replace n with ((n.-2).+2) by auto with zarith.
apply Pdk_mul_var.
apply Pdk_mul_var.
apply: (Hrec d.+1 n.-2 _ _ _ k l); auto with zarith arith.
apply pair_neq_spec; right; auto with zarith.
apply le_S_n, le_S_n; apply Nat.le_trans with n; auto with zarith.
apply Nat.le_trans with (2:=Hn); apply PeanoNat.Nat.eq_le_incl.
rewrite -PeanoNat.Nat.add_succ_r; auto with zarith arith.
(* *)
rewrite Yp3 Yq3.
fun_ext.
rewrite !fct_plus_eq !fct_mult_eq.
unfold mult, plus; simpl; ring.
Qed.

Lemma Pdk_mult :
  forall {d} k l p q,
    Pdk d k p -> Pdk d l q -> Pdk d (k + l)%coq_nat (p * q).
Proof. intros; apply Pdk_mult_le with k l; easy. Qed.

Lemma Pdk_mult_iter :
  forall {d n} (k : 'I_n -> nat) p m,
    (forall i, Pdk d (k i) (p i)) ->
    (sum k <= m)%coq_nat ->
    Pdk d m (fun x => prod_R (fun i => p i x)).
Proof.
intros d n; induction n.
(* *)
intros k p m Hi Hm.
apply Pdk_monot with O; auto with arith.
apply Pd0_eq.
fun_ext x.
unfold prod_R; now rewrite 2!sum_nil.
(* *)
intros k p m Hi Hm.
apply Pdk_ext with
 ( (fun x => (p^~ x) ord0) * (fun x => prod_R (liftF_S (p^~ x)))).
intros x; rewrite prod_R_ind_l; simpl; easy.
apply Pdk_mult_le with (k ord0) (sum (liftF_S k)).
apply Hi.
apply IHn with (liftF_S k); try easy.
intros i; apply Hi.
apply Nat.le_trans with (2:=Hm).
rewrite sum_ind_l; auto with arith.
Qed.

Lemma Pd1_am_1_equiv : forall {d} (f : FRd d), Pdk d 1 f <-> aff_map_ms f.
Proof.
intros d f; split; intros Hf.
(* => *)
inversion Hf as [L HL]; apply am_f_lc; intro; apply Monom_d1_am.
(* <= *)
destruct (proj1 (am_ms_equiv) Hf) as [lf [c [Hlf ->]]].
apply: lin_span_plus_closed.
2: apply Pdk_monot with O; [apply Nat.le_0_1 | apply Pd0_eq; easy].
rewrite (lm_decomp_1 _ Hlf); eapply Pdk_ext; [| eapply Pdk_lc].
intros x; rewrite fct_lc_r_eq; easy.
intros i; simpl; rewrite Monom_d1_S_alt; apply lin_span_inclF_diag.
Qed.

Lemma Pd1_am_equiv :
  forall {d n} (f : 'R^d -> 'R^n),
    (forall l, Pdk d 1 (scatter f l)) <-> aff_map_ms f.
Proof.
intros d n f; apply iff_trans with (forall l, aff_map_ms (scatter f l)).
split; intros; apply Pd1_am_1_equiv; easy.
rewrite scatter_am_compat am_ms_correct
    !(am_lm_equiv (0 : fct_ModuleSpace)); easy.
Qed.

Lemma Pdk_pow_am :
  forall {d n} (f : 'R^d -> 'R^n) (l : 'I_n) m,
    aff_map_ms f -> Pdk d m (fun x => pow (f x l) m).
Proof.
intros d n f l m Hf.
apply Pdk_ext with (fun x => prod_R (fun i:'I_m => f x l)).
intros x.
clear; induction m.
simpl; now rewrite prod_R_nil.
rewrite prod_R_ind_l IHm -tech_pow_Rmult; easy.
apply Pdk_mult_iter with (fun=> S O).
intros j.
now apply Pd1_am_equiv.
rewrite sum_constF_nat; auto with zarith.
Qed.

Lemma Pdk_comp_am :
  forall {d n} k (p : FRd d) (f : 'R^n -> 'R^d),
    aff_map_ms f -> Pdk d k p -> Pdk n k (p \o f).
Proof.
intros d n k p f Hf Hp; inversion Hp as [L HL].
rewrite -lc_compF_rl; apply Pdk_lc; intros idk.
apply Pdk_mult_iter with (Adk d k idk); [| apply Adk_sum].
intros j; apply Pdk_pow_am; easy.
Qed.

Lemma Pdk_comp_am_rev :
  forall {d} k (p : FRd d) (f : 'R^d -> 'R^d),
    aff_map_ms f -> bijective f -> Pdk d k (p \o f) -> Pdk d k p.
Proof.
intros d k p f Hf1 Hf2 H; rewrite -(comp_id_r p) -(f_inv_id_r Hf2).
rewrite comp_assoc; apply Pdk_comp_am; [| easy].
apply: am_bij_compat; easy.
Qed.

Lemma Pdk_lin_gen_comp :
  forall {d} k (B : '(FRd d)^(pbinom d k).+1) (f : 'R^d -> 'R^d),
  lin_gen (Pdk d k) B -> aff_map_ms f -> bijective f ->
  lin_gen (Pdk d k) (compF_l B f).
Proof.
intros d k B f HB Hf1 Hf2; apply subset_ext_equiv; split; intros p Hp.
(* *)
assert (Hf2' : bijective (f : fct_ModuleSpace -> fct_ModuleSpace)) by easy.
move: (am_bij_compat Hf2' Hf1) => Hfi1.
move: (Pdk_comp_am _ _ _ Hfi1 Hp) => H1.
rewrite HB in H1; inversion H1 as [L HL].
apply lin_span_ex; exists L; fun_ext x.
rewrite -(comp_id_r p) -(f_inv_id_l Hf2') comp_assoc lc_compF_rl HL; easy.
(* *)
inversion Hp as [L HL]; apply Pdk_lc; intros i.
apply Pdk_comp_am; [easy | apply (lin_gen_inclF HB)].
Qed.

End  Pkd_Facts1.


Section Derive_Gen.

(** We here define differentials of polynomials (from Coquelicot's derivative).
    We have any number of derivative on a function with any number of variables.
    This is a limited development (on polynomials only as the only goal here is
    to prove Monom_dk_lin_indep, that is to say Monom_dk is a lin_indep family.
*)

Lemma is_derive_ext':
  forall {K : AbsRing} {V : NormedModule K} (f : K -> V)
    (x : K) (l1 l2 : V),
    l1 = l2 -> is_derive f x l1 -> is_derive f x l2.
Proof.
intros ; subst; easy.
Qed.

Lemma ex_derive_lc :
  forall {n} (f :'I_n -> (R->R)) (L:'R^n),
    (forall i x, ex_derive (f i) x) ->
      forall x, ex_derive (lin_comb L f) x.
Proof.
intros n f L H.
assert (H0: lc_closed (fun (g:R->R) => forall x, ex_derive g x)).
2: now apply H0.
apply cms_lc.
apply plus_scal_closed_cms.
exists (fun=> 0); intros x.
apply ex_derive_const.
intros g h H1 H2 x.
apply: ex_derive_plus; easy.
intros g l H1 x.
apply ex_derive_scal; easy.
Qed.

Lemma is_derive_lc :
  forall {n} (f :'I_n -> (R->R)) (L:'R^n) (d :'I_n -> (R->R)),
    (forall i x, is_derive (f i) x (d i x)) ->
      forall x, is_derive (lin_comb L f) x (lin_comb L d x).
Proof.
intros n; induction n.
intros f L d H x.
rewrite 2!lc_nil fct_zero_eq.
unfold zero; simpl; unfold fct_zero.
eapply is_derive_ext';[| apply is_derive_const]; easy.
intros f L d H x.
rewrite 2!lc_ind_l.
apply: is_derive_plus.
apply is_derive_scal; easy.
apply IHn; try easy.
intros i y; unfold liftF_S; apply H.
Qed.

(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!
 FC (14/12/2023) suggestion : rendre ces proprietes uniformes.
 C'est-a-dire remplacer "fun ... x => ..." par "fun ... => forall x, ..." *)

Definition ex_derive_onei {n} :=
  fun (i:'I_n) (f: FRd n) (x:'R^n) =>
     ex_derive (fun y => f (replaceF x y i)) (x i).

Definition is_derive_onei {n} :=
  fun (i:'I_n) (f: FRd n) (x:'R^n) (l:R) =>
     is_derive (fun y => f (replaceF x y i)) (x i) l.

Definition Derive_onei {n} :=
   fun (i:'I_n) (f: FRd n) (x:'R^n) =>
      Derive (fun y => f (replaceF x y i)) (x i).

Definition ex_derive_multii {n} :=
  fun (i:'I_n) (j:nat) (f: FRd n) (x:'R^n) =>
      ex_derive_n (fun y => f (replaceF x y i)) j (x i).

Definition is_derive_multii {n} :=
  fun (i:'I_n) (j:nat) (f: FRd n) (x:'R^n) (l:R) =>
      is_derive_n (fun y => f (replaceF x y i)) j (x i) l.

Definition Derive_multii {n} (i:'I_n) (j:nat) (f: FRd n) (x:'R^n):=
   Derive_n (fun y => f (replaceF x y i)) j (x i).

Lemma ex_derive_onei_lc :
  forall {n m} (j:'I_n) (f :'I_m -> FRd n) (L:'R^m),
    (forall i x, ex_derive_onei j (f i) x) ->
      forall x, ex_derive_onei j (lin_comb L f) x.
Proof.
intros n m j f L; unfold ex_derive_onei; intros H x.
apply ex_derive_ext with
 (lin_comb L (fun (i : 'I_m) (y : R) => f i (replaceF x y j))).
intros t; rewrite 2!fct_lc_r_eq; easy.
apply ex_derive_lc.
intros i y; specialize (H i (replaceF x y j)).
rewrite replaceF_correct_l in H; try easy.
apply ex_derive_ext with (2:=H).
intros t; f_equal; extF k.
unfold replaceF; case (ord_eq_dec _ _); try easy.
Qed.

Lemma is_derive_onei_lc :
  forall {n m} (j:'I_n) (f :'I_m -> FRd n) (L:'R^m) (d:'I_m -> FRd n),
    (forall i x, is_derive_onei j (f i) x (d i x)) ->
      forall x, is_derive_onei j (lin_comb L f) x (lin_comb L d x).
Proof.
intros n m j f L d; unfold is_derive_onei; intros H x.
apply is_derive_ext with
 (lin_comb L (fun (i : 'I_m) (y : R) => f i (replaceF x y j))).
intros t; rewrite 2!fct_lc_r_eq; easy.
apply is_derive_ext' with (lin_comb L (fun i y => d i (replaceF x y j)) (x j)).
rewrite 2! fct_lc_r_eq.
apply lc_ext_r.
intros i; f_equal.
apply replaceF_id.
apply is_derive_lc.
intros i y; specialize (H i (replaceF x y j)).
rewrite replaceF_correct_l in H; try easy.
apply is_derive_ext with (2:=H).
intros t; f_equal; extF k.
unfold replaceF; case (ord_eq_dec _ _); try easy.
Qed.

Lemma is_derive_onei_unique : forall {n} i (f:FRd n) x l,
   is_derive_onei i f x l -> Derive_onei i f x = l.
Proof.
intros n i f x l; unfold is_derive_onei, Derive_onei; intros H.
apply is_derive_unique; easy.
Qed.

Lemma is_derive_multii_unique : forall {n} (i:'I_n) j f x l,
    is_derive_multii i j f x l -> Derive_multii i j f x = l.
Proof.
intros n i j f x l; unfold is_derive_multii, Derive_multii; intros H.
apply is_derive_n_unique; easy.
Qed.

Lemma is_derive_multii_scal : forall {n} (i:'I_n) j f x l (k:R),
     is_derive_multii i j f x l ->
     is_derive_multii i j (scal k f) x (scal k l).
Proof.
intros; apply is_derive_n_scal_l; easy.
Qed.

Lemma Derive_multii_scal : forall {n} (i:'I_n) j f x (k:R),
     Derive_multii i j (scal k f) x
       = scal k (Derive_multii i j f x).
Proof.
intros n i j f x k; unfold Derive_multii.
rewrite Derive_n_scal_l; easy.
Qed.

Definition Derive_alp {n} (alpha : 'nat^n) : '(FRd n -> FRd n)^n :=
  fun i => Derive_multii i (alpha i).

Definition DeriveF_part {n} (alpha : 'nat^n) (j:'I_n.+1) : FRd n -> FRd n :=
  comp_n_part (Derive_alp alpha) j.

Lemma DeriveF_part_ext_alp :
  forall {n} (alpha1 alpha2 : 'nat^n) j,
    eqAF (widenF (leq_ord j) alpha1) (widenF (leq_ord j) alpha2) ->
    DeriveF_part alpha1 j = DeriveF_part alpha2 j.
Proof.
move=>> H; apply comp_n_part_ext; move=>>; fun_ext.
unfold widenF, Derive_alp in *; rewrite H; easy.
Qed.

Lemma DeriveF_part_extf : forall {n} (alpha:'nat^n) j f1 f2,
    same_fun f1 f2 -> DeriveF_part alpha j f1 = DeriveF_part alpha j f2.
Proof. move=>> /fun_ext Hf; subst; easy. Qed.

Lemma DeriveF_part_0 :
  forall {n} (alpha:'nat^n) j, j = ord0 -> DeriveF_part alpha j = ssrfun.id.
Proof. intros; apply comp_n_part_0; easy. Qed.

Lemma DeriveF_part_nil :
  forall {n} (alpha:'nat^n) j, n = 0%nat -> DeriveF_part alpha j = ssrfun.id.
Proof. intros; apply comp_n_part_nil; easy. Qed.

Lemma DeriveF_part_full :
  forall {n} (alpha:'nat^n) j,
    j = ord_max -> DeriveF_part alpha j = comp_n (Derive_alp alpha).
Proof. intros; apply comp_n_part_max; easy. Qed.

Lemma DeriveF_part_ind_l :
  forall {n} (alpha:'nat^n.+1) {j} (H : j <> ord0),
    DeriveF_part alpha j =
      Derive_alp alpha ord0 \o
      comp_n_part (liftF_S (Derive_alp alpha)) (lower_S H).
Proof. intros; apply comp_n_part_ind_l; easy. Qed.

Lemma DeriveF_part_ind_r :
  forall {n} (alpha : 'nat^n) (j:'I_n),
    DeriveF_part alpha (lift_S j) =
      DeriveF_part alpha (widen_S j) \o Derive_alp alpha j.
Proof. intros; apply comp_n_part_ind_r. Qed.

Definition DeriveF {n} (alpha : 'nat^n) : FRd n -> FRd n :=
  comp_n (Derive_alp alpha).

Lemma DeriveF_correct :
  forall {n} (alpha : 'nat^n), DeriveF alpha = DeriveF_part alpha ord_max.
Proof. intros; apply eq_sym, DeriveF_part_full; easy. Qed.

Lemma DeriveF_ext_alp :
  forall {n} (alpha1 alpha2 : 'nat^n),
    eqAF alpha1 alpha2 -> DeriveF alpha1 = DeriveF alpha2.
Proof.
intros; rewrite !DeriveF_correct; apply DeriveF_part_ext_alp.
rewrite !widenF_full; easy.
Qed.

Lemma DeriveF_extf : forall {n} (alpha : 'nat^n) f1 f2,
    same_fun f1 f2 -> DeriveF alpha f1 = DeriveF alpha f2.
Proof. move=>> /fun_ext Hf; subst; easy. Qed.

Lemma DeriveF_nil :
  forall {n} (alpha : 'nat^n), n = 0%nat -> DeriveF alpha = ssrfun.id.
Proof. intros; apply comp_n_nil; easy. Qed.

Lemma DeriveF_ind_l :
  forall {n} (alpha : 'nat^n.+1),
    DeriveF alpha =
      Derive_alp alpha ord0 \o comp_n (liftF_S (Derive_alp alpha)).
Proof. intros; apply comp_n_ind_l; easy. Qed.

Lemma DeriveF_ind_r :
  forall {n} (alpha : 'nat^n.+1),
    DeriveF alpha =
      comp_n (widenF_S (Derive_alp alpha)) \o Derive_alp alpha ord_max.
Proof. intros; apply comp_n_ind_r. Qed.

(* f1 does not depend upon (x i) therefore is a constant when deriving *)
Lemma Derive_multii_scal_fun :  forall {n} (i:'I_n) j f1 f2,
    (forall x y, f1 x = f1 (replaceF x y i)) ->
     Derive_multii i j (fun x => f1 x* f2 x)
       = (fun x => f1 x*(Derive_multii i j f2 x)).
Proof.
intros n i j f1 f2 H1.
fun_ext x; unfold Derive_multii.
rewrite ->Derive_n_ext with (g:=fun y => f1 x * f2 (replaceF x y i)).
2: intros t; rewrite -H1; easy.
now rewrite Derive_n_scal_l.
Qed.

(* could have been insertF *)
Definition narrow_ord_max := fun {n:nat} (f:'R^n.+1 -> R) (x:'R^n) =>
   f (castF (eq_sym (addn1_sym n)) (concatF x (singleF 0))).

Lemma narrow_ord_max_correct : forall {n:nat} f,
   (forall (x:'R^n.+1) y, f x = f (replaceF x y ord_max)) ->
   (forall x:'R^n.+1, f x = narrow_ord_max f (widenF_S x)).
Proof.
intros n f H x; rewrite (H x 0).
unfold narrow_ord_max; f_equal.
extF i; unfold replaceF.
case (ord_eq_dec _ _); intros Hi.
subst; unfold castF; rewrite concatF_correct_r; easy.
unfold castF; rewrite concatF_correct_l; simpl; try easy.
destruct i as (j,Hj); simpl.
apply ord_neq_compat in Hi; simpl in Hi.
assert (j < n.+1)%coq_nat; [ now apply /ltP | now lia].
intros H1; unfold widenF_S; f_equal; apply ord_inj; now simpl.
Qed.

Lemma narrow_ord_max_Derive_multii :
   forall {n} (f:'R^n.+1->R) i j (H: i<> ord_max),
   (forall (x:'R^n.+1) y, f x = f (replaceF x y ord_max)) ->
    (narrow_ord_max (Derive_multii i j f)) =
       Derive_multii (narrow_S H) j (narrow_ord_max f).
Proof.
intros n f i j H H1.
assert (Hi : (i < n)%coq_nat).
generalize H; destruct i as (ii, Hii); simpl.
intros T; apply ord_neq_compat in T; simpl in T.
assert (ii < n.+1)%coq_nat; [now apply /ltP | lia].
unfold narrow_ord_max, Derive_multii; fun_ext x; f_equal.
fun_ext y; f_equal.
extF m; unfold replaceF.
case (ord_eq_dec m i); intros Hm.
subst; unfold castF; rewrite concatF_correct_l; try easy.
case (ord_eq_dec _ _); try easy.
intros T; apply ord_neq_compat in T; simpl in T; easy.
unfold castF, concatF.
case (lt_dec _ _); intros H2; try easy.
case (ord_eq_dec _ _); try easy.
intros T; contradict T.
apply ord_neq; simpl; apply ord_neq_compat; easy.
unfold castF; rewrite concatF_correct_l; try easy.
f_equal; apply ord_inj; now simpl.
Qed.

Lemma DeriveF_part_scal_fun : forall {n} i (alpha:'nat^n.+1) (Hi: i<>ord_max) (g1:R->R) (g2: 'R^n.+1 -> R),
  (forall (x:'R^n.+1) y, g2 x = g2 (replaceF x y ord_max)) ->
  DeriveF_part alpha i (fun x:'R^n.+1 => g1 (x ord_max) * g2 x)
     = (fun x:'R^n.+1 => g1 (x ord_max)
          * DeriveF_part (widenF_S alpha) (narrow_S Hi) (narrow_ord_max g2) (widenF_S x)).
Proof.
intros n (i,Hi); induction i.
intros alpha Hi0 g1 g2 H1; fun_ext x.
rewrite DeriveF_part_0; try rewrite DeriveF_part_0; try easy; f_equal.
now apply narrow_ord_max_correct.
apply ord_inj; simpl; easy.
apply ord_inj; simpl; easy.
intros alpha Hi0 g1 g2 H1.
assert (Hi' : (i < n.+1)%nat).
apply /ltP; apply PeanoNat.lt_S_n.
now apply /ltP.
assert (Hi'' : (i < n)%nat).
apply /ltP; apply ord_neq_compat in Hi0; simpl in Hi0.
assert (i < n.+1)%coq_nat by now apply /ltP.
now auto with zarith.
assert (Hi0'' : (i <> n)).
generalize Hi''; move => /ltP; lia.
replace (@Ordinal (n.+2) (i.+1) Hi) with (lift_S (Ordinal Hi')) at 1.
2: apply ord_inj; easy.
rewrite DeriveF_part_ind_r comp_correct; unfold Derive_alp.
rewrite Derive_multii_scal_fun.
rewrite IHi.
apply ord_neq; simpl.
apply Nat.lt_neq; now apply /ltP.
intros H0; fun_ext x; f_equal.
replace (narrow_S Hi0) with (lift_S (Ordinal Hi'')).
2: apply ord_inj; simpl; easy.
rewrite DeriveF_part_ind_r comp_correct; f_equal; try easy.
apply ord_inj; simpl; easy.
rewrite narrow_ord_max_Derive_multii; try easy.
apply ord_neq; now simpl.
intros H2; unfold Derive_alp; f_equal; try easy.
apply ord_inj; now simpl.
unfold widenF_S; f_equal; apply ord_inj; now simpl.
intros x y; unfold Derive_multii; f_equal.
fun_ext z.
fold (replace2F x y z ord_max (Ordinal Hi')).
rewrite replace2F_equiv_def; try easy.
apply ord_neq; simpl; easy.
rewrite replaceF_correct_r; try easy.
apply ord_neq; simpl; easy.
intros x y; rewrite replaceF_correct_r; try easy.
apply ord_neq; simpl; apply sym_not_eq; easy.
Qed.

Lemma DeriveF_ind_r_scal_fun : forall {n} (alpha : 'nat^n.+1) f (g1:R->R) (g2: 'R^n.+1 -> R),
   (forall (x:'R^n.+1), Derive_alp alpha ord_max f x = g1 (x ord_max) * g2 x) ->
   (forall (x:'R^n.+1) y, g2 x = g2 (replaceF x y ord_max)) ->
   DeriveF alpha f = fun x => g1 (x ord_max) * DeriveF (widenF_S alpha) (narrow_ord_max g2) (widenF_S x).
Proof.
intros n alpha f g1 g2 H1 H2.
rewrite DeriveF_correct.
rewrite -lift_S_max.
rewrite DeriveF_part_ind_r comp_correct.
apply fun_ext_equiv in H1; rewrite H1.
rewrite DeriveF_part_scal_fun; try easy.
apply ord_neq; simpl; easy.
intros H; fun_ext; f_equal.
rewrite DeriveF_correct; f_equal.
apply ord_inj; easy.
Qed.

Definition is_Poly {n} (f: FRd n) := exists k, Pdk n k f.

Lemma is_Poly_ext : forall {n} (f1 f2 : FRd n),
  f1 = f2 -> is_Poly f1 -> is_Poly f2.
Proof.
intros; subst; easy.
Qed.

Lemma is_Poly_zero : forall {n}, @is_Poly n zero.
Proof.
intros n; exists O.
apply Pd0_eq; easy.
Qed.

Lemma is_Poly_scal : forall {n} l (f : FRd n),
    is_Poly f -> is_Poly (scal l f).
Proof.
intros n l f (k,Hk).
exists k; apply lin_span_scal_closed; easy.
Qed.

Lemma is_Poly_plus : forall {n} (f1 f2 : FRd n),
  is_Poly f1 -> is_Poly f2 -> is_Poly (f1+f2).
Proof.
intros n f1 f2 (k1,Hk1) (k2,Hk2).
exists (max k1 k2).
apply: lin_span_plus_closed.
apply Pdk_monot with k1; auto with arith.
apply Pdk_monot with k2; auto with arith.
Qed.

Lemma is_Poly_lc  : forall {n m} (f : 'I_m -> FRd n) L,
  (forall i, is_Poly (f i)) -> is_Poly (lin_comb L f).
Proof.
intros n m; induction m.
intros f L H.
rewrite lc_nil.
apply is_Poly_zero.
intros f L H.
rewrite lc_ind_l.
apply is_Poly_plus.
apply is_Poly_scal; easy.
apply IHm.
intros i; unfold liftF_S; apply H.
Qed.

Lemma is_Poly_Monom_dk : forall {d k} idk,
    is_Poly (Monom_dk d k idk).
Proof.
intros d k idk; exists k.
apply lin_span_inclF; intros jdk; now exists jdk.
Qed.

Lemma is_Poly_powF_P : forall {d} (beta : 'nat^d),
   is_Poly (powF_P beta).
Proof.
intros d beta.
pose (k:= sum beta).
apply is_Poly_ext with (Monom_dk d k (Adk_inv d k beta)).
2: apply is_Poly_Monom_dk.
unfold Monom_dk; rewrite Adk_inv_correct_r; easy.
Qed.

Lemma is_derive_powF_P : forall {n} (beta:'nat^n) i x,
    is_derive_onei i (powF_P beta) x
      (INR (beta i) * powF_P (replaceF beta (beta i).-1 i) x).
Proof.
intros n; induction n.
intros beta i; exfalso; apply I_0_is_empty; apply (inhabits i).
intros beta i x; unfold powF_P, is_derive_onei.
eapply is_derive_ext.
intros t; rewrite prod_R_ind_l; easy.
case (ord_eq_dec i ord0); intros Hi.
(* *)
subst; unfold liftF_S.
apply is_derive_ext with
  (fun t => (prod_R
      (fun i : 'I_n =>
       powF x beta (lift_S i)) * t^(beta ord0))).
intros t; unfold mult, plus; simpl; rewrite Rmult_comm; f_equal.
unfold powF, map2F; now rewrite replaceF_correct_l.
f_equal; extF i.
unfold powF, map2F; rewrite replaceF_correct_r; easy.
apply is_derive_ext' with ( (prod_R (fun i : 'I_n => powF x beta (lift_S i))) *
       (INR (beta ord0)*1*x ord0^(beta ord0).-1)).
rewrite mult_comm_R -mult_assoc mult_one_r.
unfold mult; simpl.
f_equal; rewrite (prod_R_ind_l); unfold mult, plus; simpl; f_equal.
unfold powF, map2F; rewrite replaceF_correct_l; try easy.
f_equal; extF i.
unfold powF, map2F, liftF_S.
rewrite replaceF_correct_r; easy.
apply is_derive_scal.
apply is_derive_pow.
apply is_derive_ext' with  (@one (AbsRing.Ring R_AbsRing)); try easy.
apply is_derive_id.
(* *)
apply is_derive_ext with
 (fun t => ( (x ord0)^(beta ord0) *
            (prod_R ((powF (replaceF (liftF_S x) t (lower_S Hi)) (liftF_S beta)))))).
intros t; unfold mult, plus; simpl; f_equal.
unfold powF, map2F; rewrite replaceF_correct_r; try easy.
f_equal; extF j.
unfold powF, map2F, liftF_S; f_equal.
unfold replaceF; case (ord_eq_dec _ _); intros Hj.
rewrite Hj  lift_lower_S; case (ord_eq_dec _ _); easy.
case (ord_eq_dec _ _); intros Hj'; try easy.
absurd (lift_S j = (lift_S (lower_S Hi))).
intros H; apply Hj.
unfold lift_S in H; apply (lift_inj H).
now rewrite lift_lower_S.
apply is_derive_ext' with ( (x ord0)^(beta ord0) *
         (INR (beta i) * prod_R (powF (liftF_S x)
                                (replaceF (liftF_S beta) (beta i).-1 (lower_S Hi))))).
rewrite mult_comm_R -mult_assoc; unfold mult; simpl; f_equal; rewrite Rmult_comm.
rewrite prod_R_ind_l; unfold mult, plus; simpl; f_equal.
unfold powF, map2F; rewrite replaceF_correct_r; try easy.
f_equal; extF j.
unfold powF, map2F, liftF_S; f_equal.
unfold replaceF; case (ord_eq_dec _ _); intros Hj.
rewrite Hj  lift_lower_S; case (ord_eq_dec _ _); easy.
case (ord_eq_dec _ _); intros Hj'; try easy.
absurd (lift_S j = (lift_S (lower_S Hi))).
intros H; apply Hj.
unfold lift_S in H; apply (lift_inj H).
now rewrite lift_lower_S.
apply is_derive_scal.
specialize (IHn (liftF_S beta) (lower_S Hi) (liftF_S x)).
unfold powF_P, is_derive_onei in IHn.
replace (liftF_S beta (lower_S Hi)) with (beta i) in IHn.
replace (liftF_S x (lower_S Hi)) with (x i) in IHn.
exact IHn.
unfold liftF_S; now rewrite lift_lower_S.
unfold liftF_S; now rewrite lift_lower_S.
Qed.

Lemma is_Poly_ex_derive : forall {n} (f: FRd n),
   is_Poly f -> (forall i x, ex_derive_onei i f x).
Proof.
intros n f (k,Hf) i x; destruct Hf.
apply ex_derive_onei_lc.
intros; unfold Monom_dk; eexists.
apply is_derive_powF_P.
Qed.

Lemma is_Poly_Derive_is_Poly : forall {n} (f: FRd n),
   is_Poly f -> (forall i, is_Poly (Derive_onei i f)).
Proof.
intros n; case n.
intros f H i.
exfalso; apply I_0_is_empty; apply (inhabits i).
clear n; intros n f (k,Hk) i; inversion Hk.
eapply is_Poly_ext.
apply eq_sym; fun_ext x.
apply is_derive_onei_unique.
apply is_derive_onei_lc.
intros jSnk y; unfold Monom_dk.
apply is_derive_powF_P.
exists k.
apply lin_span_lin_span; intros jSnk.
apply: lin_span_scal_closed.
apply lin_span_ub with
  (Adk_inv n.+1 k (replaceF (Adk n.+1 k jSnk) (Adk n.+1 k jSnk i).-1 i)).
unfold Monom_dk.
rewrite Adk_inv_correct_r; try easy.
apply Nat.add_le_mono_l with (Adk n.+1 k jSnk i).
replace (_ +_)%coq_nat with
 (Adk n.+1 k jSnk i + sum (replaceF (Adk n.+1 k jSnk) (Adk n.+1 k jSnk i).-1 i)) by easy.
rewrite sum_replaceF.
generalize (Adk_sum n.+1 k jSnk).
unfold plus; simpl; auto with zarith.
Qed.

Lemma is_Poly_Derive_n_is_Poly : forall {n} (f: FRd n),
   is_Poly f -> (forall i m, is_Poly (Derive_multii i m f)).
Proof.
intros n f H i; unfold Derive_multii; induction m; simpl; try easy.
apply is_Poly_ext with (2:=H).
fun_ext x.
now rewrite replaceF_id.
eapply is_Poly_ext.
2: apply is_Poly_Derive_is_Poly.
2: apply IHm.
fun_ext x; unfold Derive_onei.
f_equal; fun_ext y.
rewrite replaceF_correct_l; try easy.
f_equal; fun_ext z; f_equal.
unfold replaceF; extF j.
case (ord_eq_dec _ _); easy.
Qed.

Lemma is_Poly_ex_derive_n : forall {n} (f: FRd n),
   is_Poly f -> (forall i m x, ex_derive_multii i m f x).
Proof.
intros n f H i m x; unfold ex_derive_multii.
induction m; try easy; simpl.
assert (H1: ex_derive_onei i (Derive_multii i m f) x).
apply is_Poly_ex_derive.
now apply is_Poly_Derive_n_is_Poly.
apply ex_derive_ext with (2:=H1).
intros t; unfold ex_derive_onei, Derive_multii; f_equal.
fun_ext y; f_equal.
unfold replaceF; extF j.
case (ord_eq_dec _ _); easy.
now apply replaceF_correct_l.
Qed.

(* d^beta x^alpha = Prod_{i=0}^{d}
     (Prod_{j=0}^{beta_i - 1} (alpha_i - j)) x_i ^(alpha_i - beta_i). *)
Lemma Derive_multii_powF_P : forall {n} (A:'nat^n) i m, exists C,
  Derive_multii i m (powF_P A)
     = (fun x => C* powF_P (replaceF A (A i-m)%coq_nat i) x)
   /\ (C = 0 <-> (A i < m)%coq_nat).
Proof.
intros n A i; induction m.
exists 1; split.
fun_ext x; unfold Derive_multii, Derive_n; simpl.
rewrite mult_one_l; rewrite Nat.sub_0_r.
now rewrite replaceF_id replaceF_id.
unfold one, zero; simpl.
split; intros H; contradict H; auto with arith.
destruct IHm as (C,(H1,H2)).
exists (C * INR (A i - m)%coq_nat); split.
unfold Derive_multii; unfold Derive_multii in H1.
fun_ext x; simpl.
generalize (fun_ext_rev H1); intros H1'.
rewrite ->Derive_ext with
   (g:=fun z => C * powF_P (replaceF A (A i - m)%coq_nat i) (replaceF x z i)).
rewrite Derive_scal.
rewrite -mult_assoc; unfold mult; simpl; f_equal.
apply is_derive_unique.
eapply is_derive_ext'.
2: apply: is_derive_powF_P.
rewrite replaceF_correct_l; try easy; f_equal; try easy.
f_equal; unfold replaceF; extF j.
case (ord_eq_dec _ _); try easy.
intros _; auto with zarith.
intros t; rewrite -H1'.
rewrite replaceF_correct_l; try easy.
f_equal; fun_ext y; f_equal.
unfold replaceF; extF j.
case (ord_eq_dec _ _); easy.
split; intros H3.
case (Rmult_integral _ _ H3); intros H4.
specialize (proj1 H2 H4); auto with arith.
case (Nat.le_gt_cases m.+1 (A i)); try easy.
intros H5; contradict H4; apply not_0_INR.
assert (1 <= A i -m)%coq_nat; auto with zarith arith.
apply PeanoNat.Nat.le_add_le_sub_l; auto with zarith.
replace (A i -m)%coq_nat with O; auto with zarith real.
Qed.

Lemma DeriveF_powF_P : forall {n} (alpha B :'nat^n), exists C,
  DeriveF alpha (powF_P B)
     = (fun x => C* powF_P (fun i => (B i- alpha i)%coq_nat) x)
   /\ (C = 0 <-> brEF lt B alpha).
             (* (exists i, (B i < alpha i)%coq_nat)).*)
Proof.
intros n; induction n.
intros alpha B; exists 1; split.
rewrite DeriveF_nil; try easy.
fun_ext x; rewrite mult_one_l.
unfold powF_P; rewrite 2!prod_R_nil; easy.
unfold one, zero; simpl; split; intros H.
contradict H; easy.
exfalso; destruct H as (i,Hi).
exfalso; apply I_0_is_empty; apply (inhabits i).
(* *)
intros alpha B.
destruct (Derive_multii_powF_P B ord_max (alpha ord_max)) as
  (C1, (Y1,Z1)); fold (Derive_alp alpha ord_max) in Y1.
destruct (IHn (widenF_S alpha) (widenF_S B)) as (C2,(Y2,Z2)).
exists (C1*C2); split.
rewrite ->DeriveF_ind_r_scal_fun with
  (g1:= fun t => C1 * t^(B ord_max - alpha ord_max))
  (g2:= fun t => powF_P (widenF_S B) (widenF_S t)).
(* . *)
fun_ext x.
rewrite -2!mult_assoc; f_equal.
rewrite ->DeriveF_extf with (f2:=powF_P (widenF_S B)).
rewrite Y2.
rewrite mult_comm_R -mult_assoc; f_equal.
unfold powF_P; rewrite prod_R_ind_r.
unfold mult, plus; simpl; f_equal.
intros t; unfold narrow_ord_max, powF_P; f_equal; f_equal.
extF i; unfold widenF_S, castF.
rewrite concatF_correct_l; simpl; try easy.
apply /ltP; destruct i; easy.
intros H0; f_equal; apply ord_inj; now simpl.
(* . *)
intros x; rewrite Y1.
rewrite -mult_assoc; f_equal.
unfold powF_P; rewrite prod_R_ind_r.
unfold mult, plus; simpl; rewrite Rmult_comm; f_equal.
unfold powF, map2F; rewrite replaceF_correct_l; try easy.
f_equal; extF i.
unfold widenF_S, powF, map2F; rewrite replaceF_correct_r; try easy.
apply widen_S_not_last.
(* . *)
intros x y; f_equal; extF i.
unfold widenF_S; f_equal.
rewrite replaceF_correct_r; try easy.
apply widen_S_not_last.
(* *)
split; intros H.
case (Rmult_integral C1 C2 H); intros H'.
exists ord_max; apply Z1; easy.
destruct ((proj1 Z2) H') as (j,Hj).
exists (widen_S j); easy.
destruct H as (j,Hj).
case (ord_eq_dec j ord_max); intros H'.
replace C1 with (0 : R). apply: mult_zero_l.
apply eq_sym, Z1; subst; easy.
replace C2 with (0 : R). apply: mult_zero_r.
apply eq_sym, Z2.
exists (narrow_S H').
unfold widenF_S; rewrite widen_narrow_S; easy.
Qed.

Lemma Derive_alp_scal :
  forall {n} (alpha : 'nat^n) i, f_scal_compat (Derive_alp alpha i).
Proof. move=>>; fun_ext; apply Derive_n_scal_l. Qed.

Lemma DeriveF_part_scal :
  forall {n} (alpha : 'nat^n) j, f_scal_compat (DeriveF_part alpha j).
Proof. intros; apply comp_n_part_f_scal_compat, Derive_alp_scal. Qed.

Lemma DeriveF_zero : forall {n} (alpha:'nat^n),
     DeriveF alpha zero = zero.
Proof.
intros n; induction n.
intros alpha; unfold DeriveF.
rewrite comp_n_nil; easy.
intros alpha.
rewrite ->DeriveF_ind_r_scal_fun with (g1:=zero) (g2:=zero); try easy.
fun_ext x.
rewrite fct_zero_eq; unfold mult, fct_zero, zero ; simpl.
rewrite Rmult_0_l; easy.
intros x; unfold Derive_alp, Derive_multii.
rewrite fct_zero_eq mult_zero_l.
rewrite ->Derive_n_ext with (g:=fun=> 0); try easy.
case (alpha ord_max); try easy.
intros m; apply Derive_n_const.
Qed.

Lemma DeriveF_scal : forall {n} (alpha:'nat^n) l f,
     DeriveF alpha (scal l f) = scal l (DeriveF alpha f).
Proof.
intros n alpha l f; unfold DeriveF.
apply comp_n_f_scal_compat.
intros i y g; unfold Derive_alp; fun_ext.
rewrite Derive_multii_scal; easy.
Qed.

Lemma DeriveF_plus : forall {n} (alpha:'nat^n) f1 f2,
   is_Poly f1 ->  is_Poly f2 ->
     DeriveF alpha (f1+f2) = (DeriveF alpha f1 + DeriveF alpha f2).
Proof.
intros n alpha f1 f2 H1 H2; unfold DeriveF.
assert (H: f_plus_compat_sub is_Poly (comp_n (Derive_alp alpha))).
2: apply H; easy.
apply comp_n_f_plus_compat_sub.
intros i g (g1,Hg1); unfold Derive_alp.
apply is_Poly_Derive_n_is_Poly; try easy.
intros i g1 g2 Hg1 Hg2.
unfold Derive_alp, Derive_multii.
fun_ext x.
rewrite Derive_n_plus; try easy.
exists (mkposreal 1 Rlt_0_1); intros y Hy k Hk.
generalize (is_Poly_ex_derive_n g1 Hg1 i k (replaceF x y i)); unfold ex_derive_multii.
rewrite replaceF_correct_l; try easy.
intros T; apply: (ex_derive_n_ext _ (fun x0 : R => g1 (replaceF x x0 i))).
2: apply T.
intros t; simpl; unfold replaceF; f_equal; extF i0.
case (ord_eq_dec i0 i); try easy.
exists (mkposreal 1 Rlt_0_1); intros y Hy k Hk.
generalize (is_Poly_ex_derive_n g2 Hg2 i k (replaceF x y i)); unfold ex_derive_multii.
rewrite replaceF_correct_l; try easy.
intros T; apply: (ex_derive_n_ext _ (fun x0 : R => g2 (replaceF x x0 i))).
2: apply T.
intros t; simpl; unfold replaceF; f_equal; extF i0.
case (ord_eq_dec i0 i); try easy.
Qed.

Lemma DeriveF_lc_Poly : forall {n m} (alpha:'nat^n) f (L:'R^m),
   (forall i, is_Poly (f i)) ->
    DeriveF alpha (lin_comb L f) = lin_comb L (fun i => DeriveF alpha (f i)).
Proof.
intros n m; induction m.
intros alpha f L H.
rewrite 2!lc_nil.
apply DeriveF_zero.
intros alpha f L H.
rewrite lc_ind_l.
rewrite DeriveF_plus.
rewrite DeriveF_scal.
rewrite IHm.
now rewrite lc_ind_l.
intros i; apply H; easy.
now apply is_Poly_scal.
apply is_Poly_lc.
intros i; unfold liftF_S; apply H.
Qed.

Lemma Monom_dk_lin_indep_aux1 : forall d k alpha idk,
     alpha = Adk d k idk ->
    DeriveF alpha (Monom_dk d k idk) zero <> zero.
Proof.
intros d k alpha idk H; rewrite H; unfold Monom_dk.
destruct (DeriveF_powF_P (Adk d k idk) (Adk d k idk)) as (C,(HC1,HC2)).
rewrite HC1.
rewrite powF_P_zero_compat_r.
rewrite mult_one_r; unfold zero; simpl.
intros T; apply HC2 in T; destruct T as (j,Hj).
contradict Hj; auto with arith.
extF j; rewrite constF_correct; auto with arith.
Qed.

Lemma Monom_dk_lin_indep_aux2 : forall d k alpha idk,
    alpha <> Adk d k idk ->
    DeriveF alpha (Monom_dk d k idk) zero = zero.
Proof.
intros d k alpha idk H; unfold Monom_dk.
destruct (DeriveF_powF_P alpha (Adk d k idk)) as (C,(HC1,HC2)).
rewrite HC1; unfold zero at 2; simpl.
apply nextF_rev in H; destruct H as (j,Hj); unfold neq in Hj.
apply not_eq in Hj; destruct Hj.
(* . *)
rewrite powF_P_zero_compat. apply: mult_zero_r.
exists j; unfold powF, map2F.
unfold zero; simpl.
apply eq_sym, pow_i; auto with zarith.
(* . *)
rewrite (proj2 HC2). apply: mult_zero_l.
now exists j.
Qed.

End Derive_Gen.


Section Pdk_Facts2.

Context {d : nat}.
Variable k : nat.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1523, p. 75. *)
Lemma Monom_dk_lin_indep : lin_indep (Monom_dk d k).
Proof.
intros L HL1; extF idk; rewrite zeroF.
assert (HL2 : DeriveF (Adk d k idk) (lin_comb L (Monom_dk d k)) zero =
                DeriveF (Adk d k idk) zero zero) by now f_equal.
rewrite DeriveF_zero fct_zero_eq DeriveF_lc_Poly in HL2;
    [| apply is_Poly_Monom_dk].
rewrite fct_lc_r_eq (lc_one_r idk) in HL2.
destruct (is_integral_contra_R _ _ HL2) as [H | H]; [easy |].
contradict H; apply Monom_dk_lin_indep_aux1; easy.
extF; rewrite zeroF; unfold skipF.
apply Monom_dk_lin_indep_aux2.
move=> /Adk_inj; apply not_eq_sym, skip_ord_correct_m.
Qed.

Lemma Monom_dk_lin_gen : lin_gen (Pdk d k) (Monom_dk d k).
Proof. easy. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1524, pp. 75-76. *)
Lemma Monom_dk_basis : basis (Pdk d k) (Monom_dk d k).
Proof. apply basis_lin_span_equiv, Monom_dk_lin_indep. Qed.

(**
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Lem 1525, p. 76. *)
Lemma Pdk_has_dim : has_dim (Pdk d k) (pbinom d k).+1.
Proof. apply has_dim_lin_span, Monom_dk_lin_indep. Qed.

End Pdk_Facts2.
