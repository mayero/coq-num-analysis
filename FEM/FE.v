(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Bibliography

 #<DIV><A NAME="Ern"></A></DIV>#
 [[Ern]]
 Alexandre Ern, Éléments finis, Aide-mémoire, Dunod/L'Usine Nouvelle, 2005,
 #<A HREF="https://www.dunod.com/sciences-techniques/aide-memoire-elements-finis">#
 https://www.dunod.com/sciences-techniques/aide-memoire-elements-finis#</A>#.

 #<DIV><A NAME="ErnGuermond"></A></DIV>#
 [[ErnGuermond]]
 Alexandre Ern and Jean-Luc Guermond,
 Finite Elements I. Approximation and Interpolation,
 Texts in Applied Mathematics, vol. 72, Springer Cham, 2021,
 #<A HREF="https://doi.org/10.1007/978-3-030-56341-7">#
 https://doi.org/10.1007/978-3-030-56341-7#</A>#,#<BR>#
 #<A HREF="https://inria.hal.science/hal-03226049">#
 https://inria.hal.science/hal-03226049#</A># (with different page numbers).

 #<DIV><A NAME="RR9557v1"></A></DIV>#
 [[RR9557v1]]
 François Clément and Vincent Martin,
 Finite element method. Detailed proofs to be formalized in Coq,
 RR-9557, first version, Inria Paris, 2024,
 #<A HREF="https://inria.hal.science/hal-04713897v1">#
 https://inria.hal.science/hal-04713897v1#</A>#.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Algebra Require Import Algebra_wDep.
From FEM Require Import geometry.

Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.


Section FE_Def.

Inductive shape_type :=  Simplex | Quad.

(* Note that notation '^' corresponds to [expn] from mathcomp. *)
Definition nvtx_of_shape (d : nat) (shape : shape_type) : nat :=
  match shape with
    | Simplex => d.+1
    | Quad => Nat.pow 2 d
  end.

Lemma nos_eq :
  forall {d1 d2 s1 s2},
    d1 = d2 -> s1 = s2 -> nvtx_of_shape d1 s1 = nvtx_of_shape d2 s2.
Proof. move=>>; apply f_equal2. Qed.

(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!
  TODO: add an admissibility property for the geometrical element?
  Il manque peut-être des hypothèses géométriques
    du type triangle pas plat ou dim (span_as vertex) = d.
  Se mettre à l'interieur ou à l'exterieur de record FE + dans geometry.v aussi ?
  Manque hyp géométrique ds le cas du Quad pour éviter les sabliers *)

(**
 #<A HREF="##Ern">#[[Ern]]#</A>#
 Def 4.1, p. 75.#<BR>#
 #<A HREF="##ErnGuermond">#[[ErnGuermond]]#</A>#
 Def 5.2, p. 51.#<BR>#
 #<A HREF="##RR9557v1">#[[RR9557v1]]#</A>#
 Def 1424, p. 53. *)
Record FE (d : nat) : Type := mk_FE {
  shape : shape_type;
  nvtx : nat := nvtx_of_shape d shape;
  vertices : 'R^{nvtx,d};
  K_geom : 'R^d -> Prop := convex_envelop vertices; (* geometric element K *)
  ndof : nat ;
  P_approx : FRd d -> Prop; (* approximation space P *)
  P_approx_has_dim : has_dim P_approx ndof;
  Sigma : '(FRd d -> R)^ndof ; (* degrees of freedom Sigma *)
  Sigma_lm : forall i, lin_map (Sigma i);
  unisolvence_inj : KerS0 P_approx (gather Sigma);
}.

End FE_Def.

(* Specify implicit arguments for the record constructor and accessors.
 This must occur out of the Section. *)
Arguments mk_FE {d} _ _ _ {P_approx} _ {Sigma} _ _.
Arguments shape {d}.
Arguments nvtx {d}.
Arguments vertices {d}.
Arguments K_geom {d}.
Arguments ndof {d}.
Arguments P_approx {d}.
Arguments P_approx_has_dim {d}.
Arguments Sigma {d}.
Arguments Sigma_lm {d}.
Arguments unisolvence_inj {d}.


Section FE_Facts0.

Lemma unisolvence :
  forall {d} (fe : FE d), bijS (P_approx fe) fullset (gather (Sigma fe)).
Proof.
intros d fe; apply lmS_bijS_KerS0 with (ndof fe) (ndof fe); try easy.
apply P_approx_has_dim.
apply has_dim_Rn.
apply gather_lm_compat, Sigma_lm.
apply unisolvence_inj.
Qed.

Lemma FE_ext :
  forall {d} {fe1 fe2 : FE d}
      (Hs : shape fe1 = shape fe2) (Hn : ndof fe1 = ndof fe2),
    vertices fe1 = castF (nos_eq eq_refl (eq_sym Hs)) (vertices fe2) ->
    P_approx fe1 = P_approx fe2 -> Sigma fe1 = castF (eq_sym Hn) (Sigma fe2) ->
    fe1 = fe2.
Proof.
intros d fe1 fe2 Hs Hn; destruct fe1, fe2; simpl in *; subst.
rewrite 2!castF_id; intros; subst; f_equal; apply proof_irrel.
Qed.

Lemma cast_FE : forall {d1 d2}, d1 = d2 -> FE d1 -> FE d2.
Proof. intros; subst; easy. Qed.

End FE_Facts0.


Section FE_degenerate_ndof.

Context {d : nat}.

(**
 When ndof = 0,
 [has_dim_0_rev] provides [P_approx = zero_sub_struct := singleton 0],
 [hat0F_eq] provides [Sigma = 0].
*)

Lemma FE_ndof_0_P_approx_eq :
  forall {fe : FE d}, ndof fe = 0 -> P_approx fe = zero_sub_struct.
Proof.
move=>> Hn; apply: has_dim_0_rev; apply (has_dim_ext _ Hn), P_approx_has_dim.
Qed.

Lemma FE_ndof_0_Sigma_eq : forall {fe : FE d}, ndof fe = 0 -> Sigma fe = 0.
Proof. move=>> Hn; apply (hat0F_eq _ Hn). Qed.

Variable shp : shape_type.
Let nvtx := nvtx_of_shape d shp.
Variable vtx : 'R^{nvtx,d}.
Let P_approx_ndof_0 : FRd d -> Prop := zero_sub_struct.
Let Sigma_ndof_0 : '(FRd d -> R)^0 := 0.

Lemma Sigma_lm_ndof_0 : forall i, lin_map (Sigma_ndof_0 i).
Proof. intros [i Hi]; easy. Qed.

Lemma unisolvence_inj_ndof_0 : KerS0 P_approx_ndof_0 (gather Sigma_ndof_0).
Proof. easy. Qed.

Definition FE_ndof_0 : FE d :=
  mk_FE shp vtx 0 has_dim_0 Sigma_lm_ndof_0 unisolvence_inj_ndof_0.

Lemma FE_ndof_0_eq :
  forall (fe : FE d) (Hs : shape fe = shp),
    vertices fe = castF (nos_eq erefl (eq_sym Hs)) vtx -> ndof fe = 0 ->
    fe = FE_ndof_0.
Proof.
intros; apply: FE_ext; [easy |..].
rewrite !FE_ndof_0_P_approx_eq; easy.
rewrite !FE_ndof_0_Sigma_eq; easy.
Qed.

End FE_degenerate_ndof.


Section FE_degenerate_ndof_uniq.

Context {d : nat}.

Lemma FE_ndof_0_uniq :
  forall (fe1 fe2 : FE d) (Hs : shape fe1 = shape fe2),
    vertices fe1 = castF (nos_eq erefl (eq_sym Hs)) (vertices fe2) ->
    ndof fe1 = 0 -> ndof fe2 = 0 ->
    fe1 = fe2.
Proof.
intros fe1 fe2 Hs Hv Hn1 Hn2.
pose (FE_eq := FE_ndof_0_eq (shape fe2) (vertices fe2)).
rewrite (FE_eq fe1)// (FE_eq fe2)//.
rewrite castF_id; easy.
Qed.

End FE_degenerate_ndof_uniq.


Section FE_degenerate_d.

(**
 When d = 0,
 - [hatm0T_eq] provides [vertices = 0],
 - [FR0_lin_gen] states that ([FRd] 0) is generated by the constant function
                 of value 1,
 - [FR0_has_dim] states dim [FRd] 0 = 1,
 - [FR0_sub_dim] provides [ndof] <> 0 -> [ndof] = 1,
 - [FR0_sub_eq] provides [ndof] <> 0 -> [P_approx] = [fullset],

 - [FR0_dual_lin_gen] states that the dual of ([FRd] 0) is generated by the
                      linear form evaluating (constant) functions at 0,
 - [FR0_dual_ext] states that two linear forms on ([FRd] 0) are equal if they
                  are equal on the constant function 1.
*)

Lemma FE_d_0_vertices_eq : forall {fe : FE 0}, vertices fe = 0.
Proof. intros fe; rewrite (hatm0T_eq (vertices _)); easy. Qed.

Lemma FE_d_0_ndof_n0 : forall {fe : FE 0} (i : 'I_(ndof fe)), ndof fe <> 0.
Proof. move=>> [i Hi] H; rewrite H in Hi; easy. Qed.

Lemma FE_d_0_ndof_eq : forall {fe : FE 0} (i : 'I_(ndof fe)), ndof fe = 1%nat.
Proof.
move=>> /FE_d_0_ndof_n0 Hn; apply: (FR0_sub_dim _ _ Hn);
    [apply eq_refl | apply P_approx_has_dim].
Qed.

Lemma FE_d_0_P_approx_eq :
  forall {fe : FE 0}, ndof fe <> 0 -> P_approx fe = fullset.
Proof. move=>> Hn; apply: (FR0_sub_eq _ (P_approx_has_dim _) Hn); easy. Qed.

Lemma FE_d_0_Sigma_ext :
  forall {fe1 fe2 : FE 0}
      (Hn2 : ndof fe2 = 1%nat) (Hn : ndof fe1 = ndof fe2) i1 i2,
    Sigma fe1 i1 1 = Sigma fe2 i2 1 ->
    Sigma fe1 = castF (eq_sym Hn) (Sigma fe2).
Proof.
intros fe1 fe2 Hn2 Hn i1 i2 HSig.
apply (eqAF_1 _ i1); [left; easy |]; unfold castF.
apply FR0_dual_ext; [easy | apply Sigma_lm.. |]; rewrite HSig; f_equal.
apply I_1_unit; easy.
Qed.

(** (* TODO FC (21/02/2025): actually unused! *)
 If there is a degree of freedom, ie [ndof <> 0], the unisolvence property
 implies that any degree of freedom is nonzero on the constant function 1.
 In fact, there is only one degree of freedom, but the size [ndof] is not
 structurally nonzero, thus to avoid casts, next lemma is stated for any index.
*)
Lemma FE_d_0_Sigma_n0 : forall {fe : FE 0} i, Sigma fe i 1 <> 0.
Proof.
intros fe i Hi; move: (FE_d_0_ndof_n0 i) => Hn0;
    move: (FR0_sub_dim eq_refl (P_approx_has_dim fe) Hn0) => Hn1.
move: (unisolvence_inj fe) => H; contradict H; apply KerS0_gather_contra.
exists 1; repeat split.
rewrite (FR0_sub_eq _ (P_approx_has_dim _)); easy.
intros j; rewrite (I_1_unit i Hn1 j); easy.
apply: fct_one_not_zero_R; apply inhabited_m.
Qed.

Variable shp : shape_type.
Let P_approx_d_0 := fullset : FRd 0 -> Prop.
Context {coeff : R}.
Hypothesis Hc : coeff <> 0.
Let Sigma_d_0 := singleF (scal coeff (fun f : FRd 0 => f 0)).

Lemma P_approx_has_dim_d_0 : has_dim P_approx_d_0 1.
Proof. apply FR0_has_dim; easy. Qed.

Lemma Sigma_lm_d_0 : forall i, lin_map (Sigma_d_0 i).
Proof. intro; apply: lm_fct_scal; apply lm_pt_eval. Qed.

Lemma unisolvence_inj_d_0 : KerS0 P_approx_d_0 (gather Sigma_d_0).
Proof.
assert (Hc' : invertible coeff) by now apply invertible_equiv_R.
intros; apply KerS0_gather_equiv; intros p _; apply (spec_hyp ord0).
unfold Sigma_d_0; rewrite singleF_0 fct_scal_eq (FR0_eq _ p)//.
move=> /(scal_zero_reg_r _ _ Hc') ->; easy.
Qed.

Definition FE_d_0 : FE 0 :=
  mk_FE shp 0 1 P_approx_has_dim_d_0 Sigma_lm_d_0 unisolvence_inj_d_0.

Lemma FE_d_0_eq :
  forall (fe : FE 0),
    shape fe = shp -> (exists i, Sigma fe i 1 = coeff) -> fe = FE_d_0.
Proof.
move=> fe Hs [i Hi]; move: (FE_d_0_ndof_eq i) => Hn; apply FE_ext with Hs Hn;
    [apply FE_d_0_vertices_eq | rewrite !FE_d_0_P_approx_eq// Hn; easy |].
assert (Hn' : ndof fe = ndof FE_d_0) by easy.
apply: (FE_d_0_Sigma_ext _ _ i (cast_ord Hn' i)); [easy.. |]; rewrite Hi.
simpl; unfold Sigma_d_0; rewrite singleF_0 fct_scal_eq scal_one_r; easy.
Qed.

End FE_degenerate_d.


Section FE_degenerate_d_uniq.

Lemma FE_d_0_uniq :
  forall (fe1 fe2 : FE 0) (c : R),
    shape fe1 = shape fe2 -> c <> 0 ->
    (exists i : 'I_(ndof fe1), Sigma fe1 i 1 = c) ->
    (exists i : 'I_(ndof fe2), Sigma fe2 i 1 = c) ->
    fe1 = fe2.
Proof.
intros fe1 fe2 c Hs Hc HSig1 HSig2.
pose (FE_eq := FE_d_0_eq (shape fe2) Hc).
rewrite (FE_eq fe1)// (FE_eq fe2)//.
Qed.

End FE_degenerate_d_uniq.


Section FE_Facts1.

Context {d : nat}.
Variable fe : FE d.

Lemma shape_dec : { shape fe = Simplex } + { shape fe = Quad }.
Proof. case shape; [left | right]; easy. Qed.

Lemma nvtx_dec : { nvtx fe = d.+1 } + { nvtx fe = Nat.pow 2 d }.
Proof. unfold nvtx; case shape; [left | right]; easy. Qed.

Lemma nvtx_Simplex : shape fe = Simplex -> nvtx fe = d.+1.
Proof. unfold nvtx; case shape; [easy | discriminate]. Qed.

Lemma nvtx_Quad : shape fe = Quad -> nvtx fe = Nat.pow 2 d.
Proof. unfold nvtx; case shape; [discriminate | easy]. Qed.

Lemma nvtx_pos : (0 < nvtx fe)%coq_nat.
Proof.
case nvtx_dec; move=> ->; [apply S_pos | apply pow_pos, Nat.lt_0_2].
Qed.

Definition is_predual_dof : '(FRd d)^(ndof fe) -> Prop :=
  fun theta => inclF theta (P_approx fe) /\ dualF theta (Sigma fe).

(** [is_predual_dof] is injective. *)
Lemma is_predual_dof_uniq :
  forall theta1 theta2,
    is_predual_dof theta1 -> is_predual_dof theta2 -> theta1 = theta2.
Proof.
intros theta1 theta2 [H1a H1b] [H2a H2b];
    apply: (predual_basis_uniq _ _ _ H1a H2a H1b H2b).
apply P_approx_has_dim.
apply gather_lm_compat, Sigma_lm.
apply unisolvence_inj.
Qed.

(**
 #<A HREF="##Ern">#[[Ern]]#</A>#
 p. 76.#<BR>#
 #<A HREF="##ErnGuermond">#[[ErnGuermond]]#</A>#
 Prop 5.5, pp. 51-52. *)
Definition shape_fun : '(FRd d)^(ndof fe) := predual_basis (unisolvence fe).

Lemma shape_fun_correct : is_predual_dof shape_fun.
Proof. split; [apply predual_basis_in_sub | apply predual_basis_dualF]. Qed.

Lemma shape_fun_basis : basis (P_approx fe) shape_fun.
Proof.
apply predual_basis_basis; [apply fe | apply gather_lm_compat, Sigma_lm].
Qed.

Lemma shape_fun_lin_gen : lin_gen (P_approx fe) shape_fun.
Proof. apply shape_fun_basis. Qed.

(**
 #<A HREF="##Ern">#[[Ern]]#</A>#
 Def 4.4, p. 78. *)
Definition local_interp : FRd d -> FRd d :=
  fun f => lin_comb (Sigma fe ^~ f) shape_fun.

Lemma local_interp_is_poly : forall f, P_approx fe (local_interp f).
Proof. intro; rewrite shape_fun_lin_gen; easy. Qed.

Lemma local_interp_lm : lin_map local_interp.
Proof. apply (fct_lc_l_lm shape_fun), Sigma_lm. Qed.

Lemma local_interp_shape_fun :
  forall i, local_interp (shape_fun i) = shape_fun i.
Proof.
intros j; unfold local_interp; rewrite (lc_ext_l (kronR^~ j));
    [apply lc_kron_l_in_l | intro; apply shape_fun_correct].
Qed.

(**
 #<A HREF="##Ern">#[[Ern]]#</A>#
 Eq (4.6), p. 78. *)
Lemma local_interp_proj : forall p, P_approx fe p -> local_interp p = p.
Proof.
intros p; rewrite shape_fun_lin_gen => Hp; inversion Hp.
rewrite lm_lc; [| apply local_interp_lm].
apply lc_ext_r, local_interp_shape_fun.
Qed.

(**
 #<A HREF="##Ern">#[[Ern]]#</A>#
 p. 78. *)
Lemma local_interp_idem :
  forall f, local_interp (local_interp f) = local_interp f.
Proof. intros; apply local_interp_proj, local_interp_is_poly. Qed.

(**
 #<A HREF="##Ern">#[[Ern]]#</A>#
 Eq (4.4), p. 77. *)
Lemma Sigma_local_interp :
  forall f i, Sigma fe i (local_interp f) = Sigma fe i f.
Proof.
intros f i; unfold local_interp.
rewrite (lm_lc (Sigma_lm fe i)) lc_comm_R (lc_ext_l (kronR i)).
rewrite lc_kron_l_in_r; easy.
apply shape_fun_correct.
Qed.

End FE_Facts1.

Arguments nvtx_Simplex {d fe}.
Arguments nvtx_Quad {d fe}.

Lemma shape_fun_ext :
  forall {d : nat} {fe1 fe2 : FE d} (H : fe1 = fe2),
    shape_fun fe1 = castF (eq_sym (f_equal ndof H)) (shape_fun fe2).
Proof. intros; subst; rewrite castF_id; easy. Qed.
