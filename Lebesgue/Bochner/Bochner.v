(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Exports support for the Bochner integral.

 * Description

 ** Support for the Bochner integral of simple functions to Banach spaces

 See [Lebesgue.Bochner.hierarchy_notations], [Lebesgue.Bochner.simpl_fun],
 [Lebesgue.Bochner.BInt_sf], and [Lebesgue.Bochner.Bsf_Lsf].

 ** Support for the Bochner integral of functions to Banach spaces

 See [Lebesgue.Bochner.Bi_fun], [Lebesgue.Bochner.BInt_Bif],
 [Lebesgue.Bochner.BInt_LInt_p], [Lebesgue.Bochner.BInt_R], and
 [Lebesgue.Bochner.B_spaces].

 * Usage

 This module is meant to be imported after all imports from the Coq standard
 library, from the Mathematical Components library, and from other external Coq
 libraries such as Coquelicot and Flocq.

 This module may also be used through the import of
 [Lebesgue.Bochner.Bochner_wDep], where it is exported.

 The exported modules below redefine some notations, thus it is convenient
 to deactivate the 'notation-overridden' and 'hiding-delimiting-key' warning
 options of the Coq compiler, e.g. in the <<_CoqProject>> file.
*)

From Lebesgue Require Export hierarchy_notations simpl_fun BInt_sf Bsf_Lsf.
From Lebesgue Require Export Bi_fun BInt_Bif BInt_LInt_p BInt_R B_spaces.
