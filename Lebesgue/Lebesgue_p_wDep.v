(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Exports complements about logic and numbers for numerical analysis, and
 support for subsets, functions, and Lebesgue integration of nonnegative
 functions.

 * Description

 ** Complements about logic and numbers, and support for subsets, functions

 See [Lebesgue.Subsets_wDep].

 ** Support for Lebesgue integration of nonnegative measurable functions.

 See [Lebesgue.Lebesgue_p].

 * Usage

 This module is meant to be imported after all imports from the Coq standard
 library, from the Mathematical Components library, and from other external Coq
 libraries such as Coquelicot and Flocq.

 This module may also be used through the import of
 [Lebesgue.Bochner.Bochner_wDep], where it is exported.
*)

From Subsets Require Export Subsets_wDep.
From Lebesgue Require Export Lebesgue_p.
