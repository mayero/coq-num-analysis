(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Exports support for Lebesgue integration of nonnegative functions.

 * Description

 ** Additional support for (extended) real numbers

 It is based on extended real numbers provided by the Coquelicot library.

 See [Lebesgue.R_compl], [Lebesgue.Rbar_compl], and [Lebesgue.sum_Rbar_nonneg].

 ** Support for measurability of subsets and functions

 See [Lebesgue.sigma_algebra], [Lebesgue.sigma_algebra_R_Rbar], and
 [Lebesgue.measurable_fun].

 ** Support for measures

 See [Lebesgue.measure], [Lebesgue.measure_uniq], and [Lebesgue.measure_R].

 ** Support for Lebesgue integration of nonnegative measurable functions

 See [Lebesgue.simple_fun], [Lebesgue.Mp], [Lebesgue.LInt_p], and
 [Lebesgue.Tonelli].

 * Usage

 This module is meant to be imported after all imports from the Coq standard
 library, from the Mathematical Components library, and from other external Coq
 libraries such as Coquelicot and Flocq.

 This module may also be used through the import of [Lebesgue.Lebesgue_p_wDep],
 or [Lebesgue.Bochner.Bochner_wDep], where it is exported.
*)

From Lebesgue Require Export R_compl Rbar_compl sum_Rbar_nonneg.
From Lebesgue Require Export sigma_algebra sigma_algebra_R_Rbar measurable_fun.
From Lebesgue Require Export measure simple_fun Mp LInt_p Tonelli.
From Lebesgue Require Export measure_uniq measure_R.
