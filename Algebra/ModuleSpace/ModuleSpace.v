(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Exports additional support for module spaces.

 * Description

 It is based on the hierarchy of algebraic structures provided by the
 Coquelicot library.

 Provides:
 - operators for finite families/tables on module spaces;
 - support for linear combinations;
 - support for linear mapping (aka module space morphisms);
 - embryonic support for bilinear mapping;
 - support for module subspaces.

 * Usage

 This module is meant to be imported after all imports from the Coq standard
 library, from the Mathematical Components library, and from other external Coq
 libraries such as Coquelicot and Flocq.

 This module may be used through the import of [Algebra.Algebra], or
 [Algebra.Algebra_wDep], where it is exported.
*)

From Algebra Require Export ModuleSpace_compl ModuleSpace_FF_FT.
From Algebra Require Export ModuleSpace_lin_comb ModuleSpace_lin_map.
From Algebra Require Export ModuleSpace_sub ModuleSpace_R_compl.
