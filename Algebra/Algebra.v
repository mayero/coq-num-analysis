(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Exports additional support for algebraic structures.

 * Description

 It is based on the hierarchy of algebraic structures provided by the
 Coquelicot library, and on binomial coefficients from the Mathematical
 Components library.

 Provides:
 - support for functions towards an algebraic structure;
 - generic support for algebraic substructures;
 - complements on commutative monoids;
 - complements on abelian groups;
 - complements on rings;
 - complements on module spaces;
 - basic support for matrices;
 - support for abstract affine spaces;
 - support for finite dimensional module spaces and affine spaces;
 - complements on binomial coefficients.

 * Usage

 This module is meant to be imported after all imports from the Coq standard
 library, from the Mathematical Components library, and from other external Coq
 libraries such as Coquelicot and Flocq.

 This module may also be used through the import of [Algebra.Algebra_wDep],
 where it is exported.
*)

From Algebra Require Export Hierarchy_compl Sub_struct.
From Algebra Require Export Monoid Group Ring ModuleSpace AffineSpace.
From Algebra Require Export Finite_dim.
From Algebra Require Export binomial_compl.
