(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Complements on the ring algebraic structure.

 * Description

 The [Ring] algebraic structure is defined in the Coquelicot library, including
 the canonical structure [prod_Ring] for the cartesian product of rings.

 Results needing a commutative Ring are only stated for the ring of
 real numbers R_Ring.

 ** Additional definitions

 Let [K : Ring].
 Let [x y : K].
 - [is_inverse x y] states that [y] is the left and right inverse of [x];
 - [invertible x] states that [x] has a left and right inverse;
 - [inv x] is the inverse of [x] if it is invertible, and [0] otherwise;
 - [div] is the multiplication by the inverse.

 - notation [/ x] is for [inv x];
 - notation [x / y] is for [div x y].

 - [is_comm K] states that [mult] is commutative, ie [K] is a commutative ring;
 - [is_integral K] states that multiplication of nonzero elements is nonzero;
 - [has_inv K] states that all nonzero elements are invertible;
   ie have an inverse;
 - [is_division K] states that [K] is a division ring, ie is not reduced to
   [zero] and that all nonzero elements have an inverse;
 - [is_field K] states that [K] is a field.

 * Used logic axioms

 - [classic], the weak form of excluded middle;
 - [classic_dec], an alias for [excluded_middle_informative],
   the strong form of excluded middle;
 - [ex_EX], an alias for [constructive_indefinite_description].

 * Usage

 This module may be used through the import of [Algebra.Ring.Ring],
 [Algebra.Algebra], or [Algebra.Algebra_wDep], where it is exported.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Subsets Require Import Subsets_wDep.
From Algebra Require Import Hierarchy_compl Monoid Group.

Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.


Section Ring_Def1.

Context {K : Ring}.

Definition is_inverse (x y : K) : Prop := y * x = 1 /\ x * y = 1.

Lemma is_inverse_comm : forall x y, is_inverse x y -> is_inverse y x.
Proof. move=>> [H1 H2]; easy. Qed.

Lemma is_inverse_compat_l :
  forall x1 x2 y, x1 = x2 -> is_inverse x1 y -> is_inverse x2 y.
Proof. intros; subst; easy. Qed.

Lemma is_inverse_compat_r :
  forall y1 y2 x, y1 = y2 -> is_inverse x y1 -> is_inverse x y2.
Proof. intros; subst; easy. Qed.

Lemma is_inverse_uniq :
  forall x y1 y2, is_inverse x y1 -> is_inverse x y2 -> y1 = y2.
Proof.
intros x y1 y2 [Hy1 _] [_ Hy2].
rewrite -(mult_one_r y1) -Hy2 mult_assoc Hy1 mult_one_l; easy.
Qed.

Inductive invertible (x : K) : Prop :=
  | Invertible : forall y, is_inverse x y -> invertible x.

Lemma invertible_ext :
  forall {x} (y : K), x = y -> invertible x <-> invertible y.
Proof. intros; subst; easy. Qed.

Lemma invertible_ex : forall {x}, (exists y, is_inverse x y) -> invertible x.
Proof. intros x [y Hy]; apply (Invertible _ _ Hy). Qed.

Lemma invertible_EX : forall {x}, invertible x -> { y | is_inverse x y }.
Proof. move=>> Hx; apply ex_EX; destruct Hx as [y Hy]; exists y; easy. Defined.

Lemma invertible_dec : forall x, { invertible x } + { ~ invertible x }.
Proof. intros; apply classic_dec. Qed.

Definition inv (x : K) : K :=
  match invertible_dec x with
  | left Hx => proj1_sig (invertible_EX Hx)
  | right _ => 0
  end.

Definition div (x y : K) : K := x * inv y.

Lemma inv_correct : forall {x y}, is_inverse x y -> inv x = y.
Proof.
intros x y H; apply (is_inverse_uniq x); try easy.
unfold inv; destruct (invertible_dec _) as [H' | H'].
apply (proj2_sig (invertible_EX _)).
contradict H'; apply (Invertible _ _ H).
Qed.

Lemma inv_0 : forall x, ~ invertible x -> inv x = 0.
Proof. intros x Hx; unfold inv; destruct (invertible_dec _); easy. Qed.

Lemma inv_correct_rev :
  forall {x y}, invertible x -> y = inv x -> is_inverse x y.
Proof.
move=>> [y' Hy'] Hy; rewrite Hy; apply (is_inverse_compat_r y'); try easy.
apply eq_sym, inv_correct; easy.
Qed.

Lemma div_eq : forall x y, div x y = x * inv y.
Proof. easy. Qed.

Lemma div_correct : forall {x y z : K}, is_inverse y z -> div x y = x * z.
Proof. move=>>; rewrite div_eq => /inv_correct ->; easy. Qed.

Lemma div_0 : forall {x y}, ~ invertible y -> div x y = 0.
Proof. move=>>; rewrite div_eq => /inv_0 ->; apply mult_zero_r. Qed.

End Ring_Def1.


Notation "/ x" := (inv x) : Ring_scope.
Notation "x / y" := (div x y) : Ring_scope.


Section Ring_Def2.

Variable K : Ring.

Definition is_comm : Prop := forall (x y : K), x * y = y * x.

Definition is_integral : Prop :=
  forall (x y : K), x <> 0 -> y <> 0 -> x * y <> 0.

Definition has_inv : Prop := forall (x : K), x <> 0 -> invertible x.

Definition is_division : Prop := nonzero_struct K /\ has_inv.

Definition is_field : Prop := is_comm /\ is_division.

End Ring_Def2.


Section Ring_Facts1.

Context {K : Ring}.

Lemma inhabited_r : inhabited K.
Proof. apply inhabited_g. Qed.

Lemma inhabited_fct_r : forall {T : Type}, inhabited (T -> K).
Proof. intros; apply inhabited_fct_g. Qed.

Lemma one_is_zero : (1 : K) = 0 -> zero_struct K.
Proof. intros H x; rewrite -(mult_one_l x) H mult_zero_l; easy. Qed.

Lemma one_not_zero : nonzero_struct K -> (1 : K) <> 0.
Proof.
intros [x Hx]; generalize Hx; rewrite -contra_equiv.
intros; apply one_is_zero; easy.
Qed.

Lemma one_is_zero_equiv : (1 : K) = 0 <-> zero_struct K.
Proof. split; [apply one_is_zero | easy]. Qed.

Lemma one_not_zero_equiv : (1 : K) <> 0 <-> nonzero_struct K.
Proof. split; [intros; exists 1; easy | apply one_not_zero]. Qed.

Lemma opp_one_not_zero : nonzero_struct K -> - (1 : K) <> 0.
Proof.
intros; rewrite -opp_zero; apply opp_neq_compat, one_not_zero; easy.
Qed.

Lemma mult_compat_l : forall x x1 x2 : K, x1 = x2 -> x * x1 = x * x2.
Proof. move=>> H; rewrite H; easy. Qed.

Lemma mult_compat_r : forall x x1 x2 : K, x1 = x2 -> x1 * x = x2 * x.
Proof. move=>> H; rewrite H; easy. Qed.

Lemma mult_zero_rev_l :
  forall {x1 x2 : K}, x1 * x2 = 0 -> ~ invertible x1 \/ x2 = 0.
Proof.
intros x1 x2 H.
destruct (classic (invertible x1)) as [[y1 [H1 _]] | H1]; [right | now left].
rewrite -(mult_one_l x2) -H1 -mult_assoc H mult_zero_r; easy.
Qed.

Lemma mult_zero_rev_r :
  forall {x1 x2 : K}, x1 * x2 = 0 -> x1 = 0 \/ ~ invertible x2.
Proof.
intros x1 x2 H.
destruct (classic (invertible x2)) as [[y2 [_ H2]] | H2]; [left | now right].
rewrite -(mult_one_r x1) -H2 mult_assoc H mult_zero_l; easy.
Qed.

Lemma mult_not_zero_l :
  forall x1 x2 : K, invertible x1 -> x2 <> 0 -> x1 * x2 <> 0.
Proof. move=>> H1 H2 H; destruct (mult_zero_rev_l H); easy. Qed.

Lemma mult_not_zero_r :
  forall x1 x2 : K, x1 <> 0 -> invertible x2 -> x1 * x2 <> 0.
Proof. move=>> H1 H2 H; destruct (mult_zero_rev_r H); easy. Qed.

Lemma mult_reg_l :
  forall x x1 x2 : K, invertible x -> x * x1 = x * x2 -> x1 = x2.
Proof.
move=>> [y [Hy _]] /(mult_compat_l y) Hx.
rewrite 2!mult_assoc Hy 2!mult_one_l in Hx; easy.
Qed.

Lemma mult_reg_r :
  forall x x1 x2 : K, invertible x -> x1 * x = x2 * x -> x1 = x2.
Proof.
move=>> [y [_ Hy]] /(mult_compat_r y) Hx.
rewrite -2!mult_assoc Hy 2!mult_one_r in Hx; easy.
Qed.

Lemma mult_plus_l : forall x y1 y2 : K, x * (y1 + y2) = x * y1 + x * y2.
Proof. exact mult_distr_l. Qed.

Lemma mult_plus_r : forall x1 x2 y : K, (x1 + x2) * y = x1 * y + x2 * y.
Proof. exact mult_distr_r. Qed.

Lemma mult_opp_l : forall x y : K, - x * y = - (x * y).
Proof. intros; apply eq_sym, opp_mult_l. Qed.

Lemma mult_opp_r : forall x y : K, x * - y = - (x * y).
Proof. intros; apply eq_sym, opp_mult_r. Qed.

Lemma mult_opp : forall x y : K, - x * - y = x * y.
Proof. intros; rewrite mult_opp_l mult_opp_r opp_opp; easy. Qed.

Lemma mult_minus_l : forall x y1 y2 : K, x * (y1 - y2) = x * y1 - x * y2.
Proof. intros; rewrite !minus_eq -mult_opp_r; apply mult_distr_l. Qed.

Lemma mult_minus_r : forall x1 x2 y : K, (x1 - x2) * y = x1 * y - x2 * y.
Proof. intros; rewrite !minus_eq -mult_opp_l; apply mult_distr_r. Qed.

Lemma is_inverse_sym : forall x y : K, is_inverse x y -> is_inverse y x.
Proof. move=>> [H1 H2]; easy. Qed.

Lemma is_inverse_sym_equiv : forall x y : K, is_inverse x y <-> is_inverse y x.
Proof. intros; split; apply is_inverse_sym. Qed.

Lemma is_inverse_zero_struct : forall x y : K, zero_struct K -> is_inverse x y.
Proof.
intros x y HK; split; rewrite (HK x) (HK y) (HK 1); apply mult_zero_l.
Qed.

Lemma invertible_zero_struct : forall x : K, zero_struct K -> invertible x.
Proof. move=> x /is_inverse_zero_struct Hx; apply (Invertible x 0); easy. Qed.

Lemma invertible_zero : invertible (0 : K) -> zero_struct K.
Proof.
intros [z [_ H]]; rewrite mult_zero_l in H; apply one_is_zero; easy.
Qed.

Lemma is_inverse_zero : forall x : K, is_inverse 0 x -> zero_struct K.
Proof. move=>> Hx; apply invertible_zero; apply (Invertible _ _ Hx). Qed.

Lemma is_inverse_one : is_inverse (1 : K) 1.
Proof. split; apply mult_one_l. Qed.

Lemma is_inverse_eq_one : forall {a : K}, a = 1 -> is_inverse a a.
Proof. intros; subst; apply is_inverse_one. Qed.

Lemma invertible_one : invertible (1 : K).
Proof. apply (Invertible _ 1), is_inverse_one. Qed.

Lemma invertible_eq_one : forall {a : K}, a = 1 -> invertible a.
Proof. intros; subst; apply invertible_one. Qed.

Lemma noninvertible_zero : nonzero_struct K -> ~ invertible (0 : K).
Proof. intros [x0 Hx0]; contradict Hx0; apply invertible_zero; easy. Qed.

Lemma is_inverse_nonzero_l :
  forall x y : K, nonzero_struct K -> is_inverse x y -> x <> 0.
Proof.
move=>> HK; apply contra_not_r_equiv.
intros; subst; move=> [_ H]; contradict H.
rewrite mult_zero_l; apply not_eq_sym, one_not_zero; easy.
Qed.

Lemma is_inverse_nonzero_r :
  forall x y : K, nonzero_struct K -> is_inverse x y -> y <> 0.
Proof. move=>> HK /is_inverse_sym; apply is_inverse_nonzero_l; easy. Qed.

Lemma is_inverse_invertible_l : forall x y : K, is_inverse x y -> invertible x.
Proof. intros x y H; apply (Invertible x y H). Qed.

Lemma is_inverse_invertible_r : forall x y : K, is_inverse x y -> invertible y.
Proof. move=>> /is_inverse_sym; apply is_inverse_invertible_l. Qed.

Lemma is_inverse_opp :
  forall (x y : K), is_inverse x y -> is_inverse (- x) (- y).
Proof. move=>> H; split; rewrite mult_opp; apply H. Qed.

Lemma is_inverse_opp_rev :
  forall (x y : K), is_inverse (- x) (- y) -> is_inverse x y.
Proof. move=>> /is_inverse_opp; rewrite !opp_opp; easy. Qed.

Lemma is_inverse_opp_equiv :
  forall (x y : K), is_inverse (- x) (- y) <-> is_inverse x y.
Proof. intros; split; [apply is_inverse_opp_rev | apply is_inverse_opp]. Qed.

Lemma invertible_opp : forall (x : K), invertible x -> invertible (- x).
Proof. move=> x [y /is_inverse_opp Hy]; apply (Invertible _ _ Hy). Qed.

Lemma invertible_opp_equiv : forall (x : K), invertible (- x) <-> invertible x.
Proof.
intros x; split; intros; [rewrite -(opp_opp x) |]; apply invertible_opp; easy.
Qed.

Lemma inv_zero : / (0 : K) = 0.
Proof.
unfold inv; destruct (invertible_dec _); [apply invertible_zero |]; easy.
Qed.

Lemma inv_one : / (1 : K) = 1.
Proof. apply inv_correct, is_inverse_one. Qed.

Lemma inv_opp : forall (x : K), invertible x -> / - x = - / x.
Proof. intros; apply inv_correct, is_inverse_opp, inv_correct_rev; easy. Qed.

Lemma inv_opp_one : / - (1 : K) = - (1).
Proof. rewrite -{2}inv_one inv_opp//; apply invertible_one. Qed.

Lemma inv_opp_0 : forall (x : K), ~ invertible x -> / - x = - / x.
Proof.
intros; rewrite !inv_0; [rewrite opp_zero; easy | easy |].
contradict H; apply invertible_opp_equiv; easy.
Qed.

Lemma mult_invertible_compat :
  forall x y : K, invertible x -> invertible y -> invertible (x * y).
Proof.
move=>> [x1 [Hxa Hxb]] [y1 [Hya Hyb]]; apply (Invertible _ (y1 * x1)); split.
rewrite -mult_assoc (mult_assoc x1) Hxa mult_one_l; easy.
rewrite -mult_assoc (mult_assoc _ y1) Hyb mult_one_l; easy.
Qed.

Lemma mult_invertible_reg_l :
  forall x y : K, invertible y -> invertible (x * y) ->  invertible x.
Proof.
intros x y Hy [z [Hza Hzb]]; apply (Invertible _ (y * z)); split.
(* *)
generalize Hy; intros [y1 Hy1].
apply (mult_reg_l y1); try now apply (is_inverse_invertible_r y).
apply (mult_reg_r y); try easy.
destruct Hy1 as [Hy1 _].
rewrite 2!mult_assoc Hy1 mult_one_l mult_one_r -mult_assoc Hy1; easy.
(* *)
rewrite mult_assoc; easy.
Qed.

Lemma mult_invertible_reg_r :
  forall x y : K, invertible x -> invertible (x * y) ->  invertible y.
Proof.
intros x y Hx [z [Hza Hzb]]; apply (Invertible _ (z * x)); split.
(* *)
rewrite -mult_assoc; easy.
(* *)
generalize Hx; intros [x1 Hx1].
apply (mult_reg_l x); try easy.
apply (mult_reg_r x1); try now apply (is_inverse_invertible_r x).
destruct Hx1 as [_ Hx1].
rewrite mult_assoc -mult_assoc -(mult_assoc z) Hx1 !mult_one_r Hx1; easy.
Qed.

Lemma mult_invertible_equiv_l :
  forall x y : K, invertible y -> invertible (x * y) <-> invertible x.
Proof.
intros; split.
apply mult_invertible_reg_l; easy.
intros; apply mult_invertible_compat; easy.
Qed.

Lemma mult_invertible_equiv_r :
  forall x y : K, invertible x -> invertible (x * y) <-> invertible y.
Proof.
intros; split.
apply mult_invertible_reg_r; easy.
intros; apply mult_invertible_compat; easy.
Qed.

Lemma mult_inv_l : forall {x : K}, invertible x -> / x * x = 1.
Proof. intros x [y Hy]; rewrite (inv_correct Hy); apply Hy. Qed.

Lemma mult_inv_r : forall {x : K}, invertible x -> x * / x = 1.
Proof. intros x [y Hy]; rewrite (inv_correct Hy); apply Hy. Qed.

Lemma mult_zero_reg_l : forall {x y : K}, invertible x -> x * y = 0 -> y = 0.
Proof. move=>> Hx H; destruct (mult_zero_rev_l H); easy. Qed.

Lemma mult_zero_reg_r : forall {x y : K}, invertible y -> x * y = 0 -> x = 0.
Proof. move=>> Hy H; destruct (mult_zero_rev_r H); easy. Qed.

Lemma is_inverse_inv_r : forall {x : K}, invertible x -> is_inverse x (/ x).
Proof. intros x Hx; apply (inv_correct_rev Hx); easy. Qed.

Lemma is_inverse_inv_l : forall {x : K}, invertible x -> is_inverse (/ x) x.
Proof. intros; apply is_inverse_sym, is_inverse_inv_r; easy. Qed.

Lemma inv_invertible : forall {x : K}, invertible x -> invertible (/ x).
Proof. intros x Hx; apply (Invertible _ x), (is_inverse_inv_l Hx). Qed.

Lemma inv_invol : forall {x : K}, invertible x -> / (/ x) = x.
Proof. intros; apply inv_correct, is_inverse_inv_l; easy. Qed.

Lemma mult_div_l : forall (x y : K), invertible y -> x / y * y = x.
Proof. intros; rewrite div_eq -mult_assoc mult_inv_l// mult_one_r; easy. Qed.

Lemma div_mult_r : forall (x y : K), invertible y -> x * y / y = x.
Proof. intros; rewrite div_eq -mult_assoc mult_inv_r// mult_one_r; easy. Qed.

Lemma div_zero_l : forall (x : K), 0 / x = 0.
Proof. intros; apply mult_zero_l. Qed.

Lemma div_eq_zero_l : forall {x : K} y, x = 0 -> x / y = 0.
Proof. intros; subst; apply div_zero_l. Qed.

Lemma div_zero_equiv : forall (x y : K), invertible y -> x / y = 0 <-> x = 0.
Proof.
intros x y Hy; split; [| apply div_eq_zero_l].
move=> /(mult_compat_r y); rewrite mult_div_l// mult_zero_l; easy.
Qed.

Lemma div_one_l : forall (x : K), 1 / x = / x.
Proof. intros; apply mult_one_l. Qed.

Lemma div_eq_one_l : forall {x : K} y, x = 1 -> x / y = / y.
Proof. intros; subst; apply div_one_l. Qed.

Lemma div_one_r : forall (x : K), x / 1 = x.
Proof. intros; rewrite div_eq inv_one; apply mult_one_r. Qed.

Lemma div_eq_one_r : forall (x : K) {y}, y = 1 -> x / y = x.
Proof. intros; subst; apply div_one_r. Qed.

Lemma div_opp_one_l : forall (x y : K), x = - (1) -> x / y = - / y.
Proof. intros; subst; rewrite div_eq mult_opp_l mult_one_l; easy. Qed.

Lemma div_opp_one_r : forall (x y : K), y = - (1) -> x / y = - x.
Proof.
intros; subst; rewrite div_eq inv_opp_one mult_opp_r mult_one_r; easy.
Qed.

Lemma div_diag : forall {x : K}, invertible x -> x / x = 1.
Proof. move=>>; apply mult_inv_r. Qed.

Lemma div_one_compat : forall {x y : K}, invertible y -> x = y -> x / y = 1.
Proof. move=>> Hy ->; apply div_diag; easy. Qed.

Lemma div_one_reg : forall {x y : K}, invertible y -> x / y = 1 -> x = y.
Proof.
move=> x y Hy /(mult_compat_r y); rewrite -mult_assoc (mult_inv_l Hy).
rewrite mult_one_l mult_one_r; easy.
Qed.

Lemma div_one_equiv : forall {x y : K}, invertible y -> x / y = 1 <-> x = y.
Proof. intros; split; [apply div_one_reg | apply div_one_compat]; easy. Qed.

Lemma div_one_minus_equiv :
  forall {x y : K}, invertible y -> 1 - x / y = 0 <-> x = y.
Proof. intros; rewrite -div_one_equiv//; apply minus_zero_sym_equiv. Qed.

Lemma div_plus : forall (x1 x2 y : K), (x1 + x2) / y = x1 / y + x2 / y.
Proof. intros; rewrite !div_eq; apply mult_distr_r. Qed.

Lemma div_opp_l : forall (x y : K), - x / y = - (x / y).
Proof. intros; rewrite !div_eq mult_opp_l; easy. Qed.

Lemma div_opp_r : forall (x y : K), invertible y -> x / - y = - (x / y).
Proof. intros; rewrite !div_eq -mult_opp_r inv_opp; easy. Qed.

Lemma div_opp : forall (x y : K), invertible y -> - x / - y = x / y.
Proof. intros; rewrite div_opp_l div_opp_r// opp_opp; easy. Qed.

Lemma div_opp_0 : forall (x y : K), ~ invertible y -> - x / - y = x / y.
Proof. intros; rewrite !div_eq inv_opp_0// mult_opp; easy. Qed.

Lemma div_minus : forall x1 x2 y : K, (x1 - x2) / y = x1 / y - x2 / y.
Proof. intros; rewrite !div_eq; apply mult_minus_r. Qed.

Lemma mult_sum_l :
  forall {n} x (u : 'K^n), sum u * x = sum (mapF (mult^~ x) u).
Proof.
intros n; induction n as [| n Hn]; intros.
rewrite !sum_nil; apply mult_zero_l.
rewrite !sum_ind_l mult_distr_r Hn; easy.
Qed.

Lemma mult_sum_r :
  forall {n} x (u : 'K^n), x * sum u = sum (mapF (mult x) u).
Proof.
intros n; induction n as [| n Hn]; intros.
rewrite !sum_nil; apply mult_zero_r.
rewrite !sum_ind_l mult_distr_l Hn; easy.
Qed.

Lemma div_sum :
  forall {n} x (u : 'K^n), sum u / x = sum (mapF (div^~ x) u).
Proof. intros; rewrite div_eq mult_sum_l; easy. Qed.

Lemma sum_nil_noninvertible :
  forall (u : 'K^0), nonzero_struct K -> ~ invertible (sum u).
Proof. move=>>; rewrite sum_nil; apply noninvertible_zero. Qed.

Lemma invertible_sum_nonnil :
  forall {n} (u : 'K^n),
    nonzero_struct K ->  invertible (sum u) -> n <> 0%nat.
Proof.
move=>> HK; apply contra_not_r_equiv; intros; subst.
apply sum_nil_noninvertible; easy.
Qed.

Lemma sum_singleF_invertible :
  forall {a : K}, invertible a -> invertible (sum (singleF a)).
Proof. move=>>; rewrite sum_singleF; easy. Qed.

Lemma invertible_sum_equiv :
  forall {n} (u : 'K^n),
    invertible (sum u) <-> invertible (sum (filter_n0F u)).
Proof. intros; rewrite sum_filter_n0F; easy. Qed.

Lemma sum_insertF_baryc :
  forall {n} (u : 'K^n) i0, sum (insertF u (1 - sum u) i0) = 1.
Proof. intros; rewrite sum_insertF plus_minus_l; easy. Qed.

Lemma invertible_sum_squeezeF :
  forall {n} (u : 'K^n.+1) {i0 i1},
    i1 <> i0 -> invertible (sum u) -> invertible (sum (squeezeF u i0 i1)).
Proof. intros; rewrite sum_squeezeF; easy. Qed.

Lemma is_integral_contra :
  is_integral K <-> forall (x y : K), x * y = 0 -> x = 0 \/ y = 0.
Proof.
split; intros HK x y.
rewrite contra_equiv not_or_equiv; intros; apply HK; easy.
intros H1 H2; contradict H1; destruct (HK _ _ H1); easy.
Qed.

End Ring_Facts1.


Section Ring_Facts2.

Context {T : Type}.
Context {K : Ring}.

Lemma fct_one_not_zero :
  inhabited T -> nonzero_struct K -> (1 : T -> K) <> 0.
Proof. intros; apply: one_not_zero; apply fct_nonzero_struct; easy. Qed.

Lemma fct_inv_eq : forall {f : T -> K} t, invertible f -> (/ f) t = / f t.
Proof.
intros; apply eq_sym, inv_correct; split;
    rewrite -fct_mult_eq; [rewrite mult_inv_l | rewrite mult_inv_r]; easy.
Qed.

Lemma fct_div_eq :
  forall (f : T -> K) {g} t, invertible g -> (f / g) t = f t / g t.
Proof. intros; unfold div; rewrite -fct_inv_eq; easy. Qed.

End Ring_Facts2.


Section Ring_R_Facts1.

Lemma one_eq_R : 1 = 1%R.
Proof. easy. Qed.

Lemma nonzero_struct_R : nonzero_struct R_AbelianGroup.
Proof. exists 1; unfold one, zero; simpl; easy. Qed.

Lemma one_not_zero_R : (1 : R_Ring) <> 0.
Proof. apply one_not_zero, nonzero_struct_R. Qed.

Lemma opp_one_not_zero_R : - (1 : R_Ring) <> 0.
Proof. apply opp_one_not_zero, nonzero_struct_R. Qed.

Lemma is_inverse_Rinv : forall a, a <> 0 -> is_inverse a (/ a)%R.
Proof.
intros; unfold is_inverse, mult; simpl; rewrite -> Rinv_l, Rinv_r; easy.
Qed.

Lemma inv_eq_R : forall a, / a = (/ a)%R.
Proof.
intros; destruct (Req_dec a 0) as [Ha | Ha].
rewrite Ha inv_zero Rinv_0; easy.
apply inv_correct, is_inverse_Rinv; easy.
Qed.

Lemma div_eq_R : forall a b, a / b = (a / b)%R.
Proof. intros; unfold div, Rdiv; rewrite inv_eq_R; easy. Qed.

Lemma invertible_equiv_R : forall a : R, invertible a <-> a <> 0.
Proof.
intros; split; intros Ha.
contradict Ha; rewrite Ha; apply (noninvertible_zero nonzero_struct_R).
apply (Invertible _ (/ a)); rewrite inv_eq_R; apply is_inverse_Rinv; easy.
Qed.

Lemma non_invertible_equiv_R : forall a : R, ~ invertible a <-> a = 0.
Proof. intros; rewrite -iff_not_r_equiv; apply invertible_equiv_R. Qed.

Lemma invertible_2 : invertible 2%R.
Proof. apply invertible_equiv_R; easy. Qed.

Lemma mult_comm_R : is_comm R_Ring.
Proof. intro; unfold mult; simpl; apply Rmult_comm. Qed.

Lemma mult_pos_R : forall a : R, (0 <= a * a)%R.
Proof. unfold mult; simpl; apply Rle_0_sqr. Qed.

Lemma mult_inv_l_R : forall a : R, a <> 0 -> (/ a)%R * a = 1.
Proof.
intros; rewrite -inv_eq_R; apply mult_inv_l; apply invertible_equiv_R; easy.
Qed.

Lemma mult_inv_r_R : forall a : R, a <> 0 -> a * (/ a)%R = 1.
Proof.
intros; rewrite -inv_eq_R; apply mult_inv_r; apply invertible_equiv_R; easy.
Qed.

Lemma is_inverse_nonzero_l_R : forall a b : R, is_inverse a b -> a <> 0.
Proof. move=>>; apply (is_inverse_nonzero_l _ _ nonzero_struct_R). Qed.

Lemma is_inverse_nonzero_r_R : forall a b : R, is_inverse a b -> b <> 0.
Proof. move=>>; apply (is_inverse_nonzero_r _ _ nonzero_struct_R). Qed.

Lemma is_inverse_equiv_R : forall a : R, is_inverse a (/ a)%R <-> a <> 0.
Proof.
intros; split; try apply is_inverse_nonzero_l_R.
split; [apply mult_inv_l_R | apply mult_inv_r_R]; easy.
Qed.

Lemma is_integral_contra_R :
  forall (x y : R_Ring), x * y = 0 -> x = 0 \/ y = 0.
Proof. apply Rmult_integral. Qed.

Lemma is_integral_R : is_integral R_Ring.
Proof. apply is_integral_contra, is_integral_contra_R. Qed.

Lemma has_inv_R : has_inv R_Ring.
Proof. move=>> /is_inverse_equiv_R H; apply (Invertible _ _ H). Qed.

Lemma is_field_R : is_field R_Ring.
Proof.
repeat split. apply mult_comm_R. apply nonzero_struct_R. apply has_inv_R.
Qed.

Lemma mult_div_r_R : forall (x y : R), invertible y -> y * (x / y) = x.
Proof. move=>>; rewrite mult_comm_R; apply mult_div_l. Qed.

Lemma div_mult_l_R : forall (x y : R), invertible y -> y * x / y = x.
Proof. move=>>; rewrite mult_comm_R; apply div_mult_r. Qed.

End Ring_R_Facts1.


Section Ring_R_Facts2.

Context {T : Type}.

Lemma fct_one_not_zero_R : inhabited T -> (1 : T -> R) <> 0.
Proof. intros HT; apply (fct_one_not_zero HT nonzero_struct_R). Qed.

End Ring_R_Facts2.
