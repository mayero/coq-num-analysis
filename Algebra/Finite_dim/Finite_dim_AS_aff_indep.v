(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Support for affine independent finite families in affine spaces.

 * Description

 Some results are only valid when the ring of scalars is commutative, or being
 ordered, they are only stated in the case of the ring of real numbers [R_Ring].

 * Usage

 This module may be used through the import of
 [Algebra.Finite_dim.Finite_dim_AS], [Algebra.Finite_dim.Finite_dim],
 [Algebra.Algebra], or [Algebra.Algebra_wDep], where it is exported.
*)

From Requisite Require Import ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Subsets Require Import Subsets_wDep.
From Algebra Require Import Hierarchy_compl.
From Algebra Require Import Monoid Group Ring ModuleSpace AffineSpace.
From Algebra Require Import Finite_dim_MS.
From Algebra Require Import Finite_dim_AS_def Finite_dim_AS_aff_gen.

Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.
Local Open Scope ModuleSpace_scope.
Local Open Scope AffineSpace_scope.


Section Affine_span_Affine_Facts.

Context {K : Ring}.
Context {V : ModuleSpace K}.
Context {E : AffineSpace V}.

Lemma aff_indep_any :
  forall {n} {A : 'E^n.+1} i0 i,
    lin_indep (frameF A i0) -> lin_indep (frameF A i).
Proof.
intros n A i0 i HA; destruct (ord_eq_dec i i0) as [Hi | Hi]; try now rewrite Hi.
destruct n as [| n]; [contradict Hi; rewrite !ord_one; easy |].
intros L; unfold frameF; rewrite (vectF_change_orig (A i0))
    lc_plus_r lc_constF_r scal_opp -vect_sym vectF_skipF.
rewrite -lc_insertF_l (lc_skip_zero_r i0 (vectF_zero_alt _ _)).
move=> /HA /extF_rev HL1.
assert (HL1a : skipF L (insert_ord (not_eq_sym Hi)) = 0).
  apply skipF_zero_compat; move=> j /skip_insert_ord_neq Hj.
  specialize (HL1 (insert_ord Hj)).
  rewrite skipF_correct insertF_correct in HL1; easy.
apply (extF_zero_skipF _ (insert_ord (not_eq_sym Hi))); try easy.
rewrite -sum_one // opp_zero_equiv; move: (HL1 (insert_ord Hi)).
rewrite zeroF skipF_correct insertF_correct_l; easy.
Qed.

Lemma aff_indep_all :
  forall {n} {A : 'E^n.+1} i, aff_indep A -> lin_indep (frameF A i).
Proof. move=>>; apply aff_indep_any. Qed.

Lemma aff_indep_all_equiv :
  forall {n} {A : 'E^n.+1}, aff_indep A <-> forall i, lin_indep (frameF A i).
Proof.
intros; split; [intros; apply aff_indep_all; easy | intros H; apply H].
Qed.

Lemma aff_dep_any :
  forall {n} {A : 'E^n.+1} i0,
    (exists i, lin_dep (frameF A i)) -> lin_dep (frameF A i0).
Proof.
intros n A i0; rewrite contra_not_r_equiv not_ex_not_all_equiv.
intros; apply (aff_indep_any i0); easy.
Qed.

Lemma aff_dep_ex :
  forall {n} {A : 'E^n.+1}, (exists i, lin_dep (frameF A i)) -> aff_dep A.
Proof. move=>>; apply aff_dep_any. Qed.

Lemma aff_dep_ex_equiv :
  forall {n} {A : 'E^n.+1}, aff_dep A <-> exists i, lin_dep (frameF A i).
Proof. intros; split; [intros H; exists ord0; easy | apply aff_dep_any]. Qed.

Lemma baryc_coord_uniq :
  forall {n L1 L2} {A : 'E^n.+1},
    aff_indep A -> sum L1 = 1 -> sum L2 = 1 ->
    barycenter L1 A = barycenter L2 A -> L1 = L2.
Proof.
intros n L1 L2 A HA HL1 HL2 HL.
assert (HL' : liftF_S L1 = liftF_S L2).
  rewrite -!skipF_first; apply minus_zero_reg, HA.
  rewrite lc_minus_l; apply: minus_zero_compat.
  rewrite -!(lc_vectF_skipF ord0) -!(baryc_correct_orig (A ord0));
      [| apply invertible_eq_one; easy..].
  rewrite HL1 HL2 HL; easy.
apply (eqxF_reg _ ord0).
apply (plus_reg_r (sum (liftF_S L1))).
rewrite {2}HL' -!sum_ind_l HL2; easy.
apply liftF_S_reg; easy.
Qed.

End Affine_span_Affine_Facts.


Section AffineSpace_FD_R_Facts.

Context {V : ModuleSpace R_Ring}.
Context {E : AffineSpace V}.

Lemma aff_indep_monot :
  forall {n1 n2} {A1 : 'E^n1.+1} {A2 : 'E^n2.+1},
    injective A1 -> invalF A1 A2 -> aff_indep A2 -> aff_indep A1.
Proof.
intros n1 n2 A1 A2 HA1 HA HA2; destruct (HA ord0) as [i2 Hi2].
apply (lin_indep_monot (frameF A2 i2)).
apply frameF_inj_equiv, skipF_inj; easy.
apply frameF_invalF_equiv; [| apply skipF_invalF]; easy.
apply aff_indep_all; easy.
Qed.

Lemma aff_dep_monot :
  forall {n1 n2} {A1 : 'E^n1.+1} {A2 : 'E^n2.+1},
    injective A1 -> invalF A1 A2 -> aff_dep A1 -> aff_dep A2.
Proof.
move=>> HA1 HA; rewrite -contra_equiv; apply aff_indep_monot; easy.
Qed.

Lemma aff_indep_1 : forall {A : 'E^1}, aff_indep A.
Proof. move=>> _; apply hat0F_eq; easy. Qed.

Lemma aff_indep_singleF : forall {A : E}, aff_indep (singleF A).
Proof. intros; apply aff_indep_1. Qed.

Lemma aff_indep_2_equiv :
  forall {A : 'E^2}, aff_indep A <-> A ord0 <> A ord_max.
Proof.
intros A; unfold aff_indep; rewrite lin_indep_1_equiv// -iff_not_equiv.
rewrite -(extF_zero_1_equiv ord0)// frameF_2_0 vect_zero_equiv; easy.
Qed.

Lemma aff_indep_coupleF_equiv :
  forall {A B : E}, aff_indep (coupleF A B) <-> A <> B.
Proof. intros; rewrite aff_indep_2_equiv coupleF_0 coupleF_1; easy. Qed.

Lemma aff_indep_skipF :
  forall {n} {A : 'E^n.+2} i0, aff_indep A -> aff_indep (skipF A i0).
Proof.
intros n A i0 HA; destruct (ord_eq_dec i0 ord0) as [Hi0 | Hi0]; [subst |].
(* *)
apply (aff_indep_any ord_max); rewrite -(insert_ord_max ord_max_not_0).
rewrite frameF_skipF_alt; apply lin_indep_skipF_compat, aff_indep_all; easy.
(* *)
apply (aff_indep_any ord0); rewrite -(insert_ord_0 (not_eq_sym Hi0)).
rewrite frameF_skipF; apply lin_indep_skipF_compat; easy.
Qed.

Lemma aff_indep_permutF :
  forall {n} {A : 'E^n.+1} {p},
    bijective p -> aff_indep A -> aff_indep (permutF p A).
Proof.
intros n A p Hp HA; apply (aff_indep_any (p ord0)).
rewrite (frameF_permutF (bij_inj Hp)); apply lin_indep_permutF.
apply skip_f_ord_bij.
apply aff_indep_all; easy.
Qed.

Lemma aff_indep_permutF_rev :
  forall {n} {A : 'E^n.+1} p,
    bijective p -> aff_indep (permutF p A) -> aff_indep A.
Proof.
intros n A p Hp; rewrite {2}(permutF_f_inv_l Hp A).
apply aff_indep_permutF, f_inv_bij.
Qed.

Lemma aff_indep_permutF_equiv :
  forall {n} {A : 'E^n.+1} p,
    bijective p -> aff_indep A <-> aff_indep (permutF p A).
Proof.
intros; split; [apply aff_indep_permutF | apply aff_indep_permutF_rev]; easy.
Qed.

Lemma aff_indep_revF_equiv :
  forall {n} {A : 'E^n.+1}, aff_indep A <-> aff_indep (revF A).
Proof. intros; apply aff_indep_permutF_equiv, rev_ord_bij. Qed.

Lemma aff_indep_moveF_equiv :
  forall {n} i0 i1 {A : 'E^n.+1}, aff_indep A <-> aff_indep (moveF i0 i1 A).
Proof. intros; apply aff_indep_permutF_equiv, move_ord_bij. Qed.

Lemma aff_indep_transpF_equiv :
  forall {n} i0 i1 {A : 'E^n.+1}, aff_indep A <-> aff_indep (transpF i0 i1 A).
Proof. intros; apply aff_indep_permutF_equiv, transp_ord_bij. Qed.

End AffineSpace_FD_R_Facts.
