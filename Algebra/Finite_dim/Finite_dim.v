(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Exports support for finite-dimensional spaces.

 * Description

 * Usage

 This module is meant to be imported after all imports from the Coq standard
 library, from the Mathematical Components library, and from other external Coq
 libraries such as Coquelicot and Flocq.

 This module may be used through the import of [Algebra.Finite_dim.Finite_dim],
 [Algebra.Algebra], or [Algebra.Algebra_wDep], where it is exported.
*)

From Algebra Require Export Finite_dim_MS Finite_dim_AS.
