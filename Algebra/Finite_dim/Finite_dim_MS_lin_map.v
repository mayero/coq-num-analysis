(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Support for linear maps in finite-dimensional module spaces.

 * Description

 For results that are only valid when the ring of scalars is commutative, or
 being ordered, see [Algebra.Finite_dim.Finite_dim_MS_lin_map_R] where they are
 only stated in the case of the ring of real numbers [R_Ring].

 * Usage

 This module may be used through the import of
 [Algebra.Finite_dim.Finite_dim_MS], [Algebra.Finite_dim.Finite_dim],
 [Algebra.Algebra], or [Algebra.Algebra_wDep], where it is exported.
*)

From Requisite Require Import ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Subsets Require Import Subsets_wDep.
From Algebra Require Import Hierarchy_compl Monoid Group Ring ModuleSpace.
From Algebra Require Import Finite_dim_MS_def.
From Algebra Require Import Finite_dim_MS_lin_span Finite_dim_MS_lin_gen.
From Algebra Require Import Finite_dim_MS_lin_indep Finite_dim_MS_basis.

Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.
Local Open Scope ModuleSpace_scope.


Section Linear_mapping_Facts1.

Context {K : Ring}.
Context {E F : ModuleSpace K}.
Context {PE : E -> Prop}.
Hypothesis HPE : compatible_ms PE.

Context {f : E -> F}.
Hypothesis Hf : lin_map f.

Lemma lm_lin_span :
  forall {n} (B : 'E^n), image f (lin_span B) = lin_span (mapF f B).
Proof.
intros; apply subset_ext_equiv; split.
apply lin_span_incl_equiv; intros y [x [L]];
    exists L; apply lm_lc, Hf.
apply lin_span_lb; [apply (image_cms (lin_span_cms _) Hf) |
    apply mapF_inclF, lin_span_inclF_diag].
Qed.

Lemma lm_lin_gen :
  forall {n} (B : 'E^n),
    inclF B PE -> lin_gen PE B -> lin_gen (image f PE) (mapF f B).
Proof. move=>> HB ->; rewrite lm_lin_span; apply lin_gen_lin_span. Qed.

Lemma lm_fin_dim : fin_dim PE -> fin_dim (image f PE).
Proof.
intros [n [B HB]]; exists n, (mapF f B); apply lm_lin_gen;
   [apply lin_gen_inclF |]; easy.
Qed.

Hypothesis Hf1 : injS PE f.

Lemma lm_lin_indep :
  forall {n} (B : 'E^n), inclF B PE -> lin_indep B -> lin_indep (mapF f B).
Proof.
move=>> HB1 HB2 L; rewrite -lm_lc// -{1}(lm_zero Hf).
move=> /Hf1 HL; apply HB2, HL; [apply cms_lc | apply cms_zero]; easy.
Qed.

Lemma lm_lin_dep :
  forall {n} (B : 'E^n), inclF B PE -> lin_dep (mapF f B) -> lin_dep B.
Proof. move=>> HB; rewrite -contra_equiv; apply lm_lin_indep; easy. Qed.

Lemma lm_basis :
  forall {n} (B : 'E^n),
    inclF B PE -> basis PE B -> basis (image f PE) (mapF f B).
Proof.
move=>> H1 [H2 H3]; split; [apply lm_lin_gen | apply lm_lin_indep]; easy.
Qed.

Lemma lm_has_dim : forall {n}, has_dim PE n -> has_dim (image f PE) n.
Proof.
intros n [B HB]; apply (Dim _ _ (mapF f B)), lm_basis;
    [apply lin_gen_inclF, HB | easy].
Qed.

End Linear_mapping_Facts1.


Section Linear_mapping_Facts2.

Context {K : Ring}.
Context {E F : ModuleSpace K}.
Context {PE : E -> Prop}.
Hypothesis HPE : compatible_ms PE.

Variable f : E -> F.
Hypothesis Hf : lin_map f.
Hypothesis Hf1 : injS PE f.
(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!
 TODO FC (23/02/2025): wait for supplementary of module subspace
 to get an f_invS which is linear on the whole space.
 Thus, temporarily replace bijS with bijective.
Let Hf2 : bijS PE (image f PE) f := bijS_image inhabited_ms Hf1.*)
Hypothesis Hf2' : bijective f.
Let fm1 := f_inv Hf2'.
Let Hfm1 : canS PE f fm1.
Proof. intros x _; unfold fm1; rewrite f_inv_can_l; easy. Qed.

Lemma lm_lin_gen_rev :
  forall {n} (B : 'E^n),
    inclF B PE -> lin_gen (image f PE) (mapF f B) -> lin_gen PE B.
Proof.
intros n B HB; replace B with (mapF id B) at 2 by easy.
rewrite -{2}(image_invol_canS Hfm1) -(f_inv_id_l Hf2') mapF_comp.
apply lm_lin_gen; [apply lm_bij_compat | apply image_inclF]; easy.
Qed.

Lemma lm_lin_gen_equiv :
  forall {n} (B : 'E^n),
    inclF B PE -> lin_gen (image f PE) (mapF f B) <-> lin_gen PE B.
Proof. intros; split; [apply lm_lin_gen_rev | apply lm_lin_gen]; easy. Qed.

Lemma lm_fin_dim_rev : fin_dim (image f PE) -> fin_dim PE.
Proof.
rewrite -{2}(image_invol_canS Hfm1); apply lm_fin_dim, lm_bij_compat; easy.
Qed.

Lemma lm_fin_dim_equiv : fin_dim (image f PE) <-> fin_dim PE.
Proof. intros; split; [apply lm_fin_dim_rev | apply lm_fin_dim]; easy. Qed.

Lemma lm_lin_indep_rev :
  forall {n} (B : 'E^n), inclF B PE -> lin_indep (mapF f B) -> lin_indep B.
Proof. move=>> HB1 HB2 L HL; apply HB2; rewrite -lm_lc// HL lm_zero; easy. Qed.

Lemma lm_lin_indep_equiv :
  forall {n} (B : 'E^n), inclF B PE -> lin_indep (mapF f B) <-> lin_indep B.
Proof.
intros; split; [apply lm_lin_indep_rev | apply (lm_lin_indep HPE)]; easy.
Qed.

Lemma lm_lin_dep_rev :
  forall {n} (B : 'E^n), inclF B PE -> lin_dep B -> lin_dep (mapF f B).
Proof. move=>> HB; rewrite -contra_equiv; apply lm_lin_indep_rev; easy. Qed.

Lemma lm_lin_dep_equiv :
  forall {n} (B : 'E^n), inclF B PE -> lin_dep (mapF f B) <-> lin_dep B.
Proof.
intros; split; [apply (lm_lin_dep HPE) | apply lm_lin_dep_rev]; easy.
Qed.

Lemma lm_basis_rev :
  forall {n} (B : 'E^n),
    inclF B PE -> basis (image f PE) (mapF f B) -> basis PE B.
Proof.
intros n B HB; replace B with (mapF id B) at 2 by easy.
rewrite -{2}(image_invol_canS Hfm1) -(f_inv_id_l Hf2') mapF_comp;
   apply lm_basis.
apply image_cms; easy.
apply lm_bij_compat; easy.
apply inj_injS, bij_inj, f_inv_bij.
apply image_inclF; easy.
Qed.

Lemma lm_basis_equiv :
  forall {n} (B : 'E^n),
    inclF B PE -> basis (image f PE) (mapF f B) <-> basis PE B.
Proof. intros; split; [apply lm_basis_rev | apply lm_basis]; easy. Qed.

Lemma lm_has_dim_rev : forall {n}, has_dim (image f PE) n -> has_dim PE n.
Proof.
intros n Hn; inversion Hn as [B HB]; apply Dim with (mapF fm1 B), lm_basis_rev.
rewrite -(image_invol_canS Hfm1); rewrite f_inv_image.
apply preimage_inclF, basis_inclF; easy.
rewrite -mapF_comp f_inv_id_r; easy.
Qed.

Lemma lm_has_dim_equiv : forall {n}, has_dim (image f PE) n <-> has_dim PE n.
Proof. intros; split; [apply lm_has_dim_rev | apply lm_has_dim]; easy. Qed.

End Linear_mapping_Facts2.


Section Linear_mapping_Facts3a.

Context {K : Ring}.
Context {E F : ModuleSpace K}.

Context {PE : E -> Prop}.
Context {nPE : nat}.
Hypothesis HPE : has_dim PE nPE.

Context {f : E -> F}.
Hypothesis Hf : lin_map f.

Lemma image_generator :
  forall {n} {B : 'E^n}, basis PE B -> lin_gen (image f PE) (mapF f B).
Proof.
intros n B [HB _]; apply lm_lin_gen; [easy | apply (lin_gen_inclF HB) | easy].
Qed.

Lemma image_fin_dim : fin_dim (image f PE).
Proof.
destruct HPE as [B HB]; rewrite (image_generator HB); apply fin_dim_lin_span.
Qed.

Lemma lmS_injS_has_dim_equiv : injS PE f <-> has_dim (KerS PE f) 0.
Proof.
rewrite (lmS_injS_equiv (has_dim_cms _ HPE) Hf).
apply iff_sym, has_dim_0_equiv.
Qed.

Lemma lmS_bijS_has_dim_equiv : bijS PE (image f PE) f <-> has_dim (KerS PE f) 0.
Proof.
rewrite bijS_image_equiv; [| apply inhabited_ms]; apply lmS_injS_has_dim_equiv.
Qed.

End Linear_mapping_Facts3a.


Section Linear_mapping_Facts3b.

Context {K : Ring}.
Context {E F : ModuleSpace K}.

Context {PE : E -> Prop}.
Variable nPE : nat.
Hypothesis HPE : has_dim PE nPE.
Let HPE' :=  has_dim_cms _ HPE.
Let PE_ms := sub_ModuleSpace HPE'.

Context {PF : F -> Prop}.

Context {f : E -> F}.
Hypothesis Hf : lin_map f.

Lemma RgS_fin_dim : funS PE PF f -> fin_dim (RgS PE PF f).
Proof. intros; rewrite RgS_eq//; apply (image_fin_dim HPE Hf). Qed.

Lemma lmS_injS_val_equiv_alt :
  injS PE f <-> forall (x : PE_ms), f (val x) = 0 -> x = 0.
Proof.
pose (f_sub := fun x : PE_ms => mk_sub (in_fullset (f (val x)))).
assert (Hf2 : forall x : PE_ms, val (f_sub x) = f (val x)) by easy.
apply: (lmS_injS_val_equiv Hf2 Hf); apply cms_fullset.
Qed.

End Linear_mapping_Facts3b.
