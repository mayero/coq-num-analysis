(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Support for duality in finite-dimensional module spaces on the ring of real
 numbers.

 * Description

 Let [E : ModuleSpace R_Ring].
 Let [PE : E -> Prop].
 Let [B : 'E^n] and [B' : '(E -> R)^n].

 - [dualF B B'] is the predicate stating that [fun i j => B' i (B j)] is the
   Kronecker delta function.

 Let [HPE : fin_dim PE] and [PE_ms := sub_ModuleSpace (fin_dim_cms HPE)]
   (where [fin_dim_cms HPE] is a proof that [compatible_ms PE],
   i.e. that [PE] is closed under linear operations).

 - [dual_basis (HB : basis PE B) : '(PE_ms -> R)^n] is the dual basis of [B].

 - [predual_basis (HB' : bijS PE fullset (gather B')) : 'E^n] is the predual
   basis of [B'], i.e. such that [B'] is its dual basis.

 * Used logic axioms

 - [ex_EX], an alias for [constructive_indefinite_description].

 * Usage

 This module may be used through the import of
 [Algebra.Finite_dim.Finite_dim_MS], [Algebra.Finite_dim.Finite_dim],
 [Algebra.Algebra], or [Algebra.Algebra_wDep], where it is exported.
*)

From Requisite Require Import stdlib_wR ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Subsets Require Import Subsets_wDep.
From Algebra Require Import Hierarchy_compl Monoid Group Ring ModuleSpace.
From Algebra Require Import matrix Finite_dim_MS_def Finite_dim_MS_lin_gen.
From Algebra Require Import Finite_dim_MS_lin_indep Finite_dim_MS_basis .
From Algebra Require Import Finite_dim_MS_lin_indep_R Finite_dim_MS_basis_R.
From Algebra Require Import Finite_dim_MS_lin_map_R.

Local Open Scope R_scope.
Local Open Scope Monoid_scope.
Local Open Scope Group_scope.
Local Open Scope Ring_scope.
Local Open Scope ModuleSpace_scope.


Section Dual_family.

Context {E : ModuleSpace R_Ring}.

Context {n : nat}.
Variable B : 'E^n.
Variable B' : '(E -> R)^n.

Definition dualF : Prop := forall i j, B' i (B j) = kronR i j.

Lemma dualF_lin_indep : dualF -> lin_indep B -> lin_indep B'.
Proof.
intros H HB L HL; extF j.
rewrite zeroF -(fct_zero_eq (B j)).
rewrite -(lc_kron_l_in_r _ L) lc_comm_R.
rewrite -(fun_ext_rev HL (B j)) fct_lc_r_eq.
apply lc_ext_r; intros i; rewrite H; apply kron_sym.
Qed.

Lemma dualF_lin_indep_rev :
  lin_map (gather B') -> dualF -> lin_indep B' -> lin_indep B.
Proof.
move=> /gather_lm_compat H0 H HB' L HL; extF i.
rewrite zeroF -(lm_zero (H0 i))//.
rewrite -(f_equal (B' i) HL) lm_lc//.
rewrite -lc_kron_l_in_r lc_comm_R.
apply lc_ext_l; intros j; rewrite mapF_correct H; easy.
Qed.

Lemma dualF_basis_equiv :
  lin_map (gather B') -> dualF ->
  basis (lin_span B) B <-> basis (lin_span B') B'.
Proof.
intros H0 H; split; intros [H1 H2]; split;
    [| apply (dualF_lin_indep H) | | apply (dualF_lin_indep_rev H0 H)]; easy.
Qed.

End Dual_family.


Section Dual_basis_Def.

Context {E : ModuleSpace R_Ring}.
Context {PE : E -> Prop}.
Hypothesis HPE : fin_dim PE.
Let HPE1 := fin_dim_cms HPE.
Let PE_ms := sub_ModuleSpace HPE1.

Context {n : nat}.
Context {B : 'E^n}.
Hypothesis HB : basis PE B.

(** Note that "eq_sym (proj1 HB)" is a proof of "lin_span B = PE".
 Thus, "Hx" is a proof of "lin_span B (val x)",
 and "proj1_sig (lin_span_EX _ _ Hx)" is the "L" such that "val x = lin_comb L B". *)
Definition dual_basis : '(PE_ms -> R)^n :=
  fun i x =>
    let Hx := eq_ind_r (@^~ (val x)) (in_sub x) (eq_sym (proj1 HB)) in
    proj1_sig (lin_span_EX _ _ Hx) i.
(*
Proof.
intros i [x Hx]; move: Hx; rewrite (proj1 HB);
    move=> /lin_span_EX [L _]; apply (L i).
Defined.
*)

End Dual_basis_Def.


Section Dual_basis_Facts0.

Context {E : ModuleSpace R_Ring}.
Context {PE : E -> Prop}.
Context {HPE : fin_dim PE}.
Let HPE1 := fin_dim_cms HPE.
Let PE_ms := sub_ModuleSpace HPE1.

Context {n : nat}.
Context {B : 'E^n}.
Context {HB : basis PE B}.

Lemma dual_basis_correct :
  forall {x : PE_ms} L,
    val x = lin_comb L B -> forall i, dual_basis HPE HB i x = L i.
Proof.
intros x L H i; unfold dual_basis;
    destruct (lin_span_EX _ _) as [L' HL']; simpl; move: i; apply fun_ext_rev.
apply (lin_indep_uniq_decomp (proj2 HB)); rewrite -HL'; easy.
Qed.

End Dual_basis_Facts0.


Section Dual_basis_Facts1.

Context {E : ModuleSpace R_Ring}.
Context {PE : E -> Prop}.
Context {HPE : fin_dim PE}.
Let HPE1 := fin_dim_cms HPE.
Let PE_ms := sub_ModuleSpace HPE1.

Context {n : nat}.
Context {B : 'E^n}.
Context {HB : basis PE B}.

Lemma dual_basis_lm : forall i, lin_map (dual_basis HPE HB i).
Proof.
pose (HB1 := proj1 HB); intros i; split; [intros x y | intros l x].
(* *)
destruct (lin_span_EX B (val x)) as [Lx HLx]; [rewrite -HB1; apply x |].
destruct (lin_span_EX B (val y)) as [Ly HLy]; [rewrite -HB1; apply y |].
rewrite (dual_basis_correct _ HLx) (dual_basis_correct _ HLy)
    (dual_basis_correct (Lx + Ly));
    [| rewrite lc_plus_l -HLx -HLy]; easy.
(* *)
destruct (lin_span_EX B (val x)) as [Lx HLx]; [rewrite -HB1; apply x |].
rewrite (dual_basis_correct _ HLx) (dual_basis_correct (scal l Lx));
    [| rewrite lc_scal_l -HLx]; easy.
Qed.

Let HBa := lin_gen_inclF (proj1 HB).
Let B_ms := fun i => (mk_sub (HBa i) : PE_ms).

Lemma dual_basis_dualF : dualF B_ms (dual_basis HPE HB).
Proof.
intros i j; rewrite (dual_basis_correct (kronR^~ j));
    [| rewrite lc_kron_l_in_l]; easy.
Qed.

Lemma dual_basis_decomp_compat :
  forall {B1 : 'E^n} (HB1 : basis PE B1) M M1,
    (forall i, B1 i = lin_comb (M i) B) -> M1 ** flipT M = II ->
    forall i, dual_basis HPE HB1 i = lin_comb (M1 i) (dual_basis HPE HB).
Proof.
intros B1 HB1 M M1 H1 HM i; fun_ext x; fold HPE1 in x.
destruct (basis_ex_decomp HB1 (val x)) as [L1 HL1]; [apply x |].
rewrite (dual_basis_correct _ HL1); rewrite (lc2_l_alt_sym H1) in HL1.
rewrite fct_lc_r_eq (fun_ext (dual_basis_correct _ HL1))
    -col1T_correct -(mx_mult_one_l (col1T L1)) -HM -mx_mult_assoc mx_mult_eq.
f_equal; extF; unfold flipT; rewrite mx_mult_eq; unfold flipT.
rewrite mapF_correct; f_equal; extF; apply col1T_correct.
Qed.

End Dual_basis_Facts1.


Section Dual_basis_Facts2.

Context {E : ModuleSpace R_Ring}.
Context {PE : E -> Prop}.
Variable HPE : fin_dim PE.
Let HPE1 := fin_dim_cms HPE.
Let PE_ms := sub_ModuleSpace HPE1.

Context {n : nat}.
Context {B : 'E^n}.
Variable HB : basis PE B.
Let HBa := lin_gen_inclF (proj1 HB).
Let B_ms := fun i => (mk_sub (HBa i) : PE_ms).

Lemma dual_basis_decomp :
  forall {f : PE_ms -> R}, lin_map f ->
    f = lin_comb (mapF f B_ms) (dual_basis HPE HB).
Proof.
pose (HB1 := proj1 HB); intros f Hf; fun_ext x.
destruct (lin_span_EX B (val x)) as [L HL]; [rewrite -HB1; apply x |].
rewrite fct_lc_r_eq (fun_ext (dual_basis_correct _ HL)).
rewrite lc_comm_R -lm_lc//; f_equal.
apply val_inj; rewrite val_lc; easy.
Qed.

End Dual_basis_Facts2.


Section Predual_basis_Def.

Context {E : ModuleSpace R_Ring}.
Context {PE : E -> Prop}.

Context {n : nat}.
Context {B' : '(E -> R)^n}.
(* Assume that (gather B' : E -> 'R^n) is a bijection from PE to 'R^n. *)
Hypothesis HB' : bijS PE fullset (gather B').

Definition predual_basis : 'E^n.
Proof.
move: (choiceF (fun (j : 'I_n) x => PE x /\
    forall i, B' i x = kronR i j)) => H.
apply ex_EX in H; [destruct H as [B HB]; apply B | clear H].
intros j; destruct HB' as [f [H1 [H2 [H3 H4]]]];
    exists (f (kronR^~ j)); split; [apply H2; easy |].
intros i; rewrite -(fun_ext_rev (H4 (kronR^~ j) (in_fullset _)) i); easy.
Defined.

(* There are other ways to define the predual basis, with other hypotheses.
 For instance, we can use the natural isomorphism between a subspace and its
 bidual (since dimensions are finite), and apply its inverse to the dual basis
 of B', assumed to be a basis.
 We can also use the Riesz-Fréchet representation of linear forms by the dot
 product with some vector, and take the orthogonal of the associated family.
 With both approaches, we have to solve a linear system, which is granted here
 by the bijectivity hypothesis HB'. *)

End Predual_basis_Def.


Section Predual_basis_Facts0a.

Context {E : ModuleSpace R_Ring}.
Context {PE : E -> Prop}.

Context {n : nat}.
Context {B' : '(E -> R)^n}.
Hypothesis HB' : bijS PE fullset (gather B').

Lemma predual_basis_in_sub : inclF (predual_basis HB') PE.
Proof. intro; unfold predual_basis; destruct (ex_EX _) as [B H]; apply H. Qed.

Lemma predual_basis_dualF : dualF (predual_basis HB') B'.
Proof.
move=>>; unfold predual_basis; destruct (ex_EX _) as [B HB]; apply HB.
Qed.

End Predual_basis_Facts0a.


Section Predual_basis_Facts0b.

Context {E : ModuleSpace R_Ring}.
Context {PE : E -> Prop}.
Context {nPE : nat}.
Hypothesis HPE : has_dim PE nPE.
Let HPE1 := has_dim_cms _ HPE.

Context {n : nat}.
Context {B' : '(E -> R)^n}.
Hypothesis HB'1 : lin_map (gather B').
Hypothesis HB' : KerS0 PE (gather B').

Lemma predual_basis_uniq :
  forall {B1 B2 : 'E^n},
    inclF B1 PE -> inclF B2 PE -> dualF B1 B' -> dualF B2 B' -> B1 = B2.
Proof.
move: HB'1 => /gather_lm_compat => HB'1a.
move=>> HB1a HB2a HB1b HB2b; extF; apply minus_zero_sym_reg.
apply HB'; [apply cms_minus; easy |]; extF.
rewrite gather_eq (lm_minus)// minus_zero_equiv HB2b HB1b; easy.
Qed.

End Predual_basis_Facts0b.


Section Predual_basis_Facts1.

Context {E : ModuleSpace R_Ring}.
Context {PE : E -> Prop}.

Context {n : nat}.
Hypothesis HPE : has_dim PE n.

Context {B' : '(E -> R)^n}.
Hypothesis HB'1 : lin_map (gather B').
Hypothesis HB' : bijS PE fullset (gather B').

Lemma predual_basis_basis : basis PE (predual_basis HB').
Proof.
apply lin_indep_basis; [easy | apply predual_basis_in_sub |].
apply (dualF_lin_indep_rev _ _ HB'1); [apply predual_basis_dualF |].
eapply (lin_indep_scatter _), bijS_surjS, HB'.
Qed.

End Predual_basis_Facts1.


(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!
 TODO FC (27/02/2024): this uses transition_matrix_ex, which is left aside for the moment.
Section Dual_subspace.

Context {E : ModuleSpace R_Ring}.
Context {PE : E -> Prop}.
Hypothesis HPE : fin_dim PE.
Let HPE1 := fin_dim_cms HPE.
Let PE_ms := sub_ModuleSpace HPE1.

Definition dual : (PE_ms -> R) -> Prop :=
  let HPE0 := ex_EX _ (fin_dim_has_dim HPE) in
  let HB := proj2_sig (has_dim_EX PE (proj1_sig HPE0) (proj2_sig HPE0)) in
  lin_span (dual_basis HPE HB).

Lemma dual_uniq :
  forall {n} {B : 'E^n} (HB : basis PE B), dual = lin_span (dual_basis HPE HB).
Proof.
intros n B HB.
pose (HPE0 := ex_EX _ (fin_dim_has_dim HPE)).
pose (n0 := proj1_sig HPE0); pose (Hn0 := proj2_sig HPE0); fold n0 in Hn0.
pose (B0 := proj1_sig (has_dim_EX PE n0 Hn0));
    pose (HB0 := proj2_sig (has_dim_EX PE n0 Hn0)); fold B0 in HB0.
assert (H1 : n = n0) by apply (dim_uniq (Dim _ _ _ HB)), Hn0.
subst; apply lin_span_ext; fold HPE0 n0; intros i; fold Hn0 HB0; apply lin_span_ex.
(* *)
destruct (transition_matrix_ex HB HB0) as [M [HM [M1 [_ HM1]]]].
unfold mult, one in HM1; simpl in HM1.
exists (flipT M1 i); apply (dual_basis_decomp_compat _ M _ HM).
rewrite mx_mult_flipT HM1; apply mx_one_sym.
(* *)
destruct (transition_matrix_ex HB0 HB) as [M [HM [M1 [_ HM1]]]].
unfold mult, one in HM1; simpl in HM1.
exists (flipT M1 i); apply (dual_basis_decomp_compat _ M _ HM).
rewrite mx_mult_flipT HM1; apply mx_one_sym.
Qed.

Lemma dual_basis_basis :
  forall {n} {B : 'E^n} (HB : basis PE B),
    basis dual (dual_basis HPE HB).
Proof.
intros n B HB; pose (HB1 := proj1 HB).
pose (B_ms := fun i => (mk_sub (lin_gen_inclF HB1 i) : PE_ms)).
rewrite (dual_uniq HB).
apply (dualF_basis_equiv B_ms (dual_basis HPE HB));
    [apply dual_basis_lm | apply dual_basis_dualF |].
apply basis_lin_span_equiv.
intros L HL; apply HB, trans_eq with (val (lin_comb L B_ms));
    [rewrite val_lc | rewrite HL]; easy.
Qed.

Lemma dual_has_dim : forall {n} {B : 'E^n}, basis PE B -> has_dim dual n.
Proof. move=>> HB; apply (Dim _ _ _ (dual_basis_basis HB)). Qed.

Lemma dual_fin_dim : fin_dim dual.
Proof.
move: (fin_dim_has_dim HPE) => [n [B HB]].
apply (has_dim_fin_dim (dual_has_dim HB)).
Qed.

Lemma dual_lm : forall f, dual f -> lin_map f.
Proof.
move=>> [L]; apply lc_lm_compat_r; intro;
    apply dual_basis_lm.
Qed.

Lemma dual_lm_rev : forall f, lin_map f -> dual f.
Proof.
intros f Hf; destruct (fin_dim_has_dim HPE) as [n [B HB]].
rewrite (dual_uniq HB) (dual_basis_decomp HPE HB Hf); easy.
Qed.

Lemma dual_lm_equiv : forall f, dual f <-> lin_map f.
Proof. intros; split; [apply dual_lm | apply dual_lm_rev]. Qed.

End Dual_subspace.


Section Bidual_basis.

Context {E : ModuleSpace R_Ring}.
Context {PE : E -> Prop}.
Hypothesis HPE : fin_dim PE.
Let HPE1 := fin_dim_cms HPE.
Let PE_ms := sub_ModuleSpace HPE1.

Let PE' := dual HPE.
Let HPE' := dual_fin_dim HPE.
Let HPE'1 := fin_dim_cms HPE'.
Let PE'_ms := sub_ModuleSpace HPE'1.

Context {n : nat}.
Context {B : 'E^n}.
Hypothesis HB : basis PE B.

Definition bidual_basis : '(PE'_ms -> R)^n :=
  dual_basis HPE' (dual_basis_basis HPE HB).

End Bidual_basis.


Section Bidual_subspace.

Context {E : ModuleSpace R_Ring}.
Context {PE : E -> Prop}.
Hypothesis HPE : fin_dim PE.
Let HPE1 := fin_dim_cms HPE.
Let PE_ms := sub_ModuleSpace HPE1.

Let PE' := dual HPE.
Let HPE' := dual_fin_dim HPE.
Let HPE'1 := fin_dim_cms HPE'.
Let PE'_ms := sub_ModuleSpace HPE'1.

Let PE'' := dual HPE'.
Let HPE'' := dual_fin_dim HPE'.
Let HPE''1 := fin_dim_cms HPE''.
Let PE''_ms := sub_ModuleSpace HPE''1.

Definition bidual : (PE'_ms -> R) -> Prop := PE''.

Lemma bidual_pt_eval : forall (x : PE_ms), bidual (fun f => val f x).
Proof. intros x; apply dual_lm_equiv; easy. Qed.

(* This is the natural isomorphism between a subspace and its bidual. *)
Definition bidual_isom (x : PE_ms) : PE''_ms :=
  mk_sub (bidual_pt_eval x).

Lemma bidual_isom_correct :
  forall (x : PE_ms) (f : PE'_ms), val (bidual_isom x) f = val f x.
Proof. easy. Qed.

Lemma bidual_isom_lm : lin_map bidual_isom.
Proof.
Aglopted.

Lemma bidual_isom_inj : injective bidual_isom.
Proof.
Aglopted.

Lemma bidual_isom_bij : bijective bidual_isom.
Proof.
Aglopted.

End Bidual_subspace.

Section Predual_basis.

Context {E : ModuleSpace R_Ring}.
Context {PE : E -> Prop}.
Hypothesis HPE : fin_dim PE.
Let HPE1 := fin_dim_cms HPE.
Let PE_ms := sub_ModuleSpace HPE1.

Let PE' := dual HPE.
Let HPE' := dual_fin_dim HPE.
Let HPE'1 := fin_dim_cms HPE'.
Let PE'_ms := sub_ModuleSpace HPE'1.

Let PE'' := dual HPE'.
Let HPE'' := dual_fin_dim HPE'.
Let HPE''1 := fin_dim_cms HPE''.
Let PE''_ms := sub_ModuleSpace HPE''1.

Context {n : nat}.
Variable B' : '(PE_ms -> R)^n.
Hypothesis HB' : basis PE' B'.
Let B'' := dual_basis HPE' HB'.
Let HB'' := dual_basis_basis HPE' HB'.

Definition predual_basis (i : 'I_n) : E :=
  val (f_inv (bidual_isom_bij HPE)
    (mk_sub (basis_inclF HB'' i))).

(*
Lemma predual_basis_dualF : dualF predual_basis (fun i => ...).
*)

Lemma predual_basis_basis : basis PE predual_basis.
Proof.
Aglopted.

Lemma predual_basis_correct : dual_basis HPE predual_basis_basis = B'.
Proof.
Aglopted.

End Predual_basis.
*)
