(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Support for monomial orders.

 * Description

 Monomial orders are homogeneous binary relations on finite families of an
 Abelian monoid that are strict or nonstrict total orders, and are compatible
 with the operation of the monoid.

 Let [G : AbelianMonoid].
 Let [R : G -> G -> Prop] be an homogeneous binary relation on [G].

 ** Elementary monomial properties

 - [br_plus_compat_l R] means that any relation [R x1 x2] is kept when
   "adding on the left" the same value to both arguments.
 - [br_plus_compat_r R] means that any relation [R x1 x2] is kept when
   "adding on the right" the same value to both arguments.

 - [br_plus_reg_l R] means that any relation [R (x + x1) (x + x2)] is kept
   when removing both "x +", ie when "subtracting on the left".
 - [br_plus_reg_r R] means that any relation [R (x1 + x) (x2 + x)] is kept
   when removing both "+ x", ie when "subtracting on the right".

 - [zero_is_left R] means that [0] is comparable on the left to all elements.
 - [zero_is_right R] means that [0] is comparable on the right to all elements.

 ** Compound monomial properties

 - [monomial_order R] means that [R] is irreflexive, asymmetric, transitive,
   connected and compatible with [plus] on the right,
   ie it is a strict total order compatible with [plus].
 - [monomial_order_zl R] means that [R] is a monomial order for which [0] is
   comparable on the left to all elements.
 - [monomial_order_zr R] means that [R] is a monomial order for which [0] is
   comparable on the right to all elements.
 - [monomial_order_ns R] means that [R] is reflexive, antisymmetric, transitive,
   strongly connected and compatible with [plus] on the right,
   ie it is a total order compatible with [plus].

 ** Operator

 Let [Rn : 'G^n -> 'G^n -> Prop] be an homogeneous binary relation on ['G^n].

 - [graded R Rn] compares the sum of the arguments with [R], and in case of
   equality, use [Rn].

 - [grlex R] is [graded R (lex R)].
 - [grcolex R] is [graded R (colex R)].
 - [grsymlex R] is [graded R (symlex R)].
 - [grevlex R] is [graded R (revlex R)].

 * Bibliography

 [[Cox_etal]]
 David A. Cox, John Little, Donal O'Shea,
 Ideals, Varieties, and Algorithms,
 fourth edition, Springer, 2015.

 [[MO]] https://en.wikipedia.org/wiki/Monomial_order.

 * Usage

 This module may be used through the import of [Algebra.Monoid.Monoid],
 [Algebra.Algebra], or [Algebra.Algebra_wDep], where it is exported.
*)

From Requisite Require Import stdlib ssr_wMC.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Subsets Require Import Subsets_wDep.
From Algebra Require Import Hierarchy_compl Monoid_compl Monoid_FF Monoid_sum.

Local Open Scope Monoid_scope.


Section Monomial_order_Def.

Context {G : AbelianMonoid}.
Variable R : G -> G -> Prop.

(** Elementary monomial properties. *)

(** "br" stands for "binary relation". *)
Definition br_plus_compat_l : Prop :=
  forall x x1 x2, R x1 x2 -> R (x + x1) (x + x2).
Definition br_plus_compat_r : Prop :=
  forall x x1 x2, R x1 x2 -> R (x1 + x) (x2 + x).

(** "br" stands for "binary relation". *)
Definition br_plus_reg_l : Prop :=
  forall x x1 x2, R (x + x1) (x + x2) -> R x1 x2.
Definition br_plus_reg_r : Prop :=
  forall x x1 x2, R (x1 + x) (x2 + x) -> R x1 x2.

Definition zero_is_left : Prop := forall x, x <> 0 -> R 0 x.
Definition zero_is_right : Prop := forall x, x <> 0 -> R x 0.

(** Compound monomial properties. *)

(** "br" stands for "binary relation". *)
Definition br_monoid_compat_zl : Prop :=
  br_plus_compat_r /\ zero_is_left.
Definition br_monoid_compat_zr : Prop :=
  br_plus_compat_r /\ zero_is_right.

Definition monomial_order : Prop := strict_total_order R /\ br_plus_compat_r.
Definition monomial_order_zl : Prop :=
  strict_total_order R /\ br_monoid_compat_zl.
Definition monomial_order_zr : Prop :=
  strict_total_order R /\ br_monoid_compat_zr.
(** "ns" stands for "nonstrict". *)
Definition monomial_order_ns : Prop := total_order R /\ br_plus_compat_r.

End Monomial_order_Def.


Section Monomial_order_Facts1.

Context {G : AbelianMonoid}.
Hypothesis HG1 : eq_dec G.

(** Results about monomial properties. *)

(** About the [br_plus_compat_l] / [br_plus_compat_r] /
 [br_plus_reg_l] / [br_plus_reg_r] elementary properties. *)

Lemma br_plus_compat_equiv :
  forall {R : G -> G -> Prop}, br_plus_compat_l R <-> br_plus_compat_r R.
Proof.
intro; split; intros H x x1 x2;
    [rewrite !(plus_comm _ x) | rewrite !(plus_comm x)]; apply H.
Qed.

Lemma br_plus_compat2_r :
  forall {R : G -> G -> Prop},
    transitive R -> br_plus_compat_r R ->
    forall a b c d, R a c -> R b d -> R (a + b) (c + d).
Proof.
intros R HR1 HR2 a b c d Hx Hy; apply HR1 with (c + b); auto.
apply br_plus_compat_equiv in HR2; auto.
Qed.

Lemma br_plus_reg_equiv :
  forall {R : G -> G -> Prop}, br_plus_reg_l R <-> br_plus_reg_r R.
Proof.
intro; split; intros H x x1 x2;
    [rewrite -!(plus_comm x) | rewrite !(plus_comm x)]; apply H.
Qed.

(* The proof uses the equality decidability hypothesis HG1. *)
Lemma br_plus_compat_reg_r :
  forall {R : G -> G -> Prop},
    irreflexive R -> asymmetric R -> connected R ->
    br_plus_compat_r R -> br_plus_reg_r R.
Proof.
intros R H1 H2 H3 H4 x x1 x2 H5; destruct (HG1 x1 x2) as [H6 | H6].
exfalso; subst; apply (H1 _ H5).
destruct (H3 _ _ H6) as [H7 | H7]; [easy | exfalso].
apply (H2 (x1 + x) (x2 + x)); [easy | apply (H4 _ _ _ H7)].
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma br_plus_compat_reg_l :
  forall {R : G -> G -> Prop},
    irreflexive R -> asymmetric R -> connected R ->
    br_plus_compat_l R -> br_plus_reg_l R.
Proof.
intro; rewrite br_plus_compat_equiv br_plus_reg_equiv;
    apply br_plus_compat_reg_r.
Qed.

Lemma br_plus_compat_is_reg_r :
  forall {R : G -> G -> Prop},
    irreflexive R -> connected R -> br_plus_compat_r R -> @plus_is_reg_r G.
Proof.
move=>> H1 /conn_equiv_contra H2 H3 x x1 x2 H4; apply H2; intros H5;
    apply (H1 (x1 + x)); [rewrite {2}H4 | rewrite {1}H4]; apply H3; easy.
Qed.

Lemma br_plus_compat_is_reg_l :
  forall {R : G -> G -> Prop},
    irreflexive R -> connected R -> br_plus_compat_l R -> @plus_is_reg_l G.
Proof.
intro; rewrite br_plus_compat_equiv plus_is_reg_equiv.
apply br_plus_compat_is_reg_r.
Qed.

Lemma monomial_order_plus_is_reg_r :
  forall (R : G -> G -> Prop), monomial_order R -> @plus_is_reg_r G.
Proof. move=>> [HR1 /br_plus_compat_is_reg_r HR2]; apply HR2; apply HR1. Qed.

Lemma monomial_order_plus_is_reg_l :
  forall (R : G -> G -> Prop), monomial_order R -> @plus_is_reg_l G.
Proof. move=>> /monomial_order_plus_is_reg_r; apply plus_is_reg_equiv. Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma br_plus_conn :
  forall {R : G -> G -> Prop},
    monomial_order R -> forall a b c d, a + b = c + d -> R a c -> R d b.
Proof.
intros R [HR1 HR2].
assert (HG2 : @plus_is_reg_r G) by now apply (monomial_order_plus_is_reg_r R).
generalize HR1; move => [/irrefl_equiv HR1A _].
assert (HR1B : trichotomous R)
    by now apply (strict_total_order_equiv_tricho HG1) in HR1.
intros a b c d H1 H2; destruct (HR1B b d) as [H | [H | H]]; [.. | easy].
exfalso; destruct H as [H _]; subst; apply (HR1A a c); [apply (HG2 d) |]; easy.
exfalso; destruct H as [_ [H _]]; apply (HR1A (a + b) (c + d)); [easy |].
apply br_plus_compat2_r; [apply HR1 | easy..].
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma br_plus_conn_equiv :
  forall {R : G -> G -> Prop},
    monomial_order R ->
    forall a b c d, a + b = c + d -> R a c <-> R d b.
Proof.
intros; split; apply br_plus_conn; [..| rewrite plus_comm (plus_comm b)]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma br_sum_conn_equiv :
  forall {R : G -> G -> Prop},
    monomial_order R ->
    forall {n} {u v : 'G^n.+1} {i0},
      sum u = sum v ->
      R (u i0) (v i0) <-> R (sum (skipF v i0)) (sum (skipF u i0)).
Proof.
move=>> HR n u v i0; rewrite !(sum_skipF _ i0); apply br_plus_conn_equiv; easy.
Qed.

(** About the [monomial_order] / [monomial_order_ns] compound properties. *)

(* "sto" stands for "strict_total_order". *)
Lemma monomial_order_sto :
  forall {R : G -> G -> Prop}, monomial_order R -> strict_total_order R.
Proof. intros R [H _]; easy. Qed.

Lemma monomial_order_irrefl :
  forall {R : G -> G -> Prop}, monomial_order R -> irreflexive R.
Proof. intros R [H _]; apply H. Qed.

Lemma monomial_order_asym :
  forall {R : G -> G -> Prop}, monomial_order R -> asymmetric R.
Proof. intros R [H _]; apply H. Qed.

Lemma monomial_order_trans :
  forall {R : G -> G -> Prop}, monomial_order R -> transitive R.
Proof. intros R [H _]; apply H. Qed.

Lemma monomial_order_conn :
  forall {R : G -> G -> Prop}, monomial_order R -> connected R.
Proof. intros R [H _]; apply H. Qed.

Lemma monomial_order_br_plus_compat_r :
  forall {R : G -> G -> Prop}, monomial_order R -> br_plus_compat_r R.
Proof. intros R [_ H]; easy. Qed.

Lemma monomial_order_br_plus_compat_l :
  forall {R : G -> G -> Prop}, monomial_order R -> br_plus_compat_l R.
Proof.
intro; rewrite br_plus_compat_equiv; apply monomial_order_br_plus_compat_r.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma monomial_order_br_plus_reg_r :
  forall {R : G -> G -> Prop}, monomial_order R -> br_plus_reg_r R.
Proof.
intros R H; apply br_plus_compat_reg_r; move: H.
apply monomial_order_irrefl.
apply monomial_order_asym.
apply monomial_order_conn.
apply monomial_order_br_plus_compat_r.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma monomial_order_br_plus_reg_l :
  forall {R : G -> G -> Prop}, monomial_order R -> br_plus_reg_l R.
Proof.
intro; rewrite br_plus_reg_equiv; apply monomial_order_br_plus_reg_r.
Qed.

(* "to" stands for "total_order". *)
Lemma monomial_order_ns_to :
  forall {R : G -> G -> Prop}, monomial_order_ns R -> total_order R.
Proof. intros R [H _]; easy. Qed.

Lemma monomial_order_ns_refl :
  forall {R : G -> G -> Prop}, monomial_order_ns R -> reflexive R.
Proof. intros R [H _]; apply H. Qed.

Lemma monomial_order_ns_antisym :
  forall {R : G -> G -> Prop}, monomial_order_ns R -> antisymmetric R.
Proof. intros R [H _]; apply H. Qed.

Lemma monomial_order_ns_trans :
  forall {R : G -> G -> Prop}, monomial_order_ns R -> transitive R.
Proof. intros R [H _]; apply H. Qed.

Lemma monomial_order_ns_str_conn :
  forall {R : G -> G -> Prop}, monomial_order_ns R -> strongly_connected R.
Proof. intros R [H _]; apply H. Qed.

Lemma monomial_order_ns_br_plus_compat_r :
  forall {R : G -> G -> Prop}, monomial_order_ns R -> br_plus_compat_r R.
Proof. intros R [_ H]; easy. Qed.

Lemma monomial_order_ns_br_plus_compat_l :
  forall {R : G -> G -> Prop}, monomial_order_ns R -> br_plus_compat_l R.
Proof.
intro; rewrite br_plus_compat_equiv; apply monomial_order_ns_br_plus_compat_r.
Qed.

(** Nonstrict monomial orders are reflexive, thus they cannot satisfy
 regularity properties [br_plus_reg_l] / [br_plus_reg_r] since those need
 irreflexivity, which is incompatible with reflexivity.

 In the same way, nonstrict monomial orders do not imply regularity of [plus]
 (ie [plus_is_reg_l] / [plus_is_reg_r] hold in the ambient abelian monoid). *)

End Monomial_order_Facts1.


Section Monomial_order_Facts2.

(** Compatibility results of monomial properties with operators. *)

Context {G : AbelianMonoid}.
Hypothesis HG1 : eq_dec G.

(** With the [converse] operator. *)

Lemma br_plus_compat_l_conv :
  forall {R : G -> G -> Prop},
    br_plus_compat_l R -> br_plus_compat_l (converse R).
Proof. intros R H x x1 x2; apply (H x x2 x1). Qed.

Lemma br_plus_compat_l_conv_equiv :
  forall {R : G -> G -> Prop},
    br_plus_compat_l (converse R) <-> br_plus_compat_l R.
Proof.
intros R; split; [rewrite -{2}(conv_invol R) |]; apply br_plus_compat_l_conv.
Qed.

Lemma br_plus_compat_r_conv :
  forall {R : G -> G -> Prop},
    br_plus_compat_r R -> br_plus_compat_r (converse R).
Proof. intros R H x x1 x2; apply (H x x2 x1). Qed.

Lemma br_plus_compat_r_conv_equiv :
  forall {R : G -> G -> Prop},
    br_plus_compat_r (converse R) <-> br_plus_compat_r R.
Proof.
intros R; split; [rewrite -{2}(conv_invol R) |]; apply br_plus_compat_r_conv.
Qed.

Lemma br_plus_reg_l_conv :
  forall {R : G -> G -> Prop}, br_plus_reg_l R -> br_plus_reg_l (converse R).
Proof. intros R H x x1 x2; apply (H x x2 x1). Qed.

Lemma br_plus_reg_l_conv_equiv :
  forall {R : G -> G -> Prop}, br_plus_reg_l (converse R) <-> br_plus_reg_l R.
Proof.
intros R; split; [rewrite -{2}(conv_invol R) |]; apply br_plus_reg_l_conv.
Qed.

Lemma br_plus_reg_r_conv :
  forall {R : G -> G -> Prop}, br_plus_reg_r R -> br_plus_reg_r (converse R).
Proof. intros R H x x1 x2; apply (H x x2 x1). Qed.

Lemma br_plus_reg_r_conv_equiv :
  forall {R : G -> G -> Prop}, br_plus_reg_r (converse R) <-> br_plus_reg_r R.
Proof.
intros R; split; [rewrite -{2}(conv_invol R) |]; apply br_plus_reg_r_conv.
Qed.

Lemma zero_is_left_conv :
  forall {R : G -> G -> Prop}, zero_is_right R -> zero_is_left (converse R).
Proof. intros R H x; apply H. Qed.

Lemma zero_is_right_conv :
  forall {R : G -> G -> Prop}, zero_is_left R -> zero_is_right (converse R).
Proof. intros R H x; apply H. Qed.

Lemma zero_is_left_conv_equiv :
  forall {R : G -> G -> Prop}, zero_is_left (converse R) <-> zero_is_right R.
Proof.
intros R; split; [rewrite -{2}(conv_invol R) |];
    [apply zero_is_right_conv | apply zero_is_left_conv].
Qed.

Lemma zero_is_right_conv_equiv :
  forall {R : G -> G -> Prop}, zero_is_right (converse R) <-> zero_is_left R.
Proof. intro; rewrite zero_is_left_conv_equiv; easy. Qed.

Lemma br_monoid_compat_zl_conv :
  forall {R : G -> G -> Prop},
    br_monoid_compat_zr R -> br_monoid_compat_zl (converse R).
Proof.
intro; apply modus_ponens_and;
    [apply br_plus_compat_r_conv | apply zero_is_left_conv].
Qed.

Lemma br_monoid_compat_zr_conv :
  forall {R : G -> G -> Prop},
    br_monoid_compat_zl R -> br_monoid_compat_zr (converse R).
Proof.
intro; apply modus_ponens_and;
    [apply br_plus_compat_r_conv | apply zero_is_right_conv].
Qed.

Lemma br_monoid_compat_zl_conv_equiv :
  forall {R : G -> G -> Prop},
    br_monoid_compat_zl (converse R) <-> br_monoid_compat_zr R.
Proof.
intros R; split; [rewrite -{2}(conv_invol R) |];
    [apply br_monoid_compat_zr_conv | apply br_monoid_compat_zl_conv].
Qed.

Lemma br_monoid_compat_zr_conv_equiv :
  forall {R : G -> G -> Prop},
    br_monoid_compat_zr (converse R) <-> br_monoid_compat_zl R.
Proof. intro; rewrite br_monoid_compat_zl_conv_equiv; easy. Qed.

Lemma monomial_order_conv :
  forall {R : G -> G -> Prop}, monomial_order R -> monomial_order (converse R).
Proof.
intro; apply modus_ponens_and;
    [apply conv_strict_total_order | apply br_plus_compat_r_conv].
Qed.

Lemma monomial_order_conv_equiv :
  forall {R : G -> G -> Prop},
    monomial_order (converse R) <-> monomial_order R.
Proof.
intros R; split; [rewrite -{2}(conv_invol R) |]; apply monomial_order_conv.
Qed.

Lemma monomial_order_zl_conv :
  forall {R : G -> G -> Prop},
    monomial_order_zr R -> monomial_order_zl (converse R).
Proof.
intro; apply modus_ponens_and;
    [apply conv_strict_total_order | apply br_monoid_compat_zl_conv].
Qed.

Lemma monomial_order_zr_conv :
  forall {R : G -> G -> Prop},
    monomial_order_zl R -> monomial_order_zr (converse R).
Proof.
intro; apply modus_ponens_and;
    [apply conv_strict_total_order | apply br_monoid_compat_zr_conv].
Qed.

Lemma monomial_order_zl_conv_equiv :
  forall {R : G -> G -> Prop},
    monomial_order_zl (converse R) <-> monomial_order_zr R.
Proof.
intros R; split; [rewrite -{2}(conv_invol R) |];
    [apply monomial_order_zr_conv | apply monomial_order_zl_conv].
Qed.

Lemma monomial_order_zr_conv_equiv :
  forall {R : G -> G -> Prop},
    monomial_order_zr (converse R) <-> monomial_order_zl R.
Proof. intro; rewrite monomial_order_zl_conv_equiv; easy. Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma monomial_order_ns_conv :
  forall {R : G -> G -> Prop},
    monomial_order_ns R -> monomial_order_ns (converse R).
Proof.
intro; apply modus_ponens_and;
    [apply conv_total_order; easy | apply br_plus_compat_r_conv].
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma monomial_order_ns_conv_equiv :
  forall {R : G -> G -> Prop},
    monomial_order_ns (converse R) <-> monomial_order_ns R.
Proof.
intros R; split; [rewrite -{2}(conv_invol R) |]; apply monomial_order_ns_conv.
Qed.

(** With the [complementary] operator. *)

Lemma br_plus_compat_l_compl :
  forall {R : G -> G -> Prop},
    br_plus_reg_l R -> br_plus_compat_l (complementary R).
Proof. intros R H x x1 x2; rewrite -contra_equiv; apply H. Qed.

Lemma br_plus_compat_r_compl :
  forall {R : G -> G -> Prop},
    br_plus_reg_r R -> br_plus_compat_r (complementary R).
Proof. intros R H x x1 x2; rewrite -contra_equiv; apply H. Qed.

Lemma br_plus_reg_l_compl :
  forall {R : G -> G -> Prop},
    br_plus_compat_l R -> br_plus_reg_l (complementary R).
Proof. intros R H x x1 x2; rewrite -contra_equiv; apply H. Qed.

Lemma br_plus_reg_r_compl :
  forall {R : G -> G -> Prop},
    br_plus_compat_r R -> br_plus_reg_r (complementary R).
Proof. intros R H x x1 x2; rewrite -contra_equiv; apply H. Qed.

Lemma br_plus_compat_l_compl_equiv :
  forall {R : G -> G -> Prop},
    br_plus_compat_l (complementary R) <-> br_plus_reg_l R.
Proof.
intros R; split; [rewrite -{2}(br_compl_invol R) |];
    [apply br_plus_reg_l_compl | apply br_plus_compat_l_compl].
Qed.

Lemma br_plus_compat_r_compl_equiv :
  forall {R : G -> G -> Prop},
    br_plus_compat_r (complementary R) <-> br_plus_reg_r R.
Proof.
intros R; split; [rewrite -{2}(br_compl_invol R) |];
    [apply br_plus_reg_r_compl | apply br_plus_compat_r_compl].
Qed.

Lemma br_plus_reg_l_compl_equiv :
  forall {R : G -> G -> Prop},
    br_plus_reg_l (complementary R) <-> br_plus_compat_l R.
Proof.
intros R; rewrite -{2}(br_compl_invol R) br_plus_compat_l_compl_equiv; easy.
Qed.

Lemma br_plus_reg_r_compl_equiv :
  forall {R : G -> G -> Prop},
    br_plus_reg_r (complementary R) <-> br_plus_compat_r R.
Proof.
intros R; rewrite -{2}(br_compl_invol R) br_plus_compat_r_compl_equiv; easy.
Qed.

(** Properties [zero_is_left] / [zero_is_right] / [br_monoid_compat_zl] /
 [br_monoid_compat_zr] / [monomial_order_zl] / [monomial_order_zl] are not
 compatible with the operator [complementary]. *)

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma monomial_order_compl :
  forall {R : G -> G -> Prop},
    total_order R -> br_plus_reg_r R -> monomial_order (complementary R).
Proof.
intros; split;
    [apply compl_strict_total_order | apply br_plus_compat_r_compl]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma monomial_order_compl_equiv :
  forall {R : G -> G -> Prop},
    monomial_order (complementary R) <-> total_order R /\ br_plus_reg_r R.
Proof.
intros; apply and_iff_compat; [apply compl_strict_total_order_equiv; easy |
    apply br_plus_compat_r_compl_equiv].
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma monomial_order_ns_compl :
  forall {R : G -> G -> Prop},
    strict_total_order R -> br_plus_reg_r R ->
    monomial_order_ns (complementary R).
Proof.
intros; split;
    [apply compl_total_order | apply br_plus_compat_r_compl]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma monomial_order_ns_compl_equiv :
  forall {R : G -> G -> Prop},
    monomial_order_ns (complementary R) <->
    strict_total_order R /\ br_plus_reg_r R.
Proof.
intros; apply and_iff_compat; [apply compl_total_order_equiv; easy |
    apply br_plus_compat_r_compl_equiv].
Qed.

(** With the [conv_compl] operator. *)

Lemma br_plus_compat_l_conv_compl :
  forall {R : G -> G -> Prop},
    br_plus_reg_l R -> br_plus_compat_l (conv_compl R).
Proof. intros; apply br_plus_compat_l_conv, br_plus_compat_l_compl; easy. Qed.

Lemma br_plus_compat_r_conv_compl :
  forall {R : G -> G -> Prop},
    br_plus_reg_r R -> br_plus_compat_r (conv_compl R).
Proof. intros; apply br_plus_compat_r_conv, br_plus_compat_r_compl; easy. Qed.

Lemma br_plus_reg_l_conv_compl :
  forall {R : G -> G -> Prop},
    br_plus_compat_l R -> br_plus_reg_l (conv_compl R).
Proof. intros; apply br_plus_reg_l_conv, br_plus_reg_l_compl; easy. Qed.

Lemma br_plus_reg_r_conv_compl :
  forall {R : G -> G -> Prop},
    br_plus_compat_r R -> br_plus_reg_r (conv_compl R).
Proof. intros; apply br_plus_reg_r_conv, br_plus_reg_r_compl; easy. Qed.

Lemma br_plus_compat_l_conv_compl_equiv :
  forall {R : G -> G -> Prop},
    br_plus_compat_l (conv_compl R) <-> br_plus_reg_l R.
Proof.
intros; rewrite br_plus_compat_l_conv_equiv br_plus_compat_l_compl_equiv; easy.
Qed.

Lemma br_plus_compat_r_conv_compl_equiv :
  forall {R : G -> G -> Prop},
    br_plus_compat_r (conv_compl R) <-> br_plus_reg_r R.
Proof.
intros; rewrite br_plus_compat_r_conv_equiv br_plus_compat_r_compl_equiv; easy.
Qed.

Lemma br_plus_reg_l_conv_compl_equiv :
  forall {R : G -> G -> Prop},
    br_plus_reg_l (conv_compl R) <-> br_plus_compat_l R.
Proof.
intros; rewrite br_plus_reg_l_conv_equiv br_plus_reg_l_compl_equiv; easy.
Qed.

Lemma br_plus_reg_r_conv_compl_equiv :
  forall {R : G -> G -> Prop},
    br_plus_reg_r (conv_compl R) <-> br_plus_compat_r R.
Proof.
intros; rewrite br_plus_reg_r_conv_equiv br_plus_reg_r_compl_equiv; easy.
Qed.

(** Properties [zero_is_left] / [zero_is_right] / [br_monoid_compat_zl] /
 [br_monoid_compat_zr] / [monomial_order_zl] / [monomial_order_zl] are not
 compatible with the operator [conv_compl]. *)

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma monomial_order_conv_compl :
  forall {R : G -> G -> Prop},
    total_order R -> br_plus_reg_r R -> monomial_order (conv_compl R).
Proof. intros; apply monomial_order_conv, monomial_order_compl; easy. Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma monomial_order_conv_compl_equiv :
  forall {R : G -> G -> Prop},
    monomial_order (conv_compl R) <-> total_order R /\ br_plus_reg_r R.
Proof.
intros; rewrite monomial_order_conv_equiv monomial_order_compl_equiv; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma monomial_order_ns_conv_compl :
  forall {R : G -> G -> Prop},
    strict_total_order R -> br_plus_reg_r R ->
    monomial_order_ns (conv_compl R).
Proof.
intros; apply monomial_order_ns_conv, monomial_order_ns_compl; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma monomial_order_ns_conv_compl_equiv :
  forall {R : G -> G -> Prop},
    monomial_order_ns (conv_compl R) <->
    strict_total_order R /\ br_plus_reg_r R.
Proof.
intro; rewrite monomial_order_ns_conv_equiv
    monomial_order_ns_compl_equiv; easy.
Qed.

End Monomial_order_Facts2.


Section Monomial_order_nat_Facts.

Let Hlt1 := Nat.add_lt_mono_l.
Let Hlt2 := Nat.add_lt_mono_r.

Lemma lt_plus_compat_l : br_plus_compat_l lt.
Proof. move=>>; apply Hlt1. Qed.

Lemma lt_plus_compat_r : br_plus_compat_r lt.
Proof. move=>>; apply Hlt2. Qed.

Lemma lt_plus_reg_l : br_plus_reg_l lt.
Proof. move=>>; apply Hlt1. Qed.

Lemma lt_plus_reg_r : br_plus_reg_r lt.
Proof. move=>>; apply Hlt2. Qed.

Lemma lt_zero_is_left : zero_is_left lt.
Proof. intro; apply nat_neq_0_equiv. Qed.

Lemma lt_monoid_compat_zl : br_monoid_compat_zl lt.
Proof. split; [apply lt_plus_compat_r | apply lt_zero_is_left]. Qed.

Lemma lt_monomial_order : monomial_order lt.
Proof. split; [apply lt_strict_total_order | apply lt_plus_compat_r]. Qed.

Lemma lt_monomial_order_zl : monomial_order_zl lt.
Proof. split; [apply lt_strict_total_order | apply lt_monoid_compat_zl]. Qed.

Let Hle1 := Nat.add_le_mono_l.
Let Hle2 := Nat.add_le_mono_r.
Let Hle3 := Nat.le_0_l.

Lemma le_plus_compat_l : br_plus_compat_l le.
Proof. move=>>; apply Hle1. Qed.

Lemma le_plus_compat_r : br_plus_compat_r le.
Proof. move=>>; apply Hle2. Qed.

Lemma le_plus_reg_l : br_plus_reg_l le.
Proof. move=>>; apply Hle1. Qed.

Lemma le_plus_reg_r : br_plus_reg_r le.
Proof. move=>>; apply Hle2. Qed.

Lemma le_zero_is_left : zero_is_left le.
Proof. intros x _; apply Hle3. Qed.

Lemma le_monoid_compat_zl : br_monoid_compat_zl le.
Proof. split; [apply le_plus_compat_r | apply le_zero_is_left]. Qed.

Lemma le_monomial_order_ns : monomial_order_ns le.
Proof. split; [apply le_total_order | apply le_plus_compat_r]. Qed.

End Monomial_order_nat_Facts.


Section Lex_order_Facts.

(** Compatibility results of the *[lex] operators with monomial properties. *)

Context {G : AbelianMonoid}.
Let HG0 := @inhabited_m G.
Hypothesis HG1 : eq_dec G.
Hypothesis HG2 : @plus_is_reg_r G.

(** With the [br_plus_compat_r] / [br_plus_compat_l] elementary properties. *)

(* The proof uses the plus regularity hypothesis HG2. *)
Lemma lex_br_plus_compat_r :
  forall {R : G -> G -> Prop},
    br_plus_compat_r R -> forall {n}, br_plus_compat_r (@lex _ R n).
Proof.
intros R H1 n x x1 x2; induction n; [easy |].
rewrite !lex_S; rewrite !fct_plus_eq; intros [[H2 H3] | [H2 H3]].
left; split; [contradict H2; apply HG2 with (x ord0) | apply H1]; easy.
right; split; [apply plus_compat_r | apply IHn]; easy.
Qed.

(* The proof uses the plus regularity hypothesis HG2. *)
Lemma colex_br_plus_compat_r :
  forall {R : G -> G -> Prop},
    br_plus_compat_r R -> forall {n}, br_plus_compat_r (@colex _ R n).
Proof.
intros R H1 n x x1 x2; induction n; [easy |].
rewrite !colex_S; rewrite !fct_plus_eq; intros [[H2 H3] | [H2 H3]].
left; split; [contradict H2; apply HG2 with (x ord_max) | apply H1]; easy.
right; split; [apply plus_compat_r | apply IHn]; easy.
Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma symlex_br_plus_compat_r :
  forall {R : G -> G -> Prop},
    br_plus_compat_r R -> forall {n}, br_plus_compat_r (@symlex _ R n).
Proof. intros; apply br_plus_compat_r_conv, lex_br_plus_compat_r; easy. Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma revlex_br_plus_compat_r :
  forall {R : G -> G -> Prop},
    br_plus_compat_r R -> forall {n}, br_plus_compat_r (@revlex _ R n).
Proof. intros; apply br_plus_compat_r_conv, colex_br_plus_compat_r; easy. Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma lex_br_plus_compat_l :
  forall {R : G -> G -> Prop},
    br_plus_compat_l R -> forall {n}, br_plus_compat_l (@lex _ R n).
Proof.
intros R H1 n; move: H1; rewrite !br_plus_compat_equiv; intros H1.
apply lex_br_plus_compat_r; easy.
Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma colex_br_plus_compat_l :
  forall {R : G -> G -> Prop},
    br_plus_compat_l R -> forall {n}, br_plus_compat_l (@colex _ R n).
Proof.
intros R H1 n; move: H1; rewrite !br_plus_compat_equiv; intros H1.
apply colex_br_plus_compat_r; easy.
Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma symlex_br_plus_compat_l :
  forall {R : G -> G -> Prop},
    br_plus_compat_l R -> forall {n}, br_plus_compat_l (@symlex _ R n).
Proof. intros; apply br_plus_compat_l_conv, lex_br_plus_compat_l; easy. Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma revlex_br_plus_compat_l :
  forall {R : G -> G -> Prop},
    br_plus_compat_l R -> forall {n}, br_plus_compat_l (@revlex _ R n).
Proof. intros; apply br_plus_compat_l_conv, colex_br_plus_compat_l; easy. Qed.

(** With the [br_plus_reg_r] / [br_plus_reg_l] elementary properties. *)

(* The proof uses the plus regularity hypothesis HG2. *)
Lemma lex_br_plus_reg_r :
  forall {R : G -> G -> Prop},
    br_plus_reg_r R -> forall {n}, br_plus_reg_r (@lex _ R n).
Proof.
intros R H1 n x x1 x2; induction n; [easy |].
rewrite !lex_S; rewrite !fct_plus_eq; intros [[H2 H3] | [H2 H3]].
left; split;
    [contradict H2; apply plus_compat_r | apply H1 with (x ord0)]; easy.
right; split;
    [apply HG2 with (x ord0) | apply IHn with (skipF x ord0)]; easy.
Qed.

(* The proof uses the plus regularity hypothesis HG2. *)
Lemma colex_br_plus_reg_r :
  forall {R : G -> G -> Prop},
    br_plus_reg_r R -> forall {n}, br_plus_reg_r (@colex _ R n).
Proof.
intros R H1 n x x1 x2; induction n; [easy |].
rewrite !colex_S; rewrite !fct_plus_eq; intros [[H2 H3] | [H2 H3]].
left; split;
    [contradict H2; apply plus_compat_r | apply H1 with (x ord_max)]; easy.
right; split;
    [apply HG2 with (x ord_max) | apply IHn with (skipF x ord_max)]; easy.
Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma symlex_br_plus_reg_r :
  forall {R : G -> G -> Prop},
    br_plus_reg_r R -> forall {n}, br_plus_reg_r (@symlex _ R n).
Proof. intros; apply br_plus_reg_r_conv, lex_br_plus_reg_r; easy. Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma revlex_br_plus_reg_r :
  forall {R : G -> G -> Prop},
    br_plus_reg_r R -> forall {n}, br_plus_reg_r (@revlex _ R n).
Proof. intros; apply br_plus_reg_r_conv, colex_br_plus_reg_r; easy. Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma lex_br_plus_reg_l :
  forall {R : G -> G -> Prop},
    br_plus_reg_l R -> forall {n}, br_plus_reg_l (@lex _ R n).
Proof.
intros R H1 n; move: H1; rewrite !br_plus_reg_equiv; intros H1.
apply lex_br_plus_reg_r; easy.
Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma colex_br_plus_reg_l :
  forall {R : G -> G -> Prop},
    br_plus_reg_l R -> forall {n}, br_plus_reg_l (@colex _ R n).
Proof.
intros R H1 n; move: H1; rewrite !br_plus_reg_equiv; intros H1.
apply colex_br_plus_reg_r; easy.
Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma symlex_br_plus_reg_l :
  forall {R : G -> G -> Prop},
    br_plus_reg_l R -> forall {n}, br_plus_reg_l (@symlex _ R n).
Proof. intros; apply br_plus_reg_l_conv, lex_br_plus_reg_l; easy. Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma revlex_br_plus_reg_l :
  forall {R : G -> G -> Prop},
    br_plus_reg_l R -> forall {n}, br_plus_reg_l (@revlex _ R n).
Proof. intros; apply br_plus_reg_l_conv, colex_br_plus_reg_l; easy. Qed.

(** With the [zero_is_left] / [zero_is_right] elementary properties. *)

(* The proof uses the equality decidability hypothesis HG1. *)
Lemma lex_zero_is_left :
  forall {R : G -> G -> Prop},
    zero_is_left R -> forall {n}, zero_is_left (@lex _ R n).
Proof.
intros R H n x Hx; induction n; [contradict Hx; apply hat0F_unit; easy |].
rewrite lex_S; destruct (HG1 0 (x ord0)) as [Hx0 | Hx0].
rewrite -Hx0; right; split; [easy |].
apply IHn; contradict Hx; apply extF_skipF with ord0; easy.
left; split; [| apply H, neq_sym_equiv]; easy.
Qed.

(* The proof uses the equality decidability hypothesis HG1. *)
Lemma colex_zero_is_left :
  forall {R : G -> G -> Prop},
    zero_is_left R -> forall {n}, zero_is_left (@colex _ R n).
Proof.
intros R H n x Hx; induction n; [contradict Hx; apply hat0F_unit; easy |].
rewrite colex_S; destruct (HG1 0 (x ord_max)) as [Hxn | Hxn].
rewrite -Hxn; right; split; [easy |].
apply IHn; contradict Hx; apply extF_skipF with ord_max; easy.
left; split; [| apply H, neq_sym_equiv]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma symlex_zero_is_right :
  forall {R : G -> G -> Prop},
    zero_is_left R -> forall {n}, zero_is_right (@symlex _ R n).
Proof. intros; apply zero_is_right_conv, lex_zero_is_left; easy. Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma revlex_zero_is_right :
  forall {R : G -> G -> Prop},
    zero_is_left R -> forall {n}, zero_is_right (@revlex _ R n).
Proof. intros; apply zero_is_right_conv, colex_zero_is_left; easy. Qed.

(* The proof uses the equality decidability hypothesis HG1. *)
Lemma lex_zero_is_right :
  forall {R : G -> G -> Prop},
    zero_is_right R -> forall {n}, zero_is_right (@lex _ R n).
Proof.
intros R H n x Hx; induction n; [contradict Hx; apply hat0F_unit; easy |].
rewrite lex_S; destruct (HG1 (x ord0) 0) as [Hx0 | Hx0].
rewrite Hx0; right; split; [easy |].
apply IHn; contradict Hx; apply extF_skipF with ord0; easy.
left; split; [| apply H]; easy.
Qed.

(* The proof uses the equality decidability hypothesis HG1. *)
Lemma colex_zero_is_right :
  forall {R : G -> G -> Prop},
    zero_is_right R -> forall {n}, zero_is_right (@colex _ R n).
Proof.
intros R H n x Hx; induction n; [contradict Hx; apply hat0F_unit; easy |].
rewrite colex_S; destruct (HG1 (x ord_max) 0) as [Hxn | Hxn].
rewrite Hxn; right; split; [easy |].
apply IHn; contradict Hx; apply extF_skipF with ord_max; easy.
left; split; [| apply H]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma symlex_zero_is_left :
  forall {R : G -> G -> Prop},
    zero_is_right R -> forall {n}, zero_is_left (@symlex _ R n).
Proof. intros; apply zero_is_left_conv, lex_zero_is_right; easy. Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma revlex_zero_is_left :
  forall {R : G -> G -> Prop},
    zero_is_right R -> forall {n}, zero_is_left (@revlex _ R n).
Proof. intros; apply zero_is_left_conv, colex_zero_is_right; easy. Qed.

(** With the [br_monoid_compat_zl] / [br_monoid_compat_zr] compound properties. *)

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma lex_br_monoid_compat_zl :
  forall {R : G -> G -> Prop},
    br_monoid_compat_zl R -> forall {n}, br_monoid_compat_zl (@lex _ R n).
Proof.
intros R H1 n; move: H1; apply modus_ponens_and; intros H1;
    [apply lex_br_plus_compat_r | apply lex_zero_is_left]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma colex_br_monoid_compat_zl :
  forall {R : G -> G -> Prop},
    br_monoid_compat_zl R -> forall {n}, br_monoid_compat_zl (@colex _ R n).
Proof.
intros R H1 n; move: H1; apply modus_ponens_and; intros H1;
    [apply colex_br_plus_compat_r | apply colex_zero_is_left]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma symlex_br_monoid_compat_zr :
  forall {R : G -> G -> Prop},
    br_monoid_compat_zl R -> forall {n}, br_monoid_compat_zr (@symlex _ R n).
Proof.
intros; apply br_monoid_compat_zr_conv, lex_br_monoid_compat_zl; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma revlex_br_monoid_compat_zr :
  forall {R : G -> G -> Prop},
    br_monoid_compat_zl R -> forall {n}, br_monoid_compat_zr (@revlex _ R n).
Proof.
intros; apply br_monoid_compat_zr_conv, colex_br_monoid_compat_zl; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma lex_br_monoid_compat_zr :
  forall {R : G -> G -> Prop},
    br_monoid_compat_zr R -> forall {n}, br_monoid_compat_zr (@lex _ R n).
Proof.
intros R H1 n; move: H1; apply modus_ponens_and; intros H1;
    [apply lex_br_plus_compat_r | apply lex_zero_is_right]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma colex_br_monoid_compat_zr :
  forall {R : G -> G -> Prop},
    br_monoid_compat_zr R -> forall {n}, br_monoid_compat_zr (@colex _ R n).
Proof.
intros R H1 n; move: H1; apply modus_ponens_and; intros H1;
    [apply colex_br_plus_compat_r | apply colex_zero_is_right]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma symlex_br_monoid_compat_zl :
  forall {R : G -> G -> Prop},
    br_monoid_compat_zr R -> forall {n}, br_monoid_compat_zl (@symlex _ R n).
Proof.
intros; apply br_monoid_compat_zl_conv, lex_br_monoid_compat_zr; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma revlex_br_monoid_compat_zl :
  forall {R : G -> G -> Prop},
    br_monoid_compat_zr R -> forall {n}, br_monoid_compat_zl (@revlex _ R n).
Proof.
intros; apply br_monoid_compat_zl_conv, colex_br_monoid_compat_zr; easy.
Qed.

(** With the [monomial_order] / [monomial_order_zl] / [monomial_order_zr] /
 [monomial_order_ns] compound properties. *)

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma lex_monomial_order :
  forall {R : G -> G -> Prop},
    monomial_order R -> forall {n}, monomial_order (@lex _ R n).
Proof.
intros R H1 n; move: H1; apply modus_ponens_and; intros H1;
    [apply lex_strict_total_order | apply lex_br_plus_compat_r]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma colex_monomial_order :
  forall {R : G -> G -> Prop},
    monomial_order R -> forall {n}, monomial_order (@colex _ R n).
Proof.
intros R H1 n; move: H1; apply modus_ponens_and; intros H1;
    [apply colex_strict_total_order | apply colex_br_plus_compat_r]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma symlex_monomial_order :
  forall {R : G -> G -> Prop},
    monomial_order R -> forall {n}, monomial_order (@symlex _ R n).
Proof. intros; apply monomial_order_conv, lex_monomial_order; easy. Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma revlex_monomial_order :
  forall {R : G -> G -> Prop},
    monomial_order R -> forall {n}, monomial_order (@revlex _ R n).
Proof. intros; apply monomial_order_conv, colex_monomial_order; easy. Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma lex_monomial_order_zl :
  forall {R : G -> G -> Prop},
    monomial_order_zl R -> forall {n}, monomial_order_zl (@lex _ R n).
Proof.
intros R H1 n; move: H1; apply modus_ponens_and; intros H1;
    [apply lex_strict_total_order | apply lex_br_monoid_compat_zl]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma colex_monomial_order_zl :
  forall {R : G -> G -> Prop},
    monomial_order_zl R -> forall {n}, monomial_order_zl (@colex _ R n).
Proof.
intros R H1 n; move: H1; apply modus_ponens_and; intros H1;
    [apply colex_strict_total_order | apply colex_br_monoid_compat_zl]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma symlex_monomial_order_zr :
  forall {R : G -> G -> Prop},
    monomial_order_zl R -> forall {n}, monomial_order_zr (@symlex _ R n).
Proof. intros; apply monomial_order_zr_conv, lex_monomial_order_zl; easy. Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma revlex_monomial_order_zr :
  forall {R : G -> G -> Prop},
    monomial_order_zl R -> forall {n}, monomial_order_zr (@revlex _ R n).
Proof.
intros; apply monomial_order_zr_conv, colex_monomial_order_zl; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma lex_monomial_order_zr :
  forall {R : G -> G -> Prop},
    monomial_order_zr R -> forall {n}, monomial_order_zr (@lex _ R n).
Proof.
intros R H n; move: H; apply modus_ponens_and; intros H;
    [apply lex_strict_total_order | apply lex_br_monoid_compat_zr]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma colex_monomial_order_zr :
  forall {R : G -> G -> Prop},
    monomial_order_zr R -> forall {n}, monomial_order_zr (@colex _ R n).
Proof.
intros R H n; move: H; apply modus_ponens_and; intros H;
    [apply colex_strict_total_order | apply colex_br_monoid_compat_zr]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma symlex_monomial_order_zl :
  forall {R : G -> G -> Prop},
    monomial_order_zr R -> forall {n}, monomial_order_zl (@symlex _ R n).
Proof. intros; apply monomial_order_zl_conv, lex_monomial_order_zr; easy. Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma revlex_monomial_order_zl :
  forall {R : G -> G -> Prop},
    monomial_order_zr R -> forall {n}, monomial_order_zl (@revlex _ R n).
Proof.
intros; apply monomial_order_zl_conv, colex_monomial_order_zr; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma lex_monomial_order_ns :
  forall {R : G -> G -> Prop},
    monomial_order_ns R -> forall {n}, monomial_order_ns (@lex _ R n).
Proof.
intros R H n; move: H; apply modus_ponens_and; intros H;
    [apply lex_total_order | apply lex_br_plus_compat_r]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma colex_monomial_order_ns :
  forall {R : G -> G -> Prop},
    monomial_order_ns R -> forall {n}, monomial_order_ns (@colex _ R n).
Proof.
intros R H n; move: H; apply modus_ponens_and; intros H;
    [apply colex_total_order | apply colex_br_plus_compat_r]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma symlex_monomial_order_ns :
  forall {R : G -> G -> Prop},
    monomial_order_ns R -> forall {n}, monomial_order_ns (@symlex _ R n).
Proof.
intros; apply monomial_order_ns_conv;
    [apply eq_decF | apply lex_monomial_order_ns]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma revlex_monomial_order_ns :
  forall {R : G -> G -> Prop},
    monomial_order_ns R -> forall {n}, monomial_order_ns (@revlex _ R n).
Proof.
intros; apply monomial_order_ns_conv;
    [apply eq_decF | apply colex_monomial_order_ns]; easy.
Qed.

End Lex_order_Facts.


Notation lex_lt    := (lex lt).
Notation colex_lt  := (colex lt).
Notation symlex_lt := (symlex lt).
Notation revlex_lt := (revlex lt).


Section Lex_orders_nat_Facts.

Let HN0 := @inhabited_m nat_AbelianMonoid.
Let HN1 := Nat.eq_dec.
Let Hlt1 := Nat.lt_irrefl.
Let Hlt2 := lt_strict_total_order.

Lemma lex_lt_nil : forall (x y : 'nat^0), ~ lex_lt x y.
Proof. intros; rewrite lex_nil; apply irrefl_not_refl; easy. Qed.

Lemma colex_lt_nil : forall (x y : 'nat^0), ~ colex_lt x y.
Proof. intros; rewrite colex_nil; apply irrefl_not_refl; easy. Qed.

Lemma symlex_lt_nil : forall (x y : 'nat^0), ~ symlex_lt x y.
Proof. intros; rewrite symlex_nil; apply irrefl_not_refl; easy. Qed.

Lemma revlex_lt_nil : forall (x y : 'nat^0), ~ revlex_lt x y.
Proof. intros; rewrite revlex_nil; apply irrefl_not_refl; easy. Qed.

Lemma lex_lt_irrefl :
  forall {n}, irreflexive (lex_lt : 'nat^n -> 'nat^n -> Prop).
Proof. intro; apply lex_irrefl; easy. Qed.

Lemma colex_lt_irrefl :
  forall {n}, irreflexive (colex_lt : 'nat^n -> 'nat^n -> Prop).
Proof. intro; apply colex_irrefl; easy. Qed.

Lemma symlex_lt_irrefl :
  forall {n}, irreflexive (symlex_lt : 'nat^n -> 'nat^n -> Prop).
Proof. intro; apply symlex_irrefl; easy. Qed.

Lemma revlex_lt_irrefl :
  forall {n}, irreflexive (revlex_lt : 'nat^n -> 'nat^n -> Prop).
Proof. intro; apply revlex_irrefl; easy. Qed.

Lemma lex_lt_strict_total_order :
  forall {n}, strict_total_order (lex_lt : 'nat^n -> 'nat^n -> Prop).
Proof. intro; apply lex_strict_total_order; easy. Qed.

Lemma colex_lt_strict_total_order :
  forall {n}, strict_total_order (colex_lt : 'nat^n -> 'nat^n -> Prop).
Proof. intro; apply colex_strict_total_order; easy. Qed.

Lemma symlex_lt_strict_total_order :
  forall {n}, strict_total_order (symlex_lt : 'nat^n -> 'nat^n -> Prop).
Proof. intro; apply symlex_strict_total_order; easy. Qed.

Lemma revlex_lt_strict_total_order :
  forall {n}, strict_total_order (revlex_lt : 'nat^n -> 'nat^n -> Prop).
Proof. intro; apply revlex_strict_total_order; easy. Qed.

End Lex_orders_nat_Facts.


Section Graded_monomial_order_Def.

(** Graded monomial orders. *)

Context {G : AbelianMonoid}.
Variable R : G -> G -> Prop.

Context {n : nat}.

Definition graded (Rn : 'G^n -> 'G^n -> Prop) (x y : 'G^n) : Prop :=
  sum x <> sum y /\ R (sum x) (sum y) \/ sum x = sum y /\ Rn x y.

End Graded_monomial_order_Def.


Section Graded_monomial_order_Facts1.

(** General properties of graded monomial orders. *)

Context {G : AbelianMonoid}.
Hypothesis HG1 : eq_dec G.
Hypothesis HG2 : @plus_is_reg_r G.

Lemma graded_l :
  forall R {n} (Rn : 'G^n -> 'G^n -> Prop) x y,
    sum x <> sum y -> R (sum x) (sum y) -> graded R Rn x y.
Proof. intros; left; easy. Qed.

Lemma graded_l_rev :
  forall R {n} (Rn : 'G^n -> 'G^n -> Prop) x y,
    sum x <> sum y -> graded R Rn x y -> R (sum x) (sum y).
Proof. move=>> H1 [H2 | H2]; easy. Qed.

Lemma graded_l_equiv :
  forall R {n} (Rn : 'G^n -> 'G^n -> Prop) x y,
    sum x <> sum y -> graded R Rn x y <-> R (sum x) (sum y).
Proof. intros; split; [apply graded_l_rev | apply graded_l]; easy. Qed.

Lemma graded_r :
  forall R {n} (Rn : 'G^n -> 'G^n -> Prop) x y,
    sum x = sum y -> Rn x y -> graded R Rn x y.
Proof. intros; right; easy. Qed.

Lemma graded_r_rev :
  forall R {n} (Rn : 'G^n -> 'G^n -> Prop) x y,
    sum x = sum y -> graded R Rn x y -> Rn x y.
Proof. move=>> H1 [H2 | H2]; easy. Qed.

Lemma graded_r_equiv :
  forall R {n} (Rn : 'G^n -> 'G^n -> Prop) x y,
    sum x = sum y -> graded R Rn x y <-> Rn x y.
Proof. intros; split; [apply graded_r_rev | apply graded_r]; easy. Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma graded_S_r_equiv :
  forall R {n} (Rn : 'G^n -> 'G^n -> Prop) (x y : 'G^n.+1) i0,
    x i0 = y i0 -> sum x = sum y ->
    graded R Rn (skipF x i0) (skipF y i0) <-> Rn (skipF x i0) (skipF y i0).
Proof. intros; apply graded_r_equiv, sum_skipF_reg; easy. Qed.

Lemma graded_diag :
  forall R {n} (Rn : 'G^n -> 'G^n -> Prop) x, graded R Rn x x <-> Rn x x.
Proof. intros; split; [intros [H | H] | apply graded_r]; easy. Qed.

Lemma graded_idem :
  forall {R1} R2 {n} {Rn : 'G^n -> 'G^n -> Prop},
    graded R1 (graded R2 Rn) = graded R1 Rn.
Proof.
intros; fun_ext2; apply prop_ext; split; intros [H | H];
    [left; easy | right | left; easy | right]; split; [easy | | easy |].
destruct H as [H1 [H2 | H2]]; easy.
right; easy.
Qed.

Lemma graded_eq : forall {n}, graded eq (@eq 'G^n) = eq.
Proof.
intros; fun_ext2; apply prop_ext; split;
    [intros [H | H] | intros; subst; right]; easy.
Qed.

(* The proof uses the equality decidability hypothesis HG1. *)
Lemma graded_neq : forall {n}, graded neq (@neq 'G^n) = neq.
Proof.
intro; fun_ext2 x y; apply prop_ext; split.
intros [[H _] | H]; [unfold neq; contradict H; subst |]; easy.
intros H; destruct (HG1 (sum x) (sum y)); [right | left]; easy.
Qed.

Lemma graded_irrefl_eq :
  forall {R} (HR : irreflexive R) {n} (Rn : 'G^n -> 'G^n -> Prop) x y,
    graded R Rn x y <-> R (sum x) (sum y) \/ sum x = sum y /\ Rn x y.
Proof.
move=> R /br_and_neq_id HR =>>; apply or_iff_compat_r; rewrite -{2}HR; easy.
Qed.

Lemma graded_irrefl_l :
  forall {R} (HR : irreflexive R) {n} (Rn : 'G^n -> 'G^n -> Prop) x y,
    R (sum x) (sum y) -> graded R Rn x y.
Proof.
move=> R /irrefl_equiv HR =>> H; apply graded_l;
    [contradict H; apply HR |]; easy.
Qed.

End Graded_monomial_order_Facts1.


Section Graded_monomial_order_Facts2.

(** Compatibility results of graded monomial orders with operators on
 homogeneous binary relations. *)

Context {G : AbelianMonoid}.
Hypothesis HG1 : eq_dec G.

Lemma graded_conv :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    graded (converse R) (converse Rn) = converse (graded R Rn).
Proof.
intros; fun_ext2; apply prop_ext; split;
    (intros [H | H]; [left; rewrite neq_sym_equiv | right]; easy).
Qed.

(* The proof uses the equality decidability hypothesis HG1. *)
Lemma graded_compl :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    graded (complementary R) (complementary Rn) = complementary (graded R Rn).
Proof.
unfold complementary; intros; fun_ext2 x y;
    apply prop_ext; rewrite not_or_equiv !not_and_equiv NNPP_equiv; split.
intros [H | H]; split; [right | left | left | right]; easy.
intros [[H1 | H1] [H2 | H2]]; [..| destruct (HG1 (sum x) (sum y))];
    [| right | left | right | left]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma graded_conv_compl :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    graded (conv_compl R) (conv_compl Rn) = conv_compl (graded R Rn).
Proof. intros; rewrite !conv_compl_eq graded_conv graded_compl; easy. Qed.

Lemma graded_br_and :
  forall {R1 R2 : G -> G -> Prop} {n} {R1n R2n : 'G^n -> 'G^n -> Prop},
    graded (br_and R1 R2) (br_and R1n R2n) =
      br_and (graded R1 R1n) (graded R2 R2n).
Proof.
intros; fun_ext2; apply prop_ext; split.
intros [[H1 [H2 H3]] | [H1 [H2 H3]]]; split; [left | left | right..]; easy.
intros [[H1 | H1] [H2 | H2]]; [left |..| right]; easy.
Qed.

Lemma graded_br_or :
  forall {R1 R2 : G -> G -> Prop} {n} {R1n R2n : 'G^n -> 'G^n -> Prop},
    graded (br_or R1 R2) (br_or R1n R2n) =
      br_or (graded R1 R1n) (graded R2 R2n).
Proof.
intros; fun_ext2; apply prop_ext; split.
intros [[H1 [H2 | H2]] | [H1 [H2 | H2]]];
    [left; left | right; left | left; right | right; right]; easy.
intros [[H | H] | [H | H]]; [left | right | left | right]; (split; [easy |]);
    [left | left | right | right]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma graded_br_and_neq :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    graded (br_and_neq R) (br_and_neq Rn) = br_and_neq (graded R Rn).
Proof. intros; rewrite !br_and_neq_eq graded_br_and graded_neq; easy. Qed.

Lemma graded_br_or_eq :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    graded (br_or_eq R) (br_or_eq Rn) = br_or_eq (graded R Rn).
Proof. intros; rewrite !br_or_eq_eq graded_br_or graded_eq; easy. Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma graded_strict :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    graded (strict R) (strict Rn) = strict (graded R Rn).
Proof. intros; rewrite !strict_eq graded_br_and graded_conv_compl; easy. Qed.

Lemma graded_equivalent :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    graded (equivalent R) (equivalent Rn) = equivalent (graded R Rn).
Proof. intros; rewrite !equivalent_eq graded_br_and graded_conv; easy. Qed.

Lemma graded_compar :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    graded (comparable R) (comparable Rn) = comparable (graded R Rn).
Proof. intros; rewrite !compar_eq graded_br_or graded_conv; easy. Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma graded_incompar :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    graded (incomparable R) (incomparable Rn) = incomparable (graded R Rn).
Proof.
move=>>; rewrite !incompar_eq graded_br_and
    graded_compl graded_conv_compl; easy.
Qed.

End Graded_monomial_order_Facts2.


Section Graded_monomial_order_Facts3.

(** Compatibility results of graded monomial orders with elementary properties
 of homogeneous binary relations. *)

Context {G : AbelianMonoid}.
Hypothesis HG1 : eq_dec G.

Lemma graded_refl :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    reflexive Rn -> reflexive (graded R Rn).
Proof. move=>> Hn x; right; easy. Qed.

Lemma graded_refl_equiv :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    reflexive (graded R Rn) <-> reflexive Rn.
Proof.
intros; split; [| apply graded_refl].
intros H x; rewrite -graded_diag; apply H.
Qed.

Lemma graded_irrefl :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    irreflexive Rn -> irreflexive (graded R Rn).
Proof. move=>> Hn x [[Hx _] | [_ Hx]]; [easy | apply (Hn _ Hx)]. Qed.

Lemma graded_irrefl_equiv :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    irreflexive (graded R Rn) <-> irreflexive Rn.
Proof.
intros; split; [| apply graded_irrefl].
intros H x; rewrite -graded_diag; apply H.
Qed.

Lemma graded_refl_nil :
  forall R (R0 : 'G^0 -> 'G^0 -> Prop) (x y : 'G^0),
    reflexive R0 -> graded R R0 x y.
Proof.
intros R R0 x y H0; rewrite (hat0F_unit x _ y)//;
    apply graded_refl_equiv; easy.
Qed.

Lemma graded_irrefl_nil :
  forall R (R0 : 'G^0 -> 'G^0 -> Prop) (x y : 'G^0),
    irreflexive R0 -> ~ graded R R0 x y.
Proof.
intros R R0 x y H0; rewrite (hat0F_unit x _ y)//;
    apply graded_irrefl_equiv; easy.
Qed.

Lemma graded_sym :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    symmetric R -> symmetric Rn -> symmetric (graded R Rn).
Proof.
move=>> /sym_equiv H /sym_equiv Hn x y [H1 | H1];
    [left; rewrite neq_sym_equiv H | right; rewrite Hn]; easy.
Qed.

Lemma graded_antisym :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    antisymmetric R -> antisymmetric Rn -> antisymmetric (graded R Rn).
Proof.
move=>> H Hn x y [[H1 H2] | [H1 H2]] [[H3 H4] | [H3 H4]].
contradict H1; apply (H _ _ H2 H4).
rewrite H3 in H1; easy.
rewrite H1 in H3; easy.
apply (Hn _ _ H2 H4).
Qed.

Lemma graded_asym :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    asymmetric R -> asymmetric Rn -> asymmetric (graded R Rn).
Proof.
move=>>; rewrite !asym_equiv_irrefl_antisym; apply modus_ponens_and3;
    [intros _; apply graded_irrefl | apply graded_antisym].
Qed.

(* The proof uses the equality decidability hypothesis HG1. *)
Lemma graded_conn :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    connected R -> connected Rn -> connected (graded R Rn).
Proof.
move=>> H Hn x y H1; destruct (HG1 (sum x) (sum y)) as [H2 | H2].
destruct (Hn _ _ H1) as [H3 | H3]; [left | right]; right; easy.
destruct (H (sum x) (sum y) H2) as [H3 | H3]; [left | right]; left;
    [| split; [apply not_eq_sym |]]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma graded_str_conn :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    strongly_connected R -> strongly_connected Rn ->
    strongly_connected (graded R Rn).
Proof.
intros R n Rn; assert (HGn : eq_dec 'G^n) by now apply eq_decF.
rewrite (str_conn_equiv_refl_conn HG1) !(str_conn_equiv_refl_conn HGn).
apply modus_ponens_and3; [intros _; apply graded_refl | apply graded_conn].
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma graded_tricho :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    trichotomous R -> trichotomous Rn -> trichotomous (graded R Rn).
Proof.
intros R n Rn; assert (HGn : eq_dec 'G^n) by now apply eq_decF.
rewrite (tricho_equiv_asym_conn HG1) !(tricho_equiv_asym_conn HGn).
apply modus_ponens_and3; [apply graded_asym | apply graded_conn].
Qed.

Lemma graded_trans :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    transitive (br_and_neq R) -> transitive Rn -> transitive (graded R Rn).
Proof.
move=>> H Hn x y z [[H1 H2] | [H1 H2]] [[H3 H4] | [H3 H4]].
left; apply (H (sum x) (sum y) (sum z)); easy.
left; rewrite -H3; easy.
left; rewrite H1; easy.
right; split; [rewrite H1 | apply Hn with y]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma graded_neg_trans :
  forall {R : G -> G -> Prop} {n} {Rn : 'G^n -> 'G^n -> Prop},
    negatively_transitive (br_or_eq R) -> negatively_transitive Rn ->
    negatively_transitive (graded R Rn).
Proof.
move=>> H Hn =>>; rewrite -!compl_alt -graded_compl//.
apply graded_trans; [rewrite br_and_neq_compl |]; easy.
Qed.

End Graded_monomial_order_Facts3.


Section Graded_monomial_order_Facts4.

(** Compatibility results of graded monomial orders with compound properties
 of homogeneous binary relations. *)

Context {G : AbelianMonoid}.
Hypothesis HG1 : eq_dec G.
Variable R : G -> G -> Prop.

Context {n : nat}.
Variable Rn : 'G^n -> 'G^n -> Prop.

Lemma graded_partial_order :
  partial_order R -> partial_order Rn -> partial_order (graded R Rn).
Proof.
intros H Hn; repeat split.
apply graded_refl, Hn.
apply graded_antisym; [apply H | apply Hn].
apply graded_trans; [apply br_and_neq_trans; apply H | apply Hn].
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma graded_total_order :
  total_order R -> total_order Rn -> total_order (graded R Rn).
Proof.
move: (eq_decF HG1 n) => HGn.
rewrite (total_order_equiv_po HG1) !(total_order_equiv_po HGn);
    apply modus_ponens_and3;
    [apply graded_partial_order | apply graded_conn; easy].
Qed.

Lemma graded_strict_partial_order :
  strict_partial_order R -> strict_partial_order Rn ->
  strict_partial_order (graded R Rn).
Proof.
intros H Hn; rewrite strict_partial_order_equiv_no_asym; split.
apply graded_irrefl, Hn.
apply graded_trans; [| apply Hn].
apply br_and_neq_trans; [apply asym_antisym |]; apply H.
Qed.

(* The proof depends on the equality decidability hypothesis HG1. *)
Lemma graded_strict_total_order :
  strict_total_order R -> strict_total_order Rn ->
  strict_total_order (graded R Rn).
Proof.
rewrite !strict_total_order_equiv_spo; apply modus_ponens_and3;
    [apply graded_strict_partial_order | apply graded_conn; easy].
Qed.

End Graded_monomial_order_Facts4.


Section Graded_monomial_order_Facts5.

(** Compatibility results of graded monomial orders with monomial properties. *)

Context {G : AbelianMonoid}.
Hypothesis HG1 : eq_dec G.
Hypothesis HG2 : @plus_is_reg_r G.
Variable R : G -> G -> Prop.

Context {n : nat}.
Variable Rn : 'G^n -> 'G^n -> Prop.

(* The proof uses the plus regularity hypothesis HG2. *)
Lemma graded_br_plus_compat_r :
  br_plus_compat_r R -> br_plus_compat_r Rn ->
  br_plus_compat_r (graded R Rn).
Proof.
intros H Hn x x1 x2 [[Hx1 Hx2] | [Hx1 Hx2]].
left; rewrite !sum_plus; split;
    [contradict Hx1; apply (HG2 (sum x)) | apply H]; easy.
right; split; [rewrite !sum_plus Hx1 | apply Hn]; easy.
Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma graded_br_plus_compat_l :
  br_plus_compat_l R -> br_plus_compat_l Rn ->
  br_plus_compat_l (graded R Rn).
Proof. rewrite !br_plus_compat_equiv; apply graded_br_plus_compat_r. Qed.

(* The proof uses the equality decidability hypothesis HG1. *)
Lemma graded_zero_is_left :
  zero_is_left R -> zero_is_left Rn -> zero_is_left (graded R Rn).
Proof.
intros H Hn x Hx; destruct (HG1 (sum x) 0) as [Hx1 | Hx1].
right; rewrite sum_zero Hx1; split; [| apply Hn]; easy.
left; rewrite sum_zero; split; [contradict Hx1 | apply H]; easy.
Qed.

(* The proof uses the equality decidability hypothesis HG1. *)
Lemma graded_zero_is_right :
  zero_is_right R -> zero_is_right Rn -> zero_is_right (graded R Rn).
Proof.
intros H Hn x Hx; destruct (HG1 (sum x) 0) as [Hx1 | Hx1].
right; rewrite sum_zero Hx1; split; [| apply Hn]; easy.
left; rewrite sum_zero; split; [contradict Hx1 | apply H]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypotheses HG2. *)
Lemma graded_br_monoid_compat_zl :
  br_monoid_compat_zl R -> br_monoid_compat_zl Rn ->
  br_monoid_compat_zl (graded R Rn).
Proof.
apply modus_ponens_and3;
    [apply graded_br_plus_compat_r | apply graded_zero_is_left].
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypothesis HG2. *)
Lemma graded_br_monoid_compat_zr :
  br_monoid_compat_zr R -> br_monoid_compat_zr Rn ->
  br_monoid_compat_zr (graded R Rn).
Proof.
apply modus_ponens_and3;
    [apply graded_br_plus_compat_r | apply graded_zero_is_right].
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypotheses HG2. *)
Lemma graded_monomial_order :
  monomial_order R -> monomial_order Rn -> monomial_order (graded R Rn).
Proof.
apply modus_ponens_and3;
    [apply graded_strict_total_order | apply graded_br_plus_compat_r]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypotheses HG2. *)
Lemma graded_monomial_order_zl :
  monomial_order_zl R -> monomial_order_zl Rn ->
  monomial_order_zl (graded R Rn).
Proof.
apply modus_ponens_and3;
    [apply graded_strict_total_order | apply graded_br_monoid_compat_zl]; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
 and the plus regularity hypotheses HG2. *)
Lemma graded_monomial_order_zr :
  monomial_order_zr R -> monomial_order_zr Rn ->
  monomial_order_zr (graded R Rn).
Proof.
apply modus_ponens_and3;
    [apply graded_strict_total_order | apply graded_br_monoid_compat_zr]; easy.
Qed.

End Graded_monomial_order_Facts5.


Notation grlex    R := (graded R (lex R)).
Notation grcolex  R := (graded R (colex R)).
Notation grsymlex R := (graded R (symlex R)).
Notation grevlex  R := (graded R (revlex R)).


Section Graded_lex_order_Facts1.

(** General properties of graded lexicographic orders. *)

Context {G : AbelianMonoid}.
Hypothesis HG1 : eq_dec G.
Hypothesis HG2 : @plus_is_reg_r G.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma grlex_S :
  forall R {n} (x y :'G^n.+1),
    grlex R x y <->
    sum x <> sum y /\ R (sum x) (sum y) \/
    sum x = sum y /\ x ord0 <> y ord0 /\ R (x ord0) (y ord0) \/
    sum x = sum y /\ x ord0 = y ord0 /\
            grlex R (skipF x ord0) (skipF y ord0).
Proof.
intros; unfold graded at 1; rewrite lex_S and_distr_l;
do 2 apply or_iff_compat_l; split; intros [H1 [H2 H3]];
    (repeat split; [easy.. |]); move: H3; apply graded_S_r_equiv; easy.
Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma grcolex_S :
  forall R {n} (x y :'G^n.+1),
    grcolex R x y <->
    sum x <> sum y /\ R (sum x) (sum y) \/
    sum x = sum y /\ x ord_max <> y ord_max /\ R (x ord_max) (y ord_max) \/
    sum x = sum y /\ x ord_max = y ord_max /\
            grcolex R (skipF x ord_max) (skipF y ord_max).
Proof.
intros; unfold graded at 1; rewrite colex_S and_distr_l.
do 2 apply or_iff_compat_l; split; intros [H1 [H2 H3]];
    (repeat split; [easy.. |]); move: H3; apply graded_S_r_equiv; easy.
Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma grsymlex_S :
  forall R {n} (x y :'G^n.+1),
    grsymlex R x y <->
    sum x <> sum y /\ R (sum x) (sum y) \/
    sum x = sum y /\ y ord0 <> x ord0 /\ R (y ord0) (x ord0) \/
    sum x = sum y /\ y ord0 = x ord0 /\
            grsymlex R (skipF x ord0) (skipF y ord0).
Proof.
intros; unfold graded at 1; rewrite symlex_S and_distr_l.
do 2 (apply or_iff_compat; [easy |]); split; intros [H1 [H2 H3]];
    (repeat split; [easy.. |]); move: H3; apply graded_S_r_equiv; easy.
Qed.

Let H0 : forall P Q R T, (P /\ Q \/ P /\ R <-> P /\ T) <-> (P -> Q \/ R <-> T).
Proof. tauto. Qed.

(** "mo" stands for "monomial_order". *)
(* The proof depends on the equality decidability hypothesis HG1,
  and the plus regularity hypothesis HG2. *)
Lemma grsymlex_S_mo :
  forall R {n} (x y :'G^n.+1),
    monomial_order R ->
    grsymlex R x y <->
    sum x <> sum y /\ R (sum x) (sum y) \/
    sum x = sum y /\ grsymlex R (skipF x ord0) (skipF y ord0).
Proof.
intros R n x y HR.
assert (HG2' : @plus_is_reg_r G) by now apply (monomial_order_plus_is_reg_r R).
rewrite grsymlex_S; apply or_iff_compat_l; apply H0; intros H1; split.
(* *)
move=> [[/eq_sym_equiv H2 H3] | H2]; [| easy]; apply graded_l.
contradict H2; move: H1; rewrite !(sum_skipF _ ord0) H2; apply HG2'.
apply br_sum_conn_equiv; easy.
(* *)
intros H2; destruct (HG1 (x ord0) (y ord0)) as [H3 | H3].
right; repeat split; easy.
left; repeat split; [easy.. |].
apply graded_l_rev, br_sum_conn_equiv in H2; [easy.. |].
contradict H3; move: H1; rewrite !(sum_skipF _ ord0) H3; apply HG2'.
Qed.

(* The proof depends on the plus regularity hypothesis HG2. *)
Lemma grevlex_S :
  forall R {n} (x y :'G^n.+1),
    grevlex R x y <->
    sum x <> sum y /\ R (sum x) (sum y) \/
    sum x = sum y /\ y ord_max <> x ord_max /\ R (y ord_max) (x ord_max) \/
    sum x = sum y /\ y ord_max = x ord_max /\
            grevlex R (skipF x ord_max) (skipF y ord_max).
Proof.
intros; unfold graded at 1; rewrite revlex_S and_distr_l.
do 2 (apply or_iff_compat; [easy |]); split; intros [H1 [H2 H3]];
    (repeat split; [easy.. |]); move: H3; apply graded_S_r_equiv; easy.
Qed.

(* The proof depends on the equality decidability hypothesis HG1,
  and the plus regularity hypothesis HG2. *)
Lemma grevlex_S_mo :
  forall R {n} (x y :'G^n.+1),
    monomial_order R ->
    grevlex R x y <->
    sum x <> sum y /\ R (sum x) (sum y) \/
    sum x = sum y /\ grevlex R (skipF x ord_max) (skipF y ord_max).
Proof.
intros R n x y HR.
assert (HG2' : @plus_is_reg_r G) by now apply (monomial_order_plus_is_reg_r R).
rewrite grevlex_S; apply or_iff_compat_l; apply H0; intros H1; split.
(* *)
move=> [[/eq_sym_equiv H2 H3] | H2]; [| easy]; apply graded_l.
contradict H2; move: H1; rewrite !(sum_skipF _ ord_max) H2; apply HG2'.
apply br_sum_conn_equiv; easy.
(* *)
intros H2; destruct (HG1 (x ord_max) (y ord_max)) as [H3 | H3].
right; repeat split; easy.
left; repeat split; [easy.. |].
apply graded_l_rev, br_sum_conn_equiv in H2; [easy.. |].
contradict H3; move: H1; rewrite !(sum_skipF _ ord_max) H3; apply HG2'.
Qed.

End Graded_lex_order_Facts1.


Notation grlex_lt    := (grlex lt).
Notation grcolex_lt  := (grcolex lt).
Notation grsymlex_lt := (grsymlex lt).
Notation grevlex_lt  := (grevlex lt).


Section Graded_monomial_order_nat_Facts.

(** Graded monomial orders on the real numbers. *)

Let HN0 := @inhabited_m nat_AbelianMonoid.
Let HN1 := Nat.eq_dec.
Let HN2 := nat_plus_reg_r.
Let Hlt1 := Nat.lt_irrefl.
Let Hlt2 := lt_strict_total_order.
Let Hlt3 := lt_monomial_order.
Let Hlex1 n := @lex_lt_irrefl n.
Let Hlex2 n := @lex_lt_strict_total_order n.
Let Hlex3 n := @colex_lt_strict_total_order n.
Let Hlex4 n := @symlex_lt_strict_total_order n.
Let Hlex5 n := @revlex_lt_strict_total_order n.

Lemma grlex_lt_eq :
  forall {n} (x y : 'nat^n),
    grlex_lt x y <->
    (sum x < sum y)%coq_nat \/ sum x = sum y /\ lex_lt x y.
Proof. intros; apply graded_irrefl_eq; easy. Qed.

Lemma grcolex_lt_eq :
  forall {n} (x y : 'nat^n),
    grcolex_lt x y <->
    (sum x < sum y)%coq_nat \/ sum x = sum y /\ colex_lt x y.
Proof. intros; apply graded_irrefl_eq; easy. Qed.

Lemma grsymlex_lt_eq :
  forall {n} (x y : 'nat^n),
    grsymlex_lt x y <->
    (sum x < sum y)%coq_nat \/ sum x = sum y /\ symlex_lt x y.
Proof. intros; apply graded_irrefl_eq; easy. Qed.

Lemma grevlex_lt_eq :
  forall {n} (x y : 'nat^n),
    grevlex_lt x y <->
    (sum x < sum y)%coq_nat \/ sum x = sum y /\ revlex_lt x y.
Proof. intros; apply graded_irrefl_eq; easy. Qed.

Lemma grlex_lt_l :
  forall {n} (x y : 'nat^n), (sum x < sum y)%coq_nat -> grlex_lt x y.
Proof. intros; apply graded_irrefl_l; easy. Qed.

Lemma grcolex_lt_l :
  forall {n} (x y : 'nat^n), (sum x < sum y)%coq_nat -> grcolex_lt x y.
Proof. intros; apply graded_irrefl_l; easy. Qed.

Lemma grsymlex_lt_l :
  forall {n} (x y : 'nat^n), (sum x < sum y)%coq_nat -> grsymlex_lt x y.
Proof. intros; apply graded_irrefl_l; easy. Qed.

Lemma grevlex_lt_l :
  forall {n} (x y : 'nat^n), (sum x < sum y)%coq_nat -> grevlex_lt x y.
Proof. intros; apply graded_irrefl_l; easy. Qed.

Lemma grlex_lt_nil : forall (x y : 'nat^0), ~ grlex_lt x y.
Proof. intros; apply graded_irrefl_nil; easy. Qed.

Lemma grcolex_lt_nil : forall (x y : 'nat^0), ~ grcolex_lt x y.
Proof. intros; apply graded_irrefl_nil; easy. Qed.

Lemma grsymlex_lt_nil : forall (x y : 'nat^0), ~ grsymlex_lt x y.
Proof. intros; apply graded_irrefl_nil; easy. Qed.

Lemma grevlex_lt_nil : forall (x y : 'nat^0), ~ grevlex_lt x y.
Proof. intros; apply graded_irrefl_nil; easy. Qed.

Lemma grlex_lt_S :
  forall {n : nat} (x y : 'nat^n.+1),
    grlex_lt x y <->
    (sum x < sum y)%coq_nat \/
    sum x = sum y /\ (x ord0 < y ord0)%coq_nat \/
    sum x = sum y /\ x ord0 = y ord0 /\ grlex_lt (skipF x ord0) (skipF y ord0).
Proof.
intros; rewrite grlex_S//; apply or_iff_compat;
    [| apply or_iff_compat_r, and_iff_compat_l]; apply br_and_neq_id; easy.
Qed.

Lemma grcolex_lt_S :
  forall {n : nat} (x y : 'nat^n.+1),
    grcolex_lt x y <->
    (sum x < sum y)%coq_nat \/
    sum x = sum y /\ (x ord_max < y ord_max)%coq_nat \/
    sum x = sum y /\ x ord_max = y ord_max /\
            grcolex_lt (skipF x ord_max) (skipF y ord_max).
Proof.
intros; rewrite grcolex_S//; apply or_iff_compat;
    [| apply or_iff_compat_r, and_iff_compat_l]; apply br_and_neq_id; easy.
Qed.

Lemma grsymlex_lt_S :
  forall {n : nat} (x y : 'nat^n.+1),
    grsymlex_lt x y <->
    (sum x < sum y)%coq_nat \/
    sum x = sum y /\ grsymlex_lt (skipF x ord0) (skipF y ord0).
Proof.
intros; rewrite grsymlex_S_mo//; apply or_iff_compat_r, br_and_neq_id; easy.
Qed.

Lemma grevlex_lt_S :
  forall {n : nat} (x y : 'nat^n.+1),
    grevlex_lt x y <->
    (sum x < sum y)%coq_nat \/
    sum x = sum y /\ grevlex_lt (skipF x ord_max) (skipF y ord_max).
Proof.
intros; rewrite grevlex_S_mo//; apply or_iff_compat_r, br_and_neq_id; easy.
Qed.

Lemma grlex_lt_strict_total_order :
  forall {n}, strict_total_order (grlex_lt : 'nat^n -> 'nat^n -> Prop).
Proof. intro; apply graded_strict_total_order; easy. Qed.

Lemma grcolex_lt_strict_total_order :
  forall {n}, strict_total_order (grcolex_lt : 'nat^n -> 'nat^n -> Prop).
Proof. intro; apply graded_strict_total_order; easy. Qed.

Lemma grsymlex_lt_strict_total_order :
  forall {n}, strict_total_order (grsymlex_lt : 'nat^n -> 'nat^n -> Prop).
Proof. intro; apply graded_strict_total_order; easy. Qed.

Lemma grevlex_lt_strict_total_order :
  forall {n}, strict_total_order (grevlex_lt : 'nat^n -> 'nat^n -> Prop).
Proof. intro; apply graded_strict_total_order; easy. Qed.

End Graded_monomial_order_nat_Facts.


(* FIXME: COMMENTS TO BE REMOVED FOR PUBLICATION!
 TODO: generalize sections Sum_R_Facts and Sum_nat_Facts to ordered monoids. *)
