(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Complements on the commutative monoid algebraic structure.

 * Description

 The [AbelianMonoid] algebraic structure (aka commutative monoid) is defined
 in the Coquelicot library, including the canonical structure
 [prod_AbelianMonoid] for the cartesian product of commutative monoids.

 The present module adds :
 - support for the zero structure;

 ** Zero structure

 Let [G : AbelianMonoid].

 - [zero_struct G] is the predicate stating that
   [G] is the singleton type with only element 0];
 - [nonzero_struct G] is the predicate stating that
   [G] contains an element distinct from [0].

 ** Support for regular monoid law.

 This is the case where simplification may occur, as for the natural numbers.

 * Used logic axioms

 - [classic_dec], an alias for [excluded_middle_informative],
   the strong form of excluded middle.

 * Usage

 This module may be used through the import of [Algebra.Monoid.Monoid],
 [Algebra.Algebra], or [Algebra.Algebra_wDep], where it is exported.
*)

From Requisite Require Import stdlib_wR ssr.

From Coquelicot Require Import Hierarchy.
Close Scope R_scope.

From Subsets Require Import Subsets_wDep.
From Algebra Require Import Hierarchy_compl.

Local Open Scope Monoid_scope.


Section Monoid_Facts.

Definition zero_struct (G : AbelianMonoid) : Prop := unit_type (0 : G).
Definition nonzero_struct (G : AbelianMonoid) : Prop := exists x : G, x <> 0.

Lemma zero_not_nonzero_struct_equiv :
  forall {G}, zero_struct G <-> ~ nonzero_struct G.
Proof. intros; apply iff_sym, not_ex_not_all_equiv. Qed.

Lemma nonzero_not_zero_struct_equiv :
  forall {G}, nonzero_struct G <-> ~ zero_struct G.
Proof. intros; apply iff_sym, not_all_ex_not_equiv. Qed.

Lemma zero_struct_dec : forall G, { zero_struct G } + { nonzero_struct G }.
Proof.
intros; destruct (classic_dec (zero_struct G));
    [left | right; apply nonzero_not_zero_struct_equiv]; easy.
Qed.

Context {T : Type}.
Context {G : AbelianMonoid}.

Lemma fct_nonzero_struct :
  inhabited T -> nonzero_struct G -> nonzero_struct (@fct_AbelianMonoid T G).
Proof.
intros [t] [x]; exists (fun=> x); apply fun_ext_contra_rev; exists t; easy.
Qed.

Lemma inhabited_m : inhabited G.
Proof. apply (inhabits 0). Qed.

Lemma inhabited_fct_m : forall {T : Type}, inhabited (T -> G).
Proof. intros; apply fun_to_nonempty_compat, inhabited_m. Qed.

Lemma plus_compat_l : forall x x1 x2 : G, x1 = x2 -> x + x1 = x + x2.
Proof. intros; f_equal; easy. Qed.

Lemma plus_compat_r : forall x x1 x2 : G, x1 = x2 -> x1 + x = x2 + x.
Proof. intros; f_equal; easy. Qed.

Lemma plus_comm3_l : forall x y z : G, x + (y + z) = y + (x + z).
Proof. intros x y z; rewrite plus_assoc (plus_comm x) -plus_assoc; easy. Qed.

Lemma plus_comm3_r : forall x y z : G, x + (y + z) = z + (y + x).
Proof. intros x y z; rewrite plus_comm -plus_assoc plus_comm3_l; easy. Qed.

Lemma plus_comm4_m :
  forall x y z t : G, (x + y) + (z + t) = (x + z) + (y + t).
Proof.
intros x y z t; rewrite !plus_assoc -(plus_assoc x y) -(plus_assoc x z).
do 2 f_equal; apply plus_comm.
Qed.

Lemma zero_uniq : forall z : G, (forall x, x + z = x) -> z = 0.
Proof. intros z H; rewrite -(plus_zero_l z); easy. Qed.

Lemma opposite_uniq : forall x y1 y2 : G, y1 + x = 0 -> x + y2 = 0 -> y1 = y2.
Proof.
intros x y1 y2 Hy1 Hy2.
rewrite -(plus_zero_r y1) -Hy2 plus_assoc Hy1 plus_zero_l; easy.
Qed.

End Monoid_Facts.


Section Monoid_regular_law_Facts.

Context {G : AbelianMonoid}.

Definition plus_is_reg_l : Prop :=
  forall (x x1 x2 : G), x + x1 = x + x2 -> x1 = x2.
Definition plus_is_reg_r : Prop :=
  forall (x x1 x2 : G), x1 + x = x2 + x -> x1 = x2.

Lemma plus_is_reg_equiv : plus_is_reg_l <-> plus_is_reg_r.
Proof.
split; move=> H x x1 x2; move: (H x x1 x2); rewrite !(plus_comm x); easy.
Qed.

End Monoid_regular_law_Facts.


Section Monoid_regular_law_nat_Facts.

Lemma nat_plus_reg_l : @plus_is_reg_l nat_AbelianMonoid.
Proof. move=>>; unfold plus; simpl; lia. Qed.

Lemma nat_plus_reg_r : @plus_is_reg_r nat_AbelianMonoid.
Proof. apply plus_is_reg_equiv, nat_plus_reg_l. Qed.

Lemma nat_plus_zero_reg_l : forall (m n : nat), m + n = m -> n = 0.
Proof. intros m n; rewrite -{2}(plus_zero_r m); apply nat_plus_reg_l. Qed.

Lemma nat_plus_zero_reg_r : forall (m n : nat), m + n = n -> m = 0.
Proof. move=>>; rewrite plus_comm; apply nat_plus_zero_reg_l. Qed.

End Monoid_regular_law_nat_Facts.
