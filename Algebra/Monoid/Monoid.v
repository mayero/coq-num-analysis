(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

(**

 * Brief description

 Exports additional support for monoids.

 * Description

 It is based on the hierarchy of algebraic structures provided by the
 Coquelicot library.

 Provides:
 - support for the zero structure;
 - operators for finite families/tables on monoids;
 - support for monoid morphisms;
 - support for the iteration of the commutative monoid law (aka [sum]);
 - support for submonoids;
 - support for multiplicative and compositional monoids;
 - support for the concatenation of a family of families;
 - support for monomial orders.

 * Usage

 This module is meant to be imported after all imports from the Coq standard
 library, from the Mathematical Components library, and from other external Coq
 libraries such as Coquelicot and Flocq.

 This module may also be used through the import of [Algebra.Algebra], or
 [Algebra.Algebra_wDep], where it is exported.
*)

From Algebra Require Export Monoid_compl Monoid_FF Monoid_FT.
From Algebra Require Export Monoid_morphism Monoid_sum Monoid_sub.
From Algebra Require Export MonoidMult MonoidComp ConcatnF Monomial_order.
