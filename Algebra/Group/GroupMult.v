(**
This file is part of the Coq Numerical Analysis library

Copyright (C) Boldo, Clément, Martin, Mayero, Mouhcine

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

From Coquelicot Require Import Hierarchy.

(** Basic support for multiplicative abelian groups. *)

(* Declaring a specific scope generates confusion in the sequel... *)
Local Notation "/ x" := (opp x).
Local Notation "x / y" := (minus x y).
